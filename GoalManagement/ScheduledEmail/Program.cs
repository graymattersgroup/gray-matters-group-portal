﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using GoalManagement.CrmConnectivity;
using GoalManagement.digitalchalk.graymattersgroup;
using GoalManagement.Support;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System.Web.Services.Protocols;

namespace ScheduledEmail
{
    class Program
    {
        private static CRMAccess crmAccess;
        private static bool isDebug = false;

        static void Main(string[] args)
        {
            bool isPM = false;
    
            var kickOffDate = DateTime.Now.Date;

            foreach (string t in args)
            {
                switch (t)
                {
                    case "-AM":
                        isPM = false;
                        break;

                    case "-PM":
                        isPM = true;
                        break;
                }
            }

            try
            {

                //DigitalChalkTestHarness();

                //return;

              
              

               



                crmAccess = new CRMAccess();

                // Find all scheduled touches triggered for today, AM or PM
                var touches = crmAccess.GetScheduledTouches(isPM);

               
                //foreach (new_scheduledtouch newScheduledtouch in touches)
                //{
                //    Console.WriteLine("L{0} {1} -  {2} {3} ", newScheduledtouch.new_TriggerDate.Value.ToLocalTime().ToString("MM/dd/yyyy"), newScheduledtouch.new_TriggerDate.Value.ToLocalTime().ToLongTimeString(), newScheduledtouch.new_TriggerDate.Value.ToString("MM/dd/yyyy"), newScheduledtouch.new_TriggerDate.Value.ToLongTimeString());

                //}
                //return;
                //  Build Status Email for each of the retrieved emails
                foreach (new_scheduledtouch newScheduledtouch in touches)
                {
                    var neScheduledtouchDate = newScheduledtouch.new_TriggerDate.Value.ToLocalTime().ToString("MM/dd/yyyy hh:mm");
                    Console.WriteLine("{0} - {1}",neScheduledtouchDate, newScheduledtouch.new_Subject);

                    if (newScheduledtouch.new_TriggerDate.Value.ToShortDateString() != kickOffDate.ToShortDateString()) continue;

                    if( isDebug)
                    {
                        if (newScheduledtouch.new_ClassId.Name != "Mark's Test  Class") continue;
                    }
                    
                    var toEmail = newScheduledtouch.new_To;

                    var subject = newScheduledtouch.new_Subject;
                    var emailBody = newScheduledtouch.new_EmailBody;

                    //GetAttachments
                    var attachments = crmAccess.GetAnnotations(newScheduledtouch.Id);

                    var classId = newScheduledtouch.new_ClassId.Id;

                    // Get email targets.
                    var students = crmAccess.GetClassStudents(classId);

                    var coachesSponsorsUnfiltered = crmAccess.GetClassCoaches(classId);

                    var allInClass = new List<Contact>();
                    var coachesSponsors = new List<Contact>();

                    // If this is sent to students, filter out any coaches.
                    if (toEmail.Value == (int)CRMAccess.EmailTargets.Students)
                    {
                        foreach (Contact coachesSponsor in coachesSponsorsUnfiltered)
                        {
                            // If the coach is also a student than remove them from the coaches / Sponsors list
                            // so they don't get an extra email as a coach.
                            if( !students.Contains(coachesSponsor))
                            {
                                coachesSponsors.Add(coachesSponsor);
                            }
                        }
                    }
                    else if (toEmail.Value == (int)CRMAccess.EmailTargets.All)
                    {
                        allInClass.AddRange(students);
                        foreach (Contact coachesSponsor in coachesSponsorsUnfiltered)
                        {
                            // If the coach is also a student than remove them from the coaches / Sponsors list
                            // so they don't get an extra email as a coach.
                            if (!students.Contains(coachesSponsor))
                            {
                                allInClass.Add(coachesSponsor);
                            }
                        }
                    }
                    else
                    {
                        coachesSponsors = coachesSponsorsUnfiltered;
                    }

                  
                    var ccEmail = newScheduledtouch.new_CCAllemail;
                    var representativeEmail = newScheduledtouch.new_SendRepresentativeCopy;

                    // Set email to be sent by owner of the entity
                    var emailSentByActivityParty = new ActivityParty
                                                       {
                                                           PartyId =new EntityReference(SystemUser.EntityLogicalName,newScheduledtouch.OwnerId.Id),
                                                           ParticipationTypeMask = new OptionSetValue(1)
                                                       };

                    var fromActivityParty = new List<ActivityParty>() {emailSentByActivityParty};


                    if (toEmail.Value == (int)CRMAccess.EmailTargets.All)
                    {
                        CrmEmail.SendEmail(classId, toEmail.Value, allInClass, null, EmailSendRule.To,
                                         emailBody,
                                         subject, attachments, fromActivityParty, null,
                                         null, crmAccess, newScheduledtouch.new_scheduledtouchId.Value, null);
                    }
                    else
                    {

                        var ccActivityParty = CrmEmail.FillActivityParty(ccEmail.Value, students, coachesSponsors,EmailSendRule.CC);

                        var representativeCopies = CrmEmail.CreateContactList(representativeEmail.Value, students,coachesSponsors);
                        
                        CrmEmail.SendEmail(classId, toEmail.Value, students, coachesSponsors, EmailSendRule.To,
                                           emailBody,
                                           subject, attachments, fromActivityParty, ccActivityParty,
                                           representativeCopies, crmAccess, newScheduledtouch.new_scheduledtouchId.Value, null);
                    }


                    crmAccess.SetTouchToSent(newScheduledtouch.Id);
                }
            }
            catch (SoapException ex)
            {
                EmailNotifications.SendExceptionMessage("GMG Scheduled Email Fatal", "Soap", ex);

            }
            catch (Exception ex)
            {
                EmailNotifications.SendExceptionMessage("GMG Scheduled Email Fatal", "Exception", ex);
            }
            
        }

        private static void DigitalChalkTestHarness()
        {
            var lmsService = new DigitalChalkAPIService
            {
                Credentials = new System.Net.NetworkCredential("mkovalcson@onecrmpro.com ", "grisgris")
            };

         //   bool userExists = DoesUserExist("mkovalcson@onecrmpro.com", lmsService);

            string result = CreateUser("Joe", "Dirt", "jdirt@test.org", "jdirt@test.org", "abc", lmsService);

        }

        private static string CreateUser(string firstname, string lastname, string emailaddress, string username, string password, DigitalChalkAPIService lmsService)
        {
            auth myAuth = GetAuthorization("v4createUser");

            var newUser = new user
                               {
                                   emailAddress = emailaddress,
                                   username = username,
                                   firstName = firstname,
                                   lastName = lastname,
                                   password = password,
                                   passwordReset = "false",
                                   licenseAgreed = "true"
                                   
                               };

     
            var createUserRequest = new createUserRequest()
            {
                auth = new auth[] { myAuth },
                user = new user[] {newUser}
            };


            var result = "";

            try
            {
                createUserResponse response = lmsService.createUser(createUserRequest);

                if (response.success == "true")
                {
                    result = "Success";
                }
            }
            catch (SoapException ex)
            {
                result = ex.Message;
            }

            return result;
        }

        private static bool DoesUserExist(string username, DigitalChalkAPIService lmsService)
        {
            auth myAuth = GetAuthorization("v4doesUserExist");

            var existRequest = new doesUserExistRequest
            {
                auth = new auth[] { myAuth },
                username = username
            };


            var userExists = false;

            try
            {
                doesUserExistResponse existResponse = lmsService.doesUserExist(existRequest);

                if (existResponse.exists == "true")
                {
                    userExists = true;
                }
            }
            catch (Exception)
            {
               
            }

            return userExists;
        }


        private static auth GetAuthorization(string apiCall)
        {
            //Computes RFC 2104-compliant HMAC signature using SHA1
            KeyedHashAlgorithm algorithm = new HMACSHA1();
            Encoding encoding = new UTF8Encoding();

            var secretkey = "3c09936e82f1452d9d4a64a4f0ededb2";
            algorithm.Key = encoding.GetBytes(secretkey);

            //var apiCall = "v4doesUserExist";
            var utcDate = DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");
            var signaturePreCoded = string.Format("{0}{1}", apiCall, utcDate);
            var signature = Convert.ToBase64String(algorithm.ComputeHash(encoding.GetBytes(signaturePreCoded.ToCharArray())));

            var digitalChalkAcessKey = "ff808081427f1f690142910d19f3473b";
            var authorization = new auth { accessKey = digitalChalkAcessKey, timestamp = utcDate, signature = signature };

            return authorization;
        }
    }
}
