﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System.Web.Services.Protocols;

namespace ScheduledEmail
{
    class Program
    {
        private static CRMAccess crmAccess;
        private static bool isDebug = false;

        static void Main(string[] args)
        {
            Console.WriteLine("Start");
            try
            {
                crmAccess = new CRMAccess();

                // Find all scheduled touches triggered for today, AM or PM
                var classes = crmAccess.GetClasses();

                Console.WriteLine("Found {0} classes", classes.ToList().Count);

                foreach (var newClass in classes)
                {
                    if( newClass.new_ClassComplete != null)
                    {
                        if ( newClass.new_ClassComplete.Value == true)
                        {
                            Console.WriteLine("Class Complete {0}", newClass.new_name);

                            var emails =  crmAccess.GetEmailbyClass(newClass.Id);

                            Console.WriteLine("Found {0} emails to delete", emails.Count);


                            foreach (var email in emails)
                            {
                                crmAccess.GrayMattersContext.DeleteObject(email);
                            }

                            try
                            {
                                crmAccess.GrayMattersContext.SaveChanges();
                            }
                            catch 
                            {
                                
                               
                            }
                          
                        }
                    }
                }
                Console.WriteLine("Complete");
                
            }
            catch (SoapException ex)
            {
                EmailNotifications.SendExceptionMessage("GMG  Email Purge", "Soap", ex);

            }
            catch (Exception ex)
            {
                EmailNotifications.SendExceptionMessage("GMG Email Purge", "Exception", ex);
            }

        }

        private static void CleanEmail(Guid id)
        {
            // Get all email for this Class




        }
    }
}
