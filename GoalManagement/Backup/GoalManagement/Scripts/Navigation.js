﻿
// Global variable used to save initial values on workspace prior to user edits		
var initialElementValues;

function PrintContent(divName)
{
    var DocumentContainer = document.getElementById(divName);
    var WindowObject = window.open("", "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
    WindowObject.document.writeln(DocumentContainer.innerHTML);
    WindowObject.document.close();
    WindowObject.focus();
    WindowObject.print();
    WindowObject.close();
}


var win = null;
function OpenWindow(page,name,w,h,scroll)
{
    var LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
    var TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
    var settings = 'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable';
    win = window.open(page,name,settings);
}

function ToggleDiv(divToShow, divToHide)
{   
	document.getElementById(divToShow).style.display="block";    
	document.getElementById(divToHide).style.display="none";
}


function SetDivVisibility(divPrefix, action) 
{
	if(action == 'hide')
	{
		document.getElementById(divPrefix + "Div").style.display="none";
		document.getElementById(divPrefix + "ShowLink").style.visibility = "visible";
		document.getElementById(divPrefix + "HideLink").style.visibility = "hidden";
	}
	else
	{
		document.getElementById(divPrefix + "Div").style.display="block";
		document.getElementById(divPrefix + "ShowLink").style.visibility = "hidden";
		document.getElementById(divPrefix + "HideLink").style.visibility = "visible";
	}
}


// Event Handler function for use by Infragistics DateChooser objects to hide selects displayed below the DateChooser, so that
// calendar drop-down is not covered by select controls (select controls will always overlay calendar HTML if visible)
function hideObj(objName)
{
	if (document.getElementById(objName) != null)
	{
	    document.getElementById(objName).style.visibility = "hidden";
	    document.getElementById(objName).style.display = "none";
    }
}

// Event handler function used by Infragistics DateChooser objects to make temporarily-hidden select controls visible when
// calendar drop-down is closed.
function showObj(objName)
{
	if (document.getElementById(objName) != null)
	{
	    document.getElementById(objName).style.visibility = "visible";
	    document.getElementById(objName).style.display = "block";    
    }
}

function ShowLoader()
{
    if (document.getElementById('ctl00_MainContent_LoaderImage') != null)
    {
        document.getElementById('ctl00_MainContent_LoaderImage').style.visibility = "visible";
    }
}



///  CreateReportWindows  Creates New Browser Windows for each report
///
///  called by the body onload event
///
///  during the PostBack attributes are set to None or set to generate a graph.
/// 
function CreateReportWindows() {           
      
        var report = document.getElementById(hiddenReport);
        if (report != null && report.value != "None") {

            // reportUrl is a pathname
            // _black = generate a new browser window
            // graphSizing sets the size of the new browser window	    
            graphSizing = 'toolbar=no,scrollbars=1,resizable=1';

            window.open(report.value, "_blank", graphSizing);
            report.value = "None";
        }
    }


  