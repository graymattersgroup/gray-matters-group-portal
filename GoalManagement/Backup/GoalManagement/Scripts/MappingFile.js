﻿﻿// Make sure to load these into Form Libraries before this Script
// jquery1.4.1.min.js
// json2.js

// Global JavaScript Variables
var LocalFields;
var EntityFields;
var IsDebug;

var ODataPath =  'https://graymattersgroup.crm.dynamics.com/XRMServices/2011/OrganizationData.svc';

// Method's called from the CRM page.

/// SetRelatedFieldData
/// Sets local Form Text Values to the results of an ODATA Query against 
/// the entity selected in a Lookup.
/// Data is mapped from the lookup entity's entityFields to the form's localFields
///
/// lookupSchemaName = example: contactid - the name of the lookup on the form.
/// setName = example: ContactSet - the name of the OData Set
/// entityFields = example: LastName:FirstName:new_MyCustomField - colon delimited list of entity fields
/// localFields = example: new_contactlastname:new_contactfirstname:new_mycustomField - colon delimited list of field names
/// isDebug - shows alerts to help see where things are going.

function SetRelatedFieldData(lookupSchemaName, setName, entityFields, localFields, isDebug)
{
    // Get Lookup id
    var thisLookup = Xrm.Page.getAttribute(lookupSchemaName);
    var lookupId = thisLookup.getValue()[0].id;     

    EntityFields = entityFields;
    LocalFields = localFields;
    IsDebug = isDebug;

    ODataRetrieveEntity(setName, lookupId, entityFields);
}

///////////////////////////////////////////////////////////////////////////////
// Internal Supporting Methods

var serverUrl;
var ThisEntity;


/// ODataRetrieveEntity
/// Generate OData url to Retrieve Entity
/// entityName = 'ContactSet' 
/// entityId =  <Guid>
/// select fields = FirstName,LastName
///
function ODataRetrieveEntity(setName, entityId, selectfields)
{    
    if (selectfields == null)
    {
        selectfields = "*";
    }
    else
    {
        selectfields = selectfields.replace(/:/g, ',');
    }

    //Calls the REST endpoint to return the selected fields fields for the entity specified
     entityId = entityId.replace(/{/,'').replace(/}/,'');
    var odataSelect = ODataPath + "/" + setName + "(guid'" + entityId + "')?$select=" + selectfields;
      

    // Display's OData Url for testing
    if (IsDebug) alert(odataSelect);

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        url: odataSelect,
        beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
        success: function (data, textStatus, XmlHttpRequest) { ProcessReturnedEntities(data.d); },
        error: function (XmlHttpRequest, textStatus, errorThrown) { alert('OData Select Failed: ' + odataSelect); }
    });

}

/// ProcessReturnedEntities
/// Parse returned JSON data from OData url
/// Called aynchronously from success: 
/// Maps entity field values returned to current form controls.
///
/// returnedEntities - an array of Entities. In this case there will be only one.
function ProcessReturnedEntities(returnedEntity)
{ 
    ThisEntity = returnedEntity      

    if (IsDebug) alert(EntityFields);
    if (IsDebug) alert(LocalFields);

    var dataFields = EntityFields.split(':');
    var formFields = LocalFields.toLowerCase().split(':');

    // Get all datafields on page
    var attributes = Xrm.Page.data.entity.attributes.get();       
                  
    var j;
    // Go through all dataFields selected
    for (j = 0; j < dataFields.length; j++)
    {
    // Find matching attribute on  page
    for (var i in attributes)
        {
            var attribute = attributes[i];                

            if ( formFields[j] ==  attribute.getName() )
            {                   
                var fieldValue = eval("ThisEntity." + dataFields[j]);

                if (IsDebug) alert( attribute.getName() + "Type: " +attribute.getAttributeType() + " Value: " +fieldValue);

                // Set each page attribute properly based on the type of Attribute it is.
                switch( attribute.getAttributeType() )
                {
                case "string": 
                    Xrm.Page.getAttribute(formFields[j]).setValue(fieldValue);
                    break;

                case "memo": 
                    Xrm.Page.getAttribute(formFields[j]).setValue(fieldValue);
                    break;

                case "datetime":                             
                    var dateValue = new Date(parseInt(fieldValue.replace("/Date(", "").replace(")/", ""), 10));
                    if (IsDebug) alert("Converted to:" + dateValue);
                    Xrm.Page.getAttribute(formFields[j]).setValue(dateValue);
                    break;

                case "decimal":                           
                    Xrm.Page.getAttribute(formFields[j]).setValue(parseFloat(fieldValue ));
                    break;  

                case "double":                           
                    Xrm.Page.getAttribute(formFields[j]).setValue(parseFloat(fieldValue ));
                    break;  

                case "integer":                           
                    Xrm.Page.getAttribute(formFields[j]).setValue(parseInt(fieldValue ));
                    break;  

                case "money":   
                    Xrm.Page.getAttribute(formFields[j]).setValue(parseFloat(eval('ThisEntity.' + dataFields[j] + '.Value')));
                    break;
                        
                case "optionset":
                    Xrm.Page.getAttribute(formFields[j]).setValue(eval('ThisEntity.' + dataFields[j] + '.Value'));
                    break;

                 default:
                    Xrm.Page.getAttribute(formFields[j]).setValue(fieldValue);
                    break;

                }                        
            }                    
        }         
    }
}


function RealTypeOf(v)
{
    if (typeof (v) == "object")
    {
        if (v === null) return "null";
        if (v.constructor == (new Array).constructor) return "array";
        if (v.constructor == (new Date).constructor) return "date";
        if (v.constructor == (new RegExp).constructor) return "regex";
        return "object";
    }
    return typeof (v);
}


function dateReviver(key, value) {
    var a;
    if (typeof value === 'string') {
        a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
        if (a) {
            return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]));
        }
    }
    return value;
};