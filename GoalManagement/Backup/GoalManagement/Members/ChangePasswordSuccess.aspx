﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="ChangePasswordSuccess.aspx.cs" Inherits="GoalManagement.Members.ChangePasswordSuccess" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div style="margin-left: 5px; margin-right: 5px; padding: 50px;">

    <h2>
        Change Password
    </h2>
    <p>
        Your password has been changed successfully.
    </p>
    </div>
</asp:Content>
