﻿<%@ Page Title="Gray Matters Group Aftercare- Home" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="CoachInfo.aspx.cs" Inherits="GoalManagement.CoachInfo" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">

table.status 
{ background-color:#dddddd; margin-left:80px; margin-top:20px; }

table.status tr td { padding:10px; }
.statusLeft { text-align:right;  white-space:nowrap;  }
.statusRight { text-align:left; }


        .style1
        {
            text-align: left;
            width: 64px;
        }


        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="HelpDiv" class="HelpScreen">
        <asp:Literal ID="HelpLiteral" runat="server"></asp:Literal>
    </div>

             <h1>Coach Info </h1>  
                 
                      
                    
                       <table class="status">
                            <tr>
                                <td colspan="2" style=" text-align:center;" >
                                    <b>
                                    <asp:Label ID="CoachName" runat="server" Text="Coach Name"></asp:Label>
                                    </b>     
                                    </td>
                                  </tr>
                                  <tr>                              
                                  <td colspan = "2">
                                    <asp:Panel ID="AddressPanel" runat="server" Visible="false">
                                        <table>
                                            <tr>
                                                <td style=" text-align:left;">
                                                    <asp:Label ID="AddressLine1" runat="server" Text="Label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label ID="City" runat="server" Text="Label"></asp:Label>,
                                                    <asp:Label ID="State" runat="server" Text="Label"></asp:Label>
                                                    <asp:Label ID="Zip" runat="server" Text="Label"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td class="statusLeft">
                                    &nbsp;email:</td>
                                <td class="style1">                                   
                                    <asp:HyperLink ID="CoachEmail" runat="server" >---------------</asp:HyperLink>
                                    </td>
                            </tr>
                            <tr>
                                <td class="statusLeft">
                                    phone:
                                </td>
                                <td class="style1">
                                    <asp:Label ID="CoachPhone" runat="server" Text="phone"></asp:Label>
                                </td>
                            </tr>                         
                          
                           
                        </table>
                    
                   
                 
               
               
         

</asp:Content>
