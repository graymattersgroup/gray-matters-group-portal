﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace GoalManagement
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             MembershipUser currentUser = Membership.GetUser();

             if (Session["ImpersonateUser"] != null)
             {
                 var otherUser = (string)Session["ImpersonateUser"];
                 currentUser = Membership.GetUser(otherUser);
             }

             var logoImage = Master.FindControl("RadBinaryImage1") as RadBinaryImage;
             logoImage.Visible = false;

            var treeview = Master.FindControl("TreeView1") as TreeView;
            treeview.Visible = false;

            //var myTable = Master.FindControl("TableOutline") as Table;
            //myTable.Visible = false;


             if (currentUser != null) Response.Redirect("~/Members/ProfileHome.aspx");
        }
    }
}