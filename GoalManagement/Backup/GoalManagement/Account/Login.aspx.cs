﻿using System;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace GoalManagement.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var logoImage = Master.FindControl("RadBinaryImage1") as RadBinaryImage;
            logoImage.Visible = false;

            var treeview = Master.FindControl("TreeView1") as TreeView;
            treeview.Visible = false;
            var body = this.Master.FindControl("body") as HtmlGenericControl;
            body.Attributes.Add("onload", "SetFocus();");

            //RegisterHyperLink.NavigateUrl = "CustomRegister.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
        }



        protected void LoginUser_Authenticate(object sender, AuthenticateEventArgs e)
        {
           
            Session.Clear();

          

            if (Membership.ValidateUser(LoginUser.UserName, LoginUser.Password))
            {
                //var isagreed = this.Page.FindControl("AgreeToUserAgreement") as CheckBox;

                e.Authenticated = true;
              
            }

            else
            {
                e.Authenticated = false;
            }


        }

        //protected void LoginButton_Click(object sender, EventArgs e)
        //{
        //    if (Membership.ValidateUser(UserName.Text, Password.Text))
        //    {

        //       // e.Authenticated = true;
        //    }

        //    else
        //    {
        //      //  e.Authenticated = false;
        //    }
        //}
    }


}
