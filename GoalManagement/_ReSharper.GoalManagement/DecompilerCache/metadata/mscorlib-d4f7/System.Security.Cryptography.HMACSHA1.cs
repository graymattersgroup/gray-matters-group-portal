// Type: System.Security.Cryptography.HMACSHA1
// Assembly: mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
// Assembly location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.dll

using System.Runtime;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
    [ComVisible(true)]
    public class HMACSHA1 : HMAC
    {
        public HMACSHA1();

        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public HMACSHA1(byte[] key);

        public HMACSHA1(byte[] key, bool useManagedSha1);
    }
}
