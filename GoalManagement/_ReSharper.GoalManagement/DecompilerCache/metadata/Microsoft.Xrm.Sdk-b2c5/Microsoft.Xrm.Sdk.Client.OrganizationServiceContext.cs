// Type: Microsoft.Xrm.Sdk.Client.OrganizationServiceContext
// Assembly: Microsoft.Xrm.Sdk, Version=6.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
// Assembly location: G:\Work\GrayMattersGroup\Solutions\GoalManagement\GoalManagement\AdditionalAssemblies\Microsoft.Xrm.Sdk.dll

using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Microsoft.Xrm.Sdk.Client
{
    public class OrganizationServiceContext : IDisposable
    {
        public OrganizationServiceContext(IOrganizationService service);
        protected virtual IQueryProvider QueryProvider { get; }
        public MergeOption MergeOption { get; set; }
        public SaveChangesOptions SaveChangesDefaultOptions { get; set; }

        #region IDisposable Members

        public void Dispose();

        #endregion

        ~OrganizationServiceContext();
        public IQueryable<TEntity> CreateQuery<TEntity>() where TEntity : Entity;
        public IQueryable<Entity> CreateQuery(string entityLogicalName);
        protected virtual IQueryable<TEntity> CreateQuery<TEntity>(IQueryProvider provider, string entityLogicalName);
        public void LoadProperty(Entity entity, string propertyName);
        public void LoadProperty(Entity entity, Relationship relationship);
        public void Attach(Entity entity);
        public bool Detach(Entity entity);
        public bool Detach(Entity entity, bool recursive);
        public void AttachLink(Entity source, Relationship relationship, Entity target);
        public bool DetachLink(Entity source, Relationship relationship, Entity target);
        public IEnumerable<Entity> GetAttachedEntities();
        public bool IsAttached(Entity entity);
        public bool IsDeleted(Entity entity);
        public bool IsAttached(Entity source, Relationship relationship, Entity target);
        public bool IsDeleted(Entity source, Relationship relationship, Entity target);
        public void AddLink(Entity source, Relationship relationship, Entity target);
        public void DeleteLink(Entity source, Relationship relationship, Entity target);
        public void AddObject(Entity entity);
        public void AddRelatedObject(Entity source, Relationship relationship, Entity target);
        public void DeleteObject(Entity entity);
        public void DeleteObject(Entity entity, bool recursive);
        public void UpdateObject(Entity entity);
        public void UpdateObject(Entity entity, bool recursive);
        public OrganizationResponse Execute(OrganizationRequest request);
        public void ClearChanges();
        public SaveChangesResultCollection SaveChanges();
        public SaveChangesResultCollection SaveChanges(SaveChangesOptions options);
        protected virtual void Dispose(bool disposing);
        protected virtual void OnExecuting(OrganizationRequest request);
        protected virtual void OnExecute(OrganizationRequest request, OrganizationResponse response);
        protected virtual void OnExecute(OrganizationRequest request, Exception exception);
        protected virtual void OnSavingChanges(SaveChangesOptions options);
        protected virtual void OnSaveChanges(SaveChangesResultCollection results);
        protected virtual void OnBeginEntityTracking(Entity entity);
        protected virtual void OnEndEntityTracking(Entity entity);
        protected virtual void OnBeginLinkTracking(Entity source, Relationship relationship, Entity target);
        protected virtual void OnEndLinkTracking(Entity entity, Relationship relationship, Entity target);
    }
}
