﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using GoalManagement.CrmConnectivity;

namespace HeartBeat
{
    class Program
    {
        static void Main(string[] args)
        {
            var now = DateTime.Now;


            var pathName = string.Format("C:\\log\\HeartbeatLog{0}.txt", now.ToString("MMddyyyy"));

         
            using (StreamWriter sw = File.AppendText(pathName))
            {
                var message = string.Format("Heartbeat started {0}.", now.ToLongTimeString());
                sw.WriteLine(message);
            }

            System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();

            var crmAccess = new CRMAccess();
            var classes = crmAccess.GetClasses();

            int count = Enumerable.Count(classes);

            using (StreamWriter sw = File.AppendText(pathName))
            {
                var message = string.Format("Heartbeat completed {0} in {1} seconds. Found {2} classes", now.ToLongTimeString(), stopwatch.Elapsed.TotalSeconds, count);
                sw.WriteLine(message);
            }

        }
    }
}
