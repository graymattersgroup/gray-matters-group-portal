using System;

namespace TelerikReporting
{
    /// <summary>
    /// Summary description for Avery5160.
    /// </summary>
    public partial class StudentGoals : Telerik.Reporting.Report
    {
             private static string myName = "";
         
        public StudentGoals(string name)
        {
            myName = name;
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
           
        }


        public static string GetName()
        {
            return myName;
        }
        public static string InsertNotNull(object value, string suffix, bool isProperCase, string prefix)
        {
            if (null == value || System.DBNull.Value == value || String.IsNullOrEmpty(value.ToString().Trim()))
            {
                return string.Empty;
            }

            if (isProperCase)
            {
                var checkValue = value.ToString().ToLower().Trim();

                if (checkValue == "po box")
                {
                    return prefix + "PO Box" + suffix;
                }

                if (checkValue.StartsWith("rr"))
                {
                    return prefix + checkValue.ToUpper() + suffix;
                }

                return prefix + System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(
                        checkValue) + suffix;
            }
            else
            {
                return prefix + value + suffix;
            }
        }
    }
}