﻿// =====================================================================
//  This file is part of the Microsoft Dynamics CRM SDK code samples.
//
//  Copyright (C) Microsoft Corporation.  All rights reserved.
//
//  This source code is intended only as a supplement to Microsoft
//  Development Tools and/or on-line documentation.  See these other
//  materials for detailed information regarding Microsoft code samples.
//
//  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
//  KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//  PARTICULAR PURPOSE.
// =====================================================================

//<snippetAuthenticateWithNoHelp>

using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Description;
using GoalManagement.Support;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
// These namespaces are found in the Microsoft.Xrm.Sdk.dll assembly
// located in the SDK\bin folder of the SDK download.

namespace GoalManagement.CrmConnectivity
{
    /// <summary>
    /// Demonstrate how to do basic authentication using IServiceManagement and SecurityTokenResponse.
    /// </summary>
    public class AuthenticateWithNoHelp
    {
        public IOrganizationService OrgService { get; set; }
        // Custom Organization Context used for Early Binding LINQ
        public GrayMattersOrg GrayMattersContext { get; set; }
        //Generic Organization Context used for Late Binding LINQ 
        public OrganizationServiceContext OrgContext { get; set; }


        #region Class Level Members
        // To get discovery service address and organization unique name, 
        // Sign in to your CRM org and click Settings, Customization, Developer Resources.
        // On Developer Resource page, find the discovery service address under Service Endpoints and organization unique name under Your Organization Information.
        private String _discoveryServiceAddress = "https://dev.crm.dynamics.com/XRMServices/2011/Discovery.svc";
       // private String _organizationUniqueName = "crmNAorg473f7";
        // Provide your user name and password.

        private String _userName = "dryan@GrayMattersGroup.onmicrosoft.com";
        private String _password = "Rhino333!";

        //private String _userName = "mkovalcson@GrayMattersGroup.onmicrosoft.com";
        //private String _password = "GrisGris02";

        // Provide domain name for the On-Premises org.
        private String _domain = ""; //graymattersgroup";

        #endregion Class Level Members

        //#region How To Sample Code
        /// <summary>
        /// 
        /// </summary>
        public AuthenticateWithNoHelp()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["Production"].ConnectionString;
            var db = new SqlAzureLayer(connectionString);

            db.SetCRMAccount(_userName, _password);

            var account = db.GetCRMAccount();

            var username = (string)account.Rows[0][1];
            var password = (string) account.Rows[0][2];
            
            //TODO: commented out for local debug
            _userName = AESEncryption.Decrypt(username, "thisuser");
            _password = AESEncryption.Decrypt(password, "thispassword");

            _userName = "dryan@graymattersgroup.onmicrosoft.com";
            _password = "Rhino333!"; 
            //<snippetAuthenticateWithNoHelp1>
            IServiceManagement<IDiscoveryService> serviceManagement =ServiceConfigurationFactory.CreateManagement<IDiscoveryService>(new Uri(_discoveryServiceAddress));
            AuthenticationProviderType endpointType = serviceManagement.AuthenticationType;

            // Set the credentials.
         //   AuthenticationCredentials authCredentials = GetCredentials(serviceManagement, endpointType);

            
            IServiceManagement<IOrganizationService> orgServiceManagement2 = ServiceConfigurationFactory.
                CreateManagement<IOrganizationService>( new Uri("https://graymattersgroup.api.crm.dynamics.com/XrmServices/2011/Organization.svc "));

            //// Set the credentials.
            AuthenticationCredentials credentials2 = GetCredentials(orgServiceManagement2, endpointType);

            // Get the organization service proxy.
            using (OrganizationServiceProxy organizationProxy =
                GetProxy<IOrganizationService, OrganizationServiceProxy>(orgServiceManagement2, credentials2))
            {
                OrgService = organizationProxy;

                // This statement is required to enable early-bound type support.
                organizationProxy.EnableProxyTypes();
                
              //  organizationProxy.ServiceConfiguration.CurrentServiceEndpoint.Behaviors.Add(new ProxyTypesBehavior());

                //Get a new instance of the Late Binding Organization Context
                OrgContext = new OrganizationServiceContext(organizationProxy);

                this.GrayMattersContext = new GrayMattersOrg(organizationProxy);
            }
        }

        //<snippetAuthenticateWithNoHelp2>
        /// <summary>
        /// Obtain the AuthenticationCredentials based on AuthenticationProviderType.
        /// </summary>
        /// <param name="service">A service management object.</param>
        /// <param name="endpointType">An AuthenticationProviderType of the CRM environment.</param>
        /// <returns>Get filled credentials.</returns>
        private AuthenticationCredentials GetCredentials<TService>(IServiceManagement<TService> service, AuthenticationProviderType endpointType)
        {
            AuthenticationCredentials authCredentials = new AuthenticationCredentials();

            switch (endpointType)
            {
                case AuthenticationProviderType.ActiveDirectory:
                    authCredentials.ClientCredentials.Windows.ClientCredential =
                        new System.Net.NetworkCredential(_userName,
                            _password,
                            _domain);
                    break;
                case AuthenticationProviderType.LiveId:
                    authCredentials.ClientCredentials.UserName.UserName = _userName;
                    authCredentials.ClientCredentials.UserName.Password = _password;
                    authCredentials.SupportingCredentials = new AuthenticationCredentials();
                    ////////authCredentials.SupportingCredentials.ClientCredentials =
                    ////////    Microsoft.Crm.Services.Utility.DeviceIdManager.LoadOrRegisterDevice();
                    break;
                default: // For Federated and OnlineFederated environments.                    
                    authCredentials.ClientCredentials.UserName.UserName = _userName;
                    authCredentials.ClientCredentials.UserName.Password = _password;
                    // For OnlineFederated single-sign on, you could just use current UserPrincipalName instead of passing user name and password.
                    // authCredentials.UserPrincipalName = UserPrincipal.Current.UserPrincipalName;  // Windows Kerberos

                    // The service is configured for User Id authentication, but the user might provide Microsoft
                    // account credentials. If so, the supporting credentials must contain the device credentials.
                    if (endpointType == AuthenticationProviderType.OnlineFederation)
                    {
                        IdentityProvider provider = service.GetIdentityProvider(authCredentials.ClientCredentials.UserName.UserName);
                        ////////if (provider != null && provider.IdentityProviderType == IdentityProviderType.LiveId)
                        ////////{
                        ////////    authCredentials.SupportingCredentials = new AuthenticationCredentials();
                        ////////    authCredentials.SupportingCredentials.ClientCredentials =
                        ////////        Microsoft.Crm.Services.Utility.DeviceIdManager.LoadOrRegisterDevice();
                        ////////}
                    }

                    break;
            }

            return authCredentials;
        }
        //</snippetAuthenticateWithNoHelp2>

        /// <summary>
        /// Discovers the organizations that the calling user belongs to.
        /// </summary>
        /// <param name="service">A Discovery service proxy instance.</param>
        /// <returns>Array containing detailed information on each organization that 
        /// the user belongs to.</returns>
        public OrganizationDetailCollection DiscoverOrganizations(
            IDiscoveryService service)
        {
            if (service == null) throw new ArgumentNullException("service");
            RetrieveOrganizationsRequest orgRequest = new RetrieveOrganizationsRequest();
            RetrieveOrganizationsResponse orgResponse =
                (RetrieveOrganizationsResponse)service.Execute(orgRequest);

            return orgResponse.Details;
        }

        /// <summary>
        /// Finds a specific organization detail in the array of organization details
        /// returned from the Discovery service.
        /// </summary>
        /// <param name="orgUniqueName">The unique name of the organization to find.</param>
        /// <param name="orgDetails">Array of organization detail object returned from the discovery service.</param>
        /// <returns>Organization details or null if the organization was not found.</returns>
        /// <seealso cref="DiscoveryOrganizations"/>
        public OrganizationDetail FindOrganization(string orgUniqueName,
            OrganizationDetail[] orgDetails)
        {
            if (String.IsNullOrWhiteSpace(orgUniqueName))
                throw new ArgumentNullException("orgUniqueName");
            if (orgDetails == null)
                throw new ArgumentNullException("orgDetails");
            OrganizationDetail orgDetail = null;

            foreach (OrganizationDetail detail in orgDetails)
            {
                if (String.Compare(detail.UniqueName, orgUniqueName,
                    StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    orgDetail = detail;
                    break;
                }
            }
            return orgDetail;
        }

        /// <summary>
        /// Generic method to obtain discovery/organization service proxy instance.
        /// </summary>
        /// <typeparam name="TService">
        /// Set IDiscoveryService or IOrganizationService type to request respective service proxy instance.
        /// </typeparam>
        /// <typeparam name="TProxy">
        /// Set the return type to either DiscoveryServiceProxy or OrganizationServiceProxy type based on TService type.
        /// </typeparam>
        /// <param name="serviceManagement">An instance of IServiceManagement</param>
        /// <param name="authCredentials">The user's Microsoft Dynamics CRM logon credentials.</param>
        /// <returns></returns>
        /// <snippetAuthenticateWithNoHelp4>
        private TProxy GetProxy<TService, TProxy>(
            IServiceManagement<TService> serviceManagement,
            AuthenticationCredentials authCredentials)
            where TService : class
            where TProxy : ServiceProxy<TService>
        {
            Type classType = typeof(TProxy);

            if (serviceManagement.AuthenticationType !=
                AuthenticationProviderType.ActiveDirectory)
            {
                AuthenticationCredentials tokenCredentials =
                    serviceManagement.Authenticate(authCredentials);
                // Obtain discovery/organization service proxy for Federated, LiveId and OnlineFederated environments. 
                // Instantiate a new class of type using the 2 parameter constructor of type IServiceManagement and SecurityTokenResponse.
                return (TProxy)classType
                    .GetConstructor(new Type[] { typeof(IServiceManagement<TService>), typeof(SecurityTokenResponse) })
                    .Invoke(new object[] { serviceManagement, tokenCredentials.SecurityTokenResponse });
            }

            // Obtain discovery/organization service proxy for ActiveDirectory environment.
            // Instantiate a new class of type using the 2 parameter constructor of type IServiceManagement and ClientCredentials.
            return (TProxy)classType
                .GetConstructor(new Type[] { typeof(IServiceManagement<TService>), typeof(ClientCredentials) })
                .Invoke(new object[] { serviceManagement, authCredentials.ClientCredentials });
        }
        /// </snippetAuthenticateWithNoHelp4>

        //#endregion How To Sample Code

        //#region Main method

        
        //static public void Main(string[] args)
        //{
        //    try
        //    {
        //        AuthenticateWithNoHelp app = new AuthenticateWithNoHelp();
        //        app.Run();
        //    }
        //    catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex)
        //    {
        //        Console.WriteLine("The application terminated with an error.");
        //        Console.WriteLine("Timestamp: {0}", ex.Detail.Timestamp);
        //        Console.WriteLine("Code: {0}", ex.Detail.ErrorCode);
        //        Console.WriteLine("Message: {0}", ex.Detail.Message);
        //        Console.WriteLine("Trace: {0}", ex.Detail.TraceText);
        //        Console.WriteLine("Inner Fault: {0}",
        //            null == ex.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault");
        //    }
        //    catch (System.TimeoutException ex)
        //    {
        //        Console.WriteLine("The application terminated with an error.");
        //        Console.WriteLine("Message: {0}", ex.Message);
        //        Console.WriteLine("Stack Trace: {0}", ex.StackTrace);
        //        Console.WriteLine("Inner Fault: {0}",
        //            null == ex.InnerException.Message ? "No Inner Fault" : ex.InnerException.Message);
        //    }
        //    catch (System.Exception ex)
        //    {
        //        Console.WriteLine("The application terminated with an error.");
        //        Console.WriteLine(ex.Message);

        //        // Display the details of the inner exception.
        //        if (ex.InnerException != null)
        //        {
        //            Console.WriteLine(ex.InnerException.Message);

        //            FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> fe = ex.InnerException
        //                as FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>;
        //            if (fe != null)
        //            {
        //                Console.WriteLine("Timestamp: {0}", fe.Detail.Timestamp);
        //                Console.WriteLine("Code: {0}", fe.Detail.ErrorCode);
        //                Console.WriteLine("Message: {0}", fe.Detail.Message);
        //                Console.WriteLine("Trace: {0}", fe.Detail.TraceText);
        //                Console.WriteLine("Inner Fault: {0}",
        //                    null == fe.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault");
        //            }
        //        }
        //    }
        //    // Additional exceptions to catch: SecurityTokenValidationException, ExpiredSecurityTokenException,
        //    // SecurityAccessDeniedException, MessageSecurityException, and SecurityNegotiationException.

        //    finally
        //    {
        //        Console.WriteLine("Press <Enter> to exit.");
        //        Console.ReadLine();
        //    }
        //}
       // #endregion Main method
    }
}
//</snippetAuthenticateWithNoHelp>