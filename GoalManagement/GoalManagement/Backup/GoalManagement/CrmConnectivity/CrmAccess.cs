using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Web;
using GoalManagement.Support;
//using MapSearch.CrmConnectivity;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Crm;
using Microsoft.Crm.Sdk;
using Microsoft.Xrm.Sdk.Discovery;
using Microsoft.Xrm.Sdk.Query;
using System.Globalization;
using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Reflection;

// These namespaces are found in the Microsoft.Xrm.Sdk.dll assembly
// located in the SDK\bin folder of the SDK download.
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
using Microsoft.Crm.Sdk.Messages;

namespace GoalManagement.CrmConnectivity
{

    public class CRMAccess
    {
        public IOrganizationService OrgService { get; set; }
        // Custom Organization Context used for Early Binding LINQ
        public GrayMattersOrg GrayMattersContext { get; set; }
        //Generic Organization Context used for Late Binding LINQ 
        public OrganizationServiceContext OrgContext { get; set; }

        public enum RxRole
        {
            Student = 100000000,
            CoachTeamLeader = 100000001,
            ExecutiveSponsor = 100000002
        } ;

        public enum EmailTargets
        {
            
            Students=1,
            Coaches=2,
            Sponsors=3,
            CoachesAndSponsors=4,
            All = 5,
            TestEmailToMe = 6,
            None = 100000000
        } ;

        public CRMAccess()
        {
            var auth = new AuthenticateWithNoHelp();
            OrgService = auth.OrgService;
            GrayMattersContext = auth.GrayMattersContext;
            OrgContext = auth.OrgContext;
        }

//        public CRMAccess()
//        {
//            if (OrgContext != null) return;

//        //    var authenticate = new Authenticate();
//       //     authenticate.Run();



//            OrganizationDetail currentOrganizationDetail;
//           // var discoUri = new Uri("https://dev.crm.dynamics.com/XRMServices/2011/Discovery.svc");
//            var discoUri = new Uri("https://graymattersgroup.api.crm.dynamics.com/XrmServices/2011/Organization.svc");
            
//var clientCreds = new ClientCredentials();
//            //clientCreds.UserName.UserName = "dryan@graymattersltd.com";
//            //clientCreds.UserName.Password = "rhino33";

//            clientCreds.UserName.UserName = "dryan@GrayMattersGroup.onmicrosoft.com";
//            clientCreds.UserName.Password = "Rhino333";

//            var deviceCreds = 
//                DeviceIdManager.LoadOrRegisterDevice("sssssssssssssssssss", "ssssssssssssssssssssss");

//            var dsp = new DiscoveryServiceProxy(discoUri, null, clientCreds, deviceCreds);
//            dsp.Authenticate();

//            var orgRequest = new RetrieveOrganizationsRequest();
//            var orgResponse = dsp.Execute(orgRequest) as RetrieveOrganizationsResponse;

//            foreach (OrganizationDetail detail in orgResponse.Details)
//            {
//                if (detail.UrlName == "graymattersgroup")
//                {
//                    currentOrganizationDetail = detail;
//                    var orgServiceUri = new Uri(currentOrganizationDetail.Endpoints[EndpointType.OrganizationService]);
//                    OrgService = new OrganizationServiceProxy(orgServiceUri, null, clientCreds, deviceCreds);

//                    var osp = OrgService as OrganizationServiceProxy;
//                    osp.ServiceConfiguration.CurrentServiceEndpoint.Behaviors.Add(new ProxyTypesBehavior());

//                    //Get a new instance of the Late Binding Organization Context
//                    this.OrgContext = new OrganizationServiceContext(OrgService);

//                    this.GrayMattersContext = new GrayMattersOrg(OrgService);
//                    break;
//                }
//            }
 
//        }


        //public new_class GetMyClasses(Guid classId)
        //{
        //   var classQuery = from r in OrgContext.CreateQuery("new_class")
        //   where r.GetAttributeValue<Guid>("new_classid") == classId
        //   orderby  r["new_name"]
        //   select new new_class
        //   {
        //       Name = r["contoso_name"].ToString(),
        //       Id = new Guid(r["contoso_roomid"].ToString())
        //   };

        //    return classQuery.FirstOrDefault();
        //}


        public IQueryable<new_class> GetClasses()
        {
            var classes = (from cl in GrayMattersContext.new_classSet
                           select cl);
                                      //{
                                      //    new_classId = cl.new_classId,
                                      //    new_name = cl.new_name,
                                      //    new_ClassDate = cl.new_ClassDate,
                                      //    new_GoalsRequired = cl.new_GoalsRequired
                                      //});

            return classes;
        }

        public IQueryable<Account> GetAccounts()
        {
            var classes = (from cl in GrayMattersContext.AccountSet
                           orderby cl.Name
                           select cl);
            return classes;
        }

        public new_class GetClass(Guid classId)
        {
            var thisClass = (from cl in GrayMattersContext.new_classSet
                           where cl.new_classId == classId
                           select cl).FirstOrDefault();

            return thisClass;
        }

        public IQueryable<trk_asset> GetAssets()
        {
            var classes = (from cl in GrayMattersContext.trk_assetSet
                           orderby cl.trk_name
                           select cl);
            return classes;
        }

        public trk_asset GetAsset(Guid assetId)
        {
            var thisAsset = (from a in GrayMattersContext.trk_assetSet
                             where a.trk_assetId == assetId
                             select a).FirstOrDefault();

            return thisAsset;
        }

        public void SaveAssetTracking(Guid assetId, Guid contactid, Guid scheduledTouchId, string assetName)
    {
        var assetTracking = new trk_assettracking()
        {
            trk_ContactId = new EntityReference(Contact.EntityLogicalName, contactid),
            trk_AssetViewedId = new EntityReference(trk_asset.EntityLogicalName, assetId),
            trk_ScheduledTouchId = new EntityReference(new_scheduledtouch.EntityLogicalName, scheduledTouchId),
            trk_name = string.Format("{0} - {1}",assetName, DateTime.Today.ToString("MMM dd, yyyy")) 
        };

        assetTracking.RelatedEntities.Clear();
        GrayMattersContext.AddObject(assetTracking);
        GrayMattersContext.SaveChanges();
    }

        public List<Annotation> GetAnnotations(Guid objectid)
        {
          
            var annotations = (from a in GrayMattersContext.AnnotationSet
                                  where a.ObjectId.Id == objectid
                                  select a).ToList();
            return annotations;
        }

        public void CopyAnnotationToScheduledTouch(Guid annotationid, Guid scheduledtouchid)
        {
            var annotation = (from a in GrayMattersContext.AnnotationSet
                               where a.AnnotationId == annotationid
                               select a).FirstOrDefault();

            //var newAnnotation = annotation;

            //var newId = Guid.NewGuid();

            //newAnnotation.RelatedEntities.Clear();
            //newAnnotation.ObjectId = new EntityReference("new_scheduledtouch", scheduledtouchid);
            //newAnnotation.ObjectTypeCode = "new_scheduledtouch";
            //newAnnotation.Id = newId;
            //newAnnotation.AnnotationId = newId;

            var newAnnotation = new Annotation()
                                    {
                                        ObjectId = new EntityReference("new_scheduledtouch", scheduledtouchid),
                                        ObjectTypeCode = "new_scheduledtouch",
                                        FileName = annotation.FileName,
                                        IsDocument = annotation.IsDocument,
                                        NoteText = annotation.NoteText,
                                        Subject = annotation.Subject,
                                        MimeType = annotation.MimeType,
                                        DocumentBody = annotation.DocumentBody
                                    };
            
            OrgService.Create(newAnnotation);
        }

        public void CreateAnnotation(Annotation newAnnotation)
        {
            OrgService.Create(newAnnotation);
        }

        public void DeleteAnnotation(Guid annotationid)
        {
            OrgService.Delete("annotation", annotationid);
        }

        public List<new_coachingadvice> GetEmailTemplates()
        {

            var emailTemplates = (from cs in GrayMattersContext.new_coachingadviceSet
                                      select cs).ToList();
            return emailTemplates;
        }

        public Contact GetStudent(string emailaddress)
        {
            var student = (from c in GrayMattersContext.ContactSet
                           where c.EMailAddress1 == emailaddress
                           select c).FirstOrDefault();

            return student;
        }


        public List<new_scheduledtouch> GetOpenTouches()
        {

            var emailTemplates = (from cs in GrayMattersContext.new_scheduledtouchSet
                                  where cs.new_Sent != true
                                  select cs).ToList();
            return emailTemplates;
        }


        public void ClearStudent(string emailaddress)
        {
            var student = (from c in GrayMattersContext.ContactSet
                           where c.EMailAddress1 == emailaddress
                           select c).FirstOrDefault();

            var myAnswers = (from ans in GrayMattersContext.new_answerSet
                             where ans.new_StudentId.Id == student.ContactId 
                             select ans);

            foreach (new_answer del in myAnswers)
            {
                GrayMattersContext.DeleteObject(del);
            }

            var myUpdate = (from sup in GrayMattersContext.new_studentupdateperiodSet
                            where sup.new_StudentId.Id == student.ContactId 
                            select sup).ToList();

            foreach (new_studentupdateperiod del in myUpdate)
            {
                GrayMattersContext.DeleteObject(del);
            }

            var studentGoals = (from sg in GrayMattersContext.new_studentgoalsSet
                                where sg.new_StudentId.Id == student.ContactId
                                select new new_studentgoals
                                {
                                    new_studentgoalsId = sg.new_studentgoalsId,
                                    new_name = sg.new_name,
                                    new_classgoalId = sg.new_classgoalId,
                                    new_Benefit = sg.new_Benefit
                                });

            foreach (new_studentgoals del in studentGoals)
            {
                GrayMattersContext.DeleteObject(del);
            }

            var Feedback = (from sup in GrayMattersContext.new_coachingSet
                                  where sup.new_CoachId.Id == student.ContactId
                                  select sup).ToList();

            foreach (new_coaching del in Feedback)
            {
                GrayMattersContext.DeleteObject(del);
            }

            var existingclassstatus = (from cs in GrayMattersContext.new_classstatusSet
                                       where cs.new_StudentId.Id == student.ContactId
                                       select cs).ToList();

            foreach (new_classstatus del in existingclassstatus)
            {
                GrayMattersContext.DeleteObject(del);
            }

            GrayMattersContext.SaveChanges();
        }

        public List<Contact> GetCoaches(Guid classid)
        {
            var coaches = (from c in GrayMattersContext.ContactSet
                           join cltos in GrayMattersContext.new_class_contactSet on c.ContactId equals cltos.contactid
                           where cltos.new_classid == classid
                           select new Contact
                           {
                               ContactId = c.ContactId,
                               FirstName = c.FirstName,
                               LastName = c.LastName,
                               ParentCustomerId = c.ParentCustomerId,
                               new_ManagerId = c.new_ManagerId,
                               ManagerName = c.ManagerName,
                               new_MyCoachId = c.new_MyCoachId,
                               EMailAddress1 = c.EMailAddress1
                           }).ToList();

            return coaches;
        }

        public List<Contact> GetClassParticipants(Guid classid)
        {
            var coaches = (from c in GrayMattersContext.ContactSet
                           join cltos in GrayMattersContext.new_class_contactSet on c.ContactId equals cltos.contactid
                           where cltos.new_classid == classid
                           select new Contact
                           {
                               ContactId = c.ContactId,
                               new_ContactRxRole = c.new_ContactRxRole
                           }).ToList();

            var students = (from c in GrayMattersContext.ContactSet
                            join cltos in GrayMattersContext.new_class_studentSet on c.ContactId equals cltos.contactid
                           where cltos.new_classid == classid
                           select new Contact
                           {
                               ContactId = c.ContactId,
                               new_ContactRxRole = c.new_ContactRxRole
                           }).ToList();

            var participants = coaches;

            participants.AddRange(students);

            return participants;
        }


        public Contact GetMyCoach(Guid coachid)
        {
            var mycoach= (from cs in GrayMattersContext.ContactSet
                                       where cs.ContactId == coachid
                                       select cs).FirstOrDefault();

            return mycoach;
        }

        /// <summary>
        /// GetStudent Classes
        /// </summary>
        /// <param name="studentid"></param>
        /// <returns></returns>
        public IQueryable<new_class> GetStudentClasses(Guid studentid)
        {
            var classes = (from cl in GrayMattersContext.new_classSet
                           join cltos in GrayMattersContext.new_class_studentSet on cl.new_classId equals cltos.new_classid
                           where cltos.contactid == studentid
                           orderby cl.new_name
                           select cl
                           );

            return classes;
        }


        public IQueryable<new_class> GetAllClasses()
        {
            var classes = (from cl in GrayMattersContext.new_classSet
                           orderby cl.new_name
                           select cl);

            return classes;
        }


        public IQueryable<trk_asset> GetAllAssets()
        {
            var assets = (from cl in GrayMattersContext.trk_assetSet
                           orderby cl.trk_name
                           select cl);

            return assets;
        }
        /// <summary>
        /// Get Coach Classes
        /// </summary>
        /// <param name="studentid"></param>
        /// <returns></returns>
        public IQueryable<new_class> GetCoachClasses(Guid studentid)
        {
            var classes = (from cl in GrayMattersContext.new_classSet
                           join cltos in GrayMattersContext.new_class_contactSet on cl.new_classId equals cltos.new_classid
                           where cltos.contactid == studentid
                           orderby cl.new_name
                           select cl
                          );

            return classes;
        }

        public List<Contact> GetClassStudents(Guid classId)
        {
            var classes = (from c in GrayMattersContext.ContactSet
                           join students in GrayMattersContext.new_class_studentSet on c.ContactId equals
                               students.contactid
                           where students.new_classid == classId
                           orderby c.LastName , c.FirstName
                           select c).ToList(); 
            //new Contact
            //               {
            //                   ContactId = c.ContactId,
            //                   FirstName = c.FirstName,
            //                   LastName = c.LastName,
            //                   ParentCustomerId = c.ParentCustomerId,
            //                   new_ManagerId = c.new_ManagerId,
            //                   ManagerName = c.ManagerName,
            //                   new_MyCoachId = c.new_MyCoachId,
            //                   EMailAddress1 = c.EMailAddress1
                              
            //               }).ToList();

            return classes;
        }

     


        public List<Contact> GetAllStudents()
        {
            var classes = (from c in GrayMattersContext.ContactSet
                           join students in GrayMattersContext.new_class_studentSet on c.ContactId equals students.contactid
                           orderby c.LastName, c.FirstName
                           select c ).ToList();

            return classes;
        }

        public List<Contact> GetAllContacts()
        {
            var classes = (from c in GrayMattersContext.ContactSet
                           where c.EMailAddress1 != null
                           orderby c.LastName, c.FirstName
                           select c).ToList();

            return classes;
        }

        public List<Contact> GetClassCoaches(Guid classId)
        {
            var coachlist = (from d in GrayMattersContext.ContactSet
                           join coaches in GrayMattersContext.new_class_contactSet on d.ContactId equals coaches.contactid
                           where coaches.new_classid == classId
                           orderby d.LastName, d.FirstName
                           select d ).ToList();
                           //new Contact
                           //{
                           //    ContactId = c.ContactId,
                           //    FirstName = c.FirstName,
                           //    LastName = c.LastName,
                           //    ParentCustomerId = c.ParentCustomerId,
                           //    new_ManagerId = c.new_ManagerId,
                           //    ManagerName = c.ManagerName,
                           //    new_MyCoachId = c.new_MyCoachId,
                           //    EMailAddress1 = c.EMailAddress1,
                           //    new_ContactRxRole = c.new_ContactRxRole
                           //}).ToList();

            return coachlist;
        }
        public IQueryable<new_classstatus> GetStudentClassStatus(Guid studentid, Guid classId)
        {
            var classestatus = (from cs in GrayMattersContext.new_classstatusSet
                                where cs.new_StudentId.Id == studentid && cs.new_ClassId.Id == classId
                                select new new_classstatus
                                {
                                    new_name = cs.new_name,
                                    new_Registered = cs.new_Registered,
                                    new_GoalsSelected = cs.new_GoalsSelected,
                                    new_LastUpdate = cs.new_LastUpdate
                                });

            return classestatus;
        }

        public List<new_classstatus> GetClassStatuses( Guid classId)
        {
            var classestatus = (from cs in GrayMattersContext.new_classstatusSet
                                where cs.new_ClassId.Id == classId
                                select cs).ToList();

            return classestatus;
        }

        public void WriteClassStatus(Guid studentid, Guid classId)
        {
            var existingclassstatus = (from cs in GrayMattersContext.new_classstatusSet
                                       where cs.new_StudentId.Id == studentid && cs.new_ClassId.Id == classId
                                       select cs).FirstOrDefault();
            //select new new_classstatus
            //{
            //    new_name = cs.new_name,
            //    new_Registered = cs.new_Registered,
            //    new_GoalsSelected = cs.new_GoalsSelected,
            //    new_LastUpdate = cs.new_LastUpdate
            //});

            bool existing = false;

            if (existingclassstatus != null)
            {
                existingclassstatus.new_Registered = true;
                existingclassstatus.new_GoalsSelected = true;
                existingclassstatus.new_LastUpdate = DateTime.Today;
                GrayMattersContext.UpdateObject(existingclassstatus);
                GrayMattersContext.SaveChanges();
                existing = true;
            }

            if (!existing)
            {
                var classstatus = new new_classstatus
                                      {
                                          new_name = "Portal Generated",
                                          new_StudentId = new EntityReference(Contact.EntityLogicalName, studentid),
                                          new_ClassId = new EntityReference(new_class.EntityLogicalName, classId),
                                          new_Registered = true,
                                          new_GoalsSelected = true,
                                          new_LastUpdate = DateTime.Today
                                      };

                classstatus.RelatedEntities.Clear();
                GrayMattersContext.AddObject(classstatus);
                GrayMattersContext.SaveChanges();
            }

        }

         

        public void SaveCoaching(List<new_coaching> mycoaching)
        {
            foreach (new_coaching newCoaching in mycoaching)
            {
                newCoaching.RelatedEntities.Clear();
                GrayMattersContext.AddObject(newCoaching);
            }

            GrayMattersContext.SaveChanges();
        }

        public void UpdateCoachingGrade(new_coaching newCoaching)
        {
            var coachingGrade = (from cs in GrayMattersContext.new_coachingSet
                                       where cs.new_coachingId == newCoaching.new_coachingId
                                       select cs).FirstOrDefault();

            coachingGrade.new_UpdateGrade = newCoaching.new_UpdateGrade;

            GrayMattersContext.UpdateObject(coachingGrade);
            GrayMattersContext.SaveChanges();
        }

        
        public void WriteCoaching(Guid studentid, Guid classId)
        {
            var existingclassstatus = (from cs in GrayMattersContext.new_classstatusSet
                                       where cs.new_StudentId.Id == studentid && cs.new_ClassId.Id == classId
                                       select cs).FirstOrDefault();
            //select new new_classstatus
            //{
            //    new_name = cs.new_name,
            //    new_Registered = cs.new_Registered,
            //    new_GoalsSelected = cs.new_GoalsSelected,
            //    new_LastUpdate = cs.new_LastUpdate
            //});

            bool existing = false;

            if (existingclassstatus != null)
            {
                existingclassstatus.new_Registered = true;
                existingclassstatus.new_GoalsSelected = true;
                existingclassstatus.new_LastUpdate = DateTime.Today;
                GrayMattersContext.UpdateObject(existingclassstatus);
                GrayMattersContext.SaveChanges();
                existing = true;
            }

            if (!existing)
            {
                var classstatus = new new_classstatus
                {
                    new_name = "Portal Generated",
                    new_StudentId = new EntityReference(Contact.EntityLogicalName, studentid),
                    new_ClassId = new EntityReference(new_class.EntityLogicalName, classId),
                    new_Registered = true,
                    new_GoalsSelected = true,
                    new_LastUpdate = DateTime.Today
                };

                classstatus.RelatedEntities.Clear();
                GrayMattersContext.AddObject(classstatus);
                GrayMattersContext.SaveChanges();
            }

        }
        /// <summary>
        /// Adds and removes Student Goal Records
        /// </summary>
        /// <param name="selectedGoals"></param>
        /// <param name="classId"></param>
        /// <param name="studentId"></param>
        public void UpdateStudentGoals(List<Guid> selectedGoals, List<string>goalNames, Guid classId, Guid studentId)
        {
            var studentGoals = (from sg in GrayMattersContext.new_studentgoalsSet
                                where sg.new_ClassId.Id == classId && sg.new_StudentId.Id == studentId
                                select new new_studentgoals
                                {
                                    new_studentgoalsId = sg.new_studentgoalsId,
                                    new_name = sg.new_name,
                                    new_classgoalId = sg.new_classgoalId,
                                    new_Benefit = sg.new_Benefit
                                });

            // Remove Goals that are not selected
            foreach (var newStudentgoal in studentGoals)
            {
                if (!selectedGoals.Contains(newStudentgoal.new_classgoalId.Id))
                {
                    GrayMattersContext.DeleteObject(newStudentgoal);
                    GrayMattersContext.SaveChanges();
                }
            }

            // Add new Goals
            int count = 0;
            foreach (var goalId in selectedGoals)
            {
                bool createNew = Enumerable.All(studentGoals, newStudentgoal => newStudentgoal.new_classgoalId.Id != goalId);

                if (createNew)
                {
                    var newGoal = new new_studentgoals()
                                      {
                                          new_ClassId = new EntityReference("new_class", classId),
                                          new_StudentId = new EntityReference("contact", studentId),
                                          new_classgoalId = new EntityReference("new_classgoal", goalId),
                                          new_name = goalNames[count],
                                          EntityState = null
                                      };

                    newGoal.RelatedEntities.Clear();
                    GrayMattersContext.AddObject(newGoal);
                    GrayMattersContext.SaveChanges();
                }
                count++;
            }
        }


        public IQueryable<new_class> GetStudentClassesByEmail(string emailaddress)
        {
            var classes = (from cl in GrayMattersContext.new_classSet
                           join cltos in GrayMattersContext.new_class_studentSet on cl.new_classId equals cltos.new_classid
                           join st in GrayMattersContext.ContactSet on cltos.contactid equals st.ContactId
                           where st.EMailAddress1.ToLower() == emailaddress.ToLower()

                           select new new_class
                           {
                               new_classId = cl.new_classId,
                               new_name = cl.new_name,
                               new_ClassDate = cl.new_ClassDate,
                               new_GoalsRequired = cl.new_GoalsRequired
                           });

            return classes;
        }

        //public void GetCreateUserGoal(Guid classgoalid, Guid studentid, Guid classId, bool isSelected)
        //{
        //    var existingGoal = (from sg in GrayMattersContext.new_studentgoalsSet
        //                        where sg.new_ClassId.Id == classId
        //                        && sg.new_classgoalId.Id == classgoalid
        //                        && sg.new_StudentId.Id == studentid
        //                        select new new_studentgoals
        //                        {
        //                            new_studentgoalsId = sg.new_studentgoalsId
        //                        });

        //    if (isSelected)
        //    {
        //        if (existingGoal.FirstOrDefault() == null)
        //        {
        //            var newGoal = new new_studentgoals
        //                              {
        //                                  new_ClassId = new EntityReference("new_class", classId),
        //                                  new_StudentId = new EntityReference("contact", studentid),
        //                                  new_classgoalId = new EntityReference("new_classgoal", classgoalid),
        //                                  new_name = "Portal Generated",
        //                                  EntityState = null
        //                              };

        //            newGoal.RelatedEntities.Clear();
        //            GrayMattersContext.AddObject(newGoal);
        //            GrayMattersContext.SaveChanges();
        //        }
        //    }
        //    else
        //    {
        //        if (existingGoal.FirstOrDefault() != null)
        //        {
        //            GrayMattersContext.DeleteObject(existingGoal.FirstOrDefault());
        //            GrayMattersContext.SaveChanges();
        //        }
        //    }

        //}

        public void UpdateStudentGoal(List<ClassGoal> goalList, Guid studentId, Guid classId)
        {
            foreach (ClassGoal goalPage in goalList)
            {
                if (goalPage.selected)
                {
                    new_studentgoals thisGoal;

                    // Update Goal Payoffs

                    var id = goalPage.id;
                    // Update goalPage.
                    thisGoal = (from sg in GrayMattersContext.new_studentgoalsSet
                                where sg.new_ClassId.Id == classId && sg.new_classgoalId.Id == id && sg.new_StudentId.Id == studentId
                                select new new_studentgoals
                                {
                                    new_studentgoalsId = sg.new_studentgoalsId,
                                    new_Benefit = sg.new_Benefit
                                    
                                }).FirstOrDefault();

                    if (goalPage.payoff != goalPage.initialpayoff)
                    {
                        thisGoal.new_Benefit = goalPage.payoff;
                        GrayMattersContext.UpdateObject(thisGoal);
                    }

                    var existingActivities = (from ga in GrayMattersContext.new_goalactivitySet
                                              where ga.new_ActivitiesId.Id == id 
                                              select new new_goalactivity()
                                                         {
                                                             new_goalactivityId = ga.new_goalactivityId,
                                                             new_Header = ga.new_Header
                                                         }).ToList();


                    // Update related studentGoal activity links Activities
                    foreach (Activity anActivity in goalPage.activities)
                    {
                        if (anActivity.selected != anActivity.initialselected)
                        {
                            foreach (new_goalactivity matchingActivity in existingActivities)
                            {
                                if (anActivity.id == matchingActivity.new_goalactivityId)
                                {
                                    if (anActivity.selected)
                                    {
                                        GrayMattersContext.AddLink(thisGoal, new Relationship(new_studentgoals_goalactivity.EntityLogicalName), matchingActivity);

                                    }
                                    else
                                    {
                                        GrayMattersContext.DeleteLink(thisGoal, new Relationship(new_studentgoals_goalactivity.EntityLogicalName), matchingActivity);
                                    }
                                }
                            }

                            anActivity.initialselected = anActivity.selected;
                        }

                        if (anActivity.subActivities.Count != 0)
                        {
                            foreach (SubActivity subActivity in anActivity.subActivities)
                            {
                                if (subActivity.selected != subActivity.initialselected)
                                {
                                   
                                    var matchingSubActivity = (from ga in GrayMattersContext.new_subactivitySet
                                                               where ga.new_subactivityId == subActivity.id
                                                               select new new_subactivity()
                                                               {
                                                                   new_subactivityId = ga.new_subactivityId,
                                                                   new_Description = ga.new_Description
                                                               }).FirstOrDefault();
                                    if (subActivity.selected)
                                    {
                                       GrayMattersContext.AddLink(thisGoal, new Relationship(new_studentgoals_subactivity.EntityLogicalName), matchingSubActivity);
                                      
                                    }
                                    else
                                    {
                                        GrayMattersContext.DeleteLink(thisGoal, new Relationship(new_studentgoals_subactivity.EntityLogicalName), matchingSubActivity);

                                    }

                                    subActivity.initialselected = subActivity.selected;
                                }
                            }
                        }
                    }

                }
            }
            GrayMattersContext.SaveChanges();
        }


        public void UpdateStudentActivities(List<Guid> selectedActivityIds, Guid goalId, Guid studentId, Guid classId, string benefitText)
        {
            var existingActivities = (from ga in GrayMattersContext.new_goalactivitySet
                                      join sgga in GrayMattersContext.new_studentgoals_goalactivitySet on ga.new_goalactivityId equals sgga.new_goalactivityid
                                      join sg in GrayMattersContext.new_studentgoalsSet on sgga.new_studentgoalsid equals sg.new_studentgoalsId
                                      where sg.new_ClassId.Id == classId && sg.new_StudentId.Id == studentId && sg.new_classgoalId.Id == goalId
                                      select new new_goalactivity()
                                      {
                                          new_goalactivityId = ga.new_goalactivityId,
                                          new_name = ga.new_name
                                      });

            var thisStudentGoal = (
                            from sg in GrayMattersContext.new_studentgoalsSet
                            where sg.new_StudentId.Id == studentId && sg.new_classgoalId.Id == goalId && sg.new_ClassId.Id == classId
                            select new new_studentgoals
                            {
                                new_studentgoalsId = sg.new_studentgoalsId,
                                new_name = sg.new_name,
                                new_Benefit = sg.new_Benefit

                            }).FirstOrDefault();

            var id = thisStudentGoal.new_studentgoalsId;

            thisStudentGoal.new_Benefit = benefitText;

            GrayMattersContext.UpdateObject(thisStudentGoal);

            // Remove Goals that are not selected);
            foreach (var existingActivity in existingActivities)
            {
                if (!selectedActivityIds.Contains(existingActivity.new_goalactivityId.Value))
                {
                    GrayMattersContext.DeleteLink(thisStudentGoal, new Relationship(new_studentgoals_goalactivity.EntityLogicalName), existingActivity);

                }
            }

            // Add new Goals
            foreach (var activityId in selectedActivityIds)
            {
                bool createNew = true;

                foreach (new_goalactivity existingActivity in existingActivities)
                {
                    if (existingActivity.new_goalactivityId == activityId)
                    {
                        createNew = false;
                    }
                }

                if (createNew)
                {

                    var thisActivity = (
                            from ga in GrayMattersContext.new_goalactivitySet
                            where ga.new_goalactivityId == activityId
                            select new new_goalactivity
                            {
                                new_goalactivityId = ga.new_goalactivityId
                            }).FirstOrDefault();

                    try
                    {
                        GrayMattersContext.AddLink(thisStudentGoal,
                                                   new Relationship(new_studentgoals_goalactivity.EntityLogicalName),
                                                   thisActivity);
                    }
                    catch (Exception ex)
                    {
                        var blah = ex.Message;
                    }

                }
            }

            GrayMattersContext.SaveChanges();
        }

        public void UpdateStudentSubActivities(List<Guid> selectedActivityIds, Guid goalId, Guid studentId, Guid classId, string benefitText)
        {


            var existingActivities = (from ga in GrayMattersContext.new_subactivitySet
                                      join sgga in GrayMattersContext.new_studentgoals_subactivitySet on ga.new_subactivityId equals sgga.new_subactivityid
                                      join sg in GrayMattersContext.new_studentgoalsSet on sgga.new_studentgoalsid equals sg.new_studentgoalsId
                                      where sg.new_ClassId.Id == classId && sg.new_StudentId.Id == studentId && sg.new_classgoalId.Id == goalId
                                      select new new_subactivity()
                                      {
                                          new_subactivityId = ga.new_subactivityId,
                                          new_name = ga.new_name
                                      });

            var thisStudentGoal = (
                            from sg in GrayMattersContext.new_studentgoalsSet
                            where sg.new_StudentId.Id == studentId && sg.new_classgoalId.Id == goalId && sg.new_ClassId.Id == classId
                            select new new_studentgoals
                            {
                                new_studentgoalsId = sg.new_studentgoalsId,
                                new_name = sg.new_name,
                                new_Benefit = sg.new_Benefit

                            }).FirstOrDefault();

            thisStudentGoal.new_Benefit = benefitText;
            GrayMattersContext.UpdateObject(thisStudentGoal);

            // Remove Goals that are not selected
            foreach (var existingActivity in existingActivities)
            {
                if (!selectedActivityIds.Contains(existingActivity.new_subactivityId.Value))
                {
                    GrayMattersContext.DeleteLink(thisStudentGoal, new Relationship(new_studentgoals_subactivity.EntityLogicalName), existingActivity);

                }
            }

            // Add new Goals
            foreach (var activityId in selectedActivityIds)
            {
                bool createNew = true;

                foreach (new_subactivity existingActivity in existingActivities)
                {
                    if (existingActivity.new_subactivityId == activityId)
                    {
                        createNew = false;
                    }
                }

                if (createNew)
                {
                    var thisSubActivity = (
                            from ga in GrayMattersContext.new_subactivitySet
                            where ga.new_subactivityId == activityId
                            select new new_subactivity
                            {
                                new_subactivityId = ga.new_subactivityId
                            }).FirstOrDefault();


                    GrayMattersContext.AddLink(thisStudentGoal, new Relationship(new_studentgoals_subactivity.EntityLogicalName), thisSubActivity);

                }
            }

            GrayMattersContext.SaveChanges();
        }



        public IQueryable<Account> GetCompanies()
        {
            var classes = (from ac in GrayMattersContext.AccountSet
                           select new Account()
                           {
                               AccountId = ac.AccountId,
                               Name = ac.Name
                           });

            return classes;
        }


        public IQueryable<Annotation> GetClassAnnotation(Guid classId)
        {
            var classes = (from a in GrayMattersContext.AnnotationSet
                           where a.IsDocument == true && a.ObjectId.Id == classId
                           select new Annotation()
                           {
                               DocumentBody = a.DocumentBody,
                               FileName = a.FileName
                           });

            return classes;
        }

        public IQueryable<ClassGoal> GetClassGoals(Guid classId)
        {
            var equip = (from cg in GrayMattersContext.new_classgoalSet
                         join clcg in GrayMattersContext.new_class_classgoalSet on cg.new_classgoalId equals clcg.new_classgoalid
                         join cl in GrayMattersContext.new_classSet on clcg.new_classid equals cl.new_classId
                         where cl.new_classId == classId
                         orderby cg.new_SortOrder
                         select new ClassGoal
                                    {
                                        name = cg.new_name,
                                        id = (Guid)cg.new_classgoalId,
                                        min = cg.new_MinimumActivities,
                                        max = cg.new_MaximumActivities,
                                        isdefault = cg.new_DefaultGoal,
                                        new_SortOrder = cg.new_SortOrder
                                    }
                         );

            return equip;
        }

        public IQueryable<new_studentgoals> GetStudentGoals(Guid classId, Guid studentId)
        {

            var studentGoals = (from sg in GrayMattersContext.new_studentgoalsSet
                                where sg.new_ClassId.Id == classId && sg.new_StudentId.Id == studentId
                                select new new_studentgoals
                                {
                                    new_studentgoalsId = sg.new_studentgoalsId,
                                    new_name = sg.new_name,
                                    new_classgoalId = sg.new_classgoalId,
                                    new_Benefit = sg.new_Benefit,
                                    new_IsComplete = sg.new_IsComplete
                                });

            return studentGoals;
        }

        public List<new_studentgoals> GetClassSelectedGoals(Guid classId)
        {

            var studentGoals = (from sg in GrayMattersContext.new_studentgoalsSet
                                where sg.new_ClassId.Id == classId
                                select new new_studentgoals
                                {
                                    new_StudentId = sg.new_StudentId,
                                    new_studentgoalsId = sg.new_studentgoalsId,
                                    new_name = sg.new_name,
                                    new_classgoalId = sg.new_classgoalId,
                                    new_Benefit = sg.new_Benefit,
                                    new_IsComplete = sg.new_IsComplete
                                }).ToList();

            return studentGoals;
        }

        public IQueryable<Activity> GetGoalActivities(Guid goalId)
        {
            var equip = (from ga in GrayMattersContext.new_goalactivitySet
                         where ga.new_new_classgoal_new_goalactivity.new_classgoalId == goalId
                         orderby ga.new_name
                         select new Activity
                         {
                             id = (Guid)ga.new_goalactivityId,
                             name = ga.new_Header,
                             sortorder = ga.new_name
                         }
                         );

            return equip;
        }

        public IQueryable<new_goalactivity> GetStudentGoalActivities(Guid classId, Guid studentId, Guid goalId)
        {

            var existingActivities = (from ga in GrayMattersContext.new_goalactivitySet
                                      join sgga in GrayMattersContext.new_studentgoals_goalactivitySet on ga.new_goalactivityId equals sgga.new_goalactivityid
                                      join sg in GrayMattersContext.new_studentgoalsSet on sgga.new_studentgoalsid equals sg.new_studentgoalsId
                                      where sg.new_ClassId.Id == classId && sg.new_StudentId.Id == studentId && sg.new_classgoalId.Id == goalId
                                      orderby ga.new_name
                                      select new new_goalactivity()
                                      {
                                          new_goalactivityId = ga.new_goalactivityId,
                                          new_Header = ga.new_Header
                                      });
            return existingActivities;
        }


        public IQueryable<SubActivity> GetSubActivities(Guid activityId)
        {
            var equip = (from sa in GrayMattersContext.new_subactivitySet
                         where sa.new_goalactivity_subactivity.new_goalactivityId == activityId
                         orderby sa.new_name
                         select new SubActivity()
                         {
                             id = (Guid)sa.new_subactivityId,
                             name = sa.new_Description
                         }
                         );

            return equip;
        }


        public IQueryable<new_subactivity> GetStudentGoalSubActivities(Guid classId, Guid studentId, Guid goalId)
        {

            var existingActivities = (from gsa in GrayMattersContext.new_subactivitySet
                                      join sgga in GrayMattersContext.new_studentgoals_subactivitySet on gsa.new_subactivityId equals sgga.new_subactivityid
                                      join sg in GrayMattersContext.new_studentgoalsSet on sgga.new_studentgoalsid equals sg.new_studentgoalsId
                                      where sg.new_ClassId.Id == classId && sg.new_StudentId.Id == studentId && sg.new_classgoalId.Id == goalId
                                      orderby gsa.new_name
                                      select new new_subactivity()
                                      {
                                          new_GoalActivityId = gsa.new_GoalActivityId,
                                          new_subactivityId = gsa.new_subactivityId,
                                          new_Description = gsa.new_Description
                                      });
            return existingActivities;
        }
        
        
        public List<StudentUpdatePeriod> GetClassStudentUpdatePeriods(Guid classId)
        {
            // Get All Update Periods for the class
            var myUpdates = (from cup in GrayMattersContext.new_classupdateperiodSet
                             join cl in GrayMattersContext.new_classSet on cup.new_ClassId.Id equals cl.new_classId
                             where cup.new_ClassId.Id == classId && cup.statecode == new_classupdateperiodState.Active
                             orderby cup.new_End ascending
                             select new StudentUpdatePeriod()
                             {
                                 ClassUpdatePeriodId = cup.new_classupdateperiodId.Value,
                                 startDate = cup.new_Start.Value,
                                 endDate = cup.new_End.Value,
                                 ImpactQuestion = cup.new_ImpactQuestionId,
                                 DefaultQuestions = cl.new_UpdateQuestionsId,
                                 PeriodId = cup.new_classupdateperiodId.Value,
                                 StudentName = cup.new_name
                             }).ToList();
            
            return myUpdates;
        }

        public List<StudentUpdatePeriod> GetDashboardClassStudentUpdatePeriods(Guid classId)
        {
            // Get All Update Periods for the class
            var myUpdates = (from cup in GrayMattersContext.new_classupdateperiodSet
                             join cl in GrayMattersContext.new_classSet on cup.new_ClassId.Id equals cl.new_classId
                             where cup.new_ClassId.Id == classId && cup.statecode == new_classupdateperiodState.Active
                             orderby cup.new_End ascending
                             select new StudentUpdatePeriod()
                             {
                                 ClassUpdatePeriodId = cup.new_classupdateperiodId.Value,
                                 startDate = cup.new_Start.Value,
                                 endDate = cup.new_End.Value,
                                 ImpactQuestion = cup.new_ImpactQuestionId,
                                 DefaultQuestions = cl.new_UpdateQuestionsId,
                                 PeriodId = cup.new_classupdateperiodId.Value,
                                 StudentName = cup.new_name
                             }).ToList();

            return myUpdates;
        }


        public List<new_classupdateperiod> GetClassUpdatePeriods(Guid classId)
        {
            // Get All Update Periods for the class
            var myUpdates = (from cup in GrayMattersContext.new_classupdateperiodSet
                             where cup.new_ClassId.Id == classId && cup.statecode == new_classupdateperiodState.Active
                             orderby cup.new_End ascending
                             select cup).ToList();
            return myUpdates;
        }

      
        public List<StudentUpdatePeriod> GetAllUpdatePeriods(Guid studentId, Guid classId)
        {
            // Get All Update Periods for the class

            var myUpdates = (from cup in GrayMattersContext.new_classupdateperiodSet
                             join cl in GrayMattersContext.new_classSet on cup.new_ClassId.Id equals cl.new_classId
                             where cup.new_ClassId.Id == classId && cup.statecode == new_classupdateperiodState.Active
                             orderby cup.new_End ascending
                             select new StudentUpdatePeriod()
                             {
                                 ClassUpdatePeriodId = cup.new_classupdateperiodId.Value,
                                 startDate = cup.new_Start.Value,
                                 endDate = cup.new_End.Value,
                                 ImpactQuestion = cup.new_ImpactQuestionId,
                                 DefaultQuestions = cl.new_UpdateQuestionsId,
                                 PeriodId = cup.new_classupdateperiodId.Value
                             }).ToList();


            var question1 = "";
            var question2 = "";

            // Get Update Questions from default or the questions specified.
            if (myUpdates[0].DefaultQuestions != null)
            {
                var questions = (from cq in GrayMattersContext.new_updatequestionsSet
                                 where cq.new_updatequestionsId == myUpdates[0].DefaultQuestions.Id
                                 select cq).FirstOrDefault();

                question1 = questions.new_Question1;
                question2 = questions.new_Question2;
            }
            else
            {
                var questions = (from cq in GrayMattersContext.new_updatequestionsSet
                                 where cq.new_Default == true
                                 select cq).FirstOrDefault();

                question1 = questions.new_Question1;
                question2 = questions.new_Question2;
            }

            // Fill all needed values for the update
            foreach (StudentUpdatePeriod studentUpdatePeriod in myUpdates)
            {
                studentUpdatePeriod.ClassId = classId;
                studentUpdatePeriod.StudentId = studentId;
                studentUpdatePeriod.GoalQuestion1 = question1;
                studentUpdatePeriod.GoalQuestion2 = question2;

                // Get the impact question for each UpdatePeriod
                if (studentUpdatePeriod.ImpactQuestion != null)
                {
                    studentUpdatePeriod.PeriodImpactQuestion = (from iq in GrayMattersContext.new_impactquestionSet
                                                                where iq.new_impactquestionId == studentUpdatePeriod.ImpactQuestion.Id
                                                                select iq).FirstOrDefault().new_question;
                }
                else
                {
                    studentUpdatePeriod.ImpactQuestion = null;
                }

                // Get all Goals the student has selected
                var studentGoals = (from sg in GrayMattersContext.new_studentgoalsSet
                                    join g in GrayMattersContext.new_classgoalSet on sg.new_classgoalId.Id equals g.new_classgoalId
                                    where sg.new_ClassId.Id == classId && sg.new_StudentId.Id == studentId
                                   // && sg.new_IsComplete != true
                                    select new new_studentgoals
                                    {
                                        new_studentgoalsId = sg.new_studentgoalsId,
                                        new_name = g.new_name,
                                        new_classgoalId = sg.new_classgoalId,
                                        new_Benefit = sg.new_Benefit
                                    }).ToList();

                foreach (new_studentgoals newStudentgoal in studentGoals)
                {
                    var existingActivities = (from ga in GrayMattersContext.new_goalactivitySet
                                              join sgga in GrayMattersContext.new_studentgoals_goalactivitySet on ga.new_goalactivityId equals sgga.new_goalactivityid
                                              join sg in GrayMattersContext.new_studentgoalsSet on sgga.new_studentgoalsid equals sg.new_studentgoalsId
                                              where sg.new_ClassId.Id == classId && sg.new_StudentId.Id == studentId && sg.new_classgoalId.Id == newStudentgoal.new_classgoalId.Id
                                              select new new_goalactivity()
                                              {
                                                  new_goalactivityId = ga.new_goalactivityId,
                                                  new_Header = ga.new_Header
                                              });


                    var newGoal = new GoalAnswers
                    {
                        Payoff = newStudentgoal.new_Benefit,
                        GoalName = newStudentgoal.new_name,
                        GoalId = newStudentgoal.new_classgoalId.Id,
                        StudentGoalId = newStudentgoal.new_studentgoalsId.Value
                    };


                    if (existingActivities.ToList().Count == 0)
                    {
                        var existingSubActivities = (from gsa in GrayMattersContext.new_subactivitySet
                                                     join sgga in GrayMattersContext.new_studentgoals_subactivitySet on
                                                         gsa.new_subactivityId equals sgga.new_subactivityid
                                                     join sg in GrayMattersContext.new_studentgoalsSet on
                                                         sgga.new_studentgoalsid equals sg.new_studentgoalsId
                                                     where
                                                         sg.new_ClassId.Id == classId &&
                                                         sg.new_StudentId.Id == studentId &&
                                                         sg.new_classgoalId.Id == newStudentgoal.new_classgoalId.Id
                                                     select new new_subactivity()
                                                                {
                                                                    new_GoalActivityId = gsa.new_GoalActivityId,
                                                                    new_subactivityId = gsa.new_subactivityId,
                                                                    new_Description = gsa.new_Description
                                                                });

                        foreach (new_subactivity existingActivity in existingSubActivities)
                        {
                            var newActivity = new Activities
                                                  {
                                                      Activityid = existingActivity.new_subactivityId.Value,
                                                      Text = existingActivity.new_Description,
                                                      IsChecked = false
                                                  };

                            newGoal.MyActivities.Add(newActivity);
                        }
                    }
                    else
                    {
                        foreach (new_goalactivity existingActivity in existingActivities)
                        {
                            var newActivity = new Activities
                                                  {
                                                      Activityid = existingActivity.new_goalactivityId.Value,
                                                      Text = existingActivity.new_Header,
                                                      IsChecked = false
                                                  };

                            newGoal.MyActivities.Add(newActivity);
                        }
                    }
                    studentUpdatePeriod.PeriodGoalAnswers.Add(newGoal);
                }

            }
            return myUpdates;


        }


        public StudentUpdatePeriod GetMyUpdatePeriod(Guid studentId, Guid updatePeriodId, StudentUpdatePeriod updatePeriod)
        {
            var myUpdate = (from sup in GrayMattersContext.new_studentupdateperiodSet
                            where sup.new_StudentId.Id == studentId && sup.new_UpdatePeriodId.Id == updatePeriodId
                            select sup).FirstOrDefault();

            if (myUpdate == null) return updatePeriod;

            updatePeriod.ImpactAnswer = myUpdate.new_ImpactAnswer;
            updatePeriod.FeedbackRequest = myUpdate.new_FeedbackRequest;

            var myAnswers = (from ans in GrayMattersContext.new_answerSet
                             where ans.new_UpdatePeriodId.Id == myUpdate.new_studentupdateperiodId
                             select ans);

            foreach (new_answer newAnswer in myAnswers)
            {
                foreach (GoalAnswers answer in updatePeriod.PeriodGoalAnswers)
                {
                    if (newAnswer.new_GoalId.Id == answer.GoalId)
                    {
                        answer.ProgressValue = newAnswer.new_progress.Value;
                        answer.Answer1 = newAnswer.new_Answer;
                        answer.Answer2 = newAnswer.new_Answer2;

                        if (!string.IsNullOrEmpty(newAnswer.new_ActivitiesUsed))
                        {
                            var activityids = newAnswer.new_ActivitiesUsed.Split(';');

                            foreach (string activityid in activityids)
                            {
                                foreach (Activities act in answer.MyActivities)
                                {
                                    try
                                    {
                                        if (act.Activityid == new Guid(activityid))
                                        {
                                            act.IsChecked = true;
                                        }
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                        }

                    }
                }
            }


            return updatePeriod;


        }


        public List<StudentUpdatePeriod> GetMyDashboardUpdatePeriods(Guid classId)
        {
            var studentUpdates = new List<StudentUpdatePeriod>();

            var classUpdates = (from sup in GrayMattersContext.new_studentupdateperiodSet
                            join up in GrayMattersContext.new_classupdateperiodSet on sup.new_UpdatePeriodId.Id equals up.new_classupdateperiodId
                            where up.new_ClassId.Id == classId && up.statecode == new_classupdateperiodState.Active
                            select sup).ToList();

            if (classUpdates.Count == 0) return studentUpdates;

            foreach (new_studentupdateperiod classUpdate in classUpdates)
            {
                if (classUpdate.new_StudentId == null) continue;

                var thisStudentUpdate = new StudentUpdatePeriod
                                            {
                                                StudentId = classUpdate.new_StudentId.Id,
                                                ClassUpdatePeriodId = classUpdate.new_UpdatePeriodId.Id,
                                                ImpactAnswer = classUpdate.new_ImpactAnswer,
                                                FeedbackRequest = classUpdate.new_FeedbackRequest
                                               };

                var myImpactQuestion = (from i in GrayMattersContext.new_impactquestionSet
                                        join up in GrayMattersContext.new_classupdateperiodSet on i.new_impactquestionId
                                            equals up.new_ImpactQuestionId.Id
                                        where up.new_classupdateperiodId == classUpdate.new_UpdatePeriodId.Id
                                        select i).FirstOrDefault();

                if( myImpactQuestion != null) thisStudentUpdate.PeriodImpactQuestion = myImpactQuestion.new_question;

                var myAnswers = (from ans in GrayMattersContext.new_answerSet
                                 where ans.new_UpdatePeriodId.Id == classUpdate.new_studentupdateperiodId
                                 select ans);

                //TODO:  Note need to handle any questions picked.
                 var questions = (from cq in GrayMattersContext.new_updatequestionsSet
                                     select cq).FirstOrDefault();

                 thisStudentUpdate.GoalQuestion1 = questions.new_Question1;
                 thisStudentUpdate.GoalQuestion2 = questions.new_Question2;
                
                
                foreach (new_answer newAnswer in myAnswers)
                {
                    var thisAnswer = new GoalAnswers
                    {
                        GoalName = newAnswer.new_name,
                        ProgressValue = newAnswer.new_progress.Value,
                        Answer1 = newAnswer.new_Answer,
                        Answer2 = newAnswer.new_Answer2
                    };
                    
                    thisStudentUpdate.PeriodGoalAnswers.Add(thisAnswer);
                }

                studentUpdates.Add(thisStudentUpdate);
            }

            return studentUpdates;


        }



        public List<new_coaching> GetCoachFeedback(Guid updatePeriodid)
        {
            var studentUpdates = (from sup in GrayMattersContext.new_coachingSet
                               where sup.new_ClassUpdatePeriodId.Id == updatePeriodid &&
                               sup.statecode == new_coachingState.Active
                               orderby  sup.ModifiedOn ascending 
                                  select sup).ToList();

            return studentUpdates;
        }

        public List<new_coaching> GetCoachFeedbackbyStudent(Guid classId, Guid studentId)
        {
            var studentUpdates = (from sup in GrayMattersContext.new_coachingSet
                                  where sup.new_StudentId.Id == studentId &&
                               sup.statecode == new_coachingState.Active
                                  orderby sup.ModifiedOn ascending 
                                  select sup).ToList();

            return studentUpdates;
        }
        

        public List<new_studentupdateperiod> GetStudentUpdates(Guid updatePeriodid)
        {
            var studentUpdates = (from sup in GrayMattersContext.new_studentupdateperiodSet
                                  where sup.new_UpdatePeriodId.Id == updatePeriodid &&  sup.statecode == new_studentupdateperiodState.Active
                                  select sup).ToList();

            return studentUpdates;
        }

        public List<new_studentupdateperiod> GetStudentUpdatesPeriods(Guid classId)
        {
            var studentUpdates = (from sup in GrayMattersContext.new_studentupdateperiodSet
                                  join up in GrayMattersContext.new_classupdateperiodSet on sup.new_UpdatePeriodId.Id equals up.new_classupdateperiodId
                                  where up.new_ClassId.Id == classId && up.statecode == new_classupdateperiodState.Active
                                  select sup).ToList();

            return studentUpdates;
        }

        public List<new_coaching> GetCoachFeedbackbyClass(Guid classId)
        {
            var studentUpdates = (from sup in GrayMattersContext.new_coachingSet
                                  join up in GrayMattersContext.new_classupdateperiodSet on sup.new_ClassUpdatePeriodId.Id equals up.new_classupdateperiodId
                                  where up.new_ClassId.Id == classId && up.statecode == new_classupdateperiodState.Active
                                  orderby sup.ModifiedOn ascending 
                                  select sup).ToList();

            return studentUpdates;
        }


        public void SaveUpdate(StudentUpdatePeriod myUpdate)
        {
            var foundUpdate = (from sup in GrayMattersContext.new_studentupdateperiodSet
                               where sup.new_StudentId.Id == myUpdate.StudentId && sup.new_UpdatePeriodId.Id == myUpdate.PeriodId
                               select sup).FirstOrDefault();

            var updateName = string.Format("{2} {0} - {1}", myUpdate.startDate.ToString("MM/dd"), myUpdate.endDate.ToString("MM/dd"), myUpdate.StudentName);

            if (foundUpdate != null)
            {
                foundUpdate.new_name = updateName;
                foundUpdate.new_ImpactAnswer = myUpdate.ImpactAnswer;
                foundUpdate.new_FeedbackRequest = myUpdate.FeedbackRequest;

                GrayMattersContext.UpdateObject(foundUpdate);

                   var myAnswers = (from ans in GrayMattersContext.new_answerSet
                             where ans.new_UpdatePeriodId.Id == foundUpdate.new_studentupdateperiodId
                             select ans);

                   foreach (new_answer newAnswer in myAnswers)
                   {
                       foreach (GoalAnswers answer in myUpdate.PeriodGoalAnswers)
                       {
                           if( answer.GoalId == newAnswer.new_GoalId.Id)
                           {
                               newAnswer.new_Answer2 = answer.Answer2;
                               newAnswer.new_Answer = answer.Answer1;
                               newAnswer.new_progress = new OptionSetValue(answer.ProgressValue);

                               var sb = new StringBuilder(256);
                               foreach (Activities act in answer.MyActivities)
                               {
                                   if (act.IsChecked) sb.AppendFormat("{0};", act.Activityid);
                               }
                               newAnswer.new_ActivitiesUsed = sb.ToString();

                               GrayMattersContext.UpdateObject(newAnswer);
                           }
                       }
                   }

                GrayMattersContext.SaveChanges();
            }
            else
            {
                var updateId = Guid.NewGuid();
                var newStudentUpdatePeriod = new new_studentupdateperiod()
                                            {
                                                new_studentupdateperiodId = updateId,
                                                new_StudentId = new EntityReference(Contact.EntityLogicalName, myUpdate.StudentId),
                                                new_ImpactAnswer = myUpdate.ImpactAnswer,
                                                new_UpdatePeriodId = new EntityReference(new_classupdateperiod.EntityLogicalName, myUpdate.PeriodId),
                                                new_name = updateName,
                                                new_FeedbackRequest = myUpdate.FeedbackRequest
                                            };

                newStudentUpdatePeriod.RelatedEntities.Clear();
                GrayMattersContext.AddObject(newStudentUpdatePeriod);

                // Add new UpdatePeriod);

                foreach (GoalAnswers goalAnswers in myUpdate.PeriodGoalAnswers)
                {
                    var answerName = string.Format("{0} {1}", myUpdate.StudentName, goalAnswers.GoalName);
                    var newStudentUpdate = new new_answer()
                                               {

                                                   new_Answer2 = goalAnswers.Answer2,
                                                   new_Answer = goalAnswers.Answer1,
                                                   new_GoalId =
                                                       new EntityReference(new_classgoal.EntityLogicalName,
                                                                           goalAnswers.GoalId),
                                                   new_StudentId =
                                                       new EntityReference(Contact.EntityLogicalName, myUpdate.StudentId),
                                                   new_progress = new OptionSetValue(goalAnswers.ProgressValue),
                                                   new_UpdatePeriodId =
                                                       new EntityReference(new_studentupdateperiod.EntityLogicalName,
                                                                           updateId),
                                                   new_name = answerName
                                               };

                    var sb = new StringBuilder(256);
                    foreach (Activities act in goalAnswers.MyActivities)
                    {
                        if (act.IsChecked) sb.AppendFormat("{0};", act.Activityid);
                    }
                    newStudentUpdate.new_ActivitiesUsed = sb.ToString();

                    newStudentUpdate.RelatedEntities.Clear();
                    GrayMattersContext.AddObject(newStudentUpdate);

                    if (goalAnswers.ProgressValue == 4) // Complete
                    {
                        //Get and update Goal to equal Complete
                        var completedStudentGoal = (from sg in GrayMattersContext.new_studentgoalsSet
                                                    where sg.new_studentgoalsId == goalAnswers.StudentGoalId
                                                    select new new_studentgoals
                                                               {
                                                                   new_studentgoalsId = sg.new_studentgoalsId,
                                                                   new_name = sg.new_name,
                                                                   new_classgoalId = sg.new_classgoalId,
                                                                   new_IsComplete = sg.new_IsComplete
                                                               }).FirstOrDefault();

                        completedStudentGoal.new_IsComplete = true;

                        GrayMattersContext.UpdateObject(completedStudentGoal);
                    }

                 

                        var existingclassstatus = (from cs in GrayMattersContext.new_classstatusSet
                                                   where
                                                       cs.new_StudentId.Id == myUpdate.StudentId &&
                                                       cs.new_ClassId.Id == myUpdate.ClassId
                                                   select cs).FirstOrDefault();

                        if (existingclassstatus != null)
                        {
                            existingclassstatus.new_LastUpdate = DateTime.Now;
                            GrayMattersContext.UpdateObject(existingclassstatus);
                        }
                  
                   

                    GrayMattersContext.SaveChanges();
                }
            }
        }

        public List<trk_assettracking> GetAssetTouches(Guid scheduledTouchId)
        {
            var tracking = (from sup in GrayMattersContext.trk_assettrackingSet
                            where sup.trk_ScheduledTouchId.Id == scheduledTouchId
                           orderby sup.trk_AssetViewedId,sup.CreatedOn ascending
                           select sup).ToList();
            return tracking;
        }

        public List<new_scheduledtouch> GetScheduledTouches(bool isPM)
        {
            var touches = (from sup in GrayMattersContext.new_scheduledtouchSet
                           where  sup.new_Sent != true
                                  && sup.new_When == isPM
                                  orderby sup.new_TriggerDate descending 
                                  select sup).ToList();

   
            return touches;
        }

        public List<new_scheduledtouch> GetScheduledTouches(Guid classId)
        {
            var touches = (from t in GrayMattersContext.new_scheduledtouchSet
                           where t.new_ClassId.Id == classId
                           select t).ToList().OrderBy(t => t.new_TriggerDate).ToList();

           return touches;
        }


        public void MoveScheduledTouches(Guid classId, int days)
        {
            var touches = (from t in GrayMattersContext.new_scheduledtouchSet
                           where t.new_ClassId.Id == classId
                           select t).ToList().OrderBy(t => t.new_TriggerDate).ToList();

            foreach (new_scheduledtouch newScheduledtouch in touches)
            {
                newScheduledtouch.new_TriggerDate = newScheduledtouch.new_TriggerDate.Value.AddDays(days);
                GrayMattersContext.UpdateObject(newScheduledtouch);
            }

            GrayMattersContext.SaveChanges();
        }
        //private static int CompareTouchesByDate(new_scheduledtouch x, new_scheduledtouch y)
        //{
        //    if (x.new_TriggerDate == y.new_TriggerDate) return 0;
        //    if (x.new_TriggerDate == null) return -1;
        //    if (y.new_TriggerDate == null) return 1;

        //    if (x.new_TriggerDate.Value < y.new_TriggerDate.Value)
        //        return -1;

        //    if (x.new_TriggerDate.Value > y.new_TriggerDate.Value)
        //        return 1;

        //    return 0;
        //}



        public void SetTouchToSent(Guid id)
        {
            var touch = (from sup in GrayMattersContext.new_scheduledtouchSet
                         where sup.Id == id
                         select sup).FirstOrDefault();

            touch.new_Sent = true;

            GrayMattersContext.UpdateObject(touch);
            GrayMattersContext.SaveChanges();

        }

        public new_scheduledtouch GetScheduledTouch(Guid id)
        {
            var touch = (from sup in GrayMattersContext.new_scheduledtouchSet
                         where sup.Id == id
                         select sup).FirstOrDefault();
            return touch;
        }

        public List<new_touchtemplate> GetTouchTemplates()
        {
            var touches = (from sup in GrayMattersContext.new_touchtemplateSet
                           orderby sup.new_name ascending
                           select sup
                          ).ToList();
            return touches;
        }

        public new_touchtemplate GetScheduledTemplate(Guid id)
        {
            var touch = (from sup in GrayMattersContext.new_touchtemplateSet
                         where sup.Id == id
                         select sup).FirstOrDefault();
            return touch;
        }


        public List<Email> GetEmailbyClass(Guid classId)
        {
            var classEmail = (from e in GrayMattersContext.EmailSet
                              where e.RegardingObjectId.Id == classId
                              select e).ToList();

            return classEmail;
        }

        public void AddNewTemplate(new_touchtemplate newTemplate)
        {
           
            newTemplate.RelatedEntities.Clear();
            GrayMattersContext.AddObject(newTemplate);
            GrayMattersContext.SaveChanges();
        }

        public void AddNewTouch(new_scheduledtouch newTouch)
        {
            TimeZone localZone = TimeZone.CurrentTimeZone;
            var localDate = newTouch.new_TriggerDate.Value;
            var currentUTC = (localZone.ToUniversalTime(localDate)).AddHours(8);
            newTouch.new_TriggerDate = currentUTC;

            newTouch.RelatedEntities.Clear();
            GrayMattersContext.AddObject(newTouch);
            GrayMattersContext.SaveChanges();
        }

        public void AddNewClass(new_class newClass)
        {
            newClass.new_ClassDate = AdjustHours(newClass.new_ClassDate.Value);
            newClass.new_StartDate = AdjustHours(newClass.new_StartDate.Value);

            newClass.RelatedEntities.Clear();
            GrayMattersContext.AddObject(newClass);
            GrayMattersContext.SaveChanges();
        }

        public void AddNewUpdatePeriod(new_classupdateperiod newupdatePeriod)
        {
            newupdatePeriod.new_Start = AdjustHours(newupdatePeriod.new_Start.Value);
            newupdatePeriod.new_End = AdjustHours(newupdatePeriod.new_End.Value);

            newupdatePeriod.RelatedEntities.Clear();
            GrayMattersContext.AddObject(newupdatePeriod);
            GrayMattersContext.SaveChanges();
        }

        public DateTime AdjustHours(DateTime inDate)
        {
            TimeZone localZone = TimeZone.CurrentTimeZone;
            var localDate = inDate;
            return (localZone.ToUniversalTime(localDate)).AddHours(8);
        }

        public void UpdateTemplate(new_touchtemplate template)
        {
            //OrgService.Update(template);
            var updatedTemplate = (from cs in GrayMattersContext.new_touchtemplateSet
                                   where cs.new_touchtemplateId == template.new_touchtemplateId
                                   select cs).FirstOrDefault();

            updatedTemplate.new_Subject = template.new_Subject;
            updatedTemplate.new_EmailBody = template.new_EmailBody;
            updatedTemplate.new_name = template.new_name;

            GrayMattersContext.UpdateObject(updatedTemplate);
            GrayMattersContext.SaveChanges();
        }

        public void UpdateTouch(new_scheduledtouch touch)
        {
            //OrgService.Update(touch);
            var updatedTouch = (from cs in GrayMattersContext.new_scheduledtouchSet
                                where cs.new_scheduledtouchId == touch.new_scheduledtouchId
                                select cs).FirstOrDefault();

            updatedTouch.new_Subject = touch.new_Subject;
            updatedTouch.new_EmailBody = touch.new_EmailBody;
            updatedTouch.new_name = touch.new_name;

            updatedTouch.new_To = touch.new_To;
            updatedTouch.new_CCAllemail = touch.new_CCAllemail;
            updatedTouch.new_SendRepresentativeCopy = touch.new_SendRepresentativeCopy;
            updatedTouch.new_Sent = touch.new_Sent;

            // Since this is running in the Western Time Zone and CRM is saving based on UTC
            // This guarantees that the date shown is correct.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            var localDate = touch.new_TriggerDate.Value;
            var currentUTC = (localZone.ToUniversalTime(localDate)).AddHours(8);
            updatedTouch.new_TriggerDate = currentUTC;
            updatedTouch.new_When = touch.new_When;

            GrayMattersContext.UpdateObject(updatedTouch);
            GrayMattersContext.SaveChanges();
        }

        public void UpdateTouchTrigger(Guid touchId, Boolean isSent, DateTime triggerDate)
        {
            //OrgService.Update(touch);
            var updatedTouch = (from cs in GrayMattersContext.new_scheduledtouchSet
                                where cs.new_scheduledtouchId == touchId
                                select cs).FirstOrDefault();
            
            updatedTouch.new_Sent = isSent;

            // Since this is running in the Western Time Zone and CRM is saving based on UTC
            // This guarantees that the date shown is correct.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            var localDate = triggerDate;
            var currentUTC = (localZone.ToUniversalTime(localDate)).AddHours(8);
            updatedTouch.new_TriggerDate = currentUTC;

            GrayMattersContext.UpdateObject(updatedTouch);
            GrayMattersContext.SaveChanges();
        }

        public void UpdateUpdatePeriod(Guid updateId, DateTime startDate, DateTime endDate)
        {
            //OrgService.Update(touch);
            var period = (from cs in GrayMattersContext.new_classupdateperiodSet
                                where cs.new_classupdateperiodId == updateId
                                select cs).FirstOrDefault();

            period.new_Start = AdjustHours(startDate);
            period.new_End = AdjustHours(endDate);

            GrayMattersContext.UpdateObject(period);
            GrayMattersContext.SaveChanges();
        }

        public SystemUser GetUser(string emailaddress)
        {
            var student = (from c in GrayMattersContext.SystemUserSet
                           where c.InternalEMailAddress == emailaddress
                           select c).FirstOrDefault();

            return student;
        }

        public string GetMimeType(string extension)
        {
            string mimeType = "";
            switch (extension.ToLower())
            {
                case ".3dm": mimeType = "x-world/x-3dmf"; break;
                case ".3dmf": mimeType = "x-world/x-3dmf"; break;
                case ".a": mimeType = "application/octet-stream"; break;
                case ".aab": mimeType = "application/x-authorware-bin"; break;
                case ".aam": mimeType = "application/x-authorware-map"; break;
                case ".aas": mimeType = "application/x-authorware-seg"; break;
                case ".abc": mimeType = "text/vnd.abc"; break;
                case ".acgi": mimeType = "text/html"; break;
                case ".afl": mimeType = "video/animaflex"; break;
                case ".ai": mimeType = "application/postscript"; break;
                case ".aif": mimeType = "audio/aiff"; break;
                case ".aifc": mimeType = "audio/aiff"; break;
                case ".aiff": mimeType = "audio/aiff"; break;
                case ".aim": mimeType = "application/x-aim"; break;
                case ".aip": mimeType = "text/x-audiosoft-intra"; break;
                case ".ani": mimeType = "application/x-navi-animation"; break;
                case ".aos": mimeType = "application/x-nokia-9000-communicator-add-on-software"; break;
                case ".aps": mimeType = "application/mime"; break;
                case ".arc": mimeType = "application/octet-stream"; break;
                case ".arj": mimeType = "application/arj"; break;
                case ".art": mimeType = "image/x-jg"; break;
                case ".asf": mimeType = "video/x-ms-asf"; break;
                case ".asm": mimeType = "text/x-asm"; break;
                case ".asp": mimeType = "text/asp"; break;
                case ".asx": mimeType = "video/x-ms-asf"; break;
                case ".au": mimeType = "audio/basic"; break;
                case ".avi": mimeType = "video/avi"; break;
                case ".avs": mimeType = "video/avs-video"; break;
                case ".bcpio": mimeType = "application/x-bcpio"; break;
                case ".bin": mimeType = "application/octet-stream"; break;
                case ".bm": mimeType = "image/bmp"; break;
                case ".bmp": mimeType = "image/bmp"; break;
                case ".boo": mimeType = "application/book"; break;
                case ".book": mimeType = "application/book"; break;
                case ".boz": mimeType = "application/x-bzip2"; break;
                case ".bsh": mimeType = "application/x-bsh"; break;
                case ".bz": mimeType = "application/x-bzip"; break;
                case ".bz2": mimeType = "application/x-bzip2"; break;
                case ".c": mimeType = "text/plain"; break;
                case ".c++": mimeType = "text/plain"; break;
                case ".cat": mimeType = "application/vnd.ms-pki.seccat"; break;
                case ".cc": mimeType = "text/plain"; break;
                case ".ccad": mimeType = "application/clariscad"; break;
                case ".cco": mimeType = "application/x-cocoa"; break;
                case ".cdf": mimeType = "application/cdf"; break;
                case ".cer": mimeType = "application/pkix-cert"; break;
                case ".cha": mimeType = "application/x-chat"; break;
                case ".chat": mimeType = "application/x-chat"; break;
                case ".class": mimeType = "application/java"; break;
                case ".com": mimeType = "application/octet-stream"; break;
                case ".conf": mimeType = "text/plain"; break;
                case ".cpio": mimeType = "application/x-cpio"; break;
                case ".cpp": mimeType = "text/x-c"; break;
                case ".cpt": mimeType = "application/x-cpt"; break;
                case ".crl": mimeType = "application/pkcs-crl"; break;
                case ".crt": mimeType = "application/pkix-cert"; break;
                case ".csh": mimeType = "application/x-csh"; break;
                case ".css": mimeType = "text/css"; break;
                case ".cxx": mimeType = "text/plain"; break;
                case ".dcr": mimeType = "application/x-director"; break;
                case ".deepv": mimeType = "application/x-deepv"; break;
                case ".def": mimeType = "text/plain"; break;
                case ".der": mimeType = "application/x-x509-ca-cert"; break;
                case ".dif": mimeType = "video/x-dv"; break;
                case ".dir": mimeType = "application/x-director"; break;
                case ".dl": mimeType = "video/dl"; break;
                case ".doc": mimeType = "application/msword"; break;
                case ".dot": mimeType = "application/msword"; break;
                case ".dp": mimeType = "application/commonground"; break;
                case ".drw": mimeType = "application/drafting"; break;
                case ".dump": mimeType = "application/octet-stream"; break;
                case ".dv": mimeType = "video/x-dv"; break;
                case ".dvi": mimeType = "application/x-dvi"; break;
                case ".dwf": mimeType = "model/vnd.dwf"; break;
                case ".dwg": mimeType = "image/vnd.dwg"; break;
                case ".dxf": mimeType = "image/vnd.dwg"; break;
                case ".dxr": mimeType = "application/x-director"; break;
                case ".el": mimeType = "text/x-script.elisp"; break;
                case ".elc": mimeType = "application/x-elc"; break;
                case ".env": mimeType = "application/x-envoy"; break;
                case ".eps": mimeType = "application/postscript"; break;
                case ".es": mimeType = "application/x-esrehber"; break;
                case ".etx": mimeType = "text/x-setext"; break;
                case ".evy": mimeType = "application/envoy"; break;
                case ".exe": mimeType = "application/octet-stream"; break;
                case ".f": mimeType = "text/plain"; break;
                case ".f77": mimeType = "text/x-fortran"; break;
                case ".f90": mimeType = "text/plain"; break;
                case ".fdf": mimeType = "application/vnd.fdf"; break;
                case ".fif": mimeType = "image/fif"; break;
                case ".fli": mimeType = "video/fli"; break;
                case ".flo": mimeType = "image/florian"; break;
                case ".flx": mimeType = "text/vnd.fmi.flexstor"; break;
                case ".fmf": mimeType = "video/x-atomic3d-feature"; break;
                case ".for": mimeType = "text/x-fortran"; break;
                case ".fpx": mimeType = "image/vnd.fpx"; break;
                case ".frl": mimeType = "application/freeloader"; break;
                case ".funk": mimeType = "audio/make"; break;
                case ".g": mimeType = "text/plain"; break;
                case ".g3": mimeType = "image/g3fax"; break;
                case ".gif": mimeType = "image/gif"; break;
                case ".gl": mimeType = "video/gl"; break;
                case ".gsd": mimeType = "audio/x-gsm"; break;
                case ".gsm": mimeType = "audio/x-gsm"; break;
                case ".gsp": mimeType = "application/x-gsp"; break;
                case ".gss": mimeType = "application/x-gss"; break;
                case ".gtar": mimeType = "application/x-gtar"; break;
                case ".gz": mimeType = "application/x-gzip"; break;
                case ".gzip": mimeType = "application/x-gzip"; break;
                case ".h": mimeType = "text/plain"; break;
                case ".hdf": mimeType = "application/x-hdf"; break;
                case ".help": mimeType = "application/x-helpfile"; break;
                case ".hgl": mimeType = "application/vnd.hp-hpgl"; break;
                case ".hh": mimeType = "text/plain"; break;
                case ".hlb": mimeType = "text/x-script"; break;
                case ".hlp": mimeType = "application/hlp"; break;
                case ".hpg": mimeType = "application/vnd.hp-hpgl"; break;
                case ".hpgl": mimeType = "application/vnd.hp-hpgl"; break;
                case ".hqx": mimeType = "application/binhex"; break;
                case ".hta": mimeType = "application/hta"; break;
                case ".htc": mimeType = "text/x-component"; break;
                case ".htm": mimeType = "text/html"; break;
                case ".html": mimeType = "text/html"; break;
                case ".htmls": mimeType = "text/html"; break;
                case ".htt": mimeType = "text/webviewhtml"; break;
                case ".htx": mimeType = "text/html"; break;
                case ".ice": mimeType = "x-conference/x-cooltalk"; break;
                case ".ico": mimeType = "image/x-icon"; break;
                case ".idc": mimeType = "text/plain"; break;
                case ".ief": mimeType = "image/ief"; break;
                case ".iefs": mimeType = "image/ief"; break;
                case ".iges": mimeType = "application/iges"; break;
                case ".igs": mimeType = "application/iges"; break;
                case ".ima": mimeType = "application/x-ima"; break;
                case ".imap": mimeType = "application/x-httpd-imap"; break;
                case ".inf": mimeType = "application/inf"; break;
                case ".ins": mimeType = "application/x-internett-signup"; break;
                case ".ip": mimeType = "application/x-ip2"; break;
                case ".isu": mimeType = "video/x-isvideo"; break;
                case ".it": mimeType = "audio/it"; break;
                case ".iv": mimeType = "application/x-inventor"; break;
                case ".ivr": mimeType = "i-world/i-vrml"; break;
                case ".ivy": mimeType = "application/x-livescreen"; break;
                case ".jam": mimeType = "audio/x-jam"; break;
                case ".jav": mimeType = "text/plain"; break;
                case ".java": mimeType = "text/plain"; break;
                case ".jcm": mimeType = "application/x-java-commerce"; break;
                case ".jfif": mimeType = "image/jpeg"; break;
                case ".jfif-tbnl": mimeType = "image/jpeg"; break;
                case ".jpe": mimeType = "image/jpeg"; break;
                case ".jpeg": mimeType = "image/jpeg"; break;
                case ".jpg": mimeType = "image/jpeg"; break;
                case ".jps": mimeType = "image/x-jps"; break;
                case ".js": mimeType = "application/x-javascript"; break;
                case ".jut": mimeType = "image/jutvision"; break;
                case ".kar": mimeType = "audio/midi"; break;
                case ".ksh": mimeType = "application/x-ksh"; break;
                case ".la": mimeType = "audio/nspaudio"; break;
                case ".lam": mimeType = "audio/x-liveaudio"; break;
                case ".latex": mimeType = "application/x-latex"; break;
                case ".lha": mimeType = "application/octet-stream"; break;
                case ".lhx": mimeType = "application/octet-stream"; break;
                case ".list": mimeType = "text/plain"; break;
                case ".lma": mimeType = "audio/nspaudio"; break;
                case ".log": mimeType = "text/plain"; break;
                case ".lsp": mimeType = "application/x-lisp"; break;
                case ".lst": mimeType = "text/plain"; break;
                case ".lsx": mimeType = "text/x-la-asf"; break;
                case ".ltx": mimeType = "application/x-latex"; break;
                case ".lzh": mimeType = "application/octet-stream"; break;
                case ".lzx": mimeType = "application/octet-stream"; break;
                case ".m": mimeType = "text/plain"; break;
                case ".m1v": mimeType = "video/mpeg"; break;
                case ".m2a": mimeType = "audio/mpeg"; break;
                case ".m2v": mimeType = "video/mpeg"; break;
                case ".m3u": mimeType = "audio/x-mpequrl"; break;
                case ".man": mimeType = "application/x-troff-man"; break;
                case ".map": mimeType = "application/x-navimap"; break;
                case ".mar": mimeType = "text/plain"; break;
                case ".mbd": mimeType = "application/mbedlet"; break;
                case ".mc$": mimeType = "application/x-magic-cap-package-1.0"; break;
                case ".mcd": mimeType = "application/mcad"; break;
                case ".mcf": mimeType = "text/mcf"; break;
                case ".mcp": mimeType = "application/netmc"; break;
                case ".me": mimeType = "application/x-troff-me"; break;
                case ".mht": mimeType = "message/rfc822"; break;
                case ".mhtml": mimeType = "message/rfc822"; break;
                case ".mid": mimeType = "audio/midi"; break;
                case ".midi": mimeType = "audio/midi"; break;
                case ".mif": mimeType = "application/x-mif"; break;
                case ".mime": mimeType = "message/rfc822"; break;
                case ".mjf": mimeType = "audio/x-vnd.audioexplosion.mjuicemediafile"; break;
                case ".mjpg": mimeType = "video/x-motion-jpeg"; break;
                case ".mm": mimeType = "application/base64"; break;
                case ".mme": mimeType = "application/base64"; break;
                case ".mod": mimeType = "audio/mod"; break;
                case ".moov": mimeType = "video/quicktime"; break;
                case ".mov": mimeType = "video/quicktime"; break;
                case ".movie": mimeType = "video/x-sgi-movie"; break;
                case ".mp2": mimeType = "audio/mpeg"; break;
                case ".mp3": mimeType = "audio/mpeg"; break;
                case ".mpa": mimeType = "audio/mpeg"; break;
                case ".mpc": mimeType = "application/x-project"; break;
                case ".mpe": mimeType = "video/mpeg"; break;
                case ".mpeg": mimeType = "video/mpeg"; break;
                case ".mpg": mimeType = "video/mpeg"; break;
                case ".mpga": mimeType = "audio/mpeg"; break;
                case ".mpp": mimeType = "application/vnd.ms-project"; break;
                case ".mpt": mimeType = "application/vnd.ms-project"; break;
                case ".mpv": mimeType = "application/vnd.ms-project"; break;
                case ".mpx": mimeType = "application/vnd.ms-project"; break;
                case ".mrc": mimeType = "application/marc"; break;
                case ".ms": mimeType = "application/x-troff-ms"; break;
                case ".mv": mimeType = "video/x-sgi-movie"; break;
                case ".my": mimeType = "audio/make"; break;
                case ".mzz": mimeType = "application/x-vnd.audioexplosion.mzz"; break;
                case ".nap": mimeType = "image/naplps"; break;
                case ".naplps": mimeType = "image/naplps"; break;
                case ".nc": mimeType = "application/x-netcdf"; break;
                case ".ncm": mimeType = "application/vnd.nokia.configuration-message"; break;
                case ".nif": mimeType = "image/x-niff"; break;
                case ".niff": mimeType = "image/x-niff"; break;
                case ".nix": mimeType = "application/x-mix-transfer"; break;
                case ".nsc": mimeType = "application/x-conference"; break;
                case ".nvd": mimeType = "application/x-navidoc"; break;
                case ".o": mimeType = "application/octet-stream"; break;
                case ".oda": mimeType = "application/oda"; break;
                case ".omc": mimeType = "application/x-omc"; break;
                case ".omcd": mimeType = "application/x-omcdatamaker"; break;
                case ".omcr": mimeType = "application/x-omcregerator"; break;
                case ".p": mimeType = "text/x-pascal"; break;
                case ".p10": mimeType = "application/pkcs10"; break;
                case ".p12": mimeType = "application/pkcs-12"; break;
                case ".p7a": mimeType = "application/x-pkcs7-signature"; break;
                case ".p7c": mimeType = "application/pkcs7-mime"; break;
                case ".p7m": mimeType = "application/pkcs7-mime"; break;
                case ".p7r": mimeType = "application/x-pkcs7-certreqresp"; break;
                case ".p7s": mimeType = "application/pkcs7-signature"; break;
                case ".part": mimeType = "application/pro_eng"; break;
                case ".pas": mimeType = "text/pascal"; break;
                case ".pbm": mimeType = "image/x-portable-bitmap"; break;
                case ".pcl": mimeType = "application/vnd.hp-pcl"; break;
                case ".pct": mimeType = "image/x-pict"; break;
                case ".pcx": mimeType = "image/x-pcx"; break;
                case ".pdb": mimeType = "chemical/x-pdb"; break;
                case ".pdf": mimeType = "application/pdf"; break;
                case ".pfunk": mimeType = "audio/make"; break;
                case ".pgm": mimeType = "image/x-portable-greymap"; break;
                case ".pic": mimeType = "image/pict"; break;
                case ".pict": mimeType = "image/pict"; break;
                case ".pkg": mimeType = "application/x-newton-compatible-pkg"; break;
                case ".pko": mimeType = "application/vnd.ms-pki.pko"; break;
                case ".pl": mimeType = "text/plain"; break;
                case ".plx": mimeType = "application/x-pixclscript"; break;
                case ".pm": mimeType = "image/x-xpixmap"; break;
                case ".pm4": mimeType = "application/x-pagemaker"; break;
                case ".pm5": mimeType = "application/x-pagemaker"; break;
                case ".png": mimeType = "image/png"; break;
                case ".pnm": mimeType = "application/x-portable-anymap"; break;
                case ".pot": mimeType = "application/vnd.ms-powerpoint"; break;
                case ".pov": mimeType = "model/x-pov"; break;
                case ".ppa": mimeType = "application/vnd.ms-powerpoint"; break;
                case ".ppm": mimeType = "image/x-portable-pixmap"; break;
                case ".pps": mimeType = "application/vnd.ms-powerpoint"; break;
                case ".ppt": mimeType = "application/vnd.ms-powerpoint"; break;
                case ".ppz": mimeType = "application/vnd.ms-powerpoint"; break;
                case ".pre": mimeType = "application/x-freelance"; break;
                case ".prt": mimeType = "application/pro_eng"; break;
                case ".ps": mimeType = "application/postscript"; break;
                case ".psd": mimeType = "application/octet-stream"; break;
                case ".pvu": mimeType = "paleovu/x-pv"; break;
                case ".pwz": mimeType = "application/vnd.ms-powerpoint"; break;
                case ".py": mimeType = "text/x-script.phyton"; break;
                case ".pyc": mimeType = "applicaiton/x-bytecode.python"; break;
                case ".qcp": mimeType = "audio/vnd.qcelp"; break;
                case ".qd3": mimeType = "x-world/x-3dmf"; break;
                case ".qd3d": mimeType = "x-world/x-3dmf"; break;
                case ".qif": mimeType = "image/x-quicktime"; break;
                case ".qt": mimeType = "video/quicktime"; break;
                case ".qtc": mimeType = "video/x-qtc"; break;
                case ".qti": mimeType = "image/x-quicktime"; break;
                case ".qtif": mimeType = "image/x-quicktime"; break;
                case ".ra": mimeType = "audio/x-pn-realaudio"; break;
                case ".ram": mimeType = "audio/x-pn-realaudio"; break;
                case ".ras": mimeType = "application/x-cmu-raster"; break;
                case ".rast": mimeType = "image/cmu-raster"; break;
                case ".rexx": mimeType = "text/x-script.rexx"; break;
                case ".rf": mimeType = "image/vnd.rn-realflash"; break;
                case ".rgb": mimeType = "image/x-rgb"; break;
                case ".rm": mimeType = "application/vnd.rn-realmedia"; break;
                case ".rmi": mimeType = "audio/mid"; break;
                case ".rmm": mimeType = "audio/x-pn-realaudio"; break;
                case ".rmp": mimeType = "audio/x-pn-realaudio"; break;
                case ".rng": mimeType = "application/ringing-tones"; break;
                case ".rnx": mimeType = "application/vnd.rn-realplayer"; break;
                case ".roff": mimeType = "application/x-troff"; break;
                case ".rp": mimeType = "image/vnd.rn-realpix"; break;
                case ".rpm": mimeType = "audio/x-pn-realaudio-plugin"; break;
                case ".rt": mimeType = "text/richtext"; break;
                case ".rtf": mimeType = "text/richtext"; break;
                case ".rtx": mimeType = "text/richtext"; break;
                case ".rv": mimeType = "video/vnd.rn-realvideo"; break;
                case ".s": mimeType = "text/x-asm"; break;
                case ".s3m": mimeType = "audio/s3m"; break;
                case ".saveme": mimeType = "application/octet-stream"; break;
                case ".sbk": mimeType = "application/x-tbook"; break;
                case ".scm": mimeType = "application/x-lotusscreencam"; break;
                case ".sdml": mimeType = "text/plain"; break;
                case ".sdp": mimeType = "application/sdp"; break;
                case ".sdr": mimeType = "application/sounder"; break;
                case ".sea": mimeType = "application/sea"; break;
                case ".set": mimeType = "application/set"; break;
                case ".sgm": mimeType = "text/sgml"; break;
                case ".sgml": mimeType = "text/sgml"; break;
                case ".sh": mimeType = "application/x-sh"; break;
                case ".shar": mimeType = "application/x-shar"; break;
                case ".shtml": mimeType = "text/html"; break;
                case ".sid": mimeType = "audio/x-psid"; break;
                case ".sit": mimeType = "application/x-sit"; break;
                case ".skd": mimeType = "application/x-koan"; break;
                case ".skm": mimeType = "application/x-koan"; break;
                case ".skp": mimeType = "application/x-koan"; break;
                case ".skt": mimeType = "application/x-koan"; break;
                case ".sl": mimeType = "application/x-seelogo"; break;
                case ".smi": mimeType = "application/smil"; break;
                case ".smil": mimeType = "application/smil"; break;
                case ".snd": mimeType = "audio/basic"; break;
                case ".sol": mimeType = "application/solids"; break;
                case ".spc": mimeType = "text/x-speech"; break;
                case ".spl": mimeType = "application/futuresplash"; break;
                case ".spr": mimeType = "application/x-sprite"; break;
                case ".sprite": mimeType = "application/x-sprite"; break;
                case ".src": mimeType = "application/x-wais-source"; break;
                case ".ssi": mimeType = "text/x-server-parsed-html"; break;
                case ".ssm": mimeType = "application/streamingmedia"; break;
                case ".sst": mimeType = "application/vnd.ms-pki.certstore"; break;
                case ".step": mimeType = "application/step"; break;
                case ".stl": mimeType = "application/sla"; break;
                case ".stp": mimeType = "application/step"; break;
                case ".sv4cpio": mimeType = "application/x-sv4cpio"; break;
                case ".sv4crc": mimeType = "application/x-sv4crc"; break;
                case ".svf": mimeType = "image/vnd.dwg"; break;
                case ".svr": mimeType = "application/x-world"; break;
                case ".swf": mimeType = "application/x-shockwave-flash"; break;
                case ".t": mimeType = "application/x-troff"; break;
                case ".talk": mimeType = "text/x-speech"; break;
                case ".tar": mimeType = "application/x-tar"; break;
                case ".tbk": mimeType = "application/toolbook"; break;
                case ".tcl": mimeType = "application/x-tcl"; break;
                case ".tcsh": mimeType = "text/x-script.tcsh"; break;
                case ".tex": mimeType = "application/x-tex"; break;
                case ".texi": mimeType = "application/x-texinfo"; break;
                case ".texinfo": mimeType = "application/x-texinfo"; break;
                case ".text": mimeType = "text/plain"; break;
                case ".tgz": mimeType = "application/x-compressed"; break;
                case ".tif": mimeType = "image/tiff"; break;
                case ".tiff": mimeType = "image/tiff"; break;
                case ".tr": mimeType = "application/x-troff"; break;
                case ".tsi": mimeType = "audio/tsp-audio"; break;
                case ".tsp": mimeType = "application/dsptype"; break;
                case ".tsv": mimeType = "text/tab-separated-values"; break;
                case ".turbot": mimeType = "image/florian"; break;
                case ".txt": mimeType = "text/plain"; break;
                case ".uil": mimeType = "text/x-uil"; break;
                case ".uni": mimeType = "text/uri-list"; break;
                case ".unis": mimeType = "text/uri-list"; break;
                case ".unv": mimeType = "application/i-deas"; break;
                case ".uri": mimeType = "text/uri-list"; break;
                case ".uris": mimeType = "text/uri-list"; break;
                case ".ustar": mimeType = "application/x-ustar"; break;
                case ".uu": mimeType = "application/octet-stream"; break;
                case ".uue": mimeType = "text/x-uuencode"; break;
                case ".vcd": mimeType = "application/x-cdlink"; break;
                case ".vcs": mimeType = "text/x-vcalendar"; break;
                case ".vda": mimeType = "application/vda"; break;
                case ".vdo": mimeType = "video/vdo"; break;
                case ".vew": mimeType = "application/groupwise"; break;
                case ".viv": mimeType = "video/vivo"; break;
                case ".vivo": mimeType = "video/vivo"; break;
                case ".vmd": mimeType = "application/vocaltec-media-desc"; break;
                case ".vmf": mimeType = "application/vocaltec-media-file"; break;
                case ".voc": mimeType = "audio/voc"; break;
                case ".vos": mimeType = "video/vosaic"; break;
                case ".vox": mimeType = "audio/voxware"; break;
                case ".vqe": mimeType = "audio/x-twinvq-plugin"; break;
                case ".vqf": mimeType = "audio/x-twinvq"; break;
                case ".vql": mimeType = "audio/x-twinvq-plugin"; break;
                case ".vrml": mimeType = "application/x-vrml"; break;
                case ".vrt": mimeType = "x-world/x-vrt"; break;
                case ".vsd": mimeType = "application/x-visio"; break;
                case ".vst": mimeType = "application/x-visio"; break;
                case ".vsw": mimeType = "application/x-visio"; break;
                case ".w60": mimeType = "application/wordperfect6.0"; break;
                case ".w61": mimeType = "application/wordperfect6.1"; break;
                case ".w6w": mimeType = "application/msword"; break;
                case ".wav": mimeType = "audio/wav"; break;
                case ".wb1": mimeType = "application/x-qpro"; break;
                case ".wbmp": mimeType = "image/vnd.wap.wbmp"; break;
                case ".web": mimeType = "application/vnd.xara"; break;
                case ".wiz": mimeType = "application/msword"; break;
                case ".wk1": mimeType = "application/x-123"; break;
                case ".wmf": mimeType = "windows/metafile"; break;
                case ".wml": mimeType = "text/vnd.wap.wml"; break;
                case ".wmlc": mimeType = "application/vnd.wap.wmlc"; break;
                case ".wmls": mimeType = "text/vnd.wap.wmlscript"; break;
                case ".wmlsc": mimeType = "application/vnd.wap.wmlscriptc"; break;
                case ".word": mimeType = "application/msword"; break;
                case ".wp": mimeType = "application/wordperfect"; break;
                case ".wp5": mimeType = "application/wordperfect"; break;
                case ".wp6": mimeType = "application/wordperfect"; break;
                case ".wpd": mimeType = "application/wordperfect"; break;
                case ".wq1": mimeType = "application/x-lotus"; break;
                case ".wri": mimeType = "application/mswrite"; break;
                case ".wrl": mimeType = "application/x-world"; break;
                case ".wrz": mimeType = "x-world/x-vrml"; break;
                case ".wsc": mimeType = "text/scriplet"; break;
                case ".wsrc": mimeType = "application/x-wais-source"; break;
                case ".wtk": mimeType = "application/x-wintalk"; break;
                case ".xbm": mimeType = "image/x-xbitmap"; break;
                case ".xdr": mimeType = "video/x-amt-demorun"; break;
                case ".xgz": mimeType = "xgl/drawing"; break;
                case ".xif": mimeType = "image/vnd.xiff"; break;
                case ".xl": mimeType = "application/excel"; break;
                case ".xla": mimeType = "application/vnd.ms-excel"; break;
                case ".xlb": mimeType = "application/vnd.ms-excel"; break;
                case ".xlc": mimeType = "application/vnd.ms-excel"; break;
                case ".xld": mimeType = "application/vnd.ms-excel"; break;
                case ".xlk": mimeType = "application/vnd.ms-excel"; break;
                case ".xll": mimeType = "application/vnd.ms-excel"; break;
                case ".xlm": mimeType = "application/vnd.ms-excel"; break;
                case ".xls": mimeType = "application/vnd.ms-excel"; break;
                case ".xlt": mimeType = "application/vnd.ms-excel"; break;
                case ".xlv": mimeType = "application/vnd.ms-excel"; break;
                case ".xlw": mimeType = "application/vnd.ms-excel"; break;
                case ".xm": mimeType = "audio/xm"; break;
                case ".xml": mimeType = "application/xml"; break;
                case ".xmz": mimeType = "xgl/movie"; break;
                case ".xpix": mimeType = "application/x-vnd.ls-xpix"; break;
                case ".xpm": mimeType = "image/xpm"; break;
                case ".x-png": mimeType = "image/png"; break;
                case ".xsr": mimeType = "video/x-amt-showrun"; break;
                case ".xwd": mimeType = "image/x-xwd"; break;
                case ".xyz": mimeType = "chemical/x-pdb"; break;
                case ".z": mimeType = "application/x-compressed"; break;
                case ".zip": mimeType = "application/zip"; break;
                case ".zoo": mimeType = "application/octet-stream"; break;
                case ".zsh": mimeType = "text/x-script.zsh"; break;
                default: mimeType = "application/octet-stream"; break;
            }
            return mimeType;
        } 

    }



    [Serializable]
    public class StudentUpdatePeriod
    {
        public Guid ClassId;
        public Guid StudentId;
        public string StudentName;
        public Guid PeriodId;
        public Guid ClassUpdatePeriodId;
        public DateTime startDate;
        public DateTime endDate;
        public string ImpactAnswer;
        public EntityReference DefaultQuestions = new EntityReference();
        public EntityReference ImpactQuestion = new EntityReference();
        public string PeriodImpactQuestion = null;
        public string GoalQuestion1;
        public string GoalQuestion2;
        public List<GoalAnswers> PeriodGoalAnswers;
        public bool isUpdate = false;
        public string FeedbackRequest = "";

        public StudentUpdatePeriod()
        {
            PeriodGoalAnswers = new List<GoalAnswers>();
        }
    }

    [Serializable]
    public class GoalAnswers
    {
        public Guid GoalId;
        public Guid StudentGoalId;
        public string GoalName;
        public string Payoff;
        public List<Activities> MyActivities = new List<Activities>();
        public string Answer1="";
        public string Answer2="";
        public int ProgressValue = -1;
    }

     [Serializable]
    public class Activities
    {
        public Guid Activityid;
        public string Text="";
        public bool IsChecked;
    }


     [Serializable]
     public class ClassGoals
     {
         public Guid classid;
         public DateTime lastupdated = DateTime.MinValue;
         public List<ClassGoal> classGoals = new List<ClassGoal>();

         //public void SetGoals(Guid classId, DateTime updated, List<Contact> studentsI, List<Contact> coachesI, List<new_studentgoals> studentgoalsI,
         // List<new_classupdateperiod> classupdateperiodI, List<StudentUpdatePeriod> studentProgressI, List<new_coaching> coachingI)
         //{
         //    classid = classId;
         //    lastupdated = updated;
         //    studentProgress = studentProgressI;

         //    foreach (var contact in studentsI)
         //    {
         //        students.Add(Set_Contacts(contact));
         //    }

         //    foreach (var contact in coachesI)
         //    {
         //        coaches.Add(Set_Contacts(contact));
         //    }

         //    foreach (var newStudentgoal in studentgoalsI)
         //    {
         //        selectedGoals.Add(SetStudentGoals(newStudentgoal));
         //    }

         //    foreach (var newClassupdateperiodS in classupdateperiodI)
         //    {
         //        progressPeriods.Add(SetClassPeriods(newClassupdateperiodS));
         //    }

          
         //}

     }

     [Serializable]
     public class ClassGoal
     {

         public Guid id;
         public string name;
         public int? min;
         public int? max;
         public bool? isdefault;
         public int? new_SortOrder;

         public List<Activity> activities = new List<Activity>();

         public bool selected;

         public bool isComplete = false;
         public bool initialselected = false;
         public string initialpayoff = "";
         public string payoff = "";
     }


     [Serializable]
     public class Activity
     {
         public Guid id;
         public string name;
         public List<SubActivity> subActivities = new List<SubActivity>();
         public bool selected = false;
         public bool initialselected = false;
         public string sortorder;
     }

     [Serializable]
     public class SubActivity
     {
         public Guid id;
         public string name;
         public bool selected = false;
         public bool initialselected = false;
         public string sortorder;
     }


    //public class new_class
    //{
    //    public string Name;
    //    public Guid Id;
    //}


    ///// <summary>
    ///// This Sample shows how to create an opportunity with 
    ///// one catalog and one write-in product.
    ///// </summary>
    //public class CrmAccess
    //{
    //    public static OrganizationServiceProxy ServiceProxy = null;


    //    //Generic Organization Context used for Late Binding LINQ 
    //    private OrganizationServiceContext OrgContext { get; set; }


    //    public CrmAccess()
    //    {
            

    //    }



    //    public void CrmAccess2()
    //    {
    //        if (ServiceProxy != null) return;

    //        var serverConnect = new ServerConnection();
    //        var serverConfig = new ServerConnection.Configuration();

    //        try
    //        {
    //            serverConfig = serverConnect.GetServerConfiguration();

    //            // Connect to the Organization service. 
    //            // The using statement assures that the service proxy will be properly disposed.
    //            using (ServiceProxy = new OrganizationServiceProxy(serverConfig.OrganizationUri,
    //                                                               serverConfig.HomeRealmUri,
    //                                                               serverConfig.Credentials,
    //                                                               serverConfig.DeviceCredentials))
    //            {
    //                // This statement is required to enable early-bound type support.
    //                ServiceProxy.ServiceConfiguration.CurrentServiceEndpoint.Behaviors.Add(new ProxyTypesBehavior());
    //            }
    //        }
    //        catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex)
    //        {
    //            var sb = new StringBuilder();
    //            sb.AppendFormat("The application terminated with an error.");
    //            sb.AppendFormat("Timestamp: {0}", ex.Detail.Timestamp);
    //            sb.AppendFormat("Code: {0}", ex.Detail.ErrorCode);
    //            sb.AppendFormat("Message: {0}", ex.Detail.Message);
    //            sb.AppendFormat("Plugin Trace: {0}", ex.Detail.TraceText);
    //            sb.AppendFormat("Inner Fault: {0}",
    //                            null == ex.Detail.InnerFault ? "Has Inner Fault" : "No Inner Fault");

    //            EmailNotifications.SendExceptionMessage("GrayMattersGroup ",
    //                                                    "CrmAccess as " + serverConfig.Credentials.UserName, ex,
    //                                                    sb.ToString());
    //        }
    //        catch (TimeoutException ex)
    //        {
    //            var sb = new StringBuilder();
    //            sb.AppendLine("The application terminated with an error.");
    //            sb.AppendFormat("Message: {0}", ex.Message);
    //            sb.AppendFormat("Stack Trace: {0}", ex.StackTrace);
    //            sb.AppendFormat("Inner Fault: {0}",
    //                            (!string.IsNullOrEmpty(ex.InnerException.Message))
    //                                ? "Has Inner Fault"
    //                                : "No Inner Fault");

    //            EmailNotifications.SendExceptionMessage("GrayMattersGroup ",
    //                                                    "CrmAccess as " + serverConfig.Credentials.UserName, ex,
    //                                                    sb.ToString());
    //        }
    //        catch (Exception ex)
    //        {
    //            var sb = new StringBuilder();
    //            sb.AppendLine("The application terminated with an error.");
    //            sb.AppendLine(ex.Message);

    //            // Display the details of the inner exception.
    //            if (ex.InnerException != null)
    //            {
    //                sb.AppendLine(ex.InnerException.Message);

    //                var fe = ex.InnerException as FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>;
    //                if (fe != null)
    //                {
    //                    sb.AppendFormat("Timestamp: {0}", fe.Detail.Timestamp);
    //                    sb.AppendFormat("Code: {0}", fe.Detail.ErrorCode);
    //                    sb.AppendFormat("Message: {0}", fe.Detail.Message);
    //                    sb.AppendFormat("Plugin Trace: {0}", fe.Detail.TraceText);
    //                    sb.AppendFormat("Inner Fault: {0}",
    //                                    null == fe.Detail.InnerFault
    //                                        ? "Has Inner Fault"
    //                                        : "No Inner Fault");
    //                }

    //            }
    //            EmailNotifications.SendExceptionMessage("GrayMattersGroup ", "CrmAccess ", ex, sb.ToString());
    //        }
    //    }

       


 
    //}
}

