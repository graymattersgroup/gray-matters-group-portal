﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Telerik.Web.UI;

namespace GoalManagement.Administration
{
    public partial class ContentEditor : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            var mainMenu = Master.FindControl("NavigationMenu") as RadMenu;

            var myContact = (Contact)Session["StudentInfo"];

            if (myContact.new_IsPortalAdministrator != true) Response.Redirect("../Default.aspx");
            
            if ( !IsPostBack)
            {
                CRMAccess crmAccess;
                if (Session["crmAccess"] == null)
                {
                    crmAccess = new CRMAccess();
                    Session["crmAccess"] = crmAccess;
                }
                else
                {
                    crmAccess = (CRMAccess)Session["crmAccess"];
                }

                var classes = crmAccess.GetAllClasses();

                ClassList.Items.Add(new ListItem("All", "All"));
                foreach (new_class newClass in classes)
                {
                    ClassList.Items.Add(new ListItem(String.Format("{0:MM/dd/yyyy}  {1}", newClass.new_ClassDate.Value, newClass.new_name),
                            newClass.new_classId.Value.ToString()));
                }

                var db = new SqlAzureLayer(GetConnectionString());

                var getMediaNames = string.Format("select name,contentid from [dbo].[ManagedContent] where classid is  null order by type,name");
                var names = db.GetDataTable(getMediaNames);

                foreach (DataRow row in names.Rows)
                {
                    PageList.Items.Add(new ListItem(row[0].ToString(), row[1].ToString()));
                }


                var getMedia = string.Format("select content,type from [dbo].[ManagedContent] where classid is  null and contentid ={0}",
                                             db.FormatGuid(new Guid(PageList.Items[0].Value)));

                var result = db.GetDataTable(getMedia);

                RadEditor1.Content = result.Rows[0][0].ToString();

                if (result.Rows[0][1].ToString() == "4")
                {
                    RadEditor1.ContentAreaCssFile = "";
                    Message.Text = "Don't forget the  #CONFIRM tag for the link";
                    RadEditor1.ContentAreaMode = EditorContentAreaMode.Iframe;
                }
                else
                {
                    RadEditor1.ContentAreaCssFile = "~/Styles/Site.css";
                    Message.Text = "";
                    RadEditor1.ContentAreaMode =  EditorContentAreaMode.Div;
                }
               
            } 

            this.Page.MaintainScrollPositionOnPostBack = true;

            var masterPage = (WideSiteMaster)Page.Master;
            masterPage.SetMenu(CentralizedData.ContentManagement);
            
        }
        

        protected void NewDisplayOption_Click(object sender, EventArgs e)
        {
              string Database = ConfigurationManager.AppSettings["UseDatabase"];
            var connectionString = ConfigurationManager.ConnectionStrings[Database].ConnectionString;
            var db = new SqlAzureLayer(connectionString);



//            var newReportField =
//                string.Format(@"INSERT INTO [DisplayOption] ([optionValue]) 
//VALUES ({0})",
//            db.FormatInt(OptionsGridView.Rows.Count + 1));

//            db.SqlExecuteNonQuery(newReportField);

           
           
        }
        
        private string GetConnectionString()
        {
            if (Application["connectionString"] == null)
            {
                var connectionString2 = ConfigurationManager.ConnectionStrings["Production"].ConnectionString;
                Application["connectionString"] = connectionString2;
            }
            return Application["connectionString"].ToString();
        }

        protected void PageList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var db = new SqlAzureLayer(GetConnectionString());

            var getMedia = string.Format("select content,type from [dbo].[ManagedContent] where contentid ={0}",
                                         db.FormatGuid(new Guid(PageList.SelectedValue)));;

            var result = db.GetDataTable(getMedia);


            if (result.Rows[0][1].ToString() == "4")
            {
                RadEditor1.ContentAreaCssFile = "";
                Message.Text = "Don't forget the  #CONFIRM tag for the link.  #EMAILADDRESS and #PASSWORD also available";
                RadEditor1.ContentAreaMode = EditorContentAreaMode.Iframe;
            }
            else
            {
                RadEditor1.ContentAreaCssFile = "~/Styles/Site.css";
                Message.Text = "";
                RadEditor1.ContentAreaMode = EditorContentAreaMode.Div;
            }

            RadEditor1.Content = result.Rows[0][0].ToString();
        }

        protected void SaveContent_Click(object sender, EventArgs e)
        {
            var db = new SqlAzureLayer(GetConnectionString());

            var updateMedia = string.Format(@"Update [dbo].[ManagedContent]

Set content = '{1}'
where contentid ={0}",
 db.FormatGuid(new Guid(PageList.SelectedValue)), RadEditor1.Content.Replace("'","`"));

            db.SqlExecuteNonQuery(updateMedia);

            
        }

        protected void ClassList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var db = new SqlAzureLayer(GetConnectionString());

            var names = new DataTable();

            if ( ClassList.SelectedValue  == "All")
            {
                var getMediaNames = string.Format("select name,contentid from [dbo].[ManagedContent] where classid IS NULL order by type,name");
                names = db.GetDataTable(getMediaNames);
            }
            else
            {
                var classId = new Guid(ClassList.SelectedValue);
                var getMediaNames =string.Format("select name,contentid from [dbo].[ManagedContent]  where classid={0} order by type,name",db.FormatGuid(classId));
                names = db.GetDataTable(getMediaNames);

                if (names.Rows.Count == 0)
                {
                    CreateCustomResource.Visible = true;
                    SaveContent.Visible = false;
                    return;
                }

            }

            PageList.Items.Clear();

            foreach (DataRow row in names.Rows)
            {
                PageList.Items.Add(new ListItem(row[0].ToString(), row[1].ToString()));
            }
            
            var getMedia = string.Format("select content,type from [dbo].[ManagedContent] where contentid ={0}",
                                         db.FormatGuid(new Guid(PageList.Items[0].Value)));

            var result = db.GetDataTable(getMedia);

            RadEditor1.Content = result.Rows[0][0].ToString();

        }

       


        protected void CreateCustomResource_Click(object sender, EventArgs e)
        {
           var db = new SqlAzureLayer(GetConnectionString());
           var classId = new Guid(ClassList.SelectedValue);
        
           CreateCustomResource.Visible = false;
           SaveContent.Visible = true;

                //Message Asking if we add a new one
                var insertMedia = string.Format(@"Insert into [dbo].[ManagedContent]
( contentid,name,content,classid,type   )
Values
( {3},{0},{1}, {2},1)   ",
                        db.FormatString("Resources and Tools"),
                        db.FormatString(""),
                        db.FormatGuid(classId),
                        db.FormatGuid(Guid.NewGuid()));

                db.SqlExecuteNonQuery(insertMedia);


               var getMediaNames = string.Format("select name,contentid from [dbo].[ManagedContent]  where classid={0} order by type,name", db.FormatGuid(classId));
               var names = db.GetDataTable(getMediaNames);
               
               PageList.Items.Clear();

               foreach (DataRow row in names.Rows)
               {
                   PageList.Items.Add(new ListItem(row[0].ToString(), row[1].ToString()));
               }

               var getMedia = string.Format("select content,type from [dbo].[ManagedContent] where contentid ={0}",
                                            db.FormatGuid(new Guid(PageList.Items[0].Value)));

               var result = db.GetDataTable(getMedia);

               RadEditor1.Content = result.Rows[0][0].ToString();
        }
    }
}