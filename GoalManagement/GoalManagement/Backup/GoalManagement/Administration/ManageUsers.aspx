﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WideSite.Master" AutoEventWireup="true"
    CodeBehind="ManageUsers.aspx.cs" Inherits="GoalManagement.Administration.ManageUsers" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">

body {background:#FFFFFF;}

        
input {font-size:11px}
table  
{ 
	border-collapse:collapse;border:0px;
	border-spacing: 0px; 
}
          
.HideMe { visibility:hidden; display:none;  }       
.directionstable{border-collapse:collapse;border:0px }
table.directionstable tr td {padding:4px;}
.number { color:DarkBlue;}
.header {background:blue; color:white; text-align:left;}
.destination { font-weight:bold; }

.sidemenu { font-size:11px; height:20px;  text-align:center; color:#FFFFFF; Background: url("/Images/tabfill.gif") repeat; }
.sideitem { font-size:9px;}

div.sideitem input
{font-size:10px;}

.lookup {font-size:10px;}

        #Button2
        {
            width: 109px;
        }
        #Button1
        {
            width: 78px;
        }
        #Button3
        {
            width: 78px;
        }
        #SaveSettings
        {
            width: 160px;
        }
        
        #ctl00_MainContent_Menu1 { margin-left:5px;}
        
       .menutab { 
color: #FFFFFF; 
background: url("/Images/wholetab150.gif"); 
text-decoration: none; 
text-align:center;
width:145px;
}

    a.selected { font-weight:bolder; background: url("/Images/wholetabselected150.gif");}
        
    ul.wholemenu li a { color:#ffffff }
        
        
    </style>
    

    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding-left: 10px;">   
          
           
        <div style="margin-left: 5px; margin-right: 5px; background:#FFFFFF;
    padding: 10px;">
            <table>
                                  
            </table>
        <%--    <asp:GridView ID="UserList" runat="server" OnRowDataBound="GridView1_RowDataBound" AutoGenerateSelectButton="True">
                </asp:GridView>--%>
            <%--<asp:ListBox ID="ExistingUsers" runat="server" AutoPostBack="True" 
                Height="200px" onselectedindexchanged="ExistingUsers_SelectedIndexChanged" 
                SelectionMode="Multiple" Width="890px" Visible="false"></asp:ListBox>--%>
           <%-- <telerik:RadGrid ID="UserRadGrid" runat="server" AllowMultiRowSelection="True" 
                Skin="Web20" AllowFilteringByColumn="True" AllowPaging="True" 
                AllowSorting="True" GridLines="None" 
                 au
                onitemdatabound="UserRadGrid_ItemDataBound">
                <ClientSettings>
                    <Selecting AllowRowSelect="true" EnableDragToSelectRows="true" />
                </ClientSettings>
            </telerik:RadGrid>--%>
            <telerik:RadGrid AutoGenerateColumns="False" ID="UserRadGrid"
                AllowFilteringByColumn="True" AllowSorting="True" PageSize="15" AllowMultiRowSelection ="False"
                ShowFooter="True" AllowPaging="True" runat="server" GridLines="None" 
                EnableLinqExpressions="False" Skin="Web20" 
                OnSelectedIndexChanged="UserRadGrid_SelectedIndexChanged" 
                ShowGroupPanel="True" >
                <PagerStyle Mode="NextPrevAndNumeric" />
                <GroupingSettings CaseSensitive="false" />
                <MasterTableView AutoGenerateColumns="false" EditMode="InPlace" AllowFilteringByColumn="True"
                    ShowFooter="True" TableLayout="Auto">
<CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridTemplateColumn UniqueName="CheckBoxTemplateColumn">
                            <HeaderTemplate>
                                <asp:CheckBox ID="headerChkbox" OnCheckedChanged="ToggleSelectedState" AutoPostBack="True"
                                    runat="server"></asp:CheckBox>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox1" OnCheckedChanged="ToggleRowSelection" AutoPostBack="True"
                                    runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>           
                       <telerik:GridBoundColumn  DataField="LastName" HeaderText="LastName"
                            SortExpression="LastName" UniqueName="LastName" >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="name" HeaderText="FirstName" SortExpression="name"
                            UniqueName="name">                            
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="username" HeaderText="username" SortExpression="username"
                            UniqueName="username" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>                                      
                        <telerik:GridCheckBoxColumn DataField="IsLockedOut" DataType="System.Boolean" HeaderText="Locked"
                            SortExpression="IsLockedOut" UniqueName="IsLockedOut">                           
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="IsApproved" DataType="System.Boolean" HeaderText="Activated"
                            SortExpression="IsApproved" UniqueName="IsApproved">
                        </telerik:GridCheckBoxColumn>                        
                    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
                </MasterTableView>
                <ClientSettings AllowDragToGroup="True">
                    <Scrolling AllowScroll="false" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Web20"></HeaderContextMenu>
            </telerik:RadGrid>
            <%-- <table style="border-bottom: 3px solid #e3eeff; ">

    <tr>
        <td>Select User
        </td>
        <td>
        </td>
        <td>Select Membership Group
        </td>
    </tr>
<tr>
<td style="vertical-align:top;"><asp:ListBox ID="ExistingUsers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ExistingUsers_SelectedIndexChanged"
        Height="150"></asp:ListBox>
</td>
    <td style=" text-align:center;">  <asp:Button ID="AddUsertoGroup" runat="server" 
            Text="Add to Group" onclick="AddUsertoGroup_Click" /><br />
        <asp:Button ID="RemoveUserFromGroup" runat="server" Text="Remove from Group" 
            onclick="RemoveUserFromGroup_Click1" /><br />
        <br />
        <br />
        <asp:TextBox ID="GroupName" runat="server"></asp:TextBox>
        <asp:Button ID="CreateGroup" runat="server" Text="Create Group" OnClick="CreateGroup_Click" />
        <br />
        <br />
        <asp:Button ID="RemoveGroup" runat="server" Text="Remove Group" OnClick="RemoveGroup_Click1" />
        <asp:ConfirmButtonExtender ID="RemoveGroup_ConfirmButtonExtender" 
            runat="server" ConfirmText="Are you sure you want to remove this group?" Enabled="True" TargetControlID="RemoveGroup">
        </asp:ConfirmButtonExtender>
    </td>
    <td>  
        <table>
        <tr>
        <td><asp:ListBox ID="Groups" runat="server" Height="150"></asp:ListBox>
        </td>
        <td valign="middle">
        </td>
        <td>
      
        </td>
        </tr>
        </table>       
      
       
    </td>
</tr>
  
</table>    --%>

   
            <br />
            <span style="color: Black;">Selected User: &nbsp;&nbsp; <b><asp:Label ID="username" runat="server" Text="None Selected"></asp:Label></b>&nbsp;&nbsp;
            </span>
            <br />
    <table style="margin-top:5px; width:100%">
    <tr>  
    
        <td>       
       
            <asp:Button ID="ImpersonateUser" runat="server" Text="Impersonate User" 
                onclick="ImpersonateUser_Click" />
            <br />
            <br />
            <asp:Button ID="ClearUser" runat="server" Text="Clear CRM Goals and Feedback #1" 
                onclick="ClearUser_Click" />
            <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to Clear this User"
                Enabled="True" TargetControlID="ClearUser">
            </asp:ConfirmButtonExtender>
            <br />
            <br />
            <asp:Button ID="DeleteUser" runat="server" Text="Delete Portal Credentials #2" OnClick="DeleteUser_Click" />
            <asp:ConfirmButtonExtender ID="DeleteUser_ConfirmButtonExtender" runat="server" ConfirmText="Are you sure you want to delete this User"
                Enabled="True" TargetControlID="DeleteUser">
            </asp:ConfirmButtonExtender>
        </td>
   
        <td>
            &nbsp;</td>
        <td>
            New Password: <asp:TextBox ID="NewPassword" runat="server" Width="100px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="ChangePassword" runat="server" Text="Change Password" OnClick="ChangePassword_Click" />            
        </td>
       
        <td>
            <asp:Button ID="ConfirmUser" runat="server" Text="Activate User" OnClick="ConfirmUser_Click" />
            <br />
            <br />
            <asp:Button ID="UnlockUser" runat="server" Text="Unlock User" OnClick="UnlockUser_Click" />
        </td>
    </tr>
    </table>
    
     </div>


        </div>
       
<%--  <asp:BoundField DataField="searchdisplayoption" HeaderText="Display Option" 
                                        SortExpression="searchdisplayoption" />--%>
 
  

    </asp:Content>
