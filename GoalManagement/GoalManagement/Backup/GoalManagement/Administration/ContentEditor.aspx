﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WideSite.Master" AutoEventWireup="true"
    CodeBehind="ContentEditor.aspx.cs" Inherits="GoalManagement.Administration.ContentEditor" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">

            
input {font-size:10px}
table  
{ 
	border-collapse:collapse;border:0px;
	border-spacing: 0px; 
}
          
.HideMe { visibility:hidden; display:none;  }       
.directionstable{border-collapse:collapse;border:0px }
table.directionstable tr td {padding:4px;}
.number { color:DarkBlue;}
.header {background:blue; color:white; text-align:left;}
.destination { font-weight:bold; }

.sidemenu { font-size:11px; height:20px;  text-align:center; color:#FFFFFF; Background: url("/Images/tabfill.gif") repeat; }
.sideitem { font-size:9px;}

div.sideitem input
{font-size:9px;}

.lookup {font-size:9px;}

        #Button2
        {
            width: 109px;
        }
        #Button1
        {
            width: 78px;
        }
        #Button3
        {
            width: 78px;
        }
        #SaveSettings
        {
            width: 160px;
        }
        
        #ctl00_MainContent_Menu1 { margin-left:5px;}
        
       .menutab { 
color: #FFFFFF; 
background: url("/Images/wholetab150.gif"); 
text-decoration: none; 
text-align:center;
width:145px;
}

    a.selected { font-weight:bolder; background: url("/Images/wholetabselected150.gif");}
        
    ul.wholemenu li a { color:#ffffff }
        
        
        .style1
        {
            width: 743px;
        }
        

        
    </style>
    

    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding-left: 10px;  background-color:#FFFFFF;">         
              
               <br />
        <asp:Button ID="CreateCustomResource" runat="server" Text="Create New Custom Resource"
            OnClick="CreateCustomResource_Click" Visible="false" />
            <asp:DropDownList ID="ClassList" runat="server" Font-Size="9pt" OnSelectedIndexChanged="ClassList_SelectedIndexChanged"
            AutoPostBack="true">
        </asp:DropDownList>
        <asp:DropDownList ID="PageList" AutoPostBack="true" runat="server" OnSelectedIndexChanged="PageList_SelectedIndexChanged">
        </asp:DropDownList>
   
                <asp:Button ID="SaveContent" runat="server" Text="Save" 
                       onclick="SaveContent_Click" />
      <span style="color:Red; background-color:White;">  <asp:Label ID="Message" runat="server" Text="Label"></asp:Label></span>
        <br />
        <br />
              
                   <telerik:RadEditor ID="RadEditor1" runat="server" ContentAreaMode="Div" Skin="Sitefinity"
                        Height="562px" Width="747px">  
                      
                   </telerik:RadEditor>
                         
              
              


               <br />
     
        </div>
       
         

</asp:Content>
