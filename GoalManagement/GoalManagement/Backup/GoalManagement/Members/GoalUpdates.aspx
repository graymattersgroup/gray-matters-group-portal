﻿<%@ Page Title="Gray Matters Group - Goal Updates" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="GoalUpdates.aspx.cs" Inherits="GoalManagement.GoalsUpdates" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<style type="text/css">
.activityHeader
{ text-decoration:underline; color:Black; font-size:12pt; padding-top:10px; padding-bottom:5px;}

.activityItem { padding-left: 10px;}
.activitycheckbox { padding-top:3px; }
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">--%>
        <ContentTemplate>
            <div id="HelpDiv" class="HelpScreen">
                <asp:Literal ID="HelpLiteral" runat="server"></asp:Literal>
            </div>

            <table  width="100%">       
            <tr>
            <td >
                <img src='/Images/help.gif' onmouseover="javascript:showObj('HelpDiv');" onmouseout="javascript:hideObj('HelpDiv');" />&nbsp;&nbsp;
                <span style="color: #cc6731; font-size: 1.6em; font-weight: bold;"><asp:Label ID="MainTitle" runat="server" Text="Goal Progress"></asp:Label></span>&nbsp; 
                <asp:Literal ID="ProgressBar" runat="server" Text=""></asp:Literal>
                <br />
                <asp:Literal ID="UpdatePeriod" runat="server" ></asp:Literal>
                                
                <asp:Panel ID="UpdatePeriodPanel" runat="server" Visible="false">
                   Update Period: <asp:DropDownList ID="UpdatePeriodList" runat="server">
                    </asp:DropDownList>
                </asp:Panel>
                <asp:Panel ID="GoalUpdatePanel" runat="server" Visible="false">
                    <br />                                                       
                    
                    <strong>For <asp:Label ID="GoalName" runat="server" Text="Goal Name"></asp:Label></strong>
                    <br />
                    <br />
                    <b>My Payoff: </b><asp:Label ID="Payoff" runat="server" Text=""></asp:Label>
                    <br />
                    <br />
                    <span style=" font-size: 1.2em; "> Activities I've used in this progress period:</span>
                    <br />
                    <span style="">
                    <asp:CheckBoxList ID="ActivityList" Font-Size="9px" runat="server" TextAlign="Right" >
                    </asp:CheckBoxList>    
                    </span>
                    <br />
                    <br />
                    <table>
                        <tr>
                            <td style="vertical-align: middle">
                                <b>Goal Progress Status</b>:
                            </td>
                            <td style="padding-left: 10px; vertical-align: middle; text-align: center; color: Blue;">
                                <asp:RadioButtonList ID="GoalProgressRadioButtonList" runat="server" TextAlign="Left"
                                    RepeatDirection="Horizontal" Width="400px">
                                    <asp:ListItem Text=" None " Value="1"></asp:ListItem>
                                    <asp:ListItem Text=" Some " Value="2"></asp:ListItem>
                                    <asp:ListItem Text=" Significant " Value="3"></asp:ListItem>
                                    <asp:ListItem Text=" Completed " Value="4"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <asp:Label ID="Question1" runat="server" Text="Question 1"></asp:Label><br />
                    <asp:TextBox ID="Answer1" runat="server" Height="100px" Width="100%" TextMode="MultiLine"></asp:TextBox>
                    <br />
                    <br />
                    <asp:Label ID="Question2" runat="server" Text="Question 2"></asp:Label><br />
                    <asp:TextBox ID="Answer2" runat="server" Height="100px" Width="100%" TextMode="MultiLine"></asp:TextBox>
                    <br />
                  

                </asp:Panel>
                <asp:Panel ID="ImpactQuestionPanel" runat="server"  Visible="false">
                <br />
                   <%-- <h3>Finish Entering Goal Progress</h3>--%>
                    <br />
                    <asp:Label ID="ErrorMessage" runat="server" Text="Impact Question Required" ForeColor="Red" Visible="false"></asp:Label><br />
                    <asp:Label ID="ImpactQuestion" runat="server" Text="ImpactQuestion" ></asp:Label><br />
                    <br />
                    <asp:TextBox ID="UpdateAnswer" runat="server" Height="100px" Width="100%" TextMode="MultiLine"></asp:TextBox><br />  
                                    
                   
                </asp:Panel>
            </td>
            </tr>
            <tr>
            <td>
                <asp:Panel ID="SavePanel" runat="server" Visible="false">
               

                     <div style="width: 100%; text-align:left;">
                         <br />
                         <br />  
                     <asp:CheckBox ID="NotifyMyCoach" 
                         Text=" Notify my Coach that my updates are complete and I would like them reviewed" 
                         runat="server" AutoPostBack="True" 
                         oncheckedchanged="NotifyMyCoach_CheckedChanged" />
                     <br />
                     <br />
                <asp:Panel ID="CoachesFeedback" runat="server" Visible="false">
                Select any additional coaches you would like to notify.<br />
                <div style=" padding:20px;" >
                <asp:CheckBoxList ID="CoachesList" runat="server">
                </asp:CheckBoxList>
                         <br />
                         Message:
                         <asp:TextBox ID="Comments" runat="server" Text="I've completed my updates. "
                             Height="150px" Width="100%" TextMode="MultiLine"></asp:TextBox>
                     </asp:Panel>
                     <br />
                     </div>
                     </div>
                <div style="width: 100%; text-align: center;">
                     <asp:Button ID="SaveUpdateInformation" runat="server" BackColor="Navy" ForeColor="White"
                         Text="Save Goal Update Information" CssClass="ButtonBlue" OnClick="SaveUpdateInformation_Click"
                         OnClientClick="javascript:Waiting('Saving....');" />
                </div>
                </asp:Panel>
               
               </td>
            </tr>
            <tr>
               
            <td style="text-align:right;">
            <br />
                <asp:Button ID="SaveWork" runat="server" Text=" Save Work " Visible="false" OnClick="SaveWork_Click"
                    OnClientClick="javascript:Waiting('Saving....');" />&nbsp;
                <asp:Button ID="PrevButton" runat="server" Text=" <- Previous " Visible="false" OnClick="PrevButton_Click"
                    OnClientClick="javascript:Waiting();" />
                <asp:Button ID="NextButton" runat="server" Text=" Next -> " OnClick="NextButton_Click"
                    OnClientClick="javascript:Waiting();" />
               
            </td>
            </tr>         
           </table>
         
      </ContentTemplate>
   <%--   </asp:UpdatePanel>--%>
</asp:Content>
