﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Microsoft.Xrm.Sdk;
using Telerik.Reporting.Processing;
using Telerik.Web.UI;


namespace GoalManagement.Members
{
    public partial class coaching : System.Web.UI.Page
    {
        protected CRMAccess crmAccess;
        protected string ThisContactEmail;

        protected void Page_Load(object sender, EventArgs e)
        {
            ThisContactEmail = "";

            if (!IsPostBack)
            {
                if (Session["ImpersonateUser"] != null)
                {
                    var otherUser = (string)Session["ImpersonateUser"];
                    var masterPage1 = (SiteMaster)Page.Master;
                    masterPage1.SetImpersonation(otherUser);
                }

                if (Request.QueryString["specificstudent"] != null)
                {
                    ThisContactEmail = Request.QueryString["specificstudent"].ToString();
                }
                RadEditor1.Content = "<br/>";

                if (Session["crmAccess"] == null)
                {
                    crmAccess = new CRMAccess();
                    Session["crmAccess"] = crmAccess;
                }
                else
                {
                    crmAccess = (CRMAccess) Session["crmAccess"];
                }
                List<new_class> coachingClasses;
                
                var me = (Contact)Session["StudentInfo"];

                var isCoach = (bool)Session["isCoach"];
                
                if (isCoach)
                {
                    if (Session["CoachingClasses"] != null)
                    {
                        coachingClasses = (List<new_class>) Session["CoachingClasses"];
                    }
                    else
                    {
                        coachingClasses = crmAccess.GetCoachClasses(me.ContactId.Value).ToList();
                        Session["CoachingClasses"] = coachingClasses;
                    }

                    var filterClasses = (bool)Session["FilterClasses"];
                    foreach (new_class coachingClass in coachingClasses)
                    {
                        var skipClass = (coachingClass.new_ClassComplete.HasValue &&
                                       coachingClass.new_ClassComplete.Value && filterClasses);

                        if (!skipClass) ClassList.Items.Add(new ListItem(coachingClass.new_name, coachingClass.new_classId.ToString()));
                    }

                    var classId = Guid.Empty;

                    if (coachingClasses.Count == 1)
                    {
                        classId = (Guid)coachingClasses[0].new_classId;
                        Session["ClassId"] = classId;
                    }

                    if (Session["ClassId"] != null)
                    {
                        classId = (Guid)Session["ClassId"];
                        ClassList.SelectedValue = classId.ToString();
                    }

                    var theseEmailTemplates = crmAccess.GetEmailTemplates();
                    emailTemplates.Items.Add(new ListItem("None", ""));

                    int count = 0;
                    Session["CoachingTemplates"] = theseEmailTemplates;
                    foreach (new_coachingadvice newCoachingadvice in theseEmailTemplates)
                    {
                        emailTemplates.Items.Add(new ListItem(newCoachingadvice.new_name, count.ToString()));
                        count++;
                    }

                    var isAll = (ThisContactEmail != "");
                    
                    if (me.new_ClassAccess.Value == 4) isAll = false;

                    var myStudents = new ListItem("My Students&nbsp;", "1") {Selected = !isAll};
                    WhichStudents.Items.Add(myStudents);

                    if (me.new_ClassAccess.Value != 4) WhichStudents.Items.Add(new ListItem("All Students", "2") {Selected = isAll});
                        
                    SetSelectedClass(classId);

                    foreach (GridDataItem gridDataItem in RadGrid2.Items)
                    {
                        var emailname = gridDataItem["email"];

                        if( emailname.Text == ThisContactEmail)
                        {
                            gridDataItem.Selected = true;
                            ViewSelectedUser.Enabled = true;
                            UpdateEmailListNow();
                        }
                    }

                    if (ThisContactEmail != "")
                    {
                        if (Request.QueryString["viewinfo"] != null)
                        {
                            ViewThisSelectedUser(true);
                        }
                        else
                        {
                            ComposeEmail.Checked = true;
                            ComposeEmailNow();
                        }
                    }

                }
                else
                {

                    if (Session["MyClasses"] != null)
                    {
                        coachingClasses = (List<new_class>)Session["MyClasses"];
                    }
                    else
                    {
                        coachingClasses = crmAccess.GetStudentClasses(me.ContactId.Value).ToList();
                        Session["MyClasses"] = coachingClasses;
                    }

                    foreach (new_class coachingClass in coachingClasses)
                    {
                        ClassList.Items.Add(new ListItem(coachingClass.new_name, coachingClass.new_classId.ToString()));
                    }

                    var classId = coachingClasses[0].new_classId.Value;

                    var myStudents = new ListItem("My Team&nbsp;", "1") { Selected = true };
                    WhichStudents.Items.Add(myStudents);

                    if (me.new_ClassAccess != null)
                    {
                        // If Only See Class is my access level
                        if( me.new_ClassAccess.Value == 3) WhichStudents.Items.Add(new ListItem("Whole Class", "2"));
                    }
                    SetSelectedClass(classId);
                    LogFeedbackEmail.Visible = false;
                    StudentProgressPanel.Visible = false;
                  //  ViewSelectedUser.Text = "View Selected User Goals";
                }
                
                var masterPage = (SiteMaster)Page.Master;
                masterPage.SetMenu( CentralizedData.CoachStudentsName[isCoach? 0:1]);
                var db = new SqlAzureLayer(masterPage.GetConnectionString());

                var result =isCoach? 
                    db.GetDataTable("select content from [dbo].[ManagedContent] where name ='Coaching Help' "):
                    db.GetDataTable("select content from [dbo].[ManagedContent] where name ='Peer Review Help' ");
                
                if (result != null && result.Rows.Count > 0)
                {
                    HelpLiteral.Text = result.Rows[0][0].ToString();
                }

                SubjectLine.Text = string.Format("Rx in Action Aftercare Feedback for {0}", ProgressPeriods.SelectedItem.Text);

               

            }
            else
            {
                var studentTable = (DataTable)Session["StudentTable"];

                // studentTable.Columns.Remove("email");
                RadGrid2.DataSource = studentTable;
            }

        }


        private void SetClassIcon(Guid classId, Contact myContact)
        {
            crmAccess = (CRMAccess)Session["crmAccess"];
            var logoImage = Master.FindControl("RadBinaryImage1") as RadBinaryImage;

            try
            {
                IQueryable<Annotation> logo;

                if (myContact.ParentCustomerId != null)
                {
                    try
                    {
                        logo = crmAccess.GetClassAnnotation(myContact.ParentCustomerId.Id);
                        if (logo.FirstOrDefault() == null) logo = crmAccess.GetClassAnnotation(classId);
                    }
                    catch
                    {
                        logo = crmAccess.GetClassAnnotation(classId);
                    }


                }
                else
                {
                    logo = crmAccess.GetClassAnnotation(classId);
                }

                if (logo.FirstOrDefault() != null)
                {
                    logoImage.Visible = true;
                    logoImage.DataValue = Convert.FromBase64String(logo.FirstOrDefault().DocumentBody);
                    Session["Logo"] = logoImage.DataValue;
                }
                else
                {
                    Session.Remove("Logo");
                    logoImage.Visible = false;
                }
            }
            catch
            {
                Session.Remove("Logo");
                logoImage.Visible = false;
            }
        }


   

        protected void ComposeEmail_CheckedChanged(object sender, EventArgs e)
        {
            ComposeEmailNow();
        }

        protected void ComposeEmailNow()
        {

            EmailPanel.Visible = ComposeEmail.Checked;
            if (SendCoachEmail.Enabled)
            {
                SendCoachEmail.Enabled = !string.IsNullOrEmpty(EmailToList.Text);
            }
            ViewSelectedUser.Enabled = (RadGrid2.SelectedItems.Count == 1);
        }

        protected void RadGrid2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var coach = (Contact) Session["StudentInfo"];
            var iswholeClass = false;
            var emailwholeClass = false;

            if (coach.new_ClassAccess != null)
            {
                iswholeClass = (coach.new_ClassAccess.Value == 2);
            }

            if (coach.new_ClassAccess != null)
            {
                emailwholeClass = (coach.new_ClassAccess.Value == 2 || coach.new_ClassAccess.Value == 5);
            }


            string toEmail = "";

            ViewSelectedUser.Enabled = (RadGrid2.SelectedItems.Count == 1);
            ViewSelectedUserGoals.Enabled = (RadGrid2.SelectedItems.Count == 1);
            bool allMine = true;

            var rating = "No Rating";

            foreach (GridDataItem selectItem in RadGrid2.SelectedItems)
            {
                try
                {
                    rating = selectItem["Rating"].Text;
                }
                catch
                {
                    
                }

                var emailaddress = selectItem["Student"].Text;

                toEmail += emailaddress + ", ";

                if (!Convert.ToBoolean(selectItem["MyStudent"].Text)) allMine = false;
            }

            SendCoachEmail.Enabled = (emailwholeClass || allMine);

            LogFeedbackEmail.Enabled = (iswholeClass || allMine);

            FeedbackRating2.Visible = allMine;
            SetRatingLabel.Visible = allMine;


            if( RadGrid2.SelectedItems.Count == 1)
            {
                int feedbackRating;

                switch( rating.Trim())
                {
                    case "Poor":
                        feedbackRating = 1;
                        break;

                    case "Good":
                        feedbackRating = 2;
                        break;

                    case "Excellent":
                        feedbackRating = 3;
                        break;

                    default:
                        feedbackRating = 9;
                        break;
                }

                FeedbackRating2.SelectedValue = feedbackRating.ToString();
            }

            if (RadGrid2.SelectedItems.Count == 0)
            {
                EmailToList.Text = "";
                SendCoachEmail.Enabled = false;
                ViewSelectedUser.Enabled = false;
                LogFeedbackEmail.Enabled = false;
               
            }
            else
            {
                EmailToList.Text = toEmail.Substring(0, toEmail.Length - 2);
            }
        }

        protected void UpdateList_Click(object sender, EventArgs e)
        {
            SendCoachEmail.Enabled = !string.IsNullOrEmpty(EmailToList.Text);
        }

        protected void LogFeedbackEmail_Click(object sender, EventArgs e)
        {
            FeedbackEmail(true);
        }

        protected void SendCoachEmail_Click(object sender, EventArgs e)
        {
            FeedbackEmail(false);
        }

        protected void FeedbackEmail(bool logFeedBack)
        {
        var coach = (Contact)Session["StudentInfo"];
            var iswholeClass = false;
            if( coach.new_ClassAccess != null)
            {
                iswholeClass =(coach.new_ClassAccess.Value == 2);
            }
            var crmAccess = (CRMAccess)Session["crmAccess"];
              
            string toEmail = "";
            foreach (GridDataItem selectItem in RadGrid2.SelectedItems)
            {
                var emailaddress = selectItem["email"].Text;

                if (iswholeClass ||Convert.ToBoolean(selectItem["MyStudent"].Text)) toEmail += emailaddress + ";";
            }
           
             var students = (List<Contact>)Session["Students"];
             var subject = SubjectLine.Text;
             var selectedPeriod = (Guid)Session["SelectedPeriodId"];
             var mycoaching = new List<new_coaching>();

           

            var ccList = new List<string>();
            foreach (ListItem coachCC in CoachesList.Items)
            {
                if (coachCC.Selected)
                {
                    var myAdvice = string.Format("{0}",RadEditor1.Content);
                    myAdvice = myAdvice.Insert(0,"Email sent to the following students" + toEmail);
                    ccList.Add(coachCC.Value);
                    EmailNotifications.sendEmailAllCC(new List<string>(), coachCC.Value, coach.EMailAddress1, subject, myAdvice,null, true, false, true);
                }
            }

            
           
                var allEmail = toEmail.Split(';');
                foreach (Contact student in students)
                {
                    if (allEmail.Contains(student.EMailAddress1))
                    {
                        var myAdvice = RadEditor1.Content.Replace("#studentfirstname#", (student.FirstName));
                        var name = string.Format("Coaching");

                      

                        EmailNotifications.sendEmailAllCC(new List<string>(), student.EMailAddress1, coach.EMailAddress1, subject, myAdvice,null, true, false, true);

                      

                        if (logFeedBack)
                        {
                            var coaching = new new_coaching
                                    {
                                        new_CoachId = new EntityReference("contact", coach.ContactId.Value),
                                        new_Advice = myAdvice,
                                        new_ClassUpdatePeriodId =new EntityReference("new_classupdateperiod", selectedPeriod),
                                        new_StudentId =new EntityReference("contact", student.ContactId.Value),
                                        new_name = name,
                                        new_UpdateGrade = new OptionSetValue(Convert.ToInt32(FeedbackGrade.SelectedValue)),
                                    };

                            mycoaching.Add(coaching);
                        }

                    }
                }

                //Save coaching
                if (logFeedBack)
                {
                    crmAccess.SaveCoaching(mycoaching);
                    UpdateProcessPeriod(selectedPeriod);
                }
        }


        //protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        //{
        //    if (e.Item is GridDataItem)
        //    {
        //        var item = (GridDataItem)e.Item;
        //        //var hl = new HyperLink();
        //        //hl.NavigateUrl = "Default.aspx?id=" + item.GetDataKeyValue("AccountID").ToString();
        //        //hl.Text = item.GetDataKeyValue("AccountName").ToString();

        //    //    item["CustomerIDLink"].Controls.Add(hl);
        //    }
        //}  



        protected void ProgressPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
             var selectedPeriod = new Guid(ProgressPeriods.SelectedValue);
            UpdateProcessPeriod(selectedPeriod);

            SubjectLine.Text = string.Format("Rx in Action Aftercare Feedback for {0}", ProgressPeriods.SelectedItem.Text);
        }


        protected void UpdateProcessPeriod(Guid selectedPeriod)
        {
            crmAccess = (CRMAccess) Session["crmAccess"];
            var classId = new Guid(ClassList.SelectedValue);
            Session["SelectedPeriodId"] = selectedPeriod;

            // Get all Students for this Class
            var students = crmAccess.GetClassStudents( classId);
            
            var coaches = crmAccess.GetClassCoaches(classId);
            var studentUpdates = crmAccess.GetStudentUpdates(selectedPeriod);
            var coachFeedback = crmAccess.GetCoachFeedback(selectedPeriod);

            Session["Students"] = students;
            Session["Coaches"] = coaches;
            Session["StudentUpdates"] = studentUpdates;
            Session["CoachFeedback"] = coachFeedback;

            FeedbackRating2.Visible = false;
            SetRatingLabel.Visible = false;

            BuildStudentTable();
        }

        protected void BuildStudentTable()
        {
           
            var coach = (Contact)Session["StudentInfo"];
            var students = (List<Contact>)Session["Students"];
            var coaches = (List<Contact>)Session["Coaches"];
            var studentUpdates = (List<new_studentupdateperiod>)Session["StudentUpdates"];
            var coachFeedback = (List<new_coaching>)Session["CoachFeedback"];

            var studentTable = new DataTable("students");
            
            var isAll = (WhichStudents.SelectedValue == "2");
            var isCoach = (bool)Session["isCoach"];

            studentTable.Columns.Add("Student");
            studentTable.Columns.Add("Status");
            studentTable.Columns.Add("Feedback");
            if(isCoach) studentTable.Columns.Add("Rating");
            studentTable.Columns.Add("Coach");
            studentTable.Columns.Add("email");
            studentTable.Columns.Add("MyStudent");

            var selectedPeriod = (Guid)Session["SelectedPeriodId"];

            foreach (Contact student in students)
            {
                var row = studentTable.NewRow();

                var myCoach = "none selected";
                bool myTeam = false;
                foreach (Contact contact in coaches)
                {
                    if (student.new_MyCoachId != null)

                        if (contact.ContactId == student.new_MyCoachId.Id)
                        {
                            myCoach = contact.FirstName + " " + contact.LastName;

                            if (isCoach)
                            {
                                // Students where I am the coach
                                myTeam = (contact.ContactId == coach.ContactId);
                            }
                            else
                            {
                                // Students where we have the same coach
                                myTeam = (coach.new_MyCoachId.Id == student.new_MyCoachId.Id);
                            }

                        }
                }

                var progress = "none";
                var coachStatus = "none";
                var rating = "none";
                DateTime? updatedWhen = null;
                foreach (new_studentupdateperiod period in studentUpdates)
                {
                    if (period.new_StudentId.Id == student.ContactId)
                    {
                        progress = "Submitted";
                        updatedWhen = period.ModifiedOn;


                    
                    }
                }

                foreach (new_coaching coaching in coachFeedback)
                {
                    if (coaching.new_ClassUpdatePeriodId.Id == selectedPeriod &&
                        coaching.new_StudentId.Id == student.ContactId)
                    {
                        coachStatus = coaching.ModifiedOn.Value.ToShortDateString();

                        if (coaching.new_UpdateGrade != null)
                        {
                            switch (coaching.new_UpdateGrade.Value)
                            {
                                case 1:
                                    rating = "Poor";
                                    break;

                                case 2:
                                    rating = "Good";
                                    break;

                                case 3:
                                    rating = "Excellent";
                                    break;

                                case 9:
                                    rating = "No Rating";
                                    break;
                            }

                        }
                    }
                }

                row["Student"] = student.FirstName + " " + student.LastName;
                row["Status"] = progress + " " + ((updatedWhen == null) ? "" : updatedWhen.Value.ToShortDateString());
                row["Feedback"] = coachStatus;
                row["Coach"] = myCoach;
                row["email"] = student.EMailAddress1;
                row["MyStudent"] = myTeam;

                if (isCoach) row["Rating"] = rating;

                if( isAll || myTeam ) studentTable.Rows.Add(row);
            }
            Session["StudentTable"] = studentTable;
            
           // studentTable.Columns.Remove("email");
            RadGrid2.DataSource = studentTable;

          
            RadGrid2.Rebind();
            
        }

        protected void ClassList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var classId = new Guid(ClassList.SelectedValue);

            Session.Remove("Logo");
            var coach = (Contact)Session["StudentInfo"];

            SetClassIcon(classId, coach);

            SetSelectedClass(classId);
            
        }

        protected void RadGrid1_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            if (e.Column.UniqueName == "email" || e.Column.UniqueName == "MyStudent")
            {
                e.Column.Visible = false;
            }
        }


        protected void SetSelectedClass(Guid classId)
        {
            Session["classId"] = classId;
            crmAccess = (CRMAccess)Session["crmAccess"];
            var coaches = crmAccess.GetCoaches(classId);

            Session["Coaches"] = coaches;

            // Get My Students Update Periods
            var allUpdates = crmAccess.GetClassStudentUpdatePeriods(classId);
            var selectedPeriod = Guid.Empty;
            ProgressPeriods.Items.Clear();
            foreach (StudentUpdatePeriod studentUpdatePeriod in allUpdates)
            {
                ProgressPeriods.Items.Add(new ListItem(studentUpdatePeriod.StudentName + string.Format(" {0} to {1}",
                                               studentUpdatePeriod.startDate.ToLocalTime().ToString("MM/dd/yyyy"),
                                               studentUpdatePeriod.endDate.ToLocalTime().ToString("MM/dd/yyyy")), studentUpdatePeriod.PeriodId.ToString()));

                if( studentUpdatePeriod.endDate < DateTime.Now)
                {
                    selectedPeriod = studentUpdatePeriod.PeriodId;
                    Session["SelectedPeriodId"] = selectedPeriod;
                    ProgressPeriods.SelectedValue = studentUpdatePeriod.PeriodId.ToString();
                }
            }

            if (selectedPeriod == Guid.Empty)
            {
                selectedPeriod = allUpdates[0].PeriodId;
                ProgressPeriods.SelectedIndex = 0;
                Session["SelectedPeriodId"] = selectedPeriod;
            }
            
            CoachesList.Items.Clear();

            foreach (Contact contact in coaches)
            {
                var aCoach = new ListItem(contact.FirstName + " " + contact.LastName + "   &nbsp;&nbsp;&nbsp;", contact.EMailAddress1);
                
                CoachesList.Items.Add(aCoach);
            }


            UpdateProcessPeriod(selectedPeriod);
        }

        protected void emailTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            var theseEmailTemplates = (List<new_coachingadvice>) Session["CoachingTemplates"];
            var count = 0;
            foreach (new_coachingadvice newCoachingadvice in theseEmailTemplates)
            {
                if (count.ToString() == emailTemplates.SelectedValue)
                {
                    RadEditor1.Content = newCoachingadvice.new_Advice;
                    break;
                }
                count++;
            }
            
           // FeedbackText.Text = emailTemplates.SelectedValue;
        }

        protected void ViewSelectedUser_Click(object sender, EventArgs e)
        {
           ViewThisSelectedUser( true);
        }


        protected void ViewSelectedUserGoals_Click(object sender, EventArgs e)
        {
            ViewThisSelectedUser(false);
        }

        protected void ViewThisSelectedUser(bool isStatus)
        {
            var classId = new Guid(ClassList.SelectedValue);
           
            crmAccess = (CRMAccess)Session["crmAccess"];

            if (Session["Coaches"] == null)
            {
                Session["Coaches"] = crmAccess.GetClassCoaches(classId);
            }
            
            var coaches = (List<Contact>)Session["Coaches"];

            if (RadGrid2.SelectedItems.Count == 1)
            {
                foreach (GridDataItem selectItem in RadGrid2.SelectedItems)
                {
                    var studentName = selectItem["Student"].Text;
                    var emailaddress = selectItem["email"].Text;
                    StudentName.Text = studentName;
                    var student = crmAccess.GetStudent(emailaddress);
                    var studentId = student.ContactId.Value;
                    Session["studentId"] = studentId;

                    studentPanel.Visible = true;
                    StudentName.Text = student.FullName;

                    var isCoach = (bool)Session["isCoach"];

                    var coachFeedback = crmAccess.GetCoachFeedbackbyStudent(classId, studentId);

                    if (isCoach && isStatus)
                    {
                        if (selectItem["MyStudent"].Text == "True")
                        {
                            GradePanel.Visible = true;
                            CurrentProgressPeriod.Text = ProgressPeriods.SelectedItem.Text;
                        }
                        else
                        {
                            GradePanel.Visible = false;
                        }

                        //Update the Grade.
                        foreach (new_coaching newCoaching in coachFeedback)
                        {
                            if (newCoaching.new_StudentId.Id == studentId &&
                                newCoaching.new_ClassUpdatePeriodId.Id == new Guid(ProgressPeriods.SelectedValue))
                            {
                                FeedbackGrade.SelectedValue = newCoaching.new_UpdateGrade.Value.ToString();
                            }
                        }

                        StudentHistoryLiteral.Text = ReportHelper.ViewThisSelectedUser(classId, crmAccess, student, coaches, coachFeedback);
                    }
                    else
                    {
                        GradePanel.Visible = false;

                        var sql = new SqlAzureLayer(GetConnectionString());

                        StudentName.Text = "Goals for: " + StudentName.Text;
                        StudentHistoryLiteral.Text = ReportHelper.ViewThisSelectedUsersGoals(classId, studentId, sql, crmAccess);
                    }

                }

            }
        }

 
        protected void ExitStudentHistory_Click(object sender, EventArgs e)
        {
            studentPanel.Visible = false;
        }

        protected void WhichStudents_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuildStudentTable();
        }

        private string GetConnectionString()
        {

            var connectionString2 = ConfigurationManager.ConnectionStrings["Production"].ConnectionString;

            return connectionString2;
        }
        

        protected void UpdateEmailList_Click(object sender, EventArgs e)
        {
            UpdateEmailListNow();
        }

        protected void UpdateEmailListNow()
        {

            var coach = (Contact)Session["StudentInfo"];
            var iswholeClass = false;

            if (coach.new_ClassAccess != null)
            {
                iswholeClass = (coach.new_ClassAccess.Value == 2);
            }
            string toEmail = "";

            ViewSelectedUser.Enabled = (RadGrid2.SelectedItems.Count == 1);

            bool allMine = true;
            foreach (GridDataItem selectItem in RadGrid2.SelectedItems)
            {
                var emailaddress = selectItem["Student"].Text;

                toEmail += emailaddress + ", ";

                if (!Convert.ToBoolean(selectItem["MyStudent"].Text)) allMine = false;
            }

            SendCoachEmail.Enabled = true;
            LogFeedbackEmail.Enabled = (iswholeClass || allMine);

            if (RadGrid2.SelectedItems.Count == 0)
            {
                EmailToList.Text = "";
                SendCoachEmail.Enabled = false;
                ViewSelectedUser.Enabled = false;
                LogFeedbackEmail.Enabled = false;
            }
            else
            {
                EmailToList.Text = toEmail.Substring(0, toEmail.Length - 2);
            }
        }

        protected void FeedbackGrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            crmAccess = (CRMAccess)Session["crmAccess"];
            var selectedPeriod = new Guid(ProgressPeriods.SelectedValue);
            //Update the Grade.
            var studentId = (Guid) Session["studentId"];

            var coachFeedback = (List<new_coaching>) Session["CoachFeedback"];
            var feedbackExists = false;

            foreach (new_coaching newCoaching in coachFeedback)
            {
                if( newCoaching.new_StudentId.Id == studentId)
                {
                    newCoaching.new_UpdateGrade = new OptionSetValue(Convert.ToInt32(FeedbackGrade.SelectedValue));
                    crmAccess.UpdateCoachingGrade(newCoaching);
                    feedbackExists = true;
                    break;
                }
            }

           
            // Create Feedback with only grade
            if (!feedbackExists)
            {
                var coach = (Contact)Session["StudentInfo"];

                var coaching = new new_coaching
                {
                    new_CoachId = new EntityReference("contact", coach.ContactId.Value),
                    new_Advice = "Grade Only",
                    new_ClassUpdatePeriodId = new EntityReference("new_classupdateperiod", selectedPeriod),
                    new_StudentId = new EntityReference("contact", studentId),
                    new_name = "Coaching",
                    new_UpdateGrade = new OptionSetValue(Convert.ToInt32(FeedbackGrade.SelectedValue)),
                };

                var coachingList = new List<new_coaching>();
                coachingList.Add(coaching);
                crmAccess.SaveCoaching(coachingList);
                
            }

            selectedPeriod = new Guid(ProgressPeriods.SelectedValue);
            UpdateProcessPeriod(selectedPeriod);
   
        }


        protected void FeedbackGrade2_SelectedIndexChanged(object sender, EventArgs e)
        {
            crmAccess = (CRMAccess)Session["crmAccess"];
            var coach = (Contact)Session["StudentInfo"];
            var selectedPeriod = (Guid)Session["SelectedPeriodId"];

            var coachingList = new List<new_coaching>();

            foreach (GridDataItem selectItem in RadGrid2.SelectedItems)
            {
                var studentName = selectItem["Student"].Text;
                var emailaddress = selectItem["email"].Text;
                StudentName.Text = studentName;
                var student = crmAccess.GetStudent(emailaddress);
                var studentId = student.ContactId.Value;

                //Update the Grade.
                var coachFeedback = (List<new_coaching>) Session["CoachFeedback"];
                var feedbackExists = false;

                foreach (new_coaching newCoaching in coachFeedback)
                {
                    if (newCoaching.new_StudentId.Id == studentId)
                    {
                        newCoaching.new_UpdateGrade = new OptionSetValue(Convert.ToInt32(FeedbackRating2.SelectedValue));
                        crmAccess.UpdateCoachingGrade(newCoaching);
                        feedbackExists = true;
                        break;
                    }
                }

                // Create Feedback with only grade
                if(!feedbackExists)
                {
                    var coaching = new new_coaching
                    {
                        new_CoachId = new EntityReference("contact", coach.ContactId.Value),
                        new_Advice = "Grade Only",
                        new_ClassUpdatePeriodId = new EntityReference("new_classupdateperiod", selectedPeriod),
                        new_StudentId = new EntityReference("contact", student.ContactId.Value),
                        new_name = "Coaching",
                        new_UpdateGrade = new OptionSetValue(Convert.ToInt32(FeedbackRating2.SelectedValue)),
                    };

                  
                    coachingList.Add(coaching);
                }
            }

            if( coachingList.Count != 0)
            {
                crmAccess.SaveCoaching(coachingList);
            }

          
            // Update Grid with new Feedback scores
            UpdateProcessPeriod(selectedPeriod);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EmailClassGoals_Click(object sender, EventArgs e)
        {
            //TODO: Need to write this to be a single report.

            //RenderingResult result = ReportHelper.GetTelerikGoalReport(classGoalPages, myContact);

            //var reportbytearray = result.DocumentBytes;

            //var toemailaddress = Session["userEmail"].ToString();
            //var fromemailaddress = "RxAssist@graymattersgroup.com";
            //var subject = "Your Goal Commitments";
            //var filename = "mygoals";
            //var text = "Attached are your goal commitments.";

            //EmailNotifications.sendEmailAll(toemailaddress, fromemailaddress, subject, text, reportbytearray, filename, false, false);
        }


     

        //private List<new_classgoal> GetGoals(Guid classId)
        //{
        //    var goals = new List<new_classgoal>();

        //    var sql = new SqlAzureLayer(GetConnectionString());

        //    var dbGoals = sql.GetDataTable(string.Format("select goalid,name,min,max,isdefault,sortorder from dbo.Goal where classid={0}", sql.FormatGuid(classId)));

        //    if (dbGoals != null && dbGoals.Rows.Count != 0)
        //    {
        //        foreach (DataRow aGoal in dbGoals.Rows)
        //        {
        //            var newGoal = new new_classgoal
        //            {
        //                new_classgoalId = sql.GetDataRowGuid(aGoal[0]),
        //                new_name = sql.GetDataRowString(aGoal[1]),
        //                new_MinimumActivities = sql.GetDataRowInt(aGoal[2]),
        //                new_MaximumActivities = sql.GetDataRowInt(aGoal[3]),
        //                new_DefaultGoal = sql.GetDataRowBool(aGoal[4]),
        //                new_SortOrder = sql.GetDataRowInt(aGoal[5])
        //            };
        //            goals.Add(newGoal);
        //        }
        //    }
        //    return goals;
        //}

        //private ClassGoals GetClassGoals(Guid classId)
        //{
        //    crmAccess = (CRMAccess)Session["crmAccess"];
        //    var sql = new SqlAzureLayer(GetConnectionString());
        //    var blob = sql.GetGoalBlob(classId);

        //    if (blob == null)
        //    {
        //        // Get all Students
        //        var goals = crmAccess.GetClassGoals(classId).ToList();

        //        blob = new ClassGoals();

        //        foreach (ClassGoal classGoal in goals)
        //        {
        //            if (classGoal.isdefault ?? false)
        //                classGoal.selected = true;

        //            var activities = crmAccess.GetGoalActivities(classGoal.id).ToList();

        //            foreach (Activity newGoalactivity in activities)
        //            {
        //                var subactivities = crmAccess.GetSubActivities(newGoalactivity.id).ToList();

        //                foreach (SubActivity subActivity in subactivities)
        //                {
        //                    newGoalactivity.subActivities.Add(subActivity);
        //                }
        //                classGoal.activities.Add(newGoalactivity);
        //            }

        //            blob.classGoals.Add(classGoal);
        //        }

        //        blob.classid = classId;
        //        blob.lastupdated = DateTime.Now;
        //        sql.UpdateGoalBlob(blob);

        //    }
        //   return blob;
        //}


        //protected void ViewThisSelectedUser()
        //{
        //    var isCoach = (bool)Session["isCoach"];
        //    var coaches = (List<Contact>)Session["Coaches"];
        //    studentPanel.Visible = true;

        //    string[] progressVAlues = new string[] { "", "None", "Some", "Significant", "Completed" };

        //    crmAccess = (CRMAccess)Session["crmAccess"];

        //    if (RadGrid2.SelectedItems.Count == 1)
        //    {
        //        foreach (GridDataItem selectItem in RadGrid2.SelectedItems)
        //        {
        //            var studentName = selectItem["Student"].Text;
        //            var emailaddress = selectItem["email"].Text;
        //            StudentName.Text = studentName;
        //            var studentInfo = crmAccess.GetStudent(emailaddress);
        //            var studentId = studentInfo.ContactId.Value;
        //            Session["studentId"] = studentId;
        //            var classId = new Guid(ClassList.SelectedValue);
        //            var allUpdates = crmAccess.GetAllUpdatePeriods(studentId, classId);
        //            var coachFeedback = crmAccess.GetCoachFeedbackbyStudent(classId, studentId);

        //            // Build complete summary of Updates and feedback.

        //            var sb = new StringBuilder(256);

        //            if (isCoach)
        //            {
        //                if (selectItem["MyStudent"].Text == "True")
        //                {
        //                    GradePanel.Visible = true;
        //                    CurrentProgressPeriod.Text = ProgressPeriods.SelectedItem.Text;
        //                }
        //                else
        //                {
        //                    GradePanel.Visible = false;
        //                }
        //                //Update the Grade.
        //                foreach (new_coaching newCoaching in coachFeedback)
        //                {
        //                    if (newCoaching.new_StudentId.Id == studentId && newCoaching.new_ClassUpdatePeriodId.Id == new Guid(ProgressPeriods.SelectedValue))
        //                    {
        //                        FeedbackGrade.SelectedValue = newCoaching.new_UpdateGrade.Value.ToString();
        //                    }
        //                }

        //                foreach (StudentUpdatePeriod thisUpdatePeriod in allUpdates)
        //                {
        //                    var myUpdate = thisUpdatePeriod;
        //                    myUpdate = crmAccess.GetMyUpdatePeriod(studentInfo.ContactId.Value,
        //                                                           thisUpdatePeriod.ClassUpdatePeriodId,
        //                                                           thisUpdatePeriod);

        //                    if (thisUpdatePeriod.ClassId != classId) continue;
        //                    var start = myUpdate.startDate;
        //                    var end = myUpdate.endDate;
        //                    sb.AppendFormat("<b>Update Period {0} - {1}</b><br/>", start.ToShortDateString(),
        //                                    end.ToShortDateString());



        //                    if (myUpdate.PeriodGoalAnswers.Count == 0 ||
        //                        myUpdate.PeriodGoalAnswers[0].ProgressValue == -1)
        //                    {
        //                        sb.AppendFormat("No Updates.");
        //                        sb.AppendFormat("<br/>");
        //                        sb.AppendFormat("<br/>");



        //                        bool coachingDiv = false;

        //                        foreach (new_coaching newCoaching in coachFeedback)
        //                        {
        //                            if (newCoaching.new_ClassUpdatePeriodId.Id == myUpdate.PeriodId)
        //                            {
        //                                if (coachingDiv == false)
        //                                {
        //                                    coachingDiv = true;
        //                                    sb.AppendFormat(
        //                                        "<div style='color:green; border: 1px solid red; padding:5px;'>");
        //                                }

        //                                var coachName = "";
        //                                foreach (Contact coach in coaches)
        //                                {
        //                                    if (newCoaching.new_CoachId.Id == coach.ContactId)
        //                                    {
        //                                        coachName = coach.FirstName + " " + coach.LastName;
        //                                        break;
        //                                    }

        //                                }
        //                                sb.AppendFormat("<b>Coach Feedback from {0} on {1}</b><br/>", coachName,
        //                                                newCoaching.ModifiedOn.Value.ToShortDateString());

        //                                if (newCoaching.new_UpdateGrade != null)
        //                                    sb.AppendFormat("Progress Grade:  {0}<br/><br/>",
        //                                                    newCoaching.new_UpdateGrade.Value);



        //                                sb.AppendFormat("{0}", newCoaching.new_Advice);
        //                                sb.AppendFormat("<br/>");

        //                            }
        //                        }
        //                        if (coachingDiv)
        //                        {
        //                            sb.Append("</div>");
        //                            sb.Append("<hr/>");
        //                            sb.AppendFormat("<br/>");
        //                        }


        //                        continue;
        //                    }


        //                    sb.AppendFormat("<br/>");
        //                    sb.AppendFormat(
        //                        "<b>Impact Question</b><br/> {0} <br/><span class='myanswers'>{1}</span><br/><br/>",
        //                        myUpdate.PeriodImpactQuestion, myUpdate.ImpactAnswer);
        //                    sb.AppendFormat("<br/>");
        //                    foreach (GoalAnswers answer in myUpdate.PeriodGoalAnswers)
        //                    {
        //                        var progress = answer.ProgressValue;
        //                        if (progress == -1) progress = 0;

        //                        sb.AppendFormat(
        //                            "<hr/><b>{0}</b> <span class='myanswers'>(progress - {1})</span><br/><br/>",
        //                            answer.GoalName, progressVAlues[progress]);
        //                        sb.AppendFormat("{0}: <br/><span class='myanswers'>{1}</span><br/><br/>",
        //                                        myUpdate.GoalQuestion1, answer.Answer1);
        //                        sb.AppendFormat("{0}: <br/><span class='myanswers'>{1}</span><br/>",
        //                                        myUpdate.GoalQuestion2, answer.Answer2);
        //                        sb.AppendFormat("<br/>");

        //                        sb.AppendFormat("I used the following activities in this goal period:<br/>");


        //                        bool noActivities = true;
        //                        foreach (Activities act in answer.MyActivities)
        //                        {
        //                            if (act.IsChecked)
        //                            {
        //                                noActivities = false;
        //                                sb.AppendFormat("- <span class='myanswers'>{0}</span><br/>", act.Text);
        //                            }
        //                        }

        //                        if (noActivities) sb.AppendFormat("- No Activities Selected");


        //                        sb.AppendFormat("<br/><br/>");
        //                    }

        //                    sb.AppendFormat("Feedback Request: <br/>");
        //                    sb.AppendFormat("<span class='myanswers'>{0}</span><br/><br/>",
        //                                    thisUpdatePeriod.FeedbackRequest);

        //                    sb.AppendFormat("<div style='color:green; border: 1px solid red; padding:5px;'>");
        //                    foreach (new_coaching newCoaching in coachFeedback)
        //                    {
        //                        if (newCoaching.new_ClassUpdatePeriodId.Id == myUpdate.PeriodId)
        //                        {
        //                            var coachName = "";
        //                            foreach (Contact coach in coaches)
        //                            {
        //                                if (newCoaching.new_CoachId.Id == coach.ContactId)
        //                                {
        //                                    coachName = coach.FirstName + " " + coach.LastName;
        //                                    break;
        //                                }

        //                            }
        //                            sb.AppendFormat("<b>Coach Feedback from {0} on {1}</b><br/>", coachName,
        //                                            newCoaching.ModifiedOn.Value.ToShortDateString());

        //                            if (newCoaching.new_UpdateGrade != null)
        //                                sb.AppendFormat("Progress Grade:  {0}<br/><br/>",
        //                                                newCoaching.new_UpdateGrade.Value);



        //                            sb.AppendFormat("{0}", newCoaching.new_Advice);
        //                            sb.AppendFormat("<br/>");

        //                        }
        //                    }
        //                    sb.Append("</div>");

        //                    sb.Append("<hr/>");
        //                    sb.AppendFormat("<br/>");
        //                }
        //            }
        //            else
        //            {
        //                GradePanel.Visible = false;
        //                var sql = new SqlAzureLayer(GetConnectionString());
        //                var goals = ReportHelper.GetClassGoals(classId, crmAccess, sql);
        //                // Selected Goals
        //                var selectedGoals = crmAccess.GetStudentGoals(classId, studentId);

        //                var classGoalPages = new List<ClassGoal>();

        //                // Loop through all Goals and create a list of all Goals and activities
        //                foreach (ClassGoal classGoal in goals.classGoals)
        //                {
        //                    //var classGoal = new ClassGoal
        //                    //                    {
        //                    //                        id = newClassgoal.new_classgoalId.Value,
        //                    //                        name = newClassgoal.new_name,
        //                    //                        selected = false,
        //                    //                        min = newClassgoal.new_MinimumActivities ?? 0,
        //                    //                        max = newClassgoal.new_MaximumActivities ?? 0
        //                    //                    };

        //                    //if (newClassgoal.new_DefaultGoal.HasValue)
        //                    //{
        //                    //    classGoal.isdefault = newClassgoal.new_DefaultGoal.Value;
        //                    //    classGoal.selected = newClassgoal.new_DefaultGoal.Value;
        //                    //}

        //                    foreach (new_studentgoals studentgoal in selectedGoals)
        //                    {
        //                        if (classGoal.id == studentgoal.new_classgoalId.Id)
        //                        {
        //                            classGoal.selected = true;
        //                            classGoal.initialselected = true;
        //                            classGoal.payoff = studentgoal.new_Benefit;
        //                            classGoal.initialpayoff = studentgoal.new_Benefit;

        //                            var goalActivities = GetGoalActivities(studentgoal.new_classgoalId.Id);

        //                            foreach (Activity activity in goalActivities)
        //                            {
        //                                var anActivity = new Activity
        //                                {
        //                                    name = activity.name,
        //                                    id = activity.id,
        //                                    selected = false
        //                                };

        //                                var subactivities = GetSubActivities(activity.id);

        //                                foreach (SubActivity newSubactivity in subactivities)
        //                                {
        //                                    var subActivity = new SubActivity
        //                                    {
        //                                        id = newSubactivity.id,
        //                                        name = newSubactivity.name,
        //                                        selected = false,
        //                                    };
        //                                    anActivity.subActivities.Add(subActivity);
        //                                }

        //                                classGoal.activities.Add(anActivity);
        //                            }

        //                            var selectedGoalActivities =
        //                                crmAccess.GetStudentGoalActivities(classId, studentId,
        //                                                                   studentgoal.new_classgoalId.Id).ToList();

        //                            // If there are no selected activities than this is all subactivities
        //                            if (selectedGoalActivities.Count != 0)

        //                                foreach (new_goalactivity selectedGoalActivity in selectedGoalActivities)
        //                                {
        //                                    foreach (Activity anActivity in classGoal.activities)
        //                                    {
        //                                        if (selectedGoalActivity.new_goalactivityId.Value == anActivity.id)
        //                                        {
        //                                            anActivity.selected = true;
        //                                            anActivity.initialselected = true;
        //                                        }
        //                                    }
        //                                }
        //                            else
        //                            {
        //                                var selectedGoalSubActivities =
        //                                    crmAccess.GetStudentGoalSubActivities(classId, studentId,
        //                                                                          studentgoal.new_classgoalId.Id).ToList();

        //                                foreach (new_subactivity selectedGoalSubActivity in selectedGoalSubActivities)
        //                                {
        //                                    foreach (Activity anActivity in classGoal.activities)
        //                                    {
        //                                        foreach (SubActivity aSubActivity in anActivity.subActivities)
        //                                        {
        //                                            if (aSubActivity.id ==
        //                                                selectedGoalSubActivity.new_subactivityId.Value)
        //                                            {
        //                                                aSubActivity.selected = true;
        //                                                aSubActivity.initialselected = true;
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }


        //                            // loadedGoals.Add(studentgoal.new_classgoalId.Id);
        //                        }
        //                    }


        //                    classGoalPages.Add(classGoal);
        //                }


        //                StudentName.Text = "Goals for: " + StudentName.Text;

        //                // Generate Table to send to Telerik report engine to generate an email with PDF attachment to the student.
        //                foreach (ClassGoal goalPage in classGoalPages)
        //                {
        //                    if (!goalPage.selected) continue;

        //                    sb.AppendFormat("<H2>{0}</H2><br/>", goalPage.name);
        //                    sb.AppendFormat("<b>Payoff:</b>  {0}<br/><br/>", goalPage.payoff);

        //                    sb.AppendFormat("<b>Activities:</b><br/>");
        //                    sb.AppendFormat("<ul>");
        //                    foreach (Activity newGoalactivity in goalPage.activities)
        //                    {
        //                        if (!newGoalactivity.selected && newGoalactivity.subActivities.Count == 0) continue;

        //                        if (newGoalactivity.subActivities.Count != 0)
        //                        {
        //                            foreach (SubActivity newSubactivity in newGoalactivity.subActivities)
        //                            {
        //                                if (newSubactivity.selected)
        //                                {
        //                                    sb.AppendFormat("<li>{0}</li>", newSubactivity.name);
        //                                }

        //                            }
        //                        }
        //                        else if (newGoalactivity.selected)
        //                        {
        //                            sb.AppendFormat("<li>{0}</li>", newGoalactivity.name);
        //                        }
        //                    }
        //                    sb.AppendFormat("</ul>");
        //                    sb.AppendFormat("<hr/>");
        //                    sb.AppendFormat("<br/>");
        //                }
        //            }

        //            StudentHistoryLiteral.Text = sb.ToString();

        //        }

        //    }
        //}



        //private void SetClassIcon(Guid classId, Guid accountId)
        //{
        //    var logoImage = Master.FindControl("RadBinaryImage1") as RadBinaryImage;
        //    try
        //    {
        //        if (Session["Logo"] != null)
        //        {
        //            logoImage.DataValue = (byte[])Session["Logo"];
        //            logoImage.Visible = true;
        //        }
        //        else
        //        {
        //            try
        //            {
        //                var logo = crmAccess.GetClassAnnotation(accountId);
        //                if (logo.FirstOrDefault() == null) logo = crmAccess.GetClassAnnotation(classId);
        //                if (logo.FirstOrDefault() != null)
        //                {
        //                    logoImage.Visible = true;
        //                    logoImage.DataValue = Convert.FromBase64String(logo.FirstOrDefault().DocumentBody);
        //                }
        //                else
        //                {
        //                    logoImage.Visible = false;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                var logo = crmAccess.GetClassAnnotation(classId);
        //                if (logo.FirstOrDefault() != null)
        //                {
        //                    logoImage.Visible = true;
        //                    logoImage.DataValue = Convert.FromBase64String(logo.FirstOrDefault().DocumentBody);
        //                }
        //                else
        //                {
        //                    logoImage.Visible = false;
        //                }
        //            }


        //        }

        //    }
        //    catch (Exception ex1)
        //    {
        //        logoImage.Visible = false;
        //    }
        //}

        //protected void SetMenu()
        //{
        //    var thisPage = Master.FindControl("PageTitle") as Label;
        //    var myMenu = Master.FindControl("TreeView1") as TreeView;
        //    myMenu.Nodes.Clear();
        //    myMenu.Nodes.Add(new TreeNode("AfterCare Home", null, null, "~/Members/ProfileHome.aspx", "_self" ));
        //    myMenu.Nodes.Add(new TreeNode("Coach Students", null, null, "~/Members/coaching.aspx", "_self") { Selected = true });
        //    myMenu.Nodes.Add(new TreeNode("Resources & Tools", null, null, "~/Members/Resources.aspx", "_self"));
        //}

    }
}