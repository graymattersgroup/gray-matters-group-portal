﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="coaching.aspx.cs" Inherits="GoalManagement.Members.coaching" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">

.studenthistoryContainer
{
    position:absolute;
    left:190px;
    top:115px;
    width:720px;
    height:695px;   
    border: 1px solid black;
    background:#ffffff;

}
.studenthistory
{
    padding:20px;
    overflow:auto;  
    width:710px;
    height:650px;
    overflow:auto; 
     background:#ffffff;
       border: 1px solid black;
}

.myanswers { color:blue; }
</style>

<script type="text/javascript">

function PrintContent(divName)
{
var DocumentContainer = document.getElementById(divName);
var WindowObject = window.open("", "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
WindowObject.document.writeln(DocumentContainer.innerHTML);
WindowObject.document.close();
WindowObject.focus();
WindowObject.print();
WindowObject.close();
}



</script>


   <%-- <telerik:HeadTag runat="server" ID="Headtag2">
    </telerik:HeadTag>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function GetSelectedItems()
            {
                alert($find("<%= RadGrid2.MasterTableView.ClientID %>").get_selectedItems().length);
            }
        </script>
    </telerik:RadCodeBlock>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
  <%--  <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel">--%>
    <div id="HelpDiv" class="HelpScreen">
        <asp:Literal ID="HelpLiteral" runat="server"></asp:Literal>
    </div>

   <asp:Panel ID="studentPanel" runat="server" Visible="false">       
        <div  class="studenthistoryContainer">
        <table style=" border-bottom: 2px solid Green; width:100%">
        <tr>
        <td style="padding: 5px; color: Green; font-size: 12pt; font-weight: bold;"> <asp:Label ID="StudentName" runat="server" Text=""></asp:Label>&nbsp;&nbsp;<img src='/Images/Print.gif' onclick="javascript:PrintContent('studentHistory');" />
        </td>
        <td style=" text-align:right; padding:5px; ">
            <asp:Button ID="ExitStudentHistory" runat="server" Text="X" ForeColor="Red" Font-Bold="true" OnClick="ExitStudentHistory_Click" />
        </td>
        </tr>
        </table>        
           

            <div id="studentHistory" class="studenthistory">
                <asp:Panel ID="GradePanel" runat="server" Visible="false">
                  <table>
               <tr>  <td>
                             
                  
             <b>Set Student Progress for Progress Period:</b><br/> <asp:Label ID="CurrentProgressPeriod" runat="server"
                       Text=""></asp:Label> &nbsp;&nbsp;
                 </td>
               <td>  
                   <asp:RadioButtonList ID="FeedbackGrade" runat="server" 
                       RepeatDirection="Horizontal" AutoPostBack="true" onselectedindexchanged="FeedbackGrade_SelectedIndexChanged"
                  >
                   <asp:ListItem Text="Poor&nbsp;&nbsp;" Value="1"></asp:ListItem>
                   <asp:ListItem Text="Good&nbsp;&nbsp;" Value="2"></asp:ListItem>
                   <asp:ListItem Text="Excellent" Value="3"></asp:ListItem>
                   <asp:ListItem Text="No Rating" Value="9" Selected="True"></asp:ListItem>
               </asp:RadioButtonList>
               </td>
               </tr>
               </table>
                </asp:Panel>
               <hr />
               <br />
                <asp:Literal ID="StudentHistoryLiteral" runat="server"></asp:Literal>       
            </div>
        </div>
   </asp:Panel>

        <table>
    <tr>
    <td><img src='/Images/help.gif' onmouseover="javascript:showObj('HelpDiv');" onmouseout="javascript:hideObj('HelpDiv');" />&nbsp;
    </td>
    <td style="text-align:right;">Class<br />
        Progress Periods
    </td>
    <td>
        &nbsp;<asp:DropDownList ID="ClassList" runat="server" Font-Size="9pt" 
            onselectedindexchanged="ClassList_SelectedIndexChanged" AutoPostBack="true">
            </asp:DropDownList><br />
        &nbsp;<asp:DropDownList ID="ProgressPeriods" runat="server" Font-Size="9pt" OnSelectedIndexChanged="ProgressPeriods_SelectedIndexChanged"
            AutoPostBack="true">
        </asp:DropDownList>
    </td>
   
            <td>
            </td>
            <td>
            </td>
        </tr>

        <tr>
        <td>
          
        </td>     
            <td colspan="2">  <br />
            <asp:Button ID="ViewSelectedUserGoals" runat="server" Text="View Selected User Goals"
                    Enabled="false" OnClick="ViewSelectedUserGoals_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="ViewSelectedUser" runat="server" Text="Review Selected Student's Activities"
                Enabled="false" OnClick="ViewSelectedUser_Click" />
                
            </td>

            <td>
                <asp:Button ID="EmailClassGoals" runat="server" Text="email MyStudents Goals" 
                    onclick="EmailClassGoals_Click"   Visible="false" />
            </td>
            <td>
                <br />
                <asp:CheckBox ID="ComposeEmail" Text="Compose Email" runat="server" AutoPostBack="true"
                    OnCheckedChanged="ComposeEmail_CheckedChanged" />
            </td>
        </tr>
        

        
    </table>


           <asp:Panel ID="EmailPanel" runat="server" Visible="false" >
               <br />
               <asp:Button ID="UpdateEmailList" runat="server" Text="Update Email" 
                   onclick="UpdateEmailList_Click" />&nbsp;
              To:&nbsp;<asp:Label ID="EmailToList" runat="server" Text=""></asp:Label>
               <br />
               <br />
               <asp:Panel ID="StudentProgressPanel" runat="server">
                  Cc: 
                  <div style=" padding-left:20px;"> <asp:CheckBoxList ID="CoachesList" runat="server" RepeatDirection="Horizontal" RepeatColumns="4" CellSpacing="5" CellPadding="5" >
                   </asp:CheckBoxList></div>
                  
               <br />
               <table>
                   <tr>
                       <td colspan="2">
                           Subject:
                           <asp:TextBox ID="SubjectLine" runat="server" Width="550px"></asp:TextBox>
                           <br />
                           <br />
                       </td>
                   </tr>
               <tr>
               <td> Feedback&nbsp;&nbsp;</td>
             <%--  <td> 
                   
                   <telerik:RadSpell ID="spell1" runat="server" ControlToCheck="FeedbackText" SupportedLanguages="en-US,English"
                   CssClass="button" DictionaryPath="~/App_Data/RadSpell" SpellCheckProvider="TelerikProvider" />
               </td>--%>
               <td> Templates: <asp:DropDownList ID="emailTemplates" 
                   runat="server" Font-Size="9pt" 
                   AutoPostBack="true" 
                   onselectedindexchanged="emailTemplates_SelectedIndexChanged">
               </asp:DropDownList></td>
               </tr>
              
               </table>
               </asp:Panel>
               <br />
           
           
            <%--   <asp:TextBox ID="FeedbackText" runat="server" Text=""  Height="150px" 
                   Width="640px" TextMode="MultiLine"></asp:TextBox>--%>
               <telerik:RadEditor ID="RadEditor1" runat="server" Skin="Sitefinity"
                   Height="250px" Width="640px">
                  <%-- <Languages>
                       <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                   </Languages>--%>
                   <Tools>
                       <telerik:EditorToolGroup>
                           <telerik:EditorTool Name="AjaxSpellCheck" />
                           <telerik:EditorTool Name="Bold" />
                           <telerik:EditorTool Name="Italic" />
                           <telerik:EditorTool Name="Underline" />
                           <telerik:EditorTool Name="StrikeThrough" />
                           <telerik:EditorSeparator />
                           <telerik:EditorTool Name="JustifyLeft" />
                           <telerik:EditorTool Name="JustifyCenter" />
                           <telerik:EditorTool Name="JustifyRight" />
                           <telerik:EditorTool Name="JustifyFull" />
                           <telerik:EditorTool Name="JustifyNone" />
                           <telerik:EditorSeparator />
                           <telerik:EditorTool Name="Indent" />
                           <telerik:EditorTool Name="Outdent" />
                           <telerik:EditorTool Name="InsertOrderedList" />
                           <telerik:EditorTool Name="InsertUnorderedList" />
                           <telerik:EditorTool Name="LinkManager" />
                           <telerik:EditorTool Name="Unlink" />
                       </telerik:EditorToolGroup>
                       <telerik:EditorToolGroup Tag="DropdownToolbar">
                           <telerik:EditorSplitButton Name="ForeColor">
                           </telerik:EditorSplitButton>
                           <telerik:EditorSeparator />
                           <telerik:EditorDropDown Name="FontName">
                           </telerik:EditorDropDown>
                           <telerik:EditorDropDown Name="RealFontSize">
                           </telerik:EditorDropDown>
                       </telerik:EditorToolGroup>
                   </Tools>
                   <Content>
                    

</Content>
               </telerik:RadEditor>

               <br />
               <br />
               <table>
               <tr>
               <td> &nbsp;<asp:Button ID="SendCoachEmail" runat="server" Text="Send Email" Enabled="false"
                   OnClientClick="javascript:Waiting();" OnClick="SendCoachEmail_Click" />&nbsp;&nbsp;
               </td>
               <td>
                   &nbsp;<asp:Button ID="LogFeedbackEmail" runat="server" Text="Log Feedback and Send Email"
                       Enabled="false" OnClientClick="javascript:Waiting();" OnClick="LogFeedbackEmail_Click" />&nbsp;&nbsp;
                  <%-- <asp:RadioButtonList ID="FeedbackOption" runat="server" RepeatDirection="Horizontal"
                   >
                   <asp:ListItem Text="Log Feedback and Send Email&nbsp;&nbsp;" Selected="True"></asp:ListItem>
                  <asp:ListItem Text="Only Send Email" ></asp:ListItem>
               </asp:RadioButtonList>--%>
                  
               </td>
               </tr>
               </table>
             
             
                  
        </asp:Panel>
                        
     
        
            <br />
        
        <table >
        <tr>
        <td>
            <asp:RadioButtonList ID="WhichStudents" runat="server" AutoPostBack="true" OnSelectedIndexChanged="WhichStudents_SelectedIndexChanged"
                RepeatDirection="Horizontal">
            </asp:RadioButtonList>
        </td>
           
        <td style="padding-left: 20px; font-weight:bold;">
           <asp:Label ID="SetRatingLabel" runat="server" Text="Set Rating" Visible="false"></asp:Label></td>
            <td style="padding-left: 10px; ">
            <asp:RadioButtonList ID="FeedbackRating2" runat="server" RepeatDirection="Horizontal" Visible="false"
                AutoPostBack="true" OnSelectedIndexChanged="FeedbackGrade2_SelectedIndexChanged">
                <asp:ListItem Text="Poor&nbsp;&nbsp;" Value="1"></asp:ListItem>
                <asp:ListItem Text="Good&nbsp;&nbsp;" Value="2"></asp:ListItem>
                <asp:ListItem Text="Excellent" Value="3"></asp:ListItem>
                <asp:ListItem Text="No Rating" Value="9" Selected="True"></asp:ListItem>
            </asp:RadioButtonList>
        </td>
        </tr>
        </table>    
       

            <telerik:RadGrid ID="RadGrid2" runat="server" Skin="Web20" 
    GridLines="None" AllowMultiRowSelection="True"
                Width="690px" Height="480px" 
    OnColumnCreated="RadGrid1_ColumnCreated" 
    OnSelectedIndexChanged="RadGrid2_SelectedIndexChanged" AllowSorting="True"
                             
                >
                <ClientSettings enablerowhoverstyle="true" EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />                   
                    <Selecting AllowRowSelect="True" />      
                </ClientSettings>
                <MasterTableView>
                    <Columns>
                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" />                      
                    </Columns>                  
                    <CommandItemSettings ExportToPdfText="Export to Pdf" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
                <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
                </HeaderContextMenu>
            </telerik:RadGrid>


  <%--  </telerik:RadAjaxPanel>--%>

<%--</ContentTemplate>
</asp:UpdatePanel>--%>
</asp:Content>
