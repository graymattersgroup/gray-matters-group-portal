﻿<%@ Page Title="Gray Matters Group Aftercare - Goal Commitments" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Goals.aspx.cs" Inherits="GoalManagement.Goals" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<script type="text/javascript">

    function PrintContent(divName)
    {
        var DocumentContainer = document.getElementById(divName);
        var WindowObject = window.open("", "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }


    window.onload = function ()
    {       
        //Get value from cookie
        var strCook = document.cookie;
        if (strCook.indexOf("!~") != 0)
        {
            if (document.getElementById('myDIV') != null)
            {
            var intS = strCook.indexOf("!~");
            var intE = strCook.indexOf("~!");
            // find y position of Div
            var strPos = strCook.substring(intS + 2, intE);
            // Set y position of Div
           
                document.getElementById('myDIV').scrollTop = strPos;
            }
        }
    }
    

   function ShowWaiting()
    {
        if (document.getElementById('runningDiv') != null)
        {
            var thisDiv = document.getElementById('runningDiv');
            thisDiv.style.visibility = "visible";
        }
   }

    function UpdateScrollPosition()
    {
        //Get value from cookie
        var strCook = document.cookie;
        if (strCook.indexOf("!~") != 0)
        {
            if (document.getElementById('myDIV') != null)
            {
                var intS = strCook.indexOf("!~");
                var intE = strCook.indexOf("~!");
                // find y position of Div
                var strPos = strCook.substring(intS + 2, intE);
                // Set y position of Div

                document.getElementById('myDIV').scrollTop = strPos;
            }
        }
    }

    // call  this function onscroll of div. You will get red line(i.e. error)
    // but don't bother about that.
    function SetDivPosition()
    {
        // To get y position of Div
        var intY = document.getElementById('myDIV').scrollTop;
        // Set cookie value
        document.cookie = "yPos=!~" + intY + "~!";
    }

</script>

<style type="text/css">
.activityHeader
{ text-decoration:underline; color:Black; font-size:12pt; padding-top:10px; padding-bottom:5px;}

.activityItem { padding-left: 10px;}
.activitycheckbox { padding-top:3px; font-size:10pt; }

.studenthistoryContainer
{
    position:absolute;
    left:190px;
    top:115px;
    width:720px;
    height:695px;   
    border: 1px solid black;
    background:#ffffff;

}
.studenthistory
{
    padding:20px;
    overflow:auto;  
    width:710px;
    height:650px;
    overflow:auto; 
     background:#ffffff;
       border: 1px solid black;  
}

</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server" >
    <div id="HelpDiv" class="HelpScreen">
        <asp:Literal ID="HelpLiteral" runat="server"></asp:Literal>
    </div>
            <asp:Panel ID="studentPanel" runat="server" Visible="false">
                <div class="studenthistoryContainer">
                    <table style="border-bottom: 2px solid Green; width: 100%">
                        <tr>
                            <td style="padding: 5px; color: Green; font-size: 12pt; font-weight: bold;">
                                <asp:Label ID="StudentName" runat="server" Text=""></asp:Label>&nbsp;&nbsp;<img src='/Images/Print.gif'
                                    onclick="javascript:PrintContent('studentHistory');" />
                            </td>
                            <td style="text-align: right; padding: 5px;">
                                <asp:Button ID="ExitStudentHistory" runat="server" Text="X" ForeColor="Red" Font-Bold="true"
                                    OnClick="ExitStudentHistory_Click" />
                            </td>
                        </tr>
                    </table>
                    <div id="studentHistory" class="studenthistory">
                        <asp:Literal ID="StudentHistoryLiteral" runat="server"></asp:Literal>
                    </div>
                </div>
            </asp:Panel>
            <table onload="">       
            <tr>
            <td >
                <i><asp:Literal ID="ClassTitle" runat="server"></asp:Literal></i>               
                <img src='/Images/help.gif' onmouseover="javascript:showObj('HelpDiv');" onmouseout="javascript:hideObj('HelpDiv');" />&nbsp;&nbsp;
                <span style="color: #cc6731; font-size: 1.6em; font-weight: bold;"><asp:Label ID="MainTitle" runat="server" Text="Goal Commitments"></asp:Label></span>
                <br />
                <h3><asp:Label ID="SelectedRequired" runat="server" >Select Goals - Selected = </asp:Label></h3>
               
                <br />
                <asp:Panel ID="GoalPanel" runat="server">
                    Uncheck to remove a goal<br />
                    <br />
                   <%-- <table>
                        
                    <tr>
                    <td><asp:Panel ID="GoalEditPanel" runat="server">
                        </asp:Panel>
                    </td>
                    <td>--%>
                    <asp:CheckBoxList ID="GoalsList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="GoalsList_SelectedIndexChanged">
                    </asp:CheckBoxList>
                   <%-- </td>
                    </tr>
                    </table>--%>
                  
                    <br />
                    <hr />             
                </asp:Panel>
               
                <asp:Panel ID="ActivityPanel" runat="server"  > 
                <div id="myDIV" onscroll="javascript:SetDivPosition();" style="height: 550px; overflow: auto; width: 100%">
                What is the Personal and Business payoff from accomplishing this goal?
                <asp:TextBox ID="PayoffText" runat="server" Width="96%" Height="70px" TextMode="MultiLine"></asp:TextBox>
                <br />
                <br />
                    <asp:Table ID="ActivityTable" Width="100%" runat="server">
                    </asp:Table> 
                </div>   
                </asp:Panel>                      
          
            </td>
            </tr>

            <tr>
            <td style="text-align:right;">
            <br />
                <asp:Button ID="SaveWork" runat="server" Text=" Save Work " 
                    OnClientClick="javascript:Waiting('Saving....');" onclick="SaveWork_Click" />

                <asp:Button ID="PrevButton" runat="server" Text=" <- Previous " Visible="false" OnClick="PrevButton_Click"  OnClientClick="javascript:Waiting();" />
           
                <asp:Button ID="NextButton" runat="server" Text=" Next -> " Enabled="false" OnClientClick="javascript:Waiting();"
                    OnClick="NextButton_Click" />
             
                <asp:Button ID="ViewCommitments" runat="server" Text=" View Commitments " Visible="false"
                    Enabled="false" BackColor="Navy" ForeColor="White" OnClientClick="javascript:Waiting('Generating....');"
                    CssClass="ButtonBlue" OnClick="ViewCommitments_Click" />&nbsp;
                <asp:Button ID="Finish" runat="server" Text=" Save Commitments " Visible="false"
                    Enabled="false" BackColor="Navy" ForeColor="White"
                    OnClick="Finish_Click" CssClass="ButtonBlue" OnClientClick="javascript:Waiting('Saving....');" />
            </td>
            </tr>         
           </table>
           
</asp:Content>
