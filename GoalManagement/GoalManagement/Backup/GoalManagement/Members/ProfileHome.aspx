﻿<%@ Page Title="Gray Matters Group Aftercare- Home" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="ProfileHome.aspx.cs" Inherits="GoalManagement.ProfileHome" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">

table.status 
{ background-color:#dddddd; margin-left:80px; margin-top:20px; }

table.status tr td { padding:10px; }
.statusLeft { text-align:right;  white-space:nowrap;  }
.statusRight { text-align:left; }


        .style1
        {
            text-align: center;
            width: 122px;
        }


    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <%--   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div id="HelpDiv" class="HelpScreen">
        <asp:Literal ID="HelpLiteral" runat="server"></asp:Literal>
    </div>

            <table width="600px">          
            <tr>
               <td style="font-weight:bold; font-size:12pt;" >
                   <img src='/Images/help.gif' onmouseover="javascript:showObj('HelpDiv');" onmouseout="javascript:hideObj('HelpDiv');" />&nbsp;&nbsp;Welcome <asp:Label ID="StudentName" runat="server" Text="Student Name"></asp:Label>
                   
                  

                   </td> 
                   <td style=" padding-bottom:5px;"> &nbsp; <asp:RadioButtonList ID="ClassFilter" runat="server" RepeatDirection="Horizontal"
                       Visible="false" AutoPostBack="True" OnSelectedIndexChanged="ClassFilter_SelectedIndexChanged">
                       <asp:ListItem Text="Open Classes" Value="1"></asp:ListItem>
                       <asp:ListItem Text="All Classes" Value="2"></asp:ListItem>
                   </asp:RadioButtonList>
                   </td>
               <td style="font-weight: bold; font-size: 12pt;">
                   <asp:Label ID="CompanyName" runat="server" Text="Company Name"></asp:Label>
               </td>
            </tr>
            <tr>
                <td colspan="3">
                <br />
                    <asp:Panel ID="ClassPanel" runat="server" Visible="false">
                      
                        <table>
                            <tr>
                                <td>
                                    Student Classes&nbsp;
                                </td>
                                <td><asp:DropDownList ID="ClassList" runat="server" 
                        AutoPostBack="true" onselectedindexchanged="ClassList_SelectedIndexChanged" Visible="false">
                    </asp:DropDownList>
                                </td>
                                </tr>
                        <tr>
                        <td></td>
                            <td ><br />
                                <a href="ChangePassword.aspx">Change Your Password</a>
                            </td>
                        </tr>
                        </table>
                        <table class="status">
                            <tr>
                                <td colspan="2" style=" text-align:center;">
                                    <b>Class Status </b>
                                </td>
                            </tr>
                            <tr>
                                <td class="statusLeft">
                                    Class Date:
                                </td>
                                <td class="statusRight">
                                    <asp:Label ID="ClassDate" runat="server" Text="XX/XX/XX"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="statusLeft">
                                    Goal Sheet Created:
                                </td>
                                <td class="statusRight">
                                    <asp:Label ID="GoalSheetCreated" runat="server" Text="No"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="statusLeft">
                                    Last Update:
                                </td>
                                <td class="statusRight">
                                    <asp:Label ID="LastUpdateDate" runat="server" Text="None"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="statusLeft">
                                    Your Manager:
                                </td>
                                <td class="statusRight">
                                    <asp:Label ID="ManagerName" runat="server" Text="Manager Name"></asp:Label>
                                </td>
                            </tr>
                           
                        </table>


                    </asp:Panel>
                    <asp:Panel ID="CoachPanel" runat="server" Visible="false">
                    <br />                     
                    <table>     
                    <tr>
                    <td>
                        Coaching Classes&nbsp;
                    </td>
                        <td> <asp:DropDownList ID="CoachingClassList" runat="server" AutoPostBack="true"
                        OnSelectedIndexChanged="ClassList_SelectedIndexChanged">
                        </asp:DropDownList>
                        </td>
                    </tr>                
                               <tr>
                           <td>
                           </td>
                                <td >  <br />                                 
                                    <a href="ChangePassword.aspx">Change Your Password</a>
                                </td>
                            </tr>
                        </table>                    
                    </asp:Panel>

      
               <br/>
                    </td>
            </tr>         
          
       
           </table>
         
  <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
