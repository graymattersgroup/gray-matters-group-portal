﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.Services.Protocols;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Telerik.Web.UI;

namespace GoalManagement
{
    public partial class GoalsUpdates : System.Web.UI.Page
    {
        protected CRMAccess crmAccess;
        protected Contact studentInfo;
        protected new_class myClass;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ( Session.IsNewSession || Session["crmAccess"] == null || Session["StudentInfo"] == null || Session["ClassInfo"]==null)
            {
                InitializeUser();
            }
           
            crmAccess = (CRMAccess)Session["crmAccess"];
            studentInfo = (Contact)Session["StudentInfo"];
            myClass = (new_class)Session["ClassInfo"];
            
            if (IsPostBack && Session["thisUpdatePeriod"] == null)
            {
                var allUpdates = crmAccess.GetAllUpdatePeriods(studentInfo.ContactId.Value, myClass.new_classId.Value);

                StudentUpdatePeriod thisUpdatePeriod = allUpdates[0];

                foreach (StudentUpdatePeriod studentUpdatePeriod in allUpdates)
                {
                    if (studentUpdatePeriod.endDate <= DateTime.Now && studentUpdatePeriod.endDate > thisUpdatePeriod.endDate)
                    {
                        thisUpdatePeriod = studentUpdatePeriod;
                    }
                   
                }

                Session["thisUpdatePeriod"] = crmAccess.GetMyUpdatePeriod(studentInfo.ContactId.Value,
                                                            new Guid(UpdatePeriodList.SelectedValue),
                                                            thisUpdatePeriod);
            }


            if (!IsPostBack )
            {
                MembershipUser currentUser = Membership.GetUser();
                if ( currentUser == null) Response.Redirect("../Default.aspx");

                if (Session["ImpersonateUser"] != null)
                {
                    var otherUser = (string)Session["ImpersonateUser"];
                    var masterPage1 = (SiteMaster)Page.Master;
                    masterPage1.SetImpersonation(otherUser);
                }

                var masterPage = (SiteMaster)Page.Master;
                masterPage.SetMenu(CentralizedData.GoalProgress);

                var db = new SqlAzureLayer(masterPage.GetConnectionString());
                var result = db.GetDataTable("select content from [dbo].[ManagedContent] where name ='Goal Progress Help' ");
                if (result != null && result.Rows.Count > 0)
                {
                    HelpLiteral.Text = result.Rows[0][0].ToString();
                }
                
                var treeview = Master.FindControl("TreeView1") as TreeView;
                treeview.Visible = true;
                
                SetClassIcon(myClass.new_classId.Value, studentInfo);

                StudentUpdatePeriod thisUpdatePeriod = null;

                if (Session["thisUpdatePeriod"] != null)
                {
                    thisUpdatePeriod = (StudentUpdatePeriod)Session["thisUpdatePeriod"];
                }
                else
                {
                    var updateDays = 30;

                    if (myClass.Contains("new_updatetimeoutdays"))
                    {
                        updateDays = myClass.new_UpdateTimeoutDays.Value;
                    }
                    // Get all Update Periods and set default to the most recent completed period.
                    string selectedValue = "";
                    bool hasStarted = false;
                    DateTime nextPeriod = DateTime.MaxValue;

                    // Find current Update Period
                    var allUpdates = crmAccess.GetAllUpdatePeriods(studentInfo.ContactId.Value, myClass.new_classId.Value);

                    
                    foreach (StudentUpdatePeriod studentUpdatePeriod in allUpdates)
                    {
                        var periodName = string.Format("{0} to {1}",
                                                       studentUpdatePeriod.startDate.ToLocalTime().ToString("MM/dd/yyyy"),
                                                       studentUpdatePeriod.endDate.ToLocalTime().ToString("MM/dd/yyyy"));

                        var newItem = new ListItem(periodName, studentUpdatePeriod.PeriodId.ToString());

                        if (studentUpdatePeriod.endDate <= DateTime.Now &&
                            DateTime.Now <= studentUpdatePeriod.endDate.AddDays(updateDays))
                        {
                            selectedValue = studentUpdatePeriod.PeriodId.ToString();
                            thisUpdatePeriod = studentUpdatePeriod;
                        }

                        // See if we have started yet.
                        if (studentUpdatePeriod.endDate <= DateTime.Now)
                        {
                            hasStarted = true;
                        }

                        //  Get the next peride
                        if (studentUpdatePeriod.endDate > DateTime.Now)
                        {
                            if (studentUpdatePeriod.endDate < nextPeriod)
                                nextPeriod = studentUpdatePeriod.endDate;
                        }

                        // for testing  only.
                        if (thisUpdatePeriod == null) thisUpdatePeriod = studentUpdatePeriod;

                        UpdatePeriodList.Items.Add(newItem);
                    }

                    if (selectedValue == "")
                    {
                        //UpdatePeriodList.SelectedIndex = 0;


                        if (hasStarted)
                        {
                            if( nextPeriod ==  DateTime.MaxValue)
                            {
                                UpdatePeriod.Text = string.Format(@"<br/> There are no remaining Update Periods for this class.");
                            }
                            else
                            {
                                UpdatePeriod.Text = string.Format(
                                    @"<br/>
                        Sorry, the time for providing Rx in Action progress updates has expired for this Progress Period.
                        Progress must be logged within <b><u>{0} days</u></b> of the initial update request. <br/><br/>
                        
                        The next window for logging your progress will be from {1} to {2}.
                        ",
                                    updateDays, nextPeriod.ToLocalTime().AddDays(1).ToShortDateString(),
                                    nextPeriod.ToLocalTime().AddDays(11).ToShortDateString());
                            }
                        }
                        else
                        {
                            UpdatePeriod.Text = string.Format(
                                   @"<br/>
                        Sorry, the time for providing Rx in Action progress updates has not started yet.<br/>
                                               
                        The next window for logging your progress will be from {1} to {2}.
                        ",
                                   updateDays, nextPeriod.ToLocalTime().AddDays(1).ToShortDateString(),
                                   nextPeriod.ToLocalTime().AddDays(11).ToShortDateString());
                        }
                        NextButton.Visible = false;

                        return;
                    }
                    else
                    {
                        UpdatePeriodList.SelectedValue = selectedValue;

                    }


                    thisUpdatePeriod = crmAccess.GetMyUpdatePeriod(studentInfo.ContactId.Value,
                                                                   new Guid(UpdatePeriodList.SelectedValue),
                                                                   thisUpdatePeriod);
                    
                }

                if (!string.IsNullOrEmpty(thisUpdatePeriod.ImpactAnswer)) UpdateAnswer.Text = thisUpdatePeriod.ImpactAnswer;

                // Get My Coaches
                List<Contact> coaches;
                if (Session["myCoaches"] == null)
                {
                    coaches = crmAccess.GetCoaches(myClass.new_classId.Value);
                    Session["myCoaches"] = coaches;
                }
                else
                {
                    coaches = (List <Contact>)Session["myCoaches"];
                }
               

                foreach (Contact contact in coaches)
                {
                    var aCoach = new ListItem(contact.FirstName + " " + contact.LastName , contact.EMailAddress1);

                    if (studentInfo.new_MyCoachId != null)
                    {
                        if (studentInfo.new_MyCoachId.Id == contact.ContactId)
                        {
                            Session["myCoachName"] = aCoach.Text;
                            aCoach.Text += " (my coach)";
                            aCoach.Selected = true;
                        }
                    }
                    CoachesList.Items.Add(aCoach);
                    
                }


                UpdatePeriod.Text = string.Format("{0} to {1}",thisUpdatePeriod.startDate.ToLocalTime().ToString("MM/dd/yyyy"),thisUpdatePeriod.endDate.ToLocalTime().ToString("MM/dd/yyyy"));
                Session["thisUpdatePeriod"] = thisUpdatePeriod;

              
                var hasImpactQuestion = !string.IsNullOrEmpty(thisUpdatePeriod.PeriodImpactQuestion);
                Session["UpdatePage"] = 0;
                ImpactQuestion.Text = thisUpdatePeriod.PeriodImpactQuestion;
                UpdateAnswer.Text = thisUpdatePeriod.ImpactAnswer;
                
                Question1.Text = thisUpdatePeriod.GoalQuestion1;
                Question2.Text = thisUpdatePeriod.GoalQuestion2;

                GoalUpdatePanel.Visible = false;
                ImpactQuestionPanel.Visible = true;
                NextButton.Visible = true;
                PrevButton.Visible = false;
                int numberofPages = thisUpdatePeriod.PeriodGoalAnswers.Count;
                SetProgress(numberofPages + (hasImpactQuestion ? 1 : 0), 0);
                }
         
        }


        public void InitializeUser()
        {
            MembershipUser currentUser = Membership.GetUser();
            if (currentUser == null) Response.Redirect("../Default.aspx");

            if (Session["ImpersonateUser"] != null)
            {
                var otherUser = (string)Session["ImpersonateUser"];
                currentUser = Membership.GetUser(otherUser);

                var masterPage1 = (SiteMaster)Page.Master;
                masterPage1.SetImpersonation(otherUser);
            }

            var myEmail = currentUser.Email;
            Session["userEmail"] = myEmail;

            CRMAccess crmAccess;

            if (Session["crmAccess"] == null)
            {
                crmAccess = new CRMAccess();
                Session["crmAccess"] = crmAccess;
            }
            else
            {
                crmAccess = (CRMAccess)Session["crmAccess"];
            }

            var myContact = crmAccess.GetStudent(myEmail);
            if (myContact == null) Response.Redirect("../Default.aspx");
            Session["StudentInfo"] = myContact;

            var classes = crmAccess.GetStudentClasses(myContact.ContactId.Value).ToList();
            bool isStudent = (classes.Count != 0);
            Session["isStudent"] = isStudent;

            var coachingClasses = crmAccess.GetCoachClasses(myContact.ContactId.Value).ToList();
            bool isCoach = (coachingClasses.Count != 0);
            Session["isCoach"] = isCoach;

            Session["CoachingClasses"] = coachingClasses;
            Session["MyClasses"] = classes;

            Boolean classFound = false;

            int count = 0;
            var myClass = new new_class();

            if (isStudent)
            {
                foreach (new_class newClass in classes)
                {
                    if (newClass.new_ClassComplete.HasValue)
                    {
                        if (!newClass.new_ClassComplete.Value)
                        {
                            myClass = newClass;
                            classFound = true;
                            count++;
                        }
                    }
                    else
                    {
                        myClass = newClass;
                        classFound = true;
                        count++;
                    }
                }

                if (isCoach) Session["StudentClassId"] = myClass.new_classId.Value;
            }

            if (isCoach)
            {
                Session["isCoach"] = isCoach;

                foreach (new_class newClass in coachingClasses)
                {
                    if (newClass.new_ClassComplete.HasValue)
                    {
                        if (newClass.new_ClassComplete.Value)
                        {
                            myClass = newClass;
                            classFound = true;
                        }
                    }
                    else
                    {
                        myClass = newClass;
                        classFound = true;
                    }
                }
            }

            if (Session["ClassId"] == null && classFound)
            {
                Session["ClassId"] = myClass.new_classId.Value;
            }

            Session["ClassInfo"] = myClass;
        }



        private void SetClassIcon(Guid classId, Contact studentInfo)
        {
            var logoImage = Master.FindControl("RadBinaryImage1") as RadBinaryImage;
            try
            {
                if (Session["Logo"] != null)
                {
                    logoImage.DataValue = (byte[]) Session["Logo"];
                    logoImage.Visible = true;
                }
                else
                {
                    if (studentInfo.ParentCustomerId != null)
                    {
                        var logo = crmAccess.GetClassAnnotation(studentInfo.ParentCustomerId.Id);
                        if (logo.FirstOrDefault() == null) logo = crmAccess.GetClassAnnotation(classId);
                        if (logo.FirstOrDefault() != null)
                        {
                            logoImage.Visible = true;
                            logoImage.DataValue = Convert.FromBase64String(logo.FirstOrDefault().DocumentBody);
                        }
                        else
                        {
                            logoImage.Visible = false;
                        }
                    }
                    else
                    {
                        logoImage.Visible = false;
                    }
                }

            }
            catch
            {
                logoImage.Visible = false;
              
            }
        }
        
        protected void NextButton_Click(object sender, EventArgs e)
        {
            UpdatebyDirection(true);
        }

        protected void UpdatebyDirection(bool isNext)
        {
            var currentPage = (int) Session["UpdatePage"];
            
            var thisUpdatePeriod = (StudentUpdatePeriod)Session["thisUpdatePeriod"];
            
            var hasImpactQuestion = !string.IsNullOrEmpty(thisUpdatePeriod.PeriodImpactQuestion);
            int numberofPages = thisUpdatePeriod.PeriodGoalAnswers.Count + (hasImpactQuestion ? 1 : 0);
            
             var saveGoal = currentPage - (hasImpactQuestion ? 1 : 0);

            if (isNext)
                currentPage++;
            else
                currentPage--;

            var displayGoal = currentPage - (hasImpactQuestion ? 1 : 0);

            //  Check for required field.
            if (hasImpactQuestion && currentPage == 1)
            {
                if (string.IsNullOrEmpty(UpdateAnswer.Text))
                {
                    ErrorMessage.Visible = true;
                    currentPage = 0;
                    Session["UpdatePage"] = currentPage;
                    PrevButton.Visible = false;
                    return;
                }

                SaveWork.Visible = true;
                thisUpdatePeriod.ImpactAnswer = UpdateAnswer.Text;
                ErrorMessage.Visible = false;
            }
            else
            {
                SaveWork.Visible = true;
            }

            SetProgress(numberofPages, currentPage);

            if (isNext)
            {
                var lastPage = currentPage-1;

                if (!hasImpactQuestion || lastPage != 0)
                {
                    //  Save Last Goal
                    thisUpdatePeriod = StoreCurrentGoal(saveGoal, thisUpdatePeriod);
                }

                // If past end of Goals
                if (currentPage >= numberofPages)
                {
                    GoalUpdatePanel.Visible = false;
                    SavePanel.Visible = true;
                    NextButton.Visible = false;
                }
                else
                {
                    GoalUpdatePanel.Visible = true;
                    SavePanel.Visible = false;
                    NextButton.Visible = true;
                    PrevButton.Visible = true;
                    ImpactQuestionPanel.Visible = false;
                    DisplayCurrentGoal(displayGoal, thisUpdatePeriod);
                }
            }
            else
            {
                var lastPage = currentPage+1;
                SavePanel.Visible = false;
                NextButton.Visible = true;

                if (lastPage <= numberofPages)
                {
                    thisUpdatePeriod = StoreCurrentGoal(saveGoal, thisUpdatePeriod);
                }

                if (currentPage == 0 && hasImpactQuestion)
                {
                    GoalUpdatePanel.Visible = false;
                    ImpactQuestionPanel.Visible = true;
                }
                else
                {
                    GoalUpdatePanel.Visible = true;
                    SavePanel.Visible = false;
                    NextButton.Visible = true;
                    PrevButton.Visible = true;
                    ImpactQuestionPanel.Visible = false;
                    DisplayCurrentGoal(displayGoal, thisUpdatePeriod);
                }


            }

            SaveWork.Visible = !SavePanel.Visible;

            PrevButton.Visible = (currentPage != 0);

            Session["thisUpdatePeriod"] = thisUpdatePeriod;
            Session["UpdatePage"] = currentPage;
        }


       private void DisplayCurrentGoal(int goalNumber, StudentUpdatePeriod thisUpdatePeriod)
       {
           var count = 0;
           foreach (GoalAnswers goalAnswer in thisUpdatePeriod.PeriodGoalAnswers)
           {
               if (count == goalNumber)
               {
                   GoalName.Text = goalAnswer.GoalName;
                   Answer1.Text = goalAnswer.Answer1;
                   Answer2.Text = goalAnswer.Answer2;
                   Payoff.Text = goalAnswer.Payoff;

                   ActivityList.Items.Clear();
                   foreach (Activities myActivity in goalAnswer.MyActivities)
                   {
                       var newitem = new ListItem(myActivity.Text, myActivity.Activityid.ToString())
                                         {Selected = myActivity.IsChecked};
                       ActivityList.Items.Add(newitem);
                   }

                   if (goalAnswer.ProgressValue != -1)
                   {
                       GoalProgressRadioButtonList.SelectedValue = goalAnswer.ProgressValue.ToString();
                   }
                   else
                   {
                       GoalProgressRadioButtonList.SelectedIndex = 0;
                   }

                   break;
               }
               count++;
           }
           
       }


    private StudentUpdatePeriod StoreCurrentGoal(int goalNumber, StudentUpdatePeriod thisUpdatePeriod)
    {
        try
        {
            var count = 0;

            foreach (GoalAnswers goalAnswer in thisUpdatePeriod.PeriodGoalAnswers)
            {
                if (count == goalNumber)
                {
                    goalAnswer.Answer1 = Answer1.Text;
                    goalAnswer.Answer2 = Answer2.Text;

                    foreach (Activities myActivity in goalAnswer.MyActivities)
                    {
                        foreach (ListItem item in ActivityList.Items)
                        {
                            if (item.Value == myActivity.Activityid.ToString())
                            {
                                myActivity.IsChecked = item.Selected;
                                break;
                            }
                        }

                        //var newitem = new ListItem(myActivity.Text, myActivity.Activityid.ToString())
                        //                  {Selected = myActivity.IsChecked};
                        //ActivityList.Items.Add(newitem);
                    }

                    if (GoalProgressRadioButtonList.SelectedValue != "")
                        goalAnswer.ProgressValue =
                            Convert.ToInt32(GoalProgressRadioButtonList.SelectedValue);
                    else
                    {
                        goalAnswer.ProgressValue = 1;
                    }
                    break;
                }
                count++;
            }
        }
        catch (SoapException ex)
        {
            EmailNotifications.SendExceptionMessage("Gray Matters StoreCurrentGoal Error ", ex.Message, ex);
        }
        catch (Exception ex)
        {
            EmailNotifications.SendExceptionMessage("Gray Matters StoreCurrentGoal Error ", ex.Message, ex);
        }

        return thisUpdatePeriod;
    }

   


       
        


        protected void SetProgress(int count, int pageNumber)
        {
            string progressIcons = "";
            for (int i = 0; i < count; i++)
            {
                if (i < pageNumber)
                {
                    progressIcons += " <img src='/Images/Completed.png' />";
                }
                else
                {
                    progressIcons += " <img src='/Images/UnCompleted.png' />";
                }
            }
            ProgressBar.Text = progressIcons;
        }

        //protected void SetMenu()
        //{

        //    var thisPage = Master.FindControl("PageTitle") as Label;
        //    var myMenu = Master.FindControl("TreeView1") as TreeView;
        //    myMenu.Nodes.Clear();
        //    myMenu.Nodes.Add(new TreeNode("AfterCare Home", null, null, "~/Members/ProfileHome.aspx", "_self"));
        //    myMenu.Nodes.Add(new TreeNode("Modify My Goals", null, null, "~/Members/Goals.aspx", "_self"));
        //    var commitment = new TreeNode("Goal Progress", null, null, "~/Members/GoalUpdates.aspx", "_self") { Selected = true };
        //    myMenu.Nodes.Add(commitment);
        //    myMenu.Nodes.Add(new TreeNode("Resources & Tools", null, null, "~/Members/Resources.aspx", "_self"));
        //}

        protected void PrevButton_Click(object sender, EventArgs e)
        {
            UpdatebyDirection(false);
        }


        protected void SaveWork_Click(object sender, EventArgs e)
        {
            try
            {
            crmAccess = (CRMAccess)Session["crmAccess"];

            var currentPage = (int)Session["UpdatePage"];
            var thisUpdatePeriod = (StudentUpdatePeriod)Session["thisUpdatePeriod"];
                
            var hasImpactQuestion = !string.IsNullOrEmpty(thisUpdatePeriod.PeriodImpactQuestion);
           
            int numberofPages = thisUpdatePeriod.PeriodGoalAnswers.Count + (hasImpactQuestion ? 1 : 0);
                
            var saveGoal = currentPage - (hasImpactQuestion ? 1 : 0);
                
            if (!hasImpactQuestion && currentPage != 0)
            {
                //  Save Last Goal
                thisUpdatePeriod = StoreCurrentGoal(saveGoal, thisUpdatePeriod);
            }

            if (currentPage != 0)
            {
                thisUpdatePeriod.ImpactAnswer = UpdateAnswer.Text;

                thisUpdatePeriod.FeedbackRequest = Comments.Text;
            }

            crmAccess.SaveUpdate(thisUpdatePeriod);

            }
            catch (SoapException ex)
            {
                EmailNotifications.SendExceptionMessage("Gray Matters Save Work Error ", ex.Message, ex);
            }
            catch (Exception ex)
            {
                EmailNotifications.SendExceptionMessage("Gray Matters Save Work Error ", ex.Message, ex);
            }
        }



        protected void SaveUpdateInformation_Click(object sender, EventArgs e)
        {
            crmAccess = (CRMAccess)Session["crmAccess"];
            
             var thisUpdatePeriod =  (StudentUpdatePeriod)Session["thisUpdatePeriod"];

             thisUpdatePeriod.ImpactAnswer = UpdateAnswer.Text;

            thisUpdatePeriod.FeedbackRequest = Comments.Text;
             crmAccess.SaveUpdate(thisUpdatePeriod);

             if (NotifyMyCoach.Checked) SendNotificationEmail(thisUpdatePeriod);

             Response.Redirect("~/Members/ProfileHome.aspx");
        }

        private void SendNotificationEmail(StudentUpdatePeriod thisUpdatePeriod)
        {
             var studentInfo = (Contact) Session["StudentInfo"];

            var msg = messageBody;

            var fromme = studentInfo.EMailAddress1;
            var myto = "";
            foreach (ListItem item in CoachesList.Items)
            {
                if(item.Selected)
                {
                    myto += item.Value +";";
                }

            }

            var sb = new StringBuilder(256);

            sb.AppendFormat("<b><span class='title'>Impact Question</b></span><br/> {0} <br/><span class='myanswers'>{1}</span><br/><br/>", thisUpdatePeriod.PeriodImpactQuestion, thisUpdatePeriod.ImpactAnswer);

            foreach (GoalAnswers answer in thisUpdatePeriod.PeriodGoalAnswers)
            {
                sb.AppendFormat("<hr/><span class='title'><b>{0}</b> (progress - <u>{1}</u>)</span><br/><br/>", answer.GoalName, progressVAlues[answer.ProgressValue]);
                sb.AppendFormat("{0}: <br/><span class='myanswers'>{1}</span><br/><br/>", thisUpdatePeriod.GoalQuestion1, answer.Answer1);
                sb.AppendFormat("{0}: <br/><span class='myanswers'>{1}</span><br/>", thisUpdatePeriod.GoalQuestion2, answer.Answer2);
                sb.AppendFormat("<br/>");

                sb.AppendFormat("I used the following activities in this goal period:");
                sb.AppendFormat("<ul>");
                bool noActivities = true;
                foreach (Activities act in answer.MyActivities)
                {
                    if (act.IsChecked)
                    {
                        noActivities = false;
                        sb.AppendFormat("<li>{0}</li>",act.Text);
                    }
                }

                if (noActivities) sb.AppendFormat("<li>No Activities Selected</li>");
                sb.AppendFormat("</ul>");
                sb.AppendFormat("<br/>");
                sb.AppendFormat("<br/>");
            }

            if (Session["myCoachName"] != null)
            {
                msg = msg.Replace("#coach#", Session["myCoachName"].ToString());
            }
            else
            {
                
            }
            msg = msg.Replace("#studentname#", studentInfo.FirstName + " " + studentInfo.LastName);
            msg = msg.Replace("#StudentMessage#", Comments.Text);
            msg = msg.Replace("#GoalUpdate#", sb.ToString());
            
            var subject = string.Format("AfterCare Update Notification from {0} {1}", studentInfo.FirstName,
                                        studentInfo.LastName);

            EmailNotifications.sendEmailAll(myto, fromme, subject, msg, null, true, false);

        }

        protected void NotifyMyCoach_CheckedChanged(object sender, EventArgs e)
        {
            CoachesFeedback.Visible = NotifyMyCoach.Checked;
        }

        private string[] progressVAlues = new string[] {"", "None", "Some", "Significant", "Completed"};
        
        public const string messageBody = @"
		<html>
			<head>
				<title>Goal Update Notification</title>
				
			<style type=""text/css"">
			
			body { font-family:Verdana; }
			.email{ font-size: 10pt; background-color:#9dfffc; font-family: Verdana; }
			table { margin-top: 15px; border-collapse:collapse;  border:0px; }
			td { padding:10px; }
			.label { font-size: 10pt; text-align:right;}
			.value { font-size: 10pt; text-align:left; font-weight:bold; }			
			.title { font-size: 13pt; background-color: #cccccc; }
			
.myanswers { color:blue; }
			.header { font-size: 10pt; border-bottom:solid 1px black; }
			
			</style>	
			</head>
			<body >

<div style='font-size: 10pt;'>
			<b>Student Message to Coach: </b><br/>#coach#<br/><br/>

#StudentMessage#
<br/>
<br/>
Please review my work at <a href='http://www.graymattersgroup.info'>Go to Aftercare Portal</a> and also see my updated activity summary below.
<br/>
Thank you,<br/>
#studentname#
</div>
<br/>
<br/>
<br/>
<div style='text-align:center;'>
<b>----------------------------------------------------------------------</b>
<br/><br/>
<span style='font-size: 13pt;'>	<strong>Rx in Action</strong><br/>
	<em>Student Progress Summary</em></span>
</div>

<br/>
#GoalUpdate#
		

			</body>
		</html>";

       
    }

 


}