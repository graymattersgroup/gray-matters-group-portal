﻿<%@ Page Title="Gray Matters Group Aftercare - Log In" Language="C#" MasterPageFile="/Site.master"
    AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="GoalManagement.Account.Login" %>
    
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        function SetFocus()
        {
            document.getElementById("ctl00_MainContent_LoginUser_UserName").focus();
        }

//        function AllowLogin()
//        {
//            var checkbox = document.getElementById("AgreeToAgreement");

//            document.getElementById("ctl00_MainContent_LoginUser_LoginButton").disabled = !checkbox.checked;

//           
//            
//        }


    </script>
    <style type="text/css">
        .style1
        {
            font-size: x-small;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div style="padding-left: 80px;  ">
   <%-- <h2>
        Log In
    </h2>--%>
   
    <asp:Login ID="LoginUser" runat="server" EnableViewState="false" 
            RenderOuterTable="false" onauthenticate="LoginUser_Authenticate">
        <LayoutTemplate>
            <span class="failureNotification">
                <asp:Literal ID="FailureText" runat="server"></asp:Literal>
            </span>
            <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification" 
                 ValidationGroup="LoginUserValidationGroup"/>
            <div class="accountInfo">
                <fieldset class="login">
                    <legend>Login</legend>
                    <p style=" padding-left:30px;">
                        Please enter your email address and password.<br />
                        <asp:HyperLink ID="RegisterHyperLink" runat="server" NavigateUrl="CustomRegister.aspx"
                            EnableViewState="false">Register</asp:HyperLink>
                        if you don't have an account.
                    </p>
                   <table>
                   <tr>
                   <td>  <p>
                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">email address:</asp:Label>
                        <asp:TextBox ID="UserName" runat="server" CssClass="textEntry"   ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                             CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required." 
                             ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                    </p>
                    <p>
                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:   (case sensitive)</asp:Label>
                        <asp:TextBox ID="Password" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                             CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required." 
                             ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                    </p>
                    <p> 
                      <span class="style1">Reset your password <asp:HyperLink ID="HyperLink1" runat="server" EnableViewState="false" 
                           NavigateUrl="ResetPassword.aspx">Click Here</asp:HyperLink>
                      </span>
                    </p>
                    
                   </td>
                   <td style=" padding-left:10px; width:auto;" >
                   <br />
        <%--           <p>
                       <input type="checkbox" onclick="javascript:AllowLogin();" id="AgreeToAgreement">
                       &nbsp;I agree to the
                       <asp:HyperLink ID="HyperLink2" CssClass="loginLink" runat="server" NavigateUrl="/Account/UserAgreement.htm"
                           Target="_Blank">User Agreement</asp:HyperLink>                       
                   </p>  --%>
                    <p>
                        <asp:CheckBox ID="RememberMe" runat="server"/>
                        <asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe" CssClass="inline">Keep me logged in</asp:Label>
                    </p>
                       <p class="submitButton">                         
                           <asp:Button ID="LoginButton" runat="server" CommandName="Login" Width="100px" Text="Log In"
                               ValidationGroup="LoginUserValidationGroup" OnClientClick="javascript:Waiting('Loading AfterCare Information....');" />
                       </p>
                   </td>
                   </tr>
                   </table>               
                
                </fieldset>
               
            </div>
        </LayoutTemplate>
    </asp:Login>
    </div>
</asp:Content>
