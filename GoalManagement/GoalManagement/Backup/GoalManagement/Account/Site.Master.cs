﻿using System;
using System.Configuration;
using System.Web.Security;
using System.Linq;
using GoalManagement.Support;
using Telerik.Web.UI;

namespace GoalManagement.Account
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetClassIcon();
        }
        
        private void SetClassIcon()
        {
                if (Session["Logo"] != null)
                {
                    RadBinaryImage1.DataValue = (byte[])Session["Logo"];
                    RadBinaryImage1.Visible = true;
                }
                else
                {
                    RadBinaryImage1.Visible = false;
                }
        }
   
        
    }
}
