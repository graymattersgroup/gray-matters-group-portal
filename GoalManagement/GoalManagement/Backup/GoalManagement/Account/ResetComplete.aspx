﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResetComplete.aspx.cs" Inherits="GoalManagement.Account.ResetComplete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            font-size: small;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="accountInfo">
        <fieldset class="login">
            <legend>Your password has been reset</legend><br />
            <span class="style1">An email has been sent to you with an automatically
            generated temporary password.<br /><br />
            Please login using your email address and temporary password,
            and then change your password.
        </span>
        </fieldset>
    </div>

</asp:Content>
