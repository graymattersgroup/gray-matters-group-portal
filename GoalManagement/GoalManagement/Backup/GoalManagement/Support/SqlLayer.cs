using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using GoalManagement.CrmConnectivity;

namespace GoalManagement.Support
{
	/// <summary>
	/// PassPort MetaData DataBase Access
	/// </summary>
	public class SqlAzureLayer
	{
		public readonly string _connectionStringValue;
     
        public SqlAzureLayer(string connectionString)
        {
            _connectionStringValue = connectionString;
        }

        public DataTable GetCRMAccount()
        {
            var sql = new SqlAzureLayer(_connectionStringValue);
            var dbActivities = sql.GetDataTable(string.Format("select * from dbo.CRM where id='1'"));
            return dbActivities;
        }

        public void SetCRMAccount(string username, string password)
        {
            var onlyPassword = (username == "");
           
            var encryptedPassword = AESEncryption.Encrypt(password, "thispassword");

            var connection = new SqlConnection(_connectionStringValue);
            connection.Open();

            SqlCommand sqlCmd;
            
            if( onlyPassword) 
                sqlCmd = new SqlCommand("Update dbo.CRM Set value=@password where id='1'", connection);
            else
            {
                sqlCmd = new SqlCommand("Update dbo.CRM Set crmconnection=@user, value=@password where id='1'", connection);

                sqlCmd.Parameters.Add("@user", SqlDbType.NVarChar);
                var encryptedUsername = AESEncryption.Encrypt(username, "thisuser");
                sqlCmd.Parameters["@user"].Value = encryptedUsername;
            }

            sqlCmd.Parameters.Add("@password", SqlDbType.NVarChar);
            sqlCmd.Parameters["@password"].Value = encryptedPassword;

            sqlCmd.ExecuteNonQuery();

            connection.Close();
        }

        public void SaveDashboardBlob(DashboardBlob blob)
        {
           
            var bytes = SerializeAnObject(blob);
            var connection = new SqlConnection(_connectionStringValue);
            connection.Open();
            var sqlCmd = new SqlCommand("INSERT INTO dbo.MainCache(classid,updated,dashboardxml) VALUES (@id,@updated,@xml)", connection);
            sqlCmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier);
            sqlCmd.Parameters["@id"].Value = blob.classid;
            sqlCmd.Parameters.Add("@updated", SqlDbType.DateTime);
            sqlCmd.Parameters["@updated"].Value = blob.lastupdated;
            sqlCmd.Parameters.Add("@xml", SqlDbType.Xml, Int32.MaxValue);
            sqlCmd.Parameters["@xml"].Value = bytes;
            sqlCmd.ExecuteNonQuery();
            connection.Close();
        }

        public void UpdateDashboardBlob(DashboardBlob blob)
        {

            var bytes = SerializeAnObject(blob);
            var connection = new SqlConnection(_connectionStringValue);
            connection.Open();
            var sqlCmd = new SqlCommand("Update dbo.MainCache Set updated=@updated, dashboardxml=@xml where classid=@id", connection);
            sqlCmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier);
            sqlCmd.Parameters["@id"].Value = blob.classid;
            sqlCmd.Parameters.Add("@updated", SqlDbType.DateTime);
            sqlCmd.Parameters["@updated"].Value = blob.lastupdated;
            sqlCmd.Parameters.Add("@xml", SqlDbType.Xml, Int32.MaxValue);
            sqlCmd.Parameters["@xml"].Value = bytes;
            sqlCmd.ExecuteNonQuery();
            connection.Close();
        }

        public void SaveGoalBlob(ClassGoals blob)
        {

            var bytes = SerializeAnObject(blob);
            var connection = new SqlConnection(_connectionStringValue);
            connection.Open();
            var sqlCmd = new SqlCommand("INSERT INTO dbo.MainCache(classid,updated,goalsxml) VALUES (@id,@updated,@xml)", connection);
            sqlCmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier);
            sqlCmd.Parameters["@id"].Value = blob.classid;
            sqlCmd.Parameters.Add("@updated", SqlDbType.DateTime);
            sqlCmd.Parameters["@updated"].Value = blob.lastupdated;
            sqlCmd.Parameters.Add("@xml", SqlDbType.Xml, Int32.MaxValue);
            sqlCmd.Parameters["@xml"].Value = bytes;
            sqlCmd.ExecuteNonQuery();
            connection.Close();
        }

        public void UpdateGoalBlob(ClassGoals blob)
        {

            var bytes = SerializeAnObject(blob);
            var connection = new SqlConnection(_connectionStringValue);
            connection.Open();
            var sqlCmd = new SqlCommand("Update dbo.MainCache Set updated=@updated, goalsxml=@xml where classid=@id", connection);
            sqlCmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier);
            sqlCmd.Parameters["@id"].Value = blob.classid;
            sqlCmd.Parameters.Add("@updated", SqlDbType.DateTime);
            sqlCmd.Parameters["@updated"].Value = blob.lastupdated;
            sqlCmd.Parameters.Add("@xml", SqlDbType.Xml, Int32.MaxValue);
            sqlCmd.Parameters["@xml"].Value = bytes;
            sqlCmd.ExecuteNonQuery();
            connection.Close();
        }

        public DashboardBlob GetDashboardBlob(Guid classid)
        {
            var sql = new SqlAzureLayer(_connectionStringValue);

            var dbActivities = sql.GetDataTable(string.Format("select dashboardxml from dbo.MainCache where classid={0} ", sql.FormatGuid(classid)));

            if (dbActivities.Rows.Count != 0)
            {
                var data = (DashboardBlob)DeSerializeAnObject(dbActivities.Rows[0][0].ToString(), typeof(DashboardBlob));

                return data;
            }
            return null;
        }

        public ClassGoals GetGoalBlob(Guid classid)
        {
            var sql = new SqlAzureLayer(_connectionStringValue);

            var dbActivities = sql.GetDataTable(string.Format("select goalsxml from dbo.MainCache where classid={0} ", sql.FormatGuid(classid)));

            if (dbActivities.Rows.Count != 0)
            {
                if( dbActivities.Rows[0][0] == DBNull.Value)
                    return null;

                var data = (ClassGoals)DeSerializeAnObject(dbActivities.Rows[0][0].ToString(), typeof(ClassGoals));
                
                return data;
            }
            return null;
        }

        public static string SerializeAnObject(object AnObject)
        {
            var Xml_Serializer = new XmlSerializer(AnObject.GetType());
            var Writer = new StringWriter();
            Xml_Serializer.Serialize(Writer, AnObject);
            return Writer.ToString();
        }

        /// ---- DeSerializeAnObject ------------------------------
        /// <summary>
        /// DeSerialize an object
        /// </summary>
        /// <param name="XmlOfAnObject">The XML string</param>
        /// <param name="ObjectType">The type of object</param>
        /// <returns>A deserialized object...must be cast to correct type</returns>

        public static Object DeSerializeAnObject(string XmlOfAnObject, Type ObjectType)
        {
            var StrReader = new StringReader(XmlOfAnObject);
            var Xml_Serializer = new XmlSerializer(ObjectType);
            var XmlReader = new XmlTextReader(StrReader);
            try
            {
                Object AnObject = Xml_Serializer.Deserialize(XmlReader);
                return AnObject;
            }
            finally
            {
                XmlReader.Close();
                StrReader.Close();
            }
        }

        public string GetHelp( string resourceName )
        {
            var table = new DataTable();
            SqlConnection connection = null;

            var queryString = string.Format("select content from [dbo].[ManagedContent] where name ={0}", FormatString(resourceName));

            try
            {
                connection = new SqlConnection(_connectionStringValue);

                connection.Open();
                var adapter = new SqlDataAdapter(queryString, connection);
                var ds = new DataSet();
                adapter.Fill(ds);
                table = ds.Tables[0];
            }
            catch (Exception ex)
            {

                EmailNotifications.SendExceptionMessage("GrayMattersGroup", "GetHelp", ex, queryString);
            }
            finally
            {
                if (connection != null) connection.Close();
            }
            
            if (table != null && table.Rows.Count > 0)
            {
                return table.Rows[0][0].ToString();
            }

            return " ";
        }

        public string GetHelp(string resourceName, Guid classId)
        {
            var table = new DataTable();
            SqlConnection connection = null;

            var queryString = string.Format("select content from [dbo].[ManagedContent] where name ={0} and classid={1}", FormatString(resourceName), FormatGuid(classId));

            try
            {
                connection = new SqlConnection(_connectionStringValue);

                connection.Open();
                var adapter = new SqlDataAdapter(queryString, connection);
                var ds = new DataSet();
                adapter.Fill(ds);
                table = ds.Tables[0];
            }
            catch (Exception ex)
            {
                EmailNotifications.SendExceptionMessage("GrayMattersGroup", "GetHelp", ex, queryString);
            }
            finally
            {
                if (connection != null) connection.Close();
            }

            if (table != null && table.Rows.Count > 0)
            {
                return table.Rows[0][0].ToString();
            }

            return " ";
        }
        public DataTable GetDataTable(string queryString)
        {
            var table = new DataTable();
            SqlConnection connection = null;
           
            try
            {
                connection = new SqlConnection(_connectionStringValue);
            
                connection.Open();
                var adapter = new SqlDataAdapter(queryString, connection);
                var ds = new DataSet();
                adapter.Fill(ds);
                table = ds.Tables[0];
            }
            catch (Exception ex)
            {
                EmailNotifications.SendExceptionMessage("GrayMattersGroup", "GetDataTable", ex, queryString);
                return null;
            }
            finally
            {
                if (connection != null) connection.Close();
            }

            return table;
        }

        public long GetCount(string queryString)
        {
            long count = 0;

            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(_connectionStringValue);
                connection.Open();
                var adapter = new SqlDataAdapter(queryString, connection);
                var ds = new DataSet();
                adapter.Fill(ds);
                count = Convert.ToInt64( ds.Tables[0].Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                EmailNotifications.SendExceptionMessage("SqlAzureLayer ", "GetCount", ex);
            }
            finally
            {
                if (connection != null) connection.Close();
            }

            return count;
        }


      
        
        public DataTable GetDataTableParms(string queryString, SqlParameter[] parameters)
        {
            DataTable table;

            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(_connectionStringValue);  
                connection.Open();

                var selectCommand = new SqlCommand(queryString, connection);
                selectCommand.Parameters.AddRange(parameters);

                var adapter = new SqlDataAdapter { SelectCommand = selectCommand };
                var ds = new DataSet();
                adapter.Fill(ds);
                table = ds.Tables[0];                  
                
            }
            catch (Exception ex)
            {
                EmailNotifications.SendExceptionMessage("GrayMattersGroup ", "GetDataTableParms", ex, queryString);
                return new DataTable();
            }
            finally
            {
                if (connection != null) connection.Close();
            }

            return table;
        }

    


        public void SqlExecuteNonQuery(string executeString)
        {
             SqlConnection connection = null;
             try
             {
                 connection = new SqlConnection(_connectionStringValue);
                 var cmd = new SqlCommand(executeString, connection);
                 connection.Open();
                 cmd.ExecuteNonQuery();
             }
             catch (Exception ex)
             {
                 EmailNotifications.SendExceptionMessage("GrayMattersGroup ", "SqlExecuteNonQuery", ex, _connectionStringValue + " " + executeString.Replace("'", "`"));
                
             }
            finally
            {
                if (connection != null) connection.Close();
            }
        }

        public void LogError(string user, Exception ex)
        {
            var executeString = string.Format(@"
insert into dbo.userlog
( 
userlogid, occuranceDate, event, report, username
)
values
( '{0}', '{1}', '{2}', '{3}','{4}' )
", Guid.NewGuid(), DateTime.Now, "error", IsExceptionFound(ex,""), user);

            try
            {
                using (var connection = new SqlConnection(_connectionStringValue))
                {                    
                    var cmd = new SqlCommand(executeString, connection);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close(); //redundant step
                }
            }
            catch(Exception ex2)
            {
                EmailNotifications.SendExceptionMessage("GrayMattersGroup ", "LogError", ex2, executeString);
            }
           
        }

        public void LogQuery(string query, string user)
        {
            var executeString = "";
            try
            {
                 executeString = string.Format(@"
insert into dbo.userlog
( 
userlogid, occuranceDate, event, query, username
)
values
( '{0}', '{1}', '{2}', '{3}','{4}')
", Guid.NewGuid(), DateTime.Now, "query", query.Replace("'", "`"), user);
                using (var connection = new SqlConnection(_connectionStringValue))
                {
                    var cmd = new SqlCommand(executeString, connection);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close(); //redundant step
                }
            }
            catch (Exception ex2)
            {
                EmailNotifications.SendExceptionMessage("GrayMattersGroup ", "LogError", ex2, executeString);
            }

        }


        // recursively traverses the Inner Exceptions
        private static string IsExceptionFound(Exception startPoint, string recursiveMessage)
        {

            if (startPoint != null)
            {
                var sb = new StringBuilder();

                sb.AppendLine(recursiveMessage);
                
                sb.AppendFormat("ExceptionType: {0}", startPoint.GetType());
                sb.AppendLine();
                sb.AppendFormat("Message: {0}", startPoint.Message);
                sb.AppendLine();
                sb.AppendFormat("Source: {0}", startPoint.Source);
                sb.AppendLine();
                sb.AppendFormat("StackTrace: {0}", startPoint.StackTrace);
                sb.AppendLine();

                recursiveMessage = sb.ToString();

                //see if any inner exceptions if so then recurse
                if (startPoint.InnerException != null)
                {
                    return IsExceptionFound(startPoint.InnerException, recursiveMessage);
                }
            }

            return recursiveMessage;
        }

        public Guid? GetDataRowGuid(object o)
        {
            try
            {
                var temp = new Guid(o.ToString());
                return temp;
            }
            catch (Exception)
            {
                return null;
            }

        }


        public string GetDataRowString(object o)
        {
            try
            {
                var temp = o.ToString();
                return HttpUtility.HtmlDecode(temp);
            }
            catch (Exception)
            {
                return null;
            }

        }

        public int? GetDataRowInt(object o)
        {
            try
            {
                var temp = Convert.ToInt32(o.ToString());
                return temp;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public string GetDataRowHtmlEncodedString(object o)
        {
            try
            {
                var temp = HttpUtility.HtmlEncode(o.ToString());
                return temp;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public bool? GetDataRowBool(object o)
        {
            try
            {
                var temp = Convert.ToBoolean(o.ToString());
                return temp;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public decimal? GetDataRowDecimal(object o)
        {
            try
            {
                var temp = Convert.ToDecimal(o.ToString());
                return temp;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public double? GetDataRowDouble(object o)
        {
            try
            {
                var temp = Convert.ToDouble(o.ToString());
                return temp;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public DateTime? GetDataRowDateTime(object o)
        {
            try
            {
                var temp = Convert.ToDateTime(o.ToString());

                //if (usingCRMDatabase == true)
                //{
                //    var offset = TimeZone.CurrentTimeZone.GetUtcOffset(temp).Hours;

                //    DateTime utcAdjustedDate = temp.AddHours(+offset);

                //    return utcAdjustedDate;
                //}
                return temp;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public string FormatGuid(Guid? aGuid)
        {
            if ( aGuid == null) return "null";

            return string.Format("'{0}'", aGuid);
        }

        public string FormatDateTime(DateTime? obj)
        {
            if (obj == null) return "null";

            // if using CRM Database make sure to adjust for offset
            //if (usingCRMDatabase == true)
            //{
            //    var offset = TimeZone.CurrentTimeZone.GetUtcOffset(obj.Value).Hours;
            //    var temp = obj;

            //    DateTime utcAdjustedDate = temp.Value.AddHours(-offset);

            //    return string.Format("'{0}'", utcAdjustedDate.ToString("yyyyMMdd HH:mm:ss"));
            //}
        
            return string.Format("'{0}'", obj.Value.ToString("yyyyMMdd HH:mm:ss"));
        }

        public string FormatDateTimeNoAdjustment(DateTime? obj)
        {
            if (obj == null) return "null";                           

            return string.Format("'{0}'", obj.Value.ToString("yyyyMMdd HH:mm:ss"));
        }



         public string FormatString(string obj)
        {
            if (obj == null) return "null";

            obj = HttpUtility.HtmlEncode(obj);
            return string.Format("'{0}'", obj.Replace("'","`"));
        }

         public string FormatStringNoEncode(string obj)
         {
             if (obj == null) return "null";            
             return string.Format("'{0}'", obj.Replace("'", "`"));
         }

         public string FormatInt(int? obj)
         {
             if (obj == null) return "null";

             return string.Format("'{0}'", obj);
         }

         public string FormatFloat(float? obj)
         {
             if (obj == null) return "null";

             return string.Format("'{0}'", obj);
         }


         public string FormatBool(bool? obj)
         {
             if (obj == null) return "null";

             return string.Format("'{0}'", obj);
         }


         public Guid SavePreset(string presetName, Guid? profileid, bool isPublished, Guid? groupid, Guid? industryid, int datatype, bool isDIG)
        {
            Guid newPresetId = Guid.NewGuid();

            var createPreset =
                string.Format(
                    @" INSERT INTO dbo.Preset
           ([presetid]
           ,[name]
           ,[profileid]
           ,[groupid]         
           ,[sharewithgroup]
            ,industryid
,datatype
,isDIG
)
     VALUES
           ({0}
           ,{1}
           ,{2}
           ,{3}           
           ,{4}
            ,{5}
,{6}
,{7})",
            FormatGuid(newPresetId),
            FormatString(presetName),
            FormatGuid(profileid),
            FormatGuid(groupid),
            FormatBool(isPublished),
            FormatGuid(industryid),
            FormatInt(datatype),
              FormatBool(isDIG)
            );

            SqlExecuteNonQuery(createPreset);

            return newPresetId;
        }

         public Guid SavePresetField(Guid presetid, Guid presetfieldid, Guid? residentialfieldid, Guid? commercialfieldid, int? inputdisplayorder, int? outputdisplayorder, string defaultValue, int datatype )
         {

             var createPreset =
                 string.Format(
                     @" INSERT INTO [CCProduction].[dbo].[Preset_Field]
           ([presetfieldid]
           ,[presetid]
           ,[residentialfieldid]
           ,[commercialfieldid]
           ,[inputdisplayorder]
           ,[outputdisplayorder]
           ,[defaultvalue]
,datatype)
     VALUES          
           ({0}
           ,{1}
           ,{2}
           ,{3}           
           ,{4}
            ,{5}
            ,{6}
,{7})",
             FormatGuid(presetfieldid),
              FormatGuid(presetid),
             FormatGuid(residentialfieldid),
             FormatGuid(commercialfieldid),
             FormatInt(inputdisplayorder),
             FormatInt(outputdisplayorder),
             FormatString(defaultValue),
            FormatInt(datatype)
             );

             SqlExecuteNonQuery(createPreset);

             return presetfieldid;
         }



         internal void UpdatePreset(Guid presetid, string presetName, bool share, Guid? groupid, Guid? industryid, int type, bool isDIG)
         {
             var updatePreset =( string.IsNullOrEmpty(presetName))?
            
                 string.Format(
                     @"UPDATE dbo.Preset
   SET [groupid] = {2}
      ,[industryid] = {4}
      ,[sharewithgroup] = {3}
,datatype = {5}
,isDIG = {6}
 WHERE [presetid]={0}", 
                      FormatGuid(presetid), 
                      FormatString(presetName),
                      FormatGuid(groupid), 
                      FormatBool(share),
                      FormatGuid(industryid),
                      FormatInt(type),
                      FormatBool(isDIG)):
                      
               string.Format(
                   @"UPDATE dbo.Preset
   SET [name] = {1}   
      ,[groupid] = {2}
      ,[industryid] = {4}
      ,[sharewithgroup] = {3}
,datatype = {5}
,isDIG = {6}
 WHERE [presetid]={0}",
                    FormatGuid(presetid),
                    FormatString(presetName),
                    FormatGuid(groupid),
                    FormatBool(share),
                    FormatGuid(industryid), FormatInt(type), FormatBool(isDIG));
      
             SqlExecuteNonQuery(updatePreset);
         }

         internal void ClearPresetFields(Guid presetid)
         {
             var clearPreset = string.Format("DELETE FROM dbo.Preset_Field WHERE presetid = {0}", FormatGuid(presetid));
             SqlExecuteNonQuery(clearPreset);
         }

	    public void CopyPresetFields(Guid newpresetid, Guid oldpresetid)
	    {
            var CopyPresetFields = string.Format(@"Insert into dbo.Preset_Field
([presetfieldid]
      ,[presetid]
      ,[residentialfieldid]
      ,[commercialfieldid]
      ,[inputdisplayorder]
      ,[outputdisplayorder]
      ,[defaultvalue]
      ,[sortorder]
      ,[sortdirection]
      ,[reportdisplayoption]
      ,[searchdisplayoption]
      ,[sort]
      ,[residentialfieldid2]
      ,[combineRule]
      ,[alias]
      ,[displayoption]
      ,[category]
,[MergeLine]
      ,[MergeColumn]
      ,[MergeDelimeter]
      ,[MergeFieldName]
)
SELECT NewId()
      ,{0} -- new presetid
      ,[residentialfieldid]
      ,[commercialfieldid]
      ,[inputdisplayorder]
      ,[outputdisplayorder]
      ,[defaultvalue]
      ,[sortorder]
      ,[sortdirection]
      ,[reportdisplayoption]
      ,[searchdisplayoption]
      ,[sort]
      ,[residentialfieldid2]
      ,[combineRule]
      ,[alias]
      ,[displayoption]
      ,[category]
,[MergeLine]
      ,[MergeColumn]
      ,[MergeDelimeter]
      ,[MergeFieldName]
FROM dbo.Preset_Field
where presetid = {1} -- old preset ", FormatGuid(newpresetid),FormatGuid(oldpresetid));


	        SqlExecuteNonQuery(CopyPresetFields);

	    }
	}
}