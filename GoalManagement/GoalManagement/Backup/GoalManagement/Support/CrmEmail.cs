﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;

namespace ScheduledEmail
{
      public enum EmailSendRule {None = 4, To = 1, CC = 2, BCC = 3, RepresentativeExample = 5, Sender =  6}
        public enum ActivityPartyType { Sender = 1, To = 2, CC = 3, BCC = 4 }
        public enum SendMailBy { IndividualStudent = 1, Team = 2, Class = 3 }
        public enum RxType { Student = 10000000, Coach = 100000001, Sponsor = 100000002 }

    public class CrmEmail
    {

        private const string MergeFirstName = "#firstname#";
        private const string CoachName = "#coachname#";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailTarget"></param>
        /// <param name="students"></param>
        /// <param name="coachesSponsors"></param>
        /// <param name="sendRule"></param>
        /// <returns></returns>
        public static List<ActivityParty> FillActivityParty(int emailTarget, List<Contact> students, List<Contact> coachesSponsors, EmailSendRule sendRule)
        {
            var activityParty = new List<ActivityParty>();

            switch (emailTarget)
            {
                case (int)CRMAccess.EmailTargets.Students:
                    activityParty.AddRange(students.Select(contact => CreateActivityFromContact(contact.Id, sendRule)));
                    break;

                case (int)CRMAccess.EmailTargets.Coaches:
                    try
                    {
                        //activityParty.AddRange(from contact in coachesSponsors
                        //                       where contact.new_ContactRxRole.Value == (int) RxType.Coach
                        //                       select CreateActivityFromContact(contact.Id, sendRule));

                        foreach (Contact contact in coachesSponsors)
                        {
                            if (contact.new_ContactRxRole.Value == (int)RxType.Coach)
                            {
                                activityParty.Add(CreateActivityFromContact(contact.Id, sendRule));
                            }
                        }
                    }
                    catch
                    {
                    }
                   
                    break;

                case (int)CRMAccess.EmailTargets.Sponsors:
                    try
                    {
                        foreach (Contact contact in coachesSponsors)
                        {
                            if (contact.new_ContactRxRole.Value == (int) RxType.Sponsor)
                            {
                                activityParty.Add(CreateActivityFromContact(contact.Id, sendRule));
                            }
                        }
                    }
                    catch
                    {
                    }
                   
                    break;

                case (int)CRMAccess.EmailTargets.CoachesAndSponsors:
                    activityParty.AddRange(coachesSponsors.Select(contact => CreateActivityFromContact(contact.Id, sendRule)));
                    break;

                default:
                    return activityParty;
            }

            return activityParty;
        }

        public static List<Contact> CreateContactList(int emailTarget, List<Contact> students, List<Contact> coachesSponsors)
        {
            var activityParty = new List<Contact>();

            switch (emailTarget)
            {
                case (int)CRMAccess.EmailTargets.Students:
                    activityParty = students;
                    break;

                case (int)CRMAccess.EmailTargets.Coaches:
                    try
                    {
                        //activityParty.AddRange(from contact in coachesSponsors
                        //                       where contact.new_ContactRxRole.Value == (int) RxType.Coach
                        //                       select CreateActivityFromContact(contact.Id, sendRule));

                        foreach (Contact contact in coachesSponsors)
                        {
                            if (contact.new_ContactRxRole.Value == (int)RxType.Coach)
                            {
                                activityParty.Add(contact);
                            }
                        }
                    }
                    catch
                    {
                    }

                    break;

                case (int)CRMAccess.EmailTargets.Sponsors:
                    try
                    {
                        foreach (Contact contact in coachesSponsors)
                        {
                            if (contact.new_ContactRxRole.Value == (int)RxType.Sponsor)
                            {
                                activityParty.Add(contact);
                            }
                        }
                    }
                    catch
                    {
                    }

                    break;

                case (int)CRMAccess.EmailTargets.CoachesAndSponsors:
                    activityParty = coachesSponsors;
                    break;

                default:
                    return activityParty;
            }

            return activityParty;
        }


        public static string representativeBody = @"
		 <table style=""border: 1 solid red;"">
        <tr>
            <td style=""font-family: arial; font-size: 16px; padding:20px;"">
            
                This is a representative email that was sent to the students you are associated
                with.
                
            </td>
            </tr>
     </table>
<br/><br/>
";


        public static void SendEmail(Guid classId, int emailTarget, List<Contact> students, List<Contact> coachesSponsors, EmailSendRule sendRule, 
            string emailBody, string subject, List<Annotation> attachments, List<ActivityParty> fromActivityParty, List<ActivityParty> ccActivityParty, 
            List<Contact> representativeContacts, CRMAccess crmAccess, Guid scheduledTouchId, Guid? testemailContactId)
        {
            List<Contact> sendToContacts = students;
            bool isTest = false;

            switch (emailTarget)
            {
                case (int)CRMAccess.EmailTargets.TestEmailToMe:
                    isTest = true;
                    break;

                case (int)CRMAccess.EmailTargets.Students:
                    sendToContacts = students;
                    break;

                case (int)CRMAccess.EmailTargets.Coaches:
                case (int)CRMAccess.EmailTargets.Sponsors:
                case (int)CRMAccess.EmailTargets.CoachesAndSponsors:
                    sendToContacts = coachesSponsors;
                    break;

                case (int)CRMAccess.EmailTargets.All:
                    sendToContacts = students;
                    break;

                default:
                    return;
            }
            
            if (isTest)
            {

                var testEmailContactId = testemailContactId?? new Guid("A45DFF64-5C14-E111-8DF2-1CC1DEE89A7F");
                // for test sends email to Dave.
                var toActivityParty = new List<ActivityParty>() { CreateActivityFromContact(testEmailContactId, EmailSendRule.To) };
                
                ccActivityParty = new List<ActivityParty>();
                var thisEmailBody = emailBody;

                thisEmailBody = thisEmailBody.Replace(MergeFirstName, "FirstName will go here");

                bool done = false;
                var infiniteLoopCount = 0;

                while( !done)
                {
                    if (thisEmailBody.IndexOf("#assetlink#") == -1) done = true;
                    else
                    {
                        var assetid = new Guid(thisEmailBody.Substring(thisEmailBody.IndexOf("#assetlink#") + 11, 36));
                        var assetText = thisEmailBody.Substring(thisEmailBody.IndexOf("#assetlink#") + 11 + 36 + 1);
                        assetText = assetText.Substring(0,assetText.IndexOf("|"));

                        var fullText = string.Format("#assetlink#{0}|{1}|", assetid, assetText);

                        var assetLink = string.Format("<a href='http://www.graymattersgroup.info/Account/Tracker.aspx?cid={0}&aid={1}&sid={2}'>{3}</a>", testEmailContactId,
                                               assetid, scheduledTouchId, assetText);

                       thisEmailBody = thisEmailBody.Replace(fullText, assetLink);

                       if (infiniteLoopCount > 100)
                       {
                           thisEmailBody = string.Format("{0} Failed assetlink bad formatting", subject);
                           subject = "Failed: " + subject;
                           done = true;
                       }
                    }
                }
          
                SendTouch(classId, attachments, fromActivityParty, toActivityParty, ccActivityParty, subject, thisEmailBody, crmAccess);
            }
            else if (emailTarget == (int)CRMAccess.EmailTargets.All)
            {
                foreach (Contact sendToContact in sendToContacts)
                {
                    var toActivityParty = new List<ActivityParty>() { CreateActivityFromContact(sendToContact.Id, sendRule) };
                    var thisEmailBody = emailBody;

                    thisEmailBody = thisEmailBody.Replace(MergeFirstName, sendToContact.FirstName);


                    bool done = false;

                    var infiniteLoopCount = 0;

                    while (!done)
                    {
                        if (thisEmailBody.IndexOf("#assetlink#") == -1) done = true;
                        else
                        {
                            infiniteLoopCount++;

                            var assetid = new Guid(thisEmailBody.Substring(thisEmailBody.IndexOf("#assetlink#") + 11, 36));
                            var assetText = thisEmailBody.Substring(thisEmailBody.IndexOf("#assetlink#") + 11 + 36 + 1);
                            assetText = assetText.Substring(0, assetText.IndexOf("|"));

                            var fullText = string.Format("#assetlink#{0}|{1}|", assetid, assetText);

                            var assetLink = string.Format("<a href='http://www.graymattersgroup.info/Account/Tracker.aspx?cid={0}&aid={1}&sid={2}'>{3}</a>", sendToContact.ContactId.Value,
                                               assetid, scheduledTouchId, assetText);

                            thisEmailBody = thisEmailBody.Replace(fullText, assetLink);


                            if(infiniteLoopCount > 100)
                            {
                                var exception =
                                    new Exception(string.Format("{0} Failed assetlink bad formatting", subject));

                                throw exception;
                            }
                        }
                    }


                    var subjectPrefix = "";

                    if( sendToContact.new_IsPortalAdministrator.HasValue)
                    {
                        if( sendToContact.new_IsPortalAdministrator.Value)
                        {
                            var thisClass = crmAccess.GetClass(classId);
                            subjectPrefix = thisClass.new_name + " - ";
                        }
                    }

                    try
                    {
                        SendTouch(classId, attachments, fromActivityParty, toActivityParty, new List<ActivityParty>(), subjectPrefix + subject, thisEmailBody, crmAccess);
                    }
                    catch (Exception ex)
                    {
                        var emailAddress = sendToContact.EMailAddress1;
                        EmailNotifications.SendExceptionMessage("GMG Scheduled Email", "Exception", ex, "Failed to Send Email to:" + sendToContact.FullName + " - " + emailAddress);
                    }

                }
            }
            else
            {

                foreach (Contact sendToContact in sendToContacts)
                {
                    var toActivityParty = new List<ActivityParty>() { CreateActivityFromContact(sendToContact.Id, sendRule) };
                    var thisEmailBody = emailBody;

                    thisEmailBody = thisEmailBody.Replace(MergeFirstName, sendToContact.FirstName);

                    bool done = false;

                    while (!done)
                    {
                        if (thisEmailBody.IndexOf("#assetlink#") == -1) 
                            done = true;
                        else
                        {
                            var assetid = new Guid(thisEmailBody.Substring(thisEmailBody.IndexOf("#assetlink#") + 11, 36));
                            var assetText = thisEmailBody.Substring(thisEmailBody.IndexOf("#assetlink#") + 11 + 36 + 1);
                            assetText = assetText.Substring(0, assetText.IndexOf("|"));

                            var fullText = string.Format("#assetlink#{0}|{1}|", assetid, assetText);

                            var assetLink = string.Format("<a href='http://www.graymattersgroup.info/Account/Tracker.aspx?cid={0}&aid={1}&sid={2}'>{3}</a>", sendToContact.ContactId.Value,
                                                assetid, scheduledTouchId, assetText);

                            thisEmailBody = thisEmailBody.Replace(fullText, assetLink);
                        }
                    }


                    var subjectPrefix = "";

                    if (sendToContact.new_IsPortalAdministrator.HasValue)
                    {
                        if (sendToContact.new_IsPortalAdministrator.Value)
                        {
                            var thisClass =  crmAccess.GetClass(classId);
                            subjectPrefix = thisClass.new_name +" - ";
                        }
                    }

                    try
                    {
                        SendTouch(classId, attachments, fromActivityParty, toActivityParty, ccActivityParty, subjectPrefix + subject, thisEmailBody, crmAccess);
                    }
                    catch (Exception ex)
                    {
                        var emailAddress = sendToContact.EMailAddress1;
                        EmailNotifications.SendExceptionMessage("GMG Scheduled Email", "Exception", ex, "Failed to Send Email to:" + sendToContact.FullName + " - " + emailAddress);
                    }

                }

                if (representativeContacts.Count > 0)
                {
                    foreach (Contact sendToContact in representativeContacts)
                    {
                        var toActivityParty = new List<ActivityParty>(){CreateActivityFromContact(sendToContact.Id, sendRule)};
                        var thisEmailBody = emailBody;

                        var repBody = representativeBody;

                        var targetName = "students";
                        if(  emailTarget == (int) CRMAccess.EmailTargets.Coaches) targetName = "coaches";


                        repBody = repBody.Replace("#TARGET#", targetName);

                        bool done = false;

                        while (!done)
                        {
                            if (thisEmailBody.IndexOf("#assetlink#") == -1)
                                done = true;
                            else
                            {
                                var assetid = new Guid(thisEmailBody.Substring(thisEmailBody.IndexOf("#assetlink#") + 11, 36));
                                var assetText = thisEmailBody.Substring(thisEmailBody.IndexOf("#assetlink#") + 11 + 36 + 1);
                                assetText = assetText.Substring(0, assetText.IndexOf("|"));
                                
                                var fullText = string.Format("#assetlink#{0}|{1}|", assetid, assetText);

                                var assetLink = string.Format("<a href='http://www.graymattersgroup.info/Account/Tracker.aspx?cid={0}&aid={1}&sid={2}'>{3}</a>", sendToContact.ContactId.Value,
                                                  assetid, scheduledTouchId, assetText);

                                thisEmailBody = thisEmailBody.Replace(fullText, assetLink);
                            }
                        }


                        thisEmailBody = repBody + thisEmailBody;

                        try
                        {
                            var subjectPrefix = "";

                            if (sendToContact.new_IsPortalAdministrator.HasValue)
                            {
                                if (sendToContact.new_IsPortalAdministrator.Value)
                                {
                                    var thisClass = crmAccess.GetClass(classId);
                                    subjectPrefix = thisClass.new_name + " - ";
                                }
                            }

                            SendTouch(classId, attachments, fromActivityParty, toActivityParty, new List<ActivityParty>(), subjectPrefix + "Example: " + subject, thisEmailBody, crmAccess);
                        }
                        catch (Exception ex)
                        {
                            var emailAddress = sendToContact.EMailAddress1;
                            EmailNotifications.SendExceptionMessage("GMG Scheduled Email", "Exception", ex,"Failed to Send Representataive Email to:" + sendToContact.FullName + " - " + emailAddress);
                        }
                    }

                }

            }
        }

        public static bool SendTouch(Guid classId, List<Annotation> attachments, List<ActivityParty> fromActivityParty, List<ActivityParty> toActivityParty, List<ActivityParty> ccActivityParty, string subject, string thisEmailBody, CRMAccess crmAccess)
        {
            var emailId = Guid.NewGuid();

            if (subject.Length >= 200)
                subject = subject.Substring(0, 199);

            var newCRMEmail = new Email()
            {
                ActivityId = emailId,
                From = fromActivityParty,

                To = toActivityParty,
                Cc = ccActivityParty,

                Subject = subject,
                Description = thisEmailBody,
                RegardingObjectId = new EntityReference(new_class.EntityLogicalName, classId),

                DirectionCode = true  // Out Going
            };

            //if (!representativeEmailSent)
            //{
            //    newCRMEmail.Bcc = representativeActivityParty;
            //}

            // Save Email
            crmAccess.OrgService.Create(newCRMEmail);

            if (attachments.Count > 0)
            {
                foreach (Annotation attachment in attachments)
                {
                    if (attachment.IsDocument == true)
                    {
                        var emailAttachment = new ActivityMimeAttachment
                        {
                            ObjectId = new EntityReference(Email.EntityLogicalName, emailId),
                            ObjectTypeCode = Email.EntityLogicalName,
                            Subject = attachment.Subject,
                            Body = attachment.DocumentBody,
                            FileName = attachment.FileName,
                            MimeType = attachment.MimeType
                        };

                        var newMimeAttachmentId = crmAccess.OrgService.Create(emailAttachment);

                    }
                }

            }


            // Send Email
            var sendEmailreq = new SendEmailRequest
            {
                EmailId = emailId,
                TrackingToken = "",
                IssueSend = true
            };

            var sendEmailresp = (SendEmailResponse)crmAccess.OrgService.Execute(sendEmailreq);

            return true;
        }

        public static bool SendRepresentativeTouch2(Guid classId, List<Annotation> attachments, List<ActivityParty> fromActivityParty, List<ActivityParty> representativeActivityParty, string subject, string thisEmailBody, bool representativeEmailSent, CRMAccess crmAccess)
        {
            var emailId = Guid.NewGuid();

            var newCRMEmail = new Email()
            {
                ActivityId = emailId,
                From = fromActivityParty,

                To = representativeActivityParty,
              
                Subject = subject,
                Description = thisEmailBody,
                RegardingObjectId = new EntityReference(new_class.EntityLogicalName, classId),

                DirectionCode = true  // Out Going
            };

            // Save Email
            crmAccess.OrgService.Create(newCRMEmail);

            if (attachments.Count > 0)
            {
                foreach (Annotation attachment in attachments)
                {
                    if (attachment.IsDocument == true)
                    {
                        var emailAttachment = new ActivityMimeAttachment
                        {
                            ObjectId = new EntityReference(Email.EntityLogicalName, emailId),
                            ObjectTypeCode = Email.EntityLogicalName,
                            Subject = attachment.Subject,
                            Body = attachment.DocumentBody,
                            FileName = attachment.FileName,
                            MimeType = attachment.MimeType
                        };

                        var newMimeAttachmentId = crmAccess.OrgService.Create(emailAttachment);

                    }
                }

            }


            // Send Email
            var sendEmailreq = new SendEmailRequest
            {
                EmailId = emailId,
                TrackingToken = "",
                IssueSend = true
            };

            var sendEmailresp = (SendEmailResponse)crmAccess.OrgService.Execute(sendEmailreq);

            return true;
        }


        private static ActivityParty CreateActivityFromContact(Guid contactid, EmailSendRule type)
        {
            int activityPartyType = 0;

            switch (type)
            {
                case EmailSendRule.Sender:
                    activityPartyType = (int)ActivityPartyType.Sender;
                    break;

                case EmailSendRule.To:
                    activityPartyType = (int)ActivityPartyType.To;
                    break;

                case EmailSendRule.CC:
                    activityPartyType = (int)ActivityPartyType.CC;
                    break;

                case EmailSendRule.BCC:
                    activityPartyType = (int)ActivityPartyType.BCC;
                    break;
            }

            var newActivityParty = new ActivityParty
            {
                PartyId = new EntityReference(Contact.EntityLogicalName, contactid),
                ParticipationTypeMask = new OptionSetValue(activityPartyType)
            };

            return newActivityParty;
        }

    }
}
