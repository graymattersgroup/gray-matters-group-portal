﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using GoalManagement.CrmConnectivity;
using Telerik.Reporting.Processing;
using TelerikReporting;

namespace GoalManagement.Support
{
    public static class CentralizedData
    {
        public static string DashboardName = "Class Performance<br/> Dashboard";
        public static string HomeName = "AfterCare Home";
        public static string[] MyGoalsName = new[] { "Create My Goals", "Modify My Goals" };
        public static string GoalProgress = "Goal Progress";
        public static string CoachingSupportName = "Coaching Support";
        public static string[] CoachStudentsName = new[] { "Coach Students", "Peer Review" };
        public static string ResourcesName = "Resources & Tools";
        public static string ContentManagement = "Content Management";
        public static string UserManagement = "User Management";
        public static string TouchManagement = "Touch Editor";
        public static string MyReports = "My Reports";
        public static string CoachInfo = "Coach Info";
        public static string ClassAdmin = "Class Admin";
    }


    public class ReportHelper
    {
        public static string ViewMyCommitments(List<ClassGoal> classGoalPages)
        {
            var sb = new StringBuilder(256);

            // Generate Table to send to Telerik report engine to generate an email with PDF attachment to the student.
            foreach (ClassGoal goalPage in classGoalPages)
            {
                if (!goalPage.selected) continue;

                sb.AppendFormat("<H2>{0}</H2><br/>", goalPage.name);
                sb.AppendFormat("<b>Payoff:</b>  {0}<br/><br/>", goalPage.payoff);

                sb.AppendFormat("<b>Activities:</b><br/>");
                sb.AppendFormat("<ul>");
                foreach (Activity newGoalactivity in goalPage.activities)
                {
                    if (!newGoalactivity.selected && newGoalactivity.subActivities.Count == 0) continue;

                    if (newGoalactivity.subActivities.Count != 0)
                    {
                        foreach (SubActivity newSubactivity in newGoalactivity.subActivities)
                        {
                            if (newSubactivity.selected)
                            {
                                sb.AppendFormat("<li>{0}</li>", newSubactivity.name);
                            }

                        }
                    }
                    else if (newGoalactivity.selected)
                    {
                        sb.AppendFormat("<li>{0}</li>", newGoalactivity.name);
                    }
                }
                sb.AppendFormat("</ul>");
                sb.AppendFormat("<hr/>");
                sb.AppendFormat("<br/>");
            }

            return sb.ToString();
        }


        public static string ViewAStudentsGoals(Guid classId, Guid studentId, CRMAccess crmAccess, SqlAzureLayer sql)
        {

            var classGoals = GetClassGoals(classId, crmAccess, sql);

         
            var selectedGoals = crmAccess.GetStudentGoals(classId, studentId);

            var classGoalPages = new List<ClassGoal>();

            foreach (ClassGoal classGoal in classGoals.classGoals)
            {
                foreach (new_studentgoals myGoals in selectedGoals)
                {
                    if (myGoals.new_classgoalId.Id == classGoal.id)
                    {
                        var newGoal = new ClassGoal
                        {
                            name = classGoal.name,
                            payoff = myGoals.new_Benefit,
                            selected = true
                        };

                        var selectedActivities = crmAccess.GetStudentGoalActivities(classId, studentId, classGoal.id);

                        var newActivities = new List<Activity>();

                        foreach (Activity anActivity in classGoal.activities)
                        {
                            //if (!newGoalactivity.selected && newGoalactivity.subActivities.Count == 0) continue;

                            foreach (new_goalactivity myActivity in selectedActivities)
                            {
                                if (anActivity.subActivities.Count != 0)
                                {
                                    var selectedsubactivites = crmAccess.GetStudentGoalSubActivities(classId, studentId, anActivity.id);

                                    bool haveSubs = false;

                                    var newActivity = new Activity
                                    {
                                        selected = false,
                                        name = anActivity.name,

                                    };
                                    foreach (SubActivity selectedsubactivity in anActivity.subActivities)
                                    {

                                        foreach (new_subactivity mySubActivity in selectedsubactivites)
                                        {
                                            if (mySubActivity.Id == selectedsubactivity.id)
                                            {
                                                haveSubs = true;

                                                var newSubActivity = new SubActivity
                                                {
                                                    name = selectedsubactivity.name,
                                                    selected = true
                                                };

                                                newActivity.subActivities.Add(newSubActivity);
                                            }
                                        }


                                    }

                                    if (haveSubs)
                                    {
                                        newActivities.Add(newActivity);
                                    }

                                }
                                else if (myActivity.Id == anActivity.id)
                                {
                                    var newActivity = new Activity { selected = true, name = anActivity.name };
                                    newActivities.Add(newActivity);
                                }
                            }
                        }

                        newGoal.activities = newActivities;

                        classGoalPages.Add(newGoal);
                    }
                }
            }

            return ViewMyCommitments(classGoalPages);
        }

        public static ClassGoals GetClassGoals(Guid classId, CRMAccess crmAccess, SqlAzureLayer sql)
        {
          
            var blob = sql.GetGoalBlob(classId);

            if (blob == null)
            {
                // Get all Students
                var goals = crmAccess.GetClassGoals(classId).ToList();

                blob = new ClassGoals();

                foreach (ClassGoal classGoal in goals)
                {
                    if (classGoal.isdefault ?? false)
                        classGoal.selected = true;

                    var activities = crmAccess.GetGoalActivities(classGoal.id).ToList();

                    foreach (Activity newGoalactivity in activities)
                    {
                        var subactivities = crmAccess.GetSubActivities(newGoalactivity.id).ToList();

                        foreach (SubActivity subActivity in subactivities)
                        {
                            newGoalactivity.subActivities.Add(subActivity);
                        }
                        classGoal.activities.Add(newGoalactivity);
                    }

                    blob.classGoals.Add(classGoal);
                }

                blob.classid = classId;
                blob.lastupdated = DateTime.Now;
                sql.UpdateGoalBlob(blob);

            }
            return blob;
        }

        public static string ViewThisSelectedUser(Guid classId, CRMAccess crmAccess, Contact student, List<Contact> coaches, List<new_coaching> coachFeedback)
        {
            var studentId = student.ContactId.Value;
            
            string[] progressVAlues = new string[] { "None", "None", "Some", "Significant", "Completed" };
            
            var allUpdates = crmAccess.GetAllUpdatePeriods(studentId, classId);
           // var coachFeedback = crmAccess.GetCoachFeedbackbyStudent(classId, studentId);

            // Build complete summary of Updates and feedback.

            var sb = new StringBuilder(256);

            foreach (StudentUpdatePeriod thisUpdatePeriod in allUpdates)
            {
                var myUpdate = thisUpdatePeriod;

                myUpdate = crmAccess.GetMyUpdatePeriod(studentId, thisUpdatePeriod.ClassUpdatePeriodId,
                                                       thisUpdatePeriod);

                if (thisUpdatePeriod.ClassId != classId) continue;
                var start = myUpdate.startDate;
                var end = myUpdate.endDate;

                sb.AppendFormat("<b>Update Period {0} - {1}</b><br/>", start.ToShortDateString(),
                                end.ToShortDateString());

                bool hasUpdates = false;

                foreach (GoalAnswers answer in myUpdate.PeriodGoalAnswers)
                {
                    var progress = answer.ProgressValue;
                    if (progress != -1) hasUpdates = true;
                    if (!string.IsNullOrEmpty(answer.Answer1)) hasUpdates = true;
                    if (!string.IsNullOrEmpty(answer.Answer2)) hasUpdates = true;
                }


                if (  !hasUpdates )  // myUpdate.PeriodGoalAnswers.Count == 0 ) // myUpdate.PeriodGoalAnswers[0].ProgressValue == -1)
                {
                    sb.AppendFormat("No Updates.");
                    sb.AppendFormat("<br/>");
                    sb.AppendFormat("<br/>");

                    bool coachingDiv = false;

                    foreach (new_coaching newCoaching in coachFeedback)
                    {
                        if (newCoaching.new_ClassUpdatePeriodId.Id == myUpdate.PeriodId)
                        {
                            if (coachingDiv == false)
                            {
                                coachingDiv = true;
                                sb.AppendFormat("<div style='color:green; border: 1px solid red; padding:5px;'>");
                            }

                            var coachName = "";
                            foreach (Contact coach in coaches)
                            {
                                if (newCoaching.new_CoachId.Id == coach.ContactId)
                                {
                                    coachName = coach.FirstName + " " + coach.LastName;
                                    break;
                                }

                            }
                            sb.AppendFormat("<b>Coach Feedback from {0} on {1}</b><br/>", coachName,
                                            newCoaching.ModifiedOn.Value.ToShortDateString());

                            if (newCoaching.new_UpdateGrade != null)
                                sb.AppendFormat("Progress Grade:  {0}<br/><br/>",
                                                newCoaching.new_UpdateGrade.Value);



                            sb.AppendFormat("{0}", newCoaching.new_Advice);
                            sb.AppendFormat("<br/>");

                        }
                    }
                    if (coachingDiv)
                    {
                        sb.Append("</div>");
                        sb.Append("<hr/>");
                        sb.AppendFormat("<br/>");
                    }


                    continue;
                }


                sb.AppendFormat("<br/>");
                sb.AppendFormat(
                    "<b>Impact Question</b><br/> {0} <br/><span class='myanswers'>{1}</span><br/><br/>",
                    myUpdate.PeriodImpactQuestion, myUpdate.ImpactAnswer);
                sb.AppendFormat("<br/>");
                foreach (GoalAnswers answer in myUpdate.PeriodGoalAnswers)
                {
                    var progress = answer.ProgressValue;
                    if (progress == -1) progress = 0;

                    sb.AppendFormat(
                        "<hr/><b>{0}</b> <span class='myanswers'>(progress - {1})</span><br/><br/>",
                        answer.GoalName, progressVAlues[progress]);

                    var answer1 = string.IsNullOrEmpty(answer.Answer1) ? "None" : answer.Answer1;
                    sb.AppendFormat("{0}: <br/><span class='myanswers'>{1}</span><br/><br/>",
                                    myUpdate.GoalQuestion1, answer1);

                    var answer2 = string.IsNullOrEmpty(answer.Answer2) ? "None" : answer.Answer2;
                    sb.AppendFormat("{0}: <br/><span class='myanswers'>{1}</span><br/>",
                                    myUpdate.GoalQuestion2, answer2);

                    sb.AppendFormat("<br/>");

                    sb.AppendFormat("I used the following activities in this goal period:<br/>");


                    bool noActivities = true;
                    foreach (Activities act in answer.MyActivities)
                    {
                        if (act.IsChecked)
                        {
                            noActivities = false;
                            sb.AppendFormat("- <span class='myanswers'>{0}</span><br/>", act.Text);
                        }
                    }

                    if (noActivities) sb.AppendFormat("- No Activities Selected");


                    sb.AppendFormat("<br/><br/>");
                }

                sb.AppendFormat("Feedback Request: <br/>");
                sb.AppendFormat("<span class='myanswers'>{0}</span><br/><br/>",
                                thisUpdatePeriod.FeedbackRequest);

                sb.AppendFormat("<div style='color:green; border: 1px solid red; padding:5px;'>");
                foreach (new_coaching newCoaching in coachFeedback)
                {
                    if (newCoaching.new_ClassUpdatePeriodId.Id == myUpdate.PeriodId)
                    {
                        var coachName = "";
                        foreach (Contact coach in coaches)
                        {
                            if (newCoaching.new_CoachId.Id == coach.ContactId)
                            {
                                coachName = coach.FirstName + " " + coach.LastName;
                                break;
                            }

                        }
                        sb.AppendFormat("<b>Coach Feedback from {0} on {1}</b><br/>", coachName,
                                        newCoaching.ModifiedOn.Value.ToShortDateString());

                        if (newCoaching.new_UpdateGrade != null)
                            sb.AppendFormat("Progress Grade:  {0}<br/><br/>",
                                            newCoaching.new_UpdateGrade.Value);



                        sb.AppendFormat("{0}", newCoaching.new_Advice);
                        sb.AppendFormat("<br/>");

                    }
                }
                sb.Append("</div>");

                sb.Append("<hr/>");
                sb.AppendFormat("<br/>");
            }

           return sb.ToString();
        }


        public static string ViewThisSelectedUsersGoals(Guid classId, Guid studentId, SqlAzureLayer sql, CRMAccess crmAccess)
        {
            var sb = new StringBuilder(1024);

            var goals = ReportHelper.GetClassGoals(classId, crmAccess, sql);
            // Selected Goals
            var selectedGoals = crmAccess.GetStudentGoals(classId, studentId);

            var classGoalPages = new List<ClassGoal>();

            // Loop through all Goals and create a list of all Goals and activities
            foreach (ClassGoal classGoal in goals.classGoals)
            {
                foreach (new_studentgoals studentgoal in selectedGoals)
                {
                    if (classGoal.id == studentgoal.new_classgoalId.Id)
                    {
                        classGoal.selected = true;
                        classGoal.initialselected = true;
                        classGoal.payoff = studentgoal.new_Benefit;
                        classGoal.initialpayoff = studentgoal.new_Benefit;

                        var goalActivities = crmAccess.GetGoalActivities(studentgoal.new_classgoalId.Id).ToList(); 

                        foreach (Activity activity in goalActivities)
                        {
                            var anActivity = new Activity
                            {
                                name = activity.name,
                                id = activity.id,
                                selected = false
                            };

                            var subactivities = crmAccess.GetSubActivities(activity.id).ToList(); 

                            foreach (SubActivity newSubactivity in subactivities)
                            {
                                var subActivity = new SubActivity
                                {
                                    id = newSubactivity.id,
                                    name = newSubactivity.name,
                                    selected = false,
                                };

                                //var subRedundant = false;
                                //foreach (var existingSubactivity in subactivities)
                                //{
                                //    if (existingSubactivity.id == subActivity.id) subRedundant = true;
                                //}
                               
                                //if (!subRedundant) 
                                
                                anActivity.subActivities.Add(subActivity);
                            }

                            Activity removeActivity = null;

                            foreach(Activity existingActivity in classGoal.activities)
                            {
                                if(  existingActivity.id == anActivity.id)
                                {
                                    removeActivity = existingActivity;
                                }
                            }

                            if( removeActivity!= null)
                            {
                                classGoal.activities.Remove(removeActivity);
                            }
                                
                            classGoal.activities.Add(anActivity);
                          
                        }

                        var selectedGoalActivities =
                            crmAccess.GetStudentGoalActivities(classId, studentId,
                                                               studentgoal.new_classgoalId.Id).ToList();

                        // If there are no selected activities than this is all subactivities
                        if (selectedGoalActivities.Count != 0)

                            foreach (new_goalactivity selectedGoalActivity in selectedGoalActivities)
                            {
                                foreach (Activity anActivity in classGoal.activities)
                                {
                                    if (selectedGoalActivity.new_goalactivityId.Value == anActivity.id)
                                    {
                                        anActivity.selected = true;
                                        anActivity.initialselected = true;
                                    }
                                }
                            }
                        else
                        {
                            var selectedGoalSubActivities =
                                crmAccess.GetStudentGoalSubActivities(classId, studentId,
                                                                      studentgoal.new_classgoalId.Id).ToList();

                            foreach (new_subactivity selectedGoalSubActivity in selectedGoalSubActivities)
                            {
                                foreach (Activity anActivity in classGoal.activities)
                                {
                                    foreach (SubActivity aSubActivity in anActivity.subActivities)
                                    {
                                        if (aSubActivity.id ==
                                            selectedGoalSubActivity.new_subactivityId.Value)
                                        {
                                            aSubActivity.selected = true;
                                            aSubActivity.initialselected = true;
                                        }
                                    }
                                }
                            }
                        }


                        // loadedGoals.Add(studentgoal.new_classgoalId.Id);
                    }
                }

                classGoalPages.Add(classGoal);
            }




            // Generate Table to send to Telerik report engine to generate an email with PDF attachment to the student.
            foreach (ClassGoal goalPage in classGoalPages)
            {
                if (!goalPage.selected) continue;

                sb.AppendFormat("<H2>{0}</H2><br/>", goalPage.name);
                sb.AppendFormat("<b>Payoff:</b>  {0}<br/><br/>", goalPage.payoff);

                sb.AppendFormat("<b>Activities:</b><br/>");
                sb.AppendFormat("<ul>");
                foreach (Activity newGoalactivity in goalPage.activities)
                {
                    if (!newGoalactivity.selected && newGoalactivity.subActivities.Count == 0) continue;

                    if (newGoalactivity.subActivities.Count != 0)
                    {
                        foreach (SubActivity newSubactivity in newGoalactivity.subActivities)
                        {
                            if (newSubactivity.selected)
                            {
                                sb.AppendFormat("<li>{0}</li>", newSubactivity.name);
                            }

                        }
                    }
                    else if (newGoalactivity.selected)
                    {
                        sb.AppendFormat("<li>{0}</li>", newGoalactivity.name);
                    }
                }
                sb.AppendFormat("</ul>");
                sb.AppendFormat("<hr/>");
                sb.AppendFormat("<br/>");
            }
            return sb.ToString();
        }

        public static RenderingResult GetTelerikGoalReport(List<ClassGoal> classGoalPages, Contact myContact)
        {
            var printTable = new DataTable();
            printTable.Columns.Add("GoalName");
            printTable.Columns.Add("Activity");
            printTable.Columns.Add("SubActivity");

            // Generate Table to send to Telerik report engine to generate an email with PDF attachment to the student.

            foreach (ClassGoal goalPage in classGoalPages)
            {
                if (!goalPage.selected) continue;

                var goalName = goalPage.name;
                var payoff = string.Format("Payoff: {0}", goalPage.payoff);

                var goalRow = printTable.NewRow();
                goalRow.ItemArray = new[] { goalName, payoff, "" };
                printTable.Rows.Add(goalRow);

                var activityName = "";
                var subActivityName = "";

                foreach (Activity newGoalactivity in goalPage.activities)
                {
                    if (!newGoalactivity.selected && newGoalactivity.subActivities.Count == 0) continue;

                    activityName = newGoalactivity.name;
                    var actRow = printTable.NewRow();
                    actRow.ItemArray = new[] { "", activityName, "" };

                    int subCount = 0;
                    var subRows = new List<DataRow>();
                    if (newGoalactivity.subActivities.Count != 0)
                    {
                        foreach (SubActivity newSubactivity in newGoalactivity.subActivities)
                        {
                            if (newSubactivity.selected)
                            {
                                subCount++;
                                subActivityName = newSubactivity.name;
                                var newRow = printTable.NewRow();
                                newRow.ItemArray = new[] { "", "", subActivityName };
                                subRows.Add(newRow);
                            }

                        }
                    }

                    if (subCount > 0)
                    {
                        foreach (DataRow dataRow in subRows)
                        {
                            printTable.Rows.Add(dataRow);
                        }
                    }
                    else if (newGoalactivity.selected)
                    {
                        printTable.Rows.Add(actRow);
                    }
                }

                var spacerRow = printTable.NewRow();
                spacerRow.ItemArray = new[] { "", "", "" };
                printTable.Rows.Add(spacerRow);
            }

            var company = "";
            if (myContact.ParentContactId != null)
            {
                company = " for " + myContact.ParentCustomerId.Name;
            }

            var name = String.Format("{0} {1} {2}", myContact.FirstName, myContact.LastName, company);
            var objectDataSource = new Telerik.Reporting.ObjectDataSource { DataSource = printTable };
            var goalsReport = new MyGoals(name) { DataSource = objectDataSource };
            var reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("PDF", goalsReport, null);

            return result;
        }

        public static RenderingResult GetTelerikTrackingReport(Guid classId, string className, CRMAccess crmAccess)
        {
            var printTable = new DataTable();
            printTable.Columns.Add("TouchName");
            printTable.Columns.Add("StudentName");
            printTable.Columns.Add("RoleName");
            printTable.Columns.Add("DateViewed");
            printTable.Columns.Add("ResourceViewed");

            var touches = crmAccess.GetScheduledTouches(classId);

            var participants = crmAccess.GetClassParticipants(classId);

           
            // Generate Table to send to Telerik report engine to generate an email with PDF attachment to the student.)
            foreach (var newScheduledtouch in touches)
            {
                if (newScheduledtouch.new_Sent == true)
                {
                   

                    var touchRow = printTable.NewRow();
                    touchRow.ItemArray = new[] {newScheduledtouch.new_TriggerDate.Value.ToString("MM/dd/yyyy")+ " - " +newScheduledtouch.new_name , "", "", "","" };
                    printTable.Rows.Add(touchRow);


                    var tracking = crmAccess.GetAssetTouches(newScheduledtouch.new_scheduledtouchId.Value);

                    var trackingPoints = new List<string>();
                    
                    foreach (var trkAssettracking in tracking)
                    {
                        if (trkAssettracking.trk_ContactId == null) continue;

                        var trackingPoint = string.Format("{0}{1}{2}", trkAssettracking.trk_ContactId.Id,trkAssettracking.trk_AssetViewedId.Id,
                                                          trkAssettracking.CreatedOn.Value.ToString("MMddyyyyHHmm"));


                        if (!trackingPoints.Contains(trackingPoint))
                        {
                            trackingPoints.Add(trackingPoint);

                            var roleName = "";
                            foreach (Contact contact in participants)
                            {
                                if (contact.ContactId.Value == trkAssettracking.trk_ContactId.Id)
                                {
                                    switch (contact.new_ContactRxRole.Value)
                                    {
                                        case 100000000:
                                            roleName = "Student";
                                            break;
                                        case 100000001:
                                            roleName = "Coach";
                                            break;
                                        case 100000002:
                                            roleName = "Sponsor";
                                            break;
                                    }
                                }
                            }
                            var trackingRow = printTable.NewRow();
                            trackingRow.ItemArray = new[]
                                                        {
                                                            "", trkAssettracking.trk_ContactId.Name, roleName,
                                                            trkAssettracking.CreatedOn.Value.ToLocalTime().ToString("g")
                                                            ,
                                                            trkAssettracking.trk_AssetViewedId.Name
                                                        };
                            printTable.Rows.Add(trackingRow);
                        }
                    }
                    var spacerRow = printTable.NewRow();
                    spacerRow.ItemArray = new[] { "", "", "", "","" };
                    printTable.Rows.Add(spacerRow);
                }
            }
            
           
            var objectDataSource = new Telerik.Reporting.ObjectDataSource { DataSource = printTable };
            var trackingReport = new Tracking(className) { DataSource = objectDataSource };
            var reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("PDF", trackingReport, null);

            return result;
        }

        private  static List<Activity> GetGoalActivities(Guid goalId, CRMAccess crmAccess)
        {
            var goalActivities = crmAccess.GetGoalActivities(goalId).ToList();
            return goalActivities;

            //            var sql = new SqlAzureLayer(GetConnectionString());
            //            var dbActivities = sql.GetDataTable(string.Format("select activityid,name,sortorder from dbo.Activity where goalid={0} order by sortorder", sql.FormatGuid(goalId)));

            //            if (dbActivities != null && dbActivities.Rows.Count != 0)
            //            {
            //                foreach (DataRow anActivity in dbActivities.Rows)
            //                {
            //                    var newGoal = new Activity()
            //                    {
            //                        id = (Guid)sql.GetDataRowGuid(anActivity[0]),
            //                        sortorder = sql.GetDataRowString(anActivity[2]),
            //                        name = sql.GetDataRowString(anActivity[1])
            //                    };
            //                    goalActivities.Add(newGoal);
            //                }


            //            }
            //            else
            //            {
            //                goalActivities = crmAccess.GetGoalActivities(goalId).ToList();

            //                // Create Goals in db
            //                foreach (Activity newActivity in goalActivities)
            //                {
            //                    var createGoal = string.Format(@"INSERT INTO dbo.Activity
            //           ([activityid]
            //           ,[goalid]
            //           ,[sortorder]
            //           ,[name])
            //     VALUES
            //           ({0}
            //           ,{1}
            //           ,{2}          
            //           ,{3}
            //           )",
            //        sql.FormatGuid(newActivity.id),
            //         sql.FormatGuid(goalId),
            //         sql.FormatString(newActivity.sortorder),
            //         sql.FormatString(newActivity.name)
            //       );
            //                    sql.SqlExecuteNonQuery(createGoal);
            //                }
            //            }

            //            return goalActivities;

        }

        private static List<SubActivity> GetSubActivities(Guid activityId, CRMAccess crmAccess)
        {
            var subActivities = crmAccess.GetSubActivities(activityId).ToList();
            return subActivities;

            //            var sql = new SqlAzureLayer(GetConnectionString());
            //            var dbSubActivities = sql.GetDataTable(string.Format("select subactivityid,name,sortorder from dbo.SubActivity where activityid={0} order by sortorder", sql.FormatGuid(activityId)));

            //            if (dbSubActivities != null && dbSubActivities.Rows.Count != 0)
            //            {
            //                foreach (DataRow aSubActivity in dbSubActivities.Rows)
            //                {
            //                    var newGoal = new SubActivity()
            //                    {
            //                        id = (Guid)sql.GetDataRowGuid(aSubActivity[0]),
            //                        name = sql.GetDataRowString(aSubActivity[1]),
            //                        sortorder = sql.GetDataRowString(aSubActivity[2])
            //                    };
            //                    subActivities.Add(newGoal);
            //                }


            //            }
            //            else
            //            {
            //                subActivities = crmAccess.GetSubActivities(activityId).ToList();

            //                // Create Goals in db
            //                foreach (SubActivity newSubActivity in subActivities)
            //                {
            //                    var createGoal = string.Format(@"INSERT INTO dbo.SubActivity
            //           ([subactivityid]
            //           ,[activityid]
            //           ,[sortorder]
            //           ,[name])
            //     VALUES
            //           ({0}
            //           ,{1}
            //           ,{2}          
            //           ,{3}
            //           )",
            //        sql.FormatGuid(newSubActivity.id),
            //         sql.FormatGuid(activityId),
            //         sql.FormatString(newSubActivity.sortorder),
            //         sql.FormatString(newSubActivity.name)
            //       );
            //                    sql.SqlExecuteNonQuery(createGoal);
            //                }
            //            }

            //            return subActivities;

        }
    }

}