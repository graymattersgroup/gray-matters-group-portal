﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="GoalManagement.ErrorPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="text-align:center;">
<h3>There has been a server error</h3>
    <br />
<h3>An error detailing the problem has been emailed to our development staff.</h3>
<br/>
    <a href="~/Account/Login.aspx">Login</a>
</div>

</asp:Content>
