﻿using System;

namespace TelerikReporting
{
    public static class RF
    {
        public static string InsertNotNull(object value, string suffix, bool isProperCase, string prefix)
        {
            if (null == value || System.DBNull.Value == value || String.IsNullOrEmpty(value.ToString().Trim()))
            {
                return string.Empty;
            }

            if (isProperCase)
            {
                var checkValue = value.ToString().ToLower().Trim();

                if (checkValue == "po box")
                {
                    return prefix + "PO Box" + suffix;
                }

                if (checkValue.StartsWith("rr"))
                {
                    return prefix + checkValue.ToUpper() + suffix;
                }

                return prefix + System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(
                        checkValue) + suffix;
            }
            else
            {
                return prefix + value + suffix;
            }
        }

        public static string Phone(object value, string suffix, string prefix)
        {
            if (null == value || System.DBNull.Value == value || String.IsNullOrEmpty(value.ToString().Trim()))
            {
                return string.Empty;
            }

            var phoneNumber = value.ToString().Trim();

            if (phoneNumber.Length == 7) phoneNumber = phoneNumber.Insert(3, "-");

            return prefix + phoneNumber + suffix;
        }

        public static string InsertNotNull2(object value, object value2, string suffix, bool isProperCase, string prefix)
        {
            if (null == value || System.DBNull.Value == value || String.IsNullOrEmpty(value.ToString().Trim()))
            {
                return string.Empty;
            }

            if (isProperCase)
            {
                var checkValue = value2.ToString().ToLower().Trim();

                if (checkValue == "po box" || checkValue.StartsWith("rr"))
                {
                    return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(
                       value.ToString().ToLower().Trim()) + suffix;
                }

                return prefix + System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(
                        value.ToString().ToLower().Trim()) + suffix;
            }
            else
            {
                return prefix + value + suffix;
            }
        }

    }
}
