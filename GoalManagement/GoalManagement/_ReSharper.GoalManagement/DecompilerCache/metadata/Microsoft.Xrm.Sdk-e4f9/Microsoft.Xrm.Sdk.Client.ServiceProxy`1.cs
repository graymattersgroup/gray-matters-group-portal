// Type: Microsoft.Xrm.Sdk.Client.ServiceProxy`1
// Assembly: Microsoft.Xrm.Sdk, Version=5.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
// Assembly location: G:\Work\CRM\SDK 5.0.1\sdk\bin\microsoft.xrm.sdk.dll

using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;

namespace Microsoft.Xrm.Sdk.Client
{
    public abstract class ServiceProxy<TService> : IDisposable where TService : class
    {
        protected ServiceProxy(IServiceConfiguration<TService> serviceConfiguration,
                               SecurityTokenResponse securityTokenResponse);

        protected ServiceProxy(IServiceConfiguration<TService> serviceConfiguration, ClientCredentials clientCredentials);

        protected ServiceProxy(Uri uri, Uri homeRealmUri, ClientCredentials clientCredentials,
                               ClientCredentials deviceCredentials);

        public IServiceConfiguration<TService> ServiceConfiguration { get; }
        public ClientCredentials ClientCredentials { get; }
        public SecurityTokenResponse SecurityTokenResponse { get; }
        public SecurityTokenResponse HomeRealmSecurityTokenResponse { get; }
        public Uri HomeRealmUri { get; }
        public ClientCredentials DeviceCredentials { get; }
        public ServiceChannel<TService> ServiceChannel { get; }
        public TimeSpan Timeout { get; set; }
        public bool IsAuthenticated { get; }
        protected ChannelFactory<TService> ChannelFactory { get; }

        #region IDisposable Members

        public void Dispose();

        #endregion

        public void Authenticate();
        public SecurityTokenResponse AuthenticateCrossRealm();
        public SecurityTokenResponse AuthenticateDevice();
        protected virtual SecurityTokenResponse AuthenticateCrossRealmCore();
        protected bool? ShouldRetry(MessageSecurityException messageSecurityException, bool? retry);
        protected virtual void ValidateAuthentication();
        protected virtual SecurityTokenResponse AuthenticateDeviceCore();
        protected virtual void AuthenticateCore();
        protected virtual void CloseChannel(bool forceClose);

        protected static void SetBindingTimeout(Binding binding, TimeSpan sendTimeout, TimeSpan openTimeout,
                                                TimeSpan closeTimeout);

        protected virtual void OnFactoryFaulted(ChannelFaultedEventArgs args);
        protected virtual void OnFactoryClosed(ChannelEventArgs args);
        protected virtual void OnFactoryOpened(ChannelEventArgs args);
        ~ServiceProxy();
        protected virtual void Dispose(bool disposing);
        public event EventHandler<ChannelFaultedEventArgs> FactoryFaulted;
        public event EventHandler<ChannelEventArgs> FactoryOpened;
        public event EventHandler<ChannelEventArgs> FactoryClosed;
    }
}
