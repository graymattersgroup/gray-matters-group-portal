// Type: System.Web.SessionState.HttpSessionState
// Assembly: System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
// Assembly location: C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0\System.Web.dll

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Runtime;
using System.Web;

namespace System.Web.SessionState
{
    public sealed class HttpSessionState : ICollection, IEnumerable
    {
        public string SessionID { get; }
        public int Timeout { get; set; }
        public bool IsNewSession { get; }
        public SessionStateMode Mode { get; }
        public bool IsCookieless { get; }
        public HttpCookieMode CookieMode { get; }
        public int LCID { get; set; }
        public int CodePage { get; set; }
        public HttpSessionState Contents { get; }
        public HttpStaticObjectsCollection StaticObjects { get; }
        public object this[string name] { get; set; }
        public object this[int index] { get; set; }
        public NameObjectCollectionBase.KeysCollection Keys { get; }
        public bool IsReadOnly { get; }

        #region ICollection Members

        public IEnumerator GetEnumerator();
        public void CopyTo(Array array, int index);
        public int Count { get; }
        public object SyncRoot { get; }
        public bool IsSynchronized { get; }

        #endregion

        public void Abandon();
        public void Add(string name, object value);
        public void Remove(string name);
        public void RemoveAt(int index);
        public void Clear();

        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public void RemoveAll();
    }
}
