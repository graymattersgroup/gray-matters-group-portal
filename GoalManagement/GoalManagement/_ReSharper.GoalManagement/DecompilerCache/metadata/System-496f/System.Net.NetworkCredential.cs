// Type: System.Net.NetworkCredential
// Assembly: System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
// Assembly location: C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0\System.dll

using System;
using System.Runtime;
using System.Security;

namespace System.Net
{
    public class NetworkCredential : ICredentials, ICredentialsByHost
    {
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public NetworkCredential();

        public NetworkCredential(string userName, string password);
        public NetworkCredential(string userName, SecureString password);
        public NetworkCredential(string userName, string password, string domain);
        public NetworkCredential(string userName, SecureString password, string domain);
        public string UserName { get; set; }
        public string Password { get; set; }
        public SecureString SecurePassword { get; set; }
        public string Domain { get; set; }

        #region ICredentials Members

        public NetworkCredential GetCredential(Uri uri, string authType);

        #endregion

        #region ICredentialsByHost Members

        public NetworkCredential GetCredential(string host, int port, string authenticationType);

        #endregion
    }
}
