// Type: Microsoft.Xrm.Sdk.Client.IServiceManagement`1
// Assembly: Microsoft.Xrm.Sdk, Version=5.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
// Assembly location: G:\Work\GrayMattersGroup\Solutions\GoalManagement\GoalManagement\bin\microsoft.xrm.sdk.dll

using System.ServiceModel;
using System.ServiceModel.Description;

namespace Microsoft.Xrm.Sdk.Client
{
    public interface IServiceManagement<TService>
    {
        ServiceEndpoint CurrentServiceEndpoint { get; set; }
        AuthenticationProviderType AuthenticationType { get; }
        IssuerEndpointDictionary IssuerEndpoints { get; }
        CrossRealmIssuerEndpointCollection CrossRealmIssuerEndpoints { get; }
        PolicyConfiguration PolicyConfiguration { get; }
        ChannelFactory<TService> CreateChannelFactory();
        ChannelFactory<TService> CreateChannelFactory(ClientAuthenticationType clientAuthenticationType);
        ChannelFactory<TService> CreateChannelFactory(ClientCredentials clientCredentials);
        AuthenticationCredentials Authenticate(AuthenticationCredentials authenticationCredentials);
        IdentityProvider GetIdentityProvider(string userPrincipalName);
    }
}
