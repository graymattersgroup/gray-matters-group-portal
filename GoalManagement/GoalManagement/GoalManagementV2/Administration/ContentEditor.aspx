﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ContentEditor.aspx.cs" Inherits="GoalManagementV2.Administration.ContentEditor" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section>
        <div class="title">
            <h1>Content Management</h1>
        </div>
        <div class="content">
            <asp:Button ID="CreateCustomResource" runat="server" Text="Create New Custom Resource"
                OnClick="CreateCustomResource_Click" Visible="false" />
            <asp:DropDownList ID="ClassList" runat="server" Font-Size="9pt" OnSelectedIndexChanged="ClassList_SelectedIndexChanged"
                AutoPostBack="true">
            </asp:DropDownList>
            <asp:DropDownList ID="PageList" AutoPostBack="true" runat="server" OnSelectedIndexChanged="PageList_SelectedIndexChanged">
            </asp:DropDownList>

            <asp:Button ID="SaveContent" runat="server" Text="Save"
                OnClick="SaveContent_Click" />
            <span style="color: Red; background-color: White;">
                <asp:Label ID="Message" runat="server" Text="Label"></asp:Label></span>
            <br />
            <br />

            <telerik:RadEditor ID="RadEditor1" runat="server" ContentAreaMode="Div" Skin="Sitefinity"
                Height="562px" Width="747px">
            </telerik:RadEditor>
            <br />
        </div>
    </section>
</asp:Content>
