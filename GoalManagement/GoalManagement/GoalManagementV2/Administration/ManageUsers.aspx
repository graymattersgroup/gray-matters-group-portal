﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ManageUsers.aspx.cs" Inherits="GoalManagementV2.Administration.ManageUsers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section>
        <div class="title">
            <h1>User Management</h1>
        </div>
        <div class="content">
            <telerik:RadGrid AutoGenerateColumns="False" ID="UserRadGrid"
                AllowFilteringByColumn="True" AllowSorting="True" PageSize="15" AllowMultiRowSelection="False"
                ShowFooter="True" AllowPaging="True" runat="server" GridLines="None"
                EnableLinqExpressions="False" Skin="Web20"
                OnSelectedIndexChanged="UserRadGrid_SelectedIndexChanged"
                ShowGroupPanel="True">
                <PagerStyle Mode="NextPrevAndNumeric" />
                <GroupingSettings CaseSensitive="false" />
                <MasterTableView AutoGenerateColumns="false" EditMode="InPlace" AllowFilteringByColumn="True"
                    ShowFooter="True" TableLayout="Auto">
                    <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>

                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridTemplateColumn UniqueName="CheckBoxTemplateColumn">
                            <HeaderTemplate>
                                <asp:CheckBox ID="headerChkbox" OnCheckedChanged="ToggleSelectedState" AutoPostBack="True"
                                    runat="server"></asp:CheckBox>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox1" OnCheckedChanged="ToggleRowSelection" AutoPostBack="True"
                                    runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="LastName" HeaderText="LastName"
                            SortExpression="LastName" UniqueName="LastName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="name" HeaderText="FirstName" SortExpression="name"
                            UniqueName="name">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="username" HeaderText="username" SortExpression="username"
                            UniqueName="username" ShowFilterIcon="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsLockedOut" DataType="System.Boolean" HeaderText="Locked"
                            SortExpression="IsLockedOut" UniqueName="IsLockedOut">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="IsApproved" DataType="System.Boolean" HeaderText="Activated"
                            SortExpression="IsApproved" UniqueName="IsApproved">
                        </telerik:GridCheckBoxColumn>
                    </Columns>

                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <ClientSettings AllowDragToGroup="True">
                    <Scrolling AllowScroll="false" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>

                <FilterMenu EnableImageSprites="False"></FilterMenu>

                <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Web20"></HeaderContextMenu>
            </telerik:RadGrid>
            <br />
            <span>Selected User: &nbsp;&nbsp; <b>
                <asp:Label ID="username" runat="server" Text="None Selected"></asp:Label></b>&nbsp;&nbsp;
            </span>
            <br />
            <table style="margin-top: 5px; width: 100%">
                <tr>

                    <td>

                        <asp:Button ID="ImpersonateUser" runat="server" Text="Impersonate User"
                            OnClick="ImpersonateUser_Click" />
                        <br />
                        <br />
                        <asp:Button ID="ClearUser" runat="server" Text="Clear CRM Goals and Feedback #1"
                            OnClick="ClearUser_Click" />
                        <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to Clear this User"
                            Enabled="True" TargetControlID="ClearUser">
                        </asp:ConfirmButtonExtender>
                        <br />
                        <br />
                        <asp:Button ID="DeleteUser" runat="server" Text="Delete Portal Credentials #2" OnClick="DeleteUser_Click" />
                        <asp:ConfirmButtonExtender ID="DeleteUser_ConfirmButtonExtender" runat="server" ConfirmText="Are you sure you want to delete this User"
                            Enabled="True" TargetControlID="DeleteUser">
                        </asp:ConfirmButtonExtender>
                    </td>

                    <td>&nbsp;</td>
                    <td>New Password:
                    <asp:TextBox ID="NewPassword" runat="server" Width="100px"></asp:TextBox>
                        <br />
                        <br />
                        <asp:Button ID="ChangePassword" runat="server" Text="Change Password" OnClick="ChangePassword_Click" />
                    </td>

                    <td>
                        <asp:Button ID="ConfirmUser" runat="server" Text="Activate User" OnClick="ConfirmUser_Click" />
                        <br />
                        <br />
                        <asp:Button ID="UnlockUser" runat="server" Text="Unlock User" OnClick="UnlockUser_Click" />
                    </td>
                </tr>
            </table>


        </div>

    </section>









</asp:Content>
