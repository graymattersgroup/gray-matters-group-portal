﻿using System;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Telerik.Web.UI;

namespace GoalManagementV2.Administration
{
    public partial class ManageUsers : System.Web.UI.Page
    {
        protected CRMAccess crmAccess;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.MaintainScrollPositionOnPostBack = true;

            if (!IsPostBack)
            {
                RefreshUsers();

                var masterPage = (SiteMaster)Page.Master; //TODO::WideSiteMaster
                masterPage.SetMenu(CentralizedData.UserManagement);
            }
            else
            {

                UserRadGrid.DataSource = (DataTable)Session["UserList"];
            }
        }

        protected void ToggleRowSelection(object sender, EventArgs e)
        {
            ((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
            UserRadGrid_SelectedIndexChanged(sender, e);
        }

        protected void ToggleSelectedState(object sender, EventArgs e)
        {
            CheckBox headerCheckBox = (sender as CheckBox);
            foreach (GridDataItem dataItem in UserRadGrid.MasterTableView.Items)
            {
                (dataItem.FindControl("CheckBox1") as CheckBox).Checked = headerCheckBox.Checked;
                dataItem.Selected = headerCheckBox.Checked;
            }
            UserRadGrid_SelectedIndexChanged(sender, e);
        }

        void RefreshUsers()
        {
            var getusers =
                @"SELECT    
	  ''as LastName
  ,''as name
  , UserName  
   
  , IsLockedOut
	,IsApproved 
  FROM  dbo.aspnet_Users u 
  join dbo.aspnet_Membership m on m.userid = u.userid
 ";

            var db = new SqlAzureLayer(GetConnectionString());
            var users = db.GetDataTable(getusers);

            if (Session["crmAccess"] == null)
            {
                crmAccess = new CRMAccess();
                Session["crmAccess"] = crmAccess;
            }
            else
            {
                crmAccess = (CRMAccess)Session["crmAccess"];
            }

            var contacts = crmAccess.GetAllContacts();

            foreach (DataRow row in users.Rows)
            {
                foreach (Contact contact in contacts)
                {
                    if (row[2].ToString().Trim().ToLower() == contact.EMailAddress1.Trim().ToLower())
                    {
                        row[0] = contact.LastName;
                        row[1] = contact.FirstName;
                        break;
                    }
                }
            }

            Session["UserList"] = users;

            UserRadGrid.DataSource = users;
            UserRadGrid.DataBind();

            //foreach(DataRow row in users.Rows)
            //{
            //    var text = string.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}", row[0], row[1], row[2], row[3], row[4], row[5], row[6]);
            //    ExistingUsers.Items.Add(new ListItem(text, row[0].ToString()));
            //}

            // var users = Membership.GetAllUsers();
            //ExistingUsers.Items.Clear();

            //foreach (MembershipUser user in users)
            //{
            //    var userRoles = Roles.GetRolesForUser(user.UserName);
            //    var showrole = "";
            //    foreach (var userRole in userRoles)
            //    {
            //        if (showrole != "") showrole += ",";
            //        showrole += userRole;
            //    }

            //    var text = string.Format("{0}, {1}, {2}, {3}", user.UserName,showrole, user.IsApproved? "Confirmed":"UnConfirmed", user.IsLockedOut ? "Locked" : "Unlocked",
            //                             showrole);
            //    ExistingUsers.Items.Add(new ListItem(text, user.UserName));
            //}
        }

        //void RefreshGroups()
        //{
        //    string[] roles = Roles.GetAllRoles();

        //    PermissionGroup.Items.Clear();

        //    foreach (var role in roles)
        //    {
        //        PermissionGroup.Items.Add(role);
        //    }
        //}


        //protected void CreateGroup_Click(object sender, EventArgs e)
        //{
        //    Roles.CreateRole(GroupName.Text);
        //    RefreshGroups();
        //}






        private void FillDropDownList(DropDownList listBox, string selectList, ListItem defaultItem, SqlAzureLayer db)
        {
            var groupTable = db.GetDataTable(selectList);

            listBox.Items.Clear();
            listBox.Items.Add(defaultItem);
            listBox.SelectedIndex = 0;

            foreach (DataRow row in groupTable.Rows)
            {
                listBox.Items.Add(new ListItem(row[0].ToString(), row[1].ToString()));
            }
        }


        private void FillListBox(ListBox listBox, string selectList, SqlAzureLayer db)
        {
            var groupTable = db.GetDataTable(selectList);
            listBox.Items.Clear();
            foreach (DataRow row in groupTable.Rows)
            {
                listBox.Items.Add(new ListItem(row[0].ToString(), row[1].ToString()));
            }
        }

        //protected void AddUsertoGroup_Click(object sender, EventArgs e)
        //{
        //    if (Groups.SelectedValue != null)
        //    {
        //        if (ExistingUsers.SelectedValue != null)
        //        {
        //            if (Roles.GetRolesForUser(ExistingUsers.SelectedValue).Length == 0)
        //            {
        //                var role = Groups.SelectedValue;
        //                Roles.AddUsersToRole(new string[] { ExistingUsers.SelectedValue }, role);

        //                RefreshUsers();
        //            }
        //        }
        //    }

        //}

        //protected void RemoveGroup_Click(object sender, EventArgs e)
        //{
        //    if (Groups.SelectedValue != null)
        //    {
        //        Roles.DeleteRole(Groups.SelectedValue);

        //        RefreshUsers();
        //        RefreshGroups();
        //    }
        //}

        //protected void RemoveUserFromGroup_Click(object sender, EventArgs e)
        //{
        //    if (Groups.SelectedValue != null)
        //    {
        //        if (ExistingUsers.SelectedValue != null)
        //        {
        //            var user = Membership.GetUser(ExistingUsers.SelectedValue);
        //            var role = Groups.SelectedValue;

        //            Roles.RemoveUserFromRole(user.UserName, role);

        //            RefreshUsers();
        //        }
        //    }
        //}

        //protected void UpdateProfile_Click(object sender, EventArgs e)
        //{
        //    var itemsSelected = UserRadGrid.SelectedIndexes; // ExistingUsers.GetSelectedIndices();

        //    if (itemsSelected.Count == 0) return;

        //    var isMultimode = (itemsSelected.Count > 1);

        //    var db = new SqlAzureLayer(GetConnectionString());

        //    foreach (GridDataItem dataItem in UserRadGrid.MasterTableView.Items)
        //    {
        //        if (dataItem.Selected)
        //        {
        //            var thisusername = dataItem["username"].Text;

        //            var myProfile = new UserProfile(db, thisusername);
        //            var p = myProfile.Profile;

        //            p.isResidential = AllowResidential.Checked;
        //            p.isCommercial = AllowCommercial.Checked;
        //            p.isNational = AllowNationalAccess.Checked;
        //            p.isGroupExport = AllowGroupExport.Checked;
        //            p.isReport = AllowReporting.Checked;
        //            p.isViewIndividual = AllowViewIndividual.Checked;
        //            if( string.IsNullOrEmpty(maxreportfields.Text))
        //            {
        //                p.maxreportfields = 1000;
        //            }
        //            else
        //            {
        //                p.maxreportfields = Convert.ToInt32(maxreportfields.Text);
        //            }

        //            p.DncStatus = Convert.ToInt32(DncStatus.SelectedValue);
        //            p.defaultState = DefaultState.SelectedValue;

        //            if ( !string.IsNullOrEmpty(PresetList.SelectedValue))  p.defaultPresetid = new Guid(PresetList.SelectedValue);

        //            p.isActive = isActive.Checked;

        //            p.CompanyName = Company.Text;
        //            p.AddressLine1 = AddressLine1.Text;
        //            p.AddressLine2 = AddressLine2.Text;
        //            p.City = City.Text;
        //            p.State = State.Text;
        //            p.ZipCode = ZipCode.Text;

        //            if (string.IsNullOrEmpty(ExpirationDate.Text))
        //            {
        //                p.expirationDate = DateTime.Today.AddDays(7);
        //            }
        //            else
        //            {
        //                 p.expirationDate = Convert.ToDateTime(ExpirationDate.Text);
        //            }


        //            if (!isMultimode)
        //            {

        //                p.recordsPurchased = Convert.ToInt64(RecordsPurchased.Text);
        //                p.name = UsersName.Text;
        //                p.routesMade = Convert.ToInt64(ReportsMade.Text);
        //                p.recordsReturned = Convert.ToInt64(RecordsReturned.Text);
        //                p.maximumSearchRows = Convert.ToInt32(ResultsToReturn.Text);

        //                p.name = UsersName.Text;

        //                p.Phone = Phone.Text;
        //                p.Notes = Notes.Text;
        //                p.LastName = LastName.Text;
        //            }


        //            if (GroupList.SelectedIndex != 0)
        //            {
        //                p.profileGroupid = new Guid(GroupList.SelectedValue);
        //            }
        //            else
        //            {
        //                p.profileGroupid = null;
        //            }

        //            if (IndustryList.SelectedIndex != 0)
        //            {
        //                p.industryid = new Guid(IndustryList.SelectedValue);
        //            }
        //            else
        //            {
        //                p.industryid = null;
        //            }

        //            // Save regions for a given user.
        //            var stateFilter = "";

        //            foreach (ListItem item in StateList.Items)
        //            {
        //                if (item.Selected)
        //                {
        //                    stateFilter += item.Value + ",";
        //                }

        //            }

        //            if (stateFilter != "")
        //            {
        //                var countyFilter = "";

        //                foreach (ListItem item in Counties.Items)
        //                {
        //                    if (item.Selected)
        //                    {
        //                        countyFilter += item.Value + ",";
        //                    }
        //                }

        //                var zipFilter = "";

        //                if (countyFilter != "")
        //                {
        //                    foreach (ListItem item in Cities.Items)
        //                    {
        //                        if (item.Selected)
        //                        {
        //                            zipFilter += item.Value + ",";
        //                        }
        //                    }
        //                }


        //                p.regionFilters = string.Format("{0}|{1}|{2}", stateFilter.TrimEnd(','),
        //                                                countyFilter.TrimEnd(','),
        //                                                zipFilter.TrimEnd(','));
        //            }


        //            var shapeList = "";

        //            foreach (ListItem item in ShapeList.Items)
        //            {
        //                if (item.Selected)
        //                {
        //                    if (shapeList != "") shapeList += ",";
        //                    shapeList += item.Value;
        //                }
        //            }

        //            p.shapeList = shapeList;

        //            myProfile.SaveProfile(p, db, true);

        //            if (!isMultimode) Presets.GetMyPresets(PresetList, db, myProfile);
        //        }
        //    }

        //}





        protected void ChangePassword_Click(object sender, EventArgs e)
        {
            var user = Membership.GetUser(username.Text);
            string resetPassword = user.ResetPassword();
            user.ChangePassword(resetPassword, NewPassword.Text);
        }



        protected void UnlockUser_Click(object sender, EventArgs e)
        {
            var user = Membership.GetUser(username.Text);

            user.UnlockUser();

            RefreshUsers();
        }

        protected void ConfirmUser_Click(object sender, EventArgs e)
        {
            var user = Membership.GetUser(username.Text);

            user.IsApproved = true;

            Membership.UpdateUser(user);

            RefreshUsers();
        }


        protected void SearchDatabaseList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }




        private enum ConfiguredFields { residentialfieldid, fieldname, description, length, type, inputdisplayorder, outputdisplayorder, lookupid, sourcetable, alias, isLookup } ;


        void FillSelectionListBoxes(ListBox selectedList, ListBox unselectedList, DataTable dataList, bool isResidential, SqlAzureLayer db)
        {
            selectedList.Items.Clear();
            unselectedList.Items.Clear();

            if (dataList.Rows.Count != 0)
            {
                foreach (DataRow row in dataList.Rows)
                {

                    var newItem = new ListItem(row[0].ToString(),
                                              row[1].ToString());
                    newItem.Attributes.Add("title", row[2].ToString());
                    selectedList.Items.Add(newItem);
                }
            }

            var results2 = (isResidential) ?
               db.GetDataTable("select * from dbo.ResidentialField where isHidden = '0' order by category, fieldname")
               : db.GetDataTable("select * from dbo.CommercialField  where isHidden = '0' order by category, fieldname");


            foreach (DataRow row in results2.Rows)
            {

                var id = row[(int)ConfiguredFields.fieldname].ToString();

                bool foundMatch = false;

                foreach (ListItem testItem in selectedList.Items)
                {
                    if (id == testItem.Text)
                    {
                        foundMatch = true;
                        break;
                    }
                }

                if (!foundMatch)
                {
                    var newItem = new ListItem(row[(int)ConfiguredFields.fieldname].ToString(),
                                                 row[(int)ConfiguredFields.residentialfieldid].ToString());

                    if (!string.IsNullOrEmpty(row[(int)ConfiguredFields.alias].ToString()))
                    {
                        newItem.Text = string.Format("{0}|{1}", row[(int)ConfiguredFields.alias], row[(int)ConfiguredFields.fieldname]);
                        newItem.Attributes.Add("title", row[(int)ConfiguredFields.description].ToString());
                    }

                    unselectedList.Items.Add(newItem);
                }
            }

        }

        //protected void CreateGroup2_Click(object sender, EventArgs e)
        //{
        //    CreateNewGroup(NewGroup2.Text);
        //}

        //protected void RemoveGroup_Click1(object sender, EventArgs e)
        //{

        //    Roles.DeleteRole(Groups.SelectedValue);
        //    RefreshGroups();

        //}

        //protected void RemoveUserFromGroup_Click1(object sender, EventArgs e)
        //{
        //    try
        //    {
        //         Roles.RemoveUserFromRole(ExistingUsers.SelectedValue, Groups.SelectedValue);
        //    }
        //    catch (Exception)
        //    {
        //    }

        //    RefreshUsers();
        //}







        protected void SwapOrder(GridView thisGrid, int index1, int index2, int idColumn, int ordercolumn, string columnName)
        {
            var db = new SqlAzureLayer(GetConnectionString());

            // SwapColumnOrder
            var rowId = new Guid(thisGrid.Rows[index1].Cells[idColumn].Text);
            var myOrder = Convert.ToInt32(thisGrid.Rows[index1].Cells[ordercolumn].Text);
            var rowAboveId = new Guid(thisGrid.Rows[index2].Cells[idColumn].Text);
            var orderAbove = Convert.ToInt32(thisGrid.Rows[index2].Cells[ordercolumn].Text);

            var UpdateRow1 =
                string.Format("UPDATE dbo.Preset_Field SET  {2} = {0} WHERE [presetfieldid] = {1}", db.FormatInt(orderAbove), db.FormatGuid(rowId), columnName);
            var UpdateRow2 =
              string.Format("UPDATE dbo.Preset_Field SET  {2} = {0} WHERE [presetfieldid] = {1}", db.FormatInt(myOrder), db.FormatGuid(rowAboveId), columnName);

            db.SqlExecuteNonQuery(UpdateRow1);
            db.SqlExecuteNonQuery(UpdateRow2);

            thisGrid.DataBind();
        }


        private string GetConnectionString()
        {
            if (Application["connectionString"] == null)
            {
                var connectionString2 = ConfigurationManager.ConnectionStrings["Production"].ConnectionString;
                Application["connectionString"] = connectionString2;
            }
            return Application["connectionString"].ToString();
        }





        protected void UserRadGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {

            if (e.Item is GridDataItem)// to access a row  
            {
                var item = (GridDataItem)e.Item;

                try
                {
                    item["username"].CssClass = "nodisplay";
                }
                catch (Exception)
                {
                }
            }
        }

        protected void UserRadGrid_SelectedIndexChanged(object sender, EventArgs e)
        {



            var itemsSelected = UserRadGrid.SelectedIndexes; // ExistingUsers.GetSelectedIndices();

            if (itemsSelected.Count == 0) return;

            var isMultimode = (itemsSelected.Count > 1);

            if (isMultimode)
            {
                username.Text = " Multi-Edit Mode";

                return;
            }

            string thisusername = "";

            foreach (GridDataItem dataItem in UserRadGrid.MasterTableView.Items)
            {
                if (dataItem.Selected)
                {
                    thisusername = dataItem["username"].Text;
                }
            }

            var user = Membership.GetUser(thisusername); //Membership.GetUser(ExistingUsers.Items[itemsSelected[0]].Value);

            var userInfo = new DataTable();
            userInfo.Columns.Add("Name");
            userInfo.Columns.Add("Email");

            var roles = Roles.GetRolesForUser(user.UserName);


            username.Text = user.UserName;



            var db = new SqlAzureLayer(GetConnectionString());


            //DefaultState.SelectedValue = myProfile.Profile.defaultState;




            //bool foundCounty = false;

            
        }

        protected void ClearUser_Click(object sender, EventArgs e)
        {
            if (Session["crmAccess"] == null)
            {
                crmAccess = new CRMAccess();
                Session["crmAccess"] = crmAccess;
            }
            else
            {
                crmAccess = (CRMAccess)Session["crmAccess"];
            }

            crmAccess.ClearStudent(username.Text);
        }


        protected void DeleteUser_Click(object sender, EventArgs e)
        {
            var db = new SqlAzureLayer(GetConnectionString());

            var itemsSelected = UserRadGrid.SelectedIndexes;

            if (itemsSelected.Count == 0) return;

            foreach (GridDataItem dataItem in UserRadGrid.MasterTableView.Items)
            {
                if (dataItem.Selected)
                {
                    Membership.DeleteUser(username.Text);
                }
            }
            RefreshUsers();
        }

        protected void ImpersonateUser_Click(object sender, EventArgs e)
        {
            if (Session["crmAccess"] == null)
            {
                crmAccess = new CRMAccess();
            }
            else
            {
                crmAccess = (CRMAccess)Session["crmAccess"];
            }

           Session.Clear();
           Session["ImpersonateUser"] = username.Text;
           Session["crmAccess"] = crmAccess;
           Response.Redirect("../Members/ProfileHome.aspx");
        }

      
    }
}