﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="TouchEditor.aspx.cs" Inherits="GoalManagementV2.Administration.TouchEditor" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section>
        <div class="title">
            <h1>Touch Editor</h1>
        </div>
        <div class="title short">
            <h1>Class</h1>
        </div>
        <div class="title line">
            <asp:DropDownList ID="ClassList" runat="server" OnSelectedIndexChanged="ClassList_SelectedIndexChanged"
                AutoPostBack="true">
            </asp:DropDownList>
        </div>
    </section>
    <section>
        <div class="content">
            <asp:Panel ID="MockupPanel" runat="server" Visible="false">
                <asp:Button ID="HideMockup" runat="server" Text="Hide Mockup"
                    OnClick="HideMockup_Click" />
                <asp:Literal ID="EmailBodyLiteral" runat="server"></asp:Literal>
            </asp:Panel>
            <telerik:RadTabStrip ID="RadTabStrip1" runat="server" OnTabClick="RadTabStrip_Click"
                Skin="Simple" SelectedIndex="0">
                <Tabs>
                    <%--    <telerik:RadTab runat="server" Value="1" Text="Email Templates" Selected="True">             
                </telerik:RadTab>--%>
                    <telerik:RadTab runat="server" Value="2" Text="Scheduled Touches" Selected="true"></telerik:RadTab>
                    <telerik:RadTab runat="server" Value="3" Text="Copy Touches"></telerik:RadTab>
                    <telerik:RadTab runat="server" Value="4" Text="Move Touches"></telerik:RadTab>
                    <telerik:RadTab runat="server" Value="5" Text="Delete Templates"></telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>

            <asp:MultiView ID="resultMultiView" runat="server" ActiveViewIndex="1" Visible="true">
                <asp:View ID="Touches" runat="server">
                    <table class="fixedRow">

                        <tr>
                            <th>Scheduled Touch&nbsp;
                            </th>
                            <td>
                                <asp:DropDownList ID="ScheduledTouchList" runat="server" OnSelectedIndexChanged="ScheduledTouchList_SelectedIndexChanged"
                                    AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <th>Trigger on</th>
                            <td>
                                <telerik:RadDatePicker ID="TriggerDate" runat="server" CssClass="nested">
                                </telerik:RadDatePicker>
                                <asp:RadioButtonList ID="TimeOfDay" runat="server"
                                    RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Text="AM" Selected="True"> </asp:ListItem>
                                    <asp:ListItem Text="PM"> </asp:ListItem>
                                </asp:RadioButtonList></td>
                        </tr>
                        <tr>
                            <th>To:</th>
                            <td>
                                <asp:DropDownList ID="ToList" runat="server">
                                    <asp:ListItem Text="Students" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Coaches" Value="2" Selected="False"></asp:ListItem>
                                    <asp:ListItem Text="Sponsors" Value="3" Selected="False"></asp:ListItem>
                                    <asp:ListItem Text="Coaches and Sponsors" Value="4" Selected="False"></asp:ListItem>
                                    <asp:ListItem Text="All" Value="5" Selected="False"></asp:ListItem>
                                    <asp:ListItem Text="None" Value="100000000" Selected="False"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <asp:DropDownList ID="CCList" runat="server">
                                    <asp:ListItem Text="Students" Value="1" Selected="False"></asp:ListItem>
                                    <asp:ListItem Text="Coaches" Value="2" Selected="False"></asp:ListItem>
                                    <asp:ListItem Text="Sponsors" Value="3" Selected="False"></asp:ListItem>
                                    <asp:ListItem Text="Coaches and Sponsors" Value="4" Selected="False"></asp:ListItem>
                                    <asp:ListItem Text="None" Value="100000000" Selected="True"></asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <th>Representative Copy:</th>
                            <td>
                                <asp:DropDownList ID="RepresentativeCopyList" runat="server">
                                    <asp:ListItem Text="Students" Value="1" Selected="False"></asp:ListItem>
                                    <asp:ListItem Text="Coaches" Value="2" Selected="False"></asp:ListItem>
                                    <asp:ListItem Text="Sponsors" Value="3" Selected="False"></asp:ListItem>
                                    <asp:ListItem Text="Coaches and Sponsors" Value="4" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="None" Value="100000000" Selected="False"></asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <th>Select Template</th>
                            <td>
                                <asp:DropDownList ID="ScheduledTemplateList" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ScheduledTemplateList_SelectedIndexChanged">
                                </asp:DropDownList><br />
                                <asp:Label ID="ModeLabel" runat="server" Text="None Selected" CssClass="red"></asp:Label><%-- Replacing with zero index selection change<asp:Button ID="ViewSelectedUser" runat="server" OnClick="NewTouch_Click"
                                    Text="New  ( Clear )" />--%></td>
                        </tr>
                        <tr>
                            <th>Touch Name</th>
                            <td>
                                <asp:TextBox ID="ScheduledTouchName" runat="server" Width="500px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <th>Subject</th>
                            <td>
                                <asp:TextBox ID="Subject" runat="server" Width="500px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <th>Select Asset</th>
                            <td>
                                <asp:DropDownList ID="AssetList" AutoPostBack="true"
                                    runat="server" OnSelectedIndexChanged="AssetList_SelectedIndexChanged">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <th>New Asset Link</th>
                            <td>
                                <asp:TextBox ID="AssetLink" runat="server" Width="600px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td style="text-align: right;" class="buttonTd">
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/save.png" OnClick="UpdateTouch_Click" AlternateText="Save Touch" ToolTip="Save Touch" />
                                <div class="dropdown">
                                    <img alt="Save As" src="../images/saveAs.png" title="Save As" class="dropbtn" />
                                    <div class="dropdown-content">
                                        <asp:LinkButton ID="lbtnNewTouch" runat="server" OnClick="SaveTouchAsNew_Click">As New Touch</asp:LinkButton>
                                        <asp:LinkButton ID="lbtnNewTemplate" runat="server" OnClick="SaveTouchAsNewTemplate_Click">As New Template</asp:LinkButton>
                                    </div>
                                </div>
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/preview.png" OnClick="ShowMockup_Click" AlternateText="Preview Email" ToolTip="Preview Email" />
                                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/images/send.png" OnClick="SendTestEmail_Click" AlternateText="Send Test Email" ToolTip="Send Test Email" />
                                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/images/sendAll.png" OnClick="SendTestEmailAll_Click" AlternateText="Send Test Email - All Touches" ToolTip="Send Test Email - All Touches" />
                            </td>
                        </tr>
                    </table>
                    <%--onmouseout="hideSaveAs();" style="position: absolute; visibility: visible; top: 5px; right: -86px; background: #FFFFFF; border: solid 1px #B5B3B3; width: 120px; height: 55px; padding: 5px; line-height: 1.5em; text-align: left;    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);"--%>
                    <%--  <asp:Button ID="SaveTouchAsNewTemplate" runat="server" OnClick="SaveTouchAsNewTemplate_Click"
                        Text="Save as New Template" />
                    <asp:Button ID="SaveTouchAsNew" runat="server" OnClick="SaveTouchAsNew_Click"
                        Text="Save as New Touch" />
                    <asp:Button ID="UpdateTouch" runat="server" OnClick="UpdateTouch_Click" Text="Update" />

                    <asp:Button ID="Button1" runat="server" OnClick="ShowMockup_Click"
                        Text="Show Mockup" /><asp:Button ID="Button2" runat="server" OnClick="SendTestEmail_Click"
                            Text="Send Test Email" /><asp:Button ID="Button3" runat="server"
                                OnClick="SendTestEmailAll_Click" Text="Send Test Email All Touches" />--%>




                    <telerik:RadEditor ID="RadEditor2" runat="server" Skin="Sitefinity" Height="495px"
                        Width="955px" NewLineBr="true">
                        <%-- <Languages>
                       <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                   </Languages>--%>
                        <Tools>
                            <telerik:EditorToolGroup>
                                <telerik:EditorTool Name="AjaxSpellCheck" />
                                <telerik:EditorTool Name="Bold" />
                                <telerik:EditorTool Name="Italic" />
                                <telerik:EditorTool Name="Underline" />
                                <telerik:EditorTool Name="StrikeThrough" />
                                <telerik:EditorSeparator />
                                <telerik:EditorTool Name="JustifyLeft" />
                                <telerik:EditorTool Name="JustifyCenter" />
                                <telerik:EditorTool Name="JustifyRight" />
                                <telerik:EditorTool Name="JustifyFull" />
                                <telerik:EditorTool Name="JustifyNone" />
                                <telerik:EditorSeparator />
                                <telerik:EditorTool Name="Indent" />
                                <telerik:EditorTool Name="Outdent" />
                                <telerik:EditorTool Name="InsertOrderedList" />
                                <telerik:EditorTool Name="InsertUnorderedList" />
                                <telerik:EditorTool Name="LinkManager" />
                                <telerik:EditorTool Name="Unlink" />
                            </telerik:EditorToolGroup>
                            <telerik:EditorToolGroup Tag="DropdownToolbar">
                                <telerik:EditorSplitButton Name="ForeColor">
                                </telerik:EditorSplitButton>
                                <telerik:EditorSeparator />
                                <telerik:EditorDropDown Name="FontName">
                                </telerik:EditorDropDown>
                                <telerik:EditorDropDown Name="RealFontSize">
                                </telerik:EditorDropDown>
                            </telerik:EditorToolGroup>
                        </Tools>
                        <ImageManager ViewPaths="~/Images" />
                        <FlashManager ViewPaths="~/Images" />
                        <MediaManager ViewPaths="~/Images" />
                        <DocumentManager ViewPaths="~/Images" />
                        <TemplateManager ViewPaths="~/Images" />
                        <Content>
                    
                        </Content>
                    </telerik:RadEditor>



                    <div style="margin: 10px; border: 1px solid black;">
                        New Attachment
                    <br />
                        <asp:FileUpload ID="FileUploadControl" runat="server" />
                        <br />
                        <asp:Button runat="server" ID="UploadButton" Text="Upload" OnClick="UploadButton_Click" OnClientClick="javascript:Waiting();" />
                        <asp:Label runat="server" ID="StatusLabel" Text="Upload status: " />
                    </div>

                    <telerik:RadGrid AutoGenerateColumns="False" ID="Attachments" runat="server" GridLines="None"
                        EnableLinqExpressions="False" Skin="Simple" OnItemDataBound="Attachments_ItemDataBound"
                        ViewStateMode="Enabled">
                        <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="False" ShowFooter="False"
                            TableLayout="Auto" CommandItemDisplay="Top">
                            <CommandItemTemplate>
                                <div style="padding: 5px 5px;">
                                    <asp:LinkButton ID="ViewSelected" runat="server" OnCommand="Attachment_View" Visible='<%# Attachments.EditIndexes.Count == 0 %>'><img style="border:0px;vertical-align:middle;" alt="" src="/Images/icon_search_16px.gif" />&nbsp;View Attachment</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                            
                                <asp:LinkButton ID="DeleteSelected" OnCommand="Attachment_Delete" OnClientClick="javascript:return confirm('Delete all selected attachments?')"
                                    runat="server" CommandName="DeleteSelected"><img style="border:0px;vertical-align:middle;" alt="" src="/Images/Delete.gif" />&nbsp;Delete selected attachments</asp:LinkButton>
                                </div>
                            </CommandItemTemplate>
                            <Columns>
                                <telerik:GridBoundColumn DataField="Name" HeaderText="File Name" SortExpression="Name" UniqueName="Name" ReadOnly="True">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FileSize" HeaderText="File Size" SortExpression="FileSize"
                                    UniqueName="FileSize" ReadOnly="True">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Type" HeaderText="Type" SortExpression="Type"
                                    UniqueName="Type" ReadOnly="True">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ID" HeaderText="ID" SortExpression="ID" UniqueName="ID" ReadOnly="False">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="True" EnableDragToSelectRows="True" />
                        </ClientSettings>
                        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Web20">
                        </HeaderContextMenu>
                    </telerik:RadGrid>
                    <%--OnItemDataBound="Attachments_ItemDataBound" --%>
                    <telerik:RadGrid AutoGenerateColumns="False" ID="TemplateGrid" runat="server" GridLines="None"
                            EnableLinqExpressions="False" Skin="Simple"
                            ViewStateMode="Enabled">
                            <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="False" ShowFooter="False"
                                TableLayout="Auto" CommandItemDisplay="Top">
                                <CommandItemTemplate>
                                    <div style="padding: 5px 5px;">
                                        <asp:LinkButton ID="DeleteSelected" OnCommand="Template_Delete" OnClientClick="javascript:return confirm('Delete Template ?')"
                                            runat="server" CommandName="DeleteSelected">
                                    <img style="border:0px;vertical-align:middle;" alt="" src="/Images/Delete.gif" />&nbsp;Delete selected Templates</asp:LinkButton>
                                    </div>
                                </CommandItemTemplate>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Name" HeaderText="Touch Name" SortExpression="Name" UniqueName="Name" ReadOnly="True">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Subject" HeaderText="Subject" SortExpression="Subject" UniqueName="Subject" ReadOnly="True">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ModifiedOn" HeaderText="ModifiedOn" SortExpression="ModifiedOn" UniqueName="ModifiedOn" ReadOnly="True">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ID" HeaderText="ID" SortExpression="ID" UniqueName="ID" ReadOnly="False">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings>
                                <Selecting AllowRowSelect="True" EnableDragToSelectRows="True" />
                            </ClientSettings>
                            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Web20">
                            </HeaderContextMenu>
                        </telerik:RadGrid>
                </asp:View>

                <asp:View ID="CopyTouches" runat="server">
                    <div class="content">
                        <table class="fixedRow">
                            <tr>
                                <th>Copy From Class</th>
                                <td>
                                    <asp:Label runat="server" ID="CopyFromClass"></asp:Label>
                                    (change selected class above)</td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <asp:Literal ID="TemplateTouchesLiteral" runat="server"></asp:Literal></td>
                            </tr>
                            <tr>
                                <th>Copy To Class</th>
                                <td>
                                    <asp:DropDownList ID="TargetClassList" runat="server" OnSelectedIndexChanged="TargetClassList_SelectedIndexChanged"
                                        AutoPostBack="true">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <asp:Literal ID="TargetTouchesLiteral" runat="server"></asp:Literal></td>
                            </tr>
                        </table>

                    </div>
                    <div class="edit-buttons">
                        <asp:Button ID="Copy" runat="server" Text="Copy Touches" OnClick="Copy_Click" OnClientClick="javascript:Waiting();" />
                    </div>
                </asp:View>
                <asp:View ID="MoveTouches" runat="server">
                    <table class="fixedRow">
                        <tr>
                            <th style="width: 25%">Move Weeks By</th>
                            <td>
                                <asp:DropDownList ID="TouchWeeks" runat="server">
                                    <asp:ListItem Text="0 Weeks" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="1 Week" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="2 Weeks" Value="14"></asp:ListItem>
                                    <asp:ListItem Text="3 Weeks" Value="21"></asp:ListItem>
                                    <asp:ListItem Text="4 Weeks" Value="28"></asp:ListItem>
                                    <asp:ListItem Text="-1 Week" Value="-7"></asp:ListItem>
                                    <asp:ListItem Text="-2 Weeks" Value="-14"></asp:ListItem>
                                    <asp:ListItem Text="-3 Weeks" Value="-21"></asp:ListItem>
                                    <asp:ListItem Text="-4 Weeks" Value="-28"></asp:ListItem>
                                </asp:DropDownList></td>
                            <tr>
                                <th>Move Days By</th>
                                <td>
                                    <asp:DropDownList ID="TouchDays" runat="server">
                                        <asp:ListItem Text="0 Days" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 Day" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 Days" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 Days" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 Days" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="-1 Day" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="-2 Days" Value="-2"></asp:ListItem>
                                        <asp:ListItem Text="-3 Days" Value="-3"></asp:ListItem>
                                        <asp:ListItem Text="-4 Days" Value="-4"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                    </table>
                    <div class="edit-buttons">
                        <asp:Button ID="Button4" runat="server" OnClick="MoveTouches_Click"
                            Text="Move All Touches" />
                    </div>
                </asp:View>
                <asp:View runat="server" ID="DeleteTemplates">
                    <div>
                        
                    </div>
                </asp:View>
            </asp:MultiView>
                                


        </div>
    </section>

</asp:Content>
