﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using ScheduledEmail;
using Telerik.Web.UI;



namespace GoalManagementV2.Administration
{
    public partial class TouchEditor : System.Web.UI.Page
    {
        protected CRMAccess crmAccess;
        protected Contact myContact;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["crmAccess"] == null)
            {
                crmAccess = new CRMAccess();
                Session["crmAccess"] = crmAccess;

                MembershipUser currentUser = Membership.GetUser();
                if (currentUser == null) Response.Redirect("../Default.aspx");

                var myEmail = currentUser.Email;
                myContact = crmAccess.GetStudent(myEmail);

                Session["StudentInfo"] = myContact;

                var classes = crmAccess.GetStudentClasses(myContact.ContactId.Value).ToList();
                var coachingClasses = crmAccess.GetAllClasses().ToList();

                Session["CoachingClasses"] = coachingClasses;
                Session["MyClasses"] = classes;
            }
            else
            {
                crmAccess = (CRMAccess)Session["crmAccess"];
                myContact = (Contact)Session["StudentInfo"];
            }

            if (myContact.new_IsPortalAdministrator != true) Response.Redirect("../Default.aspx");

            if (!IsPostBack)
            {
                TriggerDate.DbSelectedDate = DateTime.Today;

                //  RadEditor1.Content = "<br/>";
                RadEditor2.Content = "<br/>";

                //  RadEditor1.NewLineBr = true;
                RadEditor2.NewLineBr = true;

                var coachingClasses = crmAccess.GetAllClasses().ToList();
                Session["CoachingClasses"] = coachingClasses;

                foreach (new_class coachingClass in coachingClasses)
                {
                    ClassList.Items.Add(new ListItem(coachingClass.new_name, coachingClass.new_classId.ToString()));
                    // replacing with page-level ddl ClassList   TemplateClassList.Items.Add(new ListItem(coachingClass.new_name, coachingClass.new_classId.ToString()));
                    TargetClassList.Items.Add(new ListItem(coachingClass.new_name, coachingClass.new_classId.ToString()));
                }


                var assets = crmAccess.GetAssets();
                AssetList.Items.Add(new ListItem("None", "None"));
                foreach (var asset in assets)
                {
                    AssetList.Items.Add(new ListItem(asset.trk_name, asset.trk_assetId.Value.ToString()));
                }


                var classId = coachingClasses[0].new_classId.Value;
                if (Session["ClassId"] != null)
                {
                    classId = (Guid)Session["ClassId"];
                    ClassList.SelectedValue = classId.ToString();
                }

                ClassList_SelectedIndexChanged(null, null);

                UpdateTouchList();

                //var scheduledTouches = crmAccess.GetScheduledTouches(classId);

                //ScheduledTouchList.Items.Add(new ListItem("New", Guid.Empty.ToString()));

                //foreach (new_scheduledtouch newScheduledtouch in scheduledTouches)
                //{
                //    ScheduledTouchList.Items.Add(new ListItem(newScheduledtouch.new_name,newScheduledtouch.Id.ToString()));
                //}

                // RadEditor1.ContentAreaMode = EditorContentAreaMode.Div;
                RadEditor2.ContentAreaMode = EditorContentAreaMode.Div;

                var templates = crmAccess.GetTouchTemplates();

                SetTemplateTable(templates);

                ScheduledTemplateList.Items.Add(new ListItem("None/New (Clear)", Guid.Empty.ToString()));
                // TemplateList.Items.Add(new ListItem("New", Guid.Empty.ToString()));

                foreach (new_touchtemplate newTouchtemplate in templates)
                {
                    // TemplateList.Items.Add(new ListItem(newTouchtemplate.new_name, newTouchtemplate.Id.ToString()));
                    ScheduledTemplateList.Items.Add(new ListItem(newTouchtemplate.new_name, newTouchtemplate.Id.ToString()));
                }

                //TemplateSubject.Text = templates[0].new_Subject;
                //RadEditor1.Content = templates[0].new_EmailBody;


                resultMultiView.SetActiveView(Touches);

            }

            this.Page.MaintainScrollPositionOnPostBack = true;

            var masterPage = (SiteMaster)Page.Master;
            masterPage.SetMenu(CentralizedData.TouchManagement);

        }


        //        protected void NewDisplayOption_Click(object sender, EventArgs e)
        //        {
        //              string Database = ConfigurationManager.AppSettings["UseDatabase"];
        //            var connectionString = ConfigurationManager.ConnectionStrings[Database].ConnectionString;
        //            var db = new SqlAzureLayer(connectionString);

        ////            var newReportField =
        ////                string.Format(@"INSERT INTO [DisplayOption] ([optionValue]) 
        ////VALUES ({0})",
        ////            db.FormatInt(OptionsGridView.Rows.Count + 1));

        ////            db.SqlExecuteNonQuery(newReportField);



        //        }


        protected void RadTabStrip_Click(object sender, RadTabStripEventArgs e)
        {
            View selected;

            switch (e.Tab.Value)
            {

                case "2":
                    selected = Touches;
                    break;

                case "3":
                    selected = CopyTouches;
                    break;

                case "4":
                    selected = MoveTouches;
                    break;

                case "5":
                    selected = DeleteTemplates;
                    break;

                default:
                    selected = Touches;
                    break;
            }

            resultMultiView.SetActiveView(selected);
        }

        protected void ClassList_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTouchList();
            FillBulletList(TemplateTouchesLiteral, ClassList.SelectedValue);
            CopyFromClass.Text = ClassList.SelectedItem.Text;
        }

        protected void UpdateTouchList()
        {
            var classId = new Guid(ClassList.SelectedValue);
            var touches = crmAccess.GetScheduledTouches(classId);

            ScheduledTouchList.Items.Clear();
            ScheduledTouchList.Items.Add(new ListItem("New", Guid.Empty.ToString()));

            foreach (new_scheduledtouch newScheduledtouch in touches)
            {
                var touchName = string.Format("{0} - {1}",
                    newScheduledtouch.new_TriggerDate.Value.ToString("MM/dd/yyyy ddd").ToUpper(), newScheduledtouch.new_name);
                //newScheduledtouch.new_TriggerDate.Value.DayOfWeek,

                ScheduledTouchList.Items.Add(new ListItem(touchName, newScheduledtouch.Id.ToString()));
            }
        }


        protected void ScheduledTouchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ScheduledTouchList.SelectedValue == Guid.Empty.ToString())
            {
                ScheduledTouchName.Text = "";
                Subject.Text = "";
                RadEditor2.Content = "<br/>";
            }
            else
            {
                ModeLabel.Text = "Editing Scheduled Touch.";
                ScheduledTemplateList.SelectedIndex = 0;

                var selectedTouchId = new Guid(ScheduledTouchList.SelectedValue);
                Session["SelectedScheduledTouchID"] = selectedTouchId;
                //Retrieve scheduled touch.
                var touch = crmAccess.GetScheduledTouch(selectedTouchId);

                ToList.SelectedValue = touch.new_To.Value.ToString();
                CCList.SelectedValue = touch.new_CCAllemail.Value.ToString();
                RepresentativeCopyList.SelectedValue = touch.new_SendRepresentativeCopy.Value.ToString();

                Session["isSent"] = touch.new_Sent;

                if (touch.new_TriggerDate != null)
                {
                    TriggerDate.DbSelectedDate = touch.new_TriggerDate.Value;
                }

                ScheduledTouchName.Text = touch.new_name;

                Subject.Text = touch.new_Subject;

                if (touch.new_When != null) TimeOfDay.SelectedValue = (touch.new_When.Value == true) ? "PM" : "AM";

                RadEditor2.Content = touch.new_EmailBody;

                var templateAttachments = crmAccess.GetAnnotations(touch.new_scheduledtouchId.Value);

                // ScheduledTouchAttachedFileList.Items.Clear();

                SetAttachmentTable(templateAttachments);
            }
        }

        private void SetAttachmentTable(List<Annotation> templateAttachments)
        {
            var attachmentTable = new DataTable();

            attachmentTable.Columns.Add("Name");
            attachmentTable.Columns.Add("FileSize");
            attachmentTable.Columns.Add("Type");
            attachmentTable.Columns.Add("ID");

            foreach (Annotation templateAttachment in templateAttachments)
            {
                if (templateAttachment.IsDocument == true)
                {
                    // ScheduledTouchAttachedFileList.Items.Add((new ListItem(templateAttachment.FileName)));
                    var newRow = attachmentTable.NewRow();
                    newRow[0] = templateAttachment.FileName;
                    newRow[1] = templateAttachment.FileSize;
                    newRow[2] = templateAttachment.MimeType;
                    newRow[3] = templateAttachment.AnnotationId.Value.ToString();
                    attachmentTable.Rows.Add(newRow);
                }
            }

            Session["ScheduledTouches"] = attachmentTable;

            Attachments.DataSource = attachmentTable;
            Attachments.DataBind();
        }


        protected void NewTouch_Click(object sender, EventArgs e)
        {
            ModeLabel.Text = "Not Selected.";
            Subject.Text = "";
            RadEditor2.Content = "<br/>";
            ScheduledTouchList.SelectedIndex = 0;
            ScheduledTemplateList.SelectedIndex = 0;
        }


        //protected void NewTemplate_Click(object sender, EventArgs e)
        //{
        //    TemplateSubject.Text = "";
        //    RadEditor1.Content = "<br/>";
        //}

        //protected void TemplateList_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (TemplateList.SelectedValue == Guid.Empty.ToString())
        //    {
        //        TemplateName.Text = "";
        //        TemplateSubject.Text = "";
        //        RadEditor1.Content = "<br/>";
        //    }
        //    else
        //    {
        //        var touch = crmAccess.GetScheduledTemplate(new Guid(TemplateList.SelectedValue));

        //        TemplateName.Text = touch.new_name;
        //        TemplateSubject.Text = touch.new_Subject;
        //        RadEditor1.Content = touch.new_EmailBody;

        //        var templateAttachments = crmAccess.GetAnnotations(touch.new_touchtemplateId.Value);

        //        TemplateAttachedFileList.Items.Clear();
        //        foreach (Annotation templateAttachment in templateAttachments)
        //        {
        //            if( templateAttachment.IsDocument ==  true)
        //            {
        //                TemplateAttachedFileList.Items.Add((new ListItem(templateAttachment.FileName)));
        //            }
        //        }

        //    }
        //}

        protected void ScheduledTemplateList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ScheduledTemplateList.SelectedValue == Guid.Empty.ToString())
            {
                Subject.Text = "";
                RadEditor2.Content = "<br/>";
                NewTouch_Click(null, null);
            }
            else
            {
                ModeLabel.Text = "Editing Template.";
                ScheduledTouchList.SelectedIndex = 0;
                Session["SelectedScheduledTouchID"] = null;

                var template = crmAccess.GetScheduledTemplate(new Guid(ScheduledTemplateList.SelectedValue));

                ScheduledTouchName.Text = template.new_name;
                Subject.Text = template.new_Subject;
                RadEditor2.Content = template.new_EmailBody;

                var templateAttachments = crmAccess.GetAnnotations(template.new_touchtemplateId.Value);

                SetAttachmentTable(templateAttachments);

                Session["TouchTempleteID"] = template.new_touchtemplateId.Value;

                //ScheduledTouchAttachedFileList.Items.Clear();
                //foreach (Annotation templateAttachment in templateAttachments)
                //{
                //    if (templateAttachment.IsDocument == true)
                //    {
                //        ScheduledTouchAttachedFileList.Items.Add((new ListItem(templateAttachment.FileName, templateAttachment.AnnotationId.ToString())));
                //    }
                //}

            }

        }



        //protected void SaveAsNewTemplate_Click(object sender, EventArgs e)
        //{
        //    var id = Guid.NewGuid();

        //    var template = new new_touchtemplate()
        //    {
        //        new_touchtemplateId = id,
        //        new_name = TemplateName.Text,
        //        new_Subject = TemplateSubject.Text,
        //        new_EmailBody = RadEditor1.Content
        //    };

        //    crmAccess.AddNewTemplate(template);

        //    TemplateList.Items.Add(new ListItem(TemplateName.Text, id.ToString()));
        //    TemplateList.SelectedIndex = TemplateList.Items.Count - 1;

        //}

        protected void SaveTouchAsNew_Click(object sender, EventArgs e)
        {
            var id = Guid.NewGuid();

            var touch = new new_scheduledtouch
                            {
                                new_scheduledtouchId = id,
                                new_name = ScheduledTouchName.Text,
                                new_Subject = Subject.Text,
                                new_EmailBody = RadEditor2.Content,
                                new_To = new OptionSetValue(Convert.ToInt32(ToList.SelectedValue)),
                                new_CCAllemail = new OptionSetValue(Convert.ToInt32(CCList.SelectedValue)),
                                new_SendRepresentativeCopy =
                                    new OptionSetValue(Convert.ToInt32(RepresentativeCopyList.SelectedValue)),
                                new_TriggerDate = TriggerDate.SelectedDate,
                                new_When = TimeOfDay.SelectedValue == "PM",
                                new_ClassId = new EntityReference("new_class", new Guid(ClassList.SelectedValue)),
                                new_Sent = false

                            };

            crmAccess.AddNewTouch(touch);

            ScheduledTouchList.Items.Add(new ListItem(ScheduledTouchName.Text, id.ToString()));
            ScheduledTouchList.SelectedIndex = ScheduledTouchList.Items.Count - 1;

            var touchesData = (DataTable)Session["ScheduledTouches"];

            Session["TouchTempleteID"] = null;
            Session["SelectedScheduledTouchID"] = id;

            foreach (DataRow dataRow in touchesData.Rows)
            {
                crmAccess.CopyAnnotationToScheduledTouch(new Guid(dataRow[3].ToString()), id);
            }

            ModeLabel.Text = "Editing Scheduled Touch.";

            UpdateTouchList();
        }

        protected void UpdateTouch_Click(object sender, EventArgs e)
        {
            if (ScheduledTouchList.SelectedIndex != 0)
            {
                bool isSent = false;

                if (new Guid(ScheduledTouchList.SelectedValue) == (Guid)Session["SelectedScheduledTouchID"])
                {
                    isSent = (bool)Session["isSent"];
                }

                var touch = new new_scheduledtouch
                                {
                                    new_scheduledtouchId = new Guid(ScheduledTouchList.SelectedValue),
                                    new_name = ScheduledTouchName.Text,
                                    new_Subject = Subject.Text,
                                    new_EmailBody = RadEditor2.Content,
                                    new_To = new OptionSetValue(Convert.ToInt32(ToList.SelectedValue)),
                                    new_CCAllemail = new OptionSetValue(Convert.ToInt32(CCList.SelectedValue)),
                                    new_SendRepresentativeCopy =
                                        new OptionSetValue(Convert.ToInt32(RepresentativeCopyList.SelectedValue)),
                                    new_TriggerDate = TriggerDate.SelectedDate,
                                    new_When = TimeOfDay.SelectedValue == "PM",
                                    new_Sent = isSent
                                };



                crmAccess.UpdateTouch(touch);
            }
            else if (ScheduledTemplateList.SelectedIndex != 0)
            {
                var template = new new_touchtemplate()
                 {
                     new_touchtemplateId = new Guid(ScheduledTemplateList.SelectedValue),
                     new_name = ScheduledTouchName.Text,
                     new_Subject = Subject.Text,
                     new_EmailBody = RadEditor2.Content
                 };

                crmAccess.UpdateTemplate(template);
            }
        }


        protected void SendTestEmail_Click(object sender, EventArgs e)
        {
            // Send Test Email
            var newScheduledtouch = new new_scheduledtouch
            {
                new_scheduledtouchId = new Guid(ScheduledTouchList.SelectedValue),
                new_name = ScheduledTouchName.Text,
                new_Subject = Subject.Text,
                new_EmailBody = RadEditor2.Content,
                new_To = new OptionSetValue(Convert.ToInt32(ToList.SelectedValue)),
                new_CCAllemail = new OptionSetValue(Convert.ToInt32(CCList.SelectedValue)),
                new_SendRepresentativeCopy = new OptionSetValue(Convert.ToInt32(RepresentativeCopyList.SelectedValue)),
                new_TriggerDate = TriggerDate.SelectedDate,
                new_When = TimeOfDay.SelectedValue == "PM",
                new_ClassId = new EntityReference("new_class", new Guid(ClassList.SelectedValue))
            };

            var subject = newScheduledtouch.new_Subject;
            var emailBody = newScheduledtouch.new_EmailBody;

            //GetAttachments
            var attachments = crmAccess.GetAnnotations(newScheduledtouch.Id);

            var classId = newScheduledtouch.new_ClassId.Id;
            var students = crmAccess.GetClassStudents(classId);
            var coachesSponsors = crmAccess.GetClassCoaches(classId);

            var toEmail = (int)CRMAccess.EmailTargets.TestEmailToMe;

            //var testEmailAddress = ConfigurationManager.AppSettings["sendTestEmail"];
            var myUser = crmAccess.GetUser("rxsupport@graymattersgroup.com");  //crmAccess.GetUser("dryan@graymattersltd.com");
            // Set email to be sent by owner of the entity
            var emailSentByActivityParty = new ActivityParty
            {
                PartyId = new EntityReference(SystemUser.EntityLogicalName, myUser.Id),
                ParticipationTypeMask = new OptionSetValue(1)
            };

            var fromActivityParty = new List<ActivityParty>() { emailSentByActivityParty };

            var ccActivityParty = new List<ActivityParty>();

            var representativeCopies = CrmEmail.CreateContactList(toEmail, students, coachesSponsors);

            CrmEmail.SendEmail(classId, toEmail, students, coachesSponsors, EmailSendRule.To, emailBody, subject, attachments, fromActivityParty, ccActivityParty, representativeCopies, crmAccess, new Guid(ScheduledTouchList.SelectedValue), myContact.ContactId.Value);
        }

        //replacing with page level ClassList
        //protected void TemplateClassList_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    FillBulletList(TemplateTouchesLiteral, TemplateClassList.SelectedValue);
        //}

        protected void TargetClassList_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillBulletList(TargetTouchesLiteral, TargetClassList.SelectedValue);
        }

        protected void FillBulletList(Literal list, string classId)
        {
            var touches = crmAccess.GetScheduledTouches(new Guid(classId));

            list.Text = "<table class='nested'>";
            foreach (new_scheduledtouch touch in touches)
            {
                list.Text += string.Format("<tr><td class='TouchDayofWeek'>{0}</td><td class='TouchDay'><b>{1}</b></td><td class='TouchName'>{2}</td></tr>",
                    touch.new_TriggerDate.Value.DayOfWeek, touch.new_TriggerDate.Value.ToString("MM/dd/yyyy"), touch.new_name);
            }
            list.Text += "</table>";
        }

        protected void Copy_Click(object sender, EventArgs e)
        {
            var templateClassId = new Guid(ClassList.SelectedValue); // new Guid(TemplateClassList.SelectedValue);
            var targetClassId = new Guid(TargetClassList.SelectedValue);

            var templateClass = crmAccess.GetClass(templateClassId);
            var targetClass = crmAccess.GetClass(targetClassId);

            var templateAfterHoursStartDate = (DateTime)templateClass.new_StartDate;
            var targetAfterHoursStartDate = (DateTime)targetClass.new_StartDate;

            var offsetDays = (targetAfterHoursStartDate - templateAfterHoursStartDate).Days;

            if (templateAfterHoursStartDate > targetAfterHoursStartDate)
            {
                offsetDays = -(templateAfterHoursStartDate - targetAfterHoursStartDate).Days;
            }

            var touches = crmAccess.GetScheduledTouches(templateClassId);

            foreach (new_scheduledtouch newScheduledtouch in touches)
            {
                var id = Guid.NewGuid();

                var templateDate = newScheduledtouch.new_TriggerDate;

                var targetDate = templateDate.Value.AddDays(offsetDays);

                if (templateDate.Value.DayOfWeek > targetDate.DayOfWeek)
                {
                    targetDate = targetDate.AddDays(1);
                }
                if (templateDate.Value.DayOfWeek < targetDate.DayOfWeek)
                {
                    targetDate = targetDate.AddDays(-1);
                }

                var touch = new new_scheduledtouch
                {
                    new_scheduledtouchId = id,
                    new_name = newScheduledtouch.new_name,
                    new_Subject = newScheduledtouch.new_Subject,
                    new_EmailBody = newScheduledtouch.new_EmailBody,
                    new_To = newScheduledtouch.new_To,
                    new_CCAllemail = newScheduledtouch.new_CCAllemail,
                    new_SendRepresentativeCopy = newScheduledtouch.new_SendRepresentativeCopy,
                    new_TriggerDate = targetDate,
                    new_When = newScheduledtouch.new_When,
                    new_ClassId = new EntityReference("new_class", targetClassId)
                };

                crmAccess.AddNewTouch(touch);

                //  Get Attachments for touch
                var templateAttachments = crmAccess.GetAnnotations(newScheduledtouch.new_scheduledtouchId.Value);

                foreach (Annotation templateAttachment in templateAttachments)
                {
                    crmAccess.CopyAnnotationToScheduledTouch(templateAttachment.AnnotationId.Value, id);
                }

            }

            FillBulletList(TargetTouchesLiteral, TargetClassList.SelectedValue);
        }

        private void ExportAttachment(string fileName, byte[] content, string type)
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = type;
            Response.BinaryWrite(content);
            Response.End();
        }


        protected void AssetList_Click(object sender, EventArgs e)
        {

        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (FileUploadControl.HasFile)
            {
                try
                {
                    crmAccess = (CRMAccess)Session["crmAccess"];

                    // Find touchid
                    if (Session["SelectedScheduledTouchID"] == null && Session["TouchTempleteID"] == null)
                    {
                        StatusLabel.Text = "Upload status: Scheduled Touch or Touch Template must be selected ";
                        return;
                    }



                    string filename = Path.GetFileName(FileUploadControl.FileName);
                    string extention = Path.GetExtension(FileUploadControl.FileName);

                    var mimetype = crmAccess.GetMimeType(extention);

                    byte[] filecontents = FileUploadControl.FileBytes;

                    Guid touchid = Guid.Empty;

                    string objectName = "new_scheduledtouch";

                    if (Session["SelectedScheduledTouchID"] != null)
                    {
                        touchid = (Guid)Session["SelectedScheduledTouchID"];
                    }
                    else if (Session["TouchTempleteID"] != null)
                    {
                        touchid = (Guid)Session["TouchTempleteID"];
                        objectName = "new_touchtemplate";
                    }

                    var newAnnotation = new Annotation()
                    {
                        ObjectId = new EntityReference(objectName, touchid),
                        ObjectTypeCode = objectName,
                        FileName = filename,
                        IsDocument = true,
                        NoteText = "",
                        Subject = filename,
                        MimeType = mimetype,
                        DocumentBody = Convert.ToBase64String(filecontents)
                    };

                    crmAccess.CreateAnnotation(newAnnotation);

                    var templateAttachments = crmAccess.GetAnnotations(touchid);

                    SetAttachmentTable(templateAttachments);


                    StatusLabel.Text = "Upload status: File uploaded successfully. ";

                }
                catch (Exception ex)
                {
                    StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                }
            }
        }

        protected void Attachments_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = (GridDataItem)e.Item;
                item["ID"].CssClass = "nodisplay";


                foreach (GridColumn column1 in e.Item.OwnerTableView.RenderColumns)
                {
                    if (column1.UniqueName == "ID")
                    {
                        column1.Visible = false;
                        break;
                    }
                }

            }
            else
                if (e.Item is GridHeaderItem)
                {
                    var item = (GridHeaderItem)e.Item;
                    try
                    {
                        item["ID"].CssClass = "nodisplay";
                    }
                    catch { }

                }
                else
                    if (e.Item is GridFooterItem)
                    {
                        var item = (GridFooterItem)e.Item;
                        try
                        {
                            item["ID"].CssClass = "nodisplay";
                        }
                        catch { }

                    }
        }


        protected void Attachment_View(object sender, CommandEventArgs e)
        {
            foreach (GridDataItem selectedItem in Attachments.SelectedItems)
            {

                var id = (selectedItem)["ID"].Text; //.Controls[0] as TextBox;

                var annotationId = new Guid(id);

                crmAccess = (CRMAccess)Session["crmAccess"];

                var cols = new ColumnSet(true);

                var thisAnnotation = (Annotation)crmAccess.OrgService.Retrieve("annotation", annotationId, cols);

                string extention = Path.GetExtension(thisAnnotation.FileName);

                var mimetype = crmAccess.GetMimeType(extention);

                byte[] fileContent = Convert.FromBase64String(thisAnnotation.DocumentBody);

                ExportAttachment(thisAnnotation.FileName, fileContent, mimetype);
            }

            var touchesData = (DataTable)Session["ScheduledTouches"];
            Attachments.DataSource = touchesData;
            Attachments.DataBind();
        }


        protected void Attachment_Delete(object sender, CommandEventArgs e)
        {
            var touchesData = (DataTable)Session["ScheduledTouches"];

            foreach (GridDataItem selectedItem in Attachments.SelectedItems)
            {
                var id = (selectedItem)["ID"].Text;
                var annotationId = new Guid(id);

                crmAccess = (CRMAccess)Session["crmAccess"];

                crmAccess.DeleteAnnotation(annotationId);

                if (Session["ScheduledTouches"] != null)
                {
                    DataRow deleteRow = null;
                    foreach (DataRow dataRow in touchesData.Rows)
                    {
                        if (dataRow[3].ToString() == id)
                        {
                            deleteRow = dataRow;
                        }
                    }
                    if (deleteRow != null)
                    {
                        touchesData.Rows.Remove(deleteRow);
                        Session["ScheduledTouches"] = touchesData;
                    }

                }
            }
            Attachments.DataSource = touchesData;

            Attachments.DataBind();
        }

        protected void AssetList_SelectedIndexChanged(object sender, EventArgs e)
        {
            AssetLink.Text = string.Format("#assetlink#{0}|HERE|", AssetList.SelectedValue);
        }

        protected void MoveTouches_Click(object sender, EventArgs e)
        {
            var classId = new Guid(ClassList.SelectedValue);
            var numberOfDays1 = Convert.ToInt32(TouchDays.SelectedValue);
            var numberOfDays2 = Convert.ToInt32(TouchWeeks.SelectedValue);

            var numberOfDays = numberOfDays1 + numberOfDays2;

            crmAccess.MoveScheduledTouches(classId, numberOfDays);

            UpdateTouchList();
        }

        protected void HideMockup_Click(object sender, EventArgs e)
        {
            MockupPanel.Visible = false;
        }

        protected void ShowMockup_Click(object sender, EventArgs e)
        {
            MockupPanel.Visible = true;

            var adjustAssetLink = RadEditor2.Content;

            var infiniteLoopCount = 0;
            var done = false;

            while (!done)
            {
                if (adjustAssetLink.IndexOf("#assetlink#") == -1) done = true;
                else
                {
                    var assetid = new Guid(adjustAssetLink.Substring(adjustAssetLink.IndexOf("#assetlink#") + 11, 36));
                    var assetText = adjustAssetLink.Substring(adjustAssetLink.IndexOf("#assetlink#") + 11 + 36 + 1);
                    assetText = assetText.Substring(0, assetText.IndexOf("|"));

                    var fullText = string.Format("#assetlink#{0}|{1}|", assetid, assetText);

                    Guid scheduledTouchId = Guid.Empty;

                    if (Session["SelectedScheduledTouchID"] != null)
                        scheduledTouchId = (Guid)Session["SelectedScheduledTouchID"];

                    var testEmailContactId = myContact.ContactId;

                    var assetLink =
                        string.Format(
                            "<a href='http://www.graymattersgroup.info/Account/Tracker.aspx?cid={0}&aid={1}&sid={2}'>{3}</a>",
                            testEmailContactId,
                            assetid, scheduledTouchId, assetText);

                    adjustAssetLink = adjustAssetLink.Replace(fullText, assetLink);

                    if (infiniteLoopCount > 100)
                    {
                        adjustAssetLink = string.Format("{0} Failed assetlink bad formatting", assetText);
                        done = true;
                    }
                }
            }
            EmailBodyLiteral.Text = adjustAssetLink;

        }


        protected void SendTestEmailAll_Click(object sender, EventArgs e)
        {
            // Send an test email of all the Scheduled Items for this Class
            foreach (ListItem thisTouch in ScheduledTouchList.Items)
            {
                var thisTouchId = new Guid(thisTouch.Value);
                var testTouch = crmAccess.GetScheduledTouch(thisTouchId);
                var attachments = crmAccess.GetAnnotations(testTouch.Id);

                var classId = testTouch.new_ClassId.Id;
                var students = crmAccess.GetClassStudents(classId);
                var coachesSponsors = crmAccess.GetClassCoaches(classId);

                // Will find the CRM Contact I am associated with and send the email to me.
                const int toEmail = (int)CRMAccess.EmailTargets.TestEmailToMe;

                // All email is sent from CRM by this user.
                var myUser = crmAccess.GetUser("rxsupport@graymattersgroup.com");

                var emailSentByActivityParty = new ActivityParty
                {
                    PartyId = new EntityReference(SystemUser.EntityLogicalName, myUser.Id),
                    ParticipationTypeMask = new OptionSetValue(1)
                };

                var fromActivityParty = new List<ActivityParty>() { emailSentByActivityParty };

                var ccActivityParty = new List<ActivityParty>();

                var representativeCopies = CrmEmail.CreateContactList(toEmail, students, coachesSponsors);

                CrmEmail.SendEmail(classId, toEmail, students, coachesSponsors, EmailSendRule.To, testTouch.new_EmailBody, testTouch.new_Subject, attachments, fromActivityParty, ccActivityParty, representativeCopies, crmAccess, new Guid(ScheduledTouchList.SelectedValue),
                    myContact.ContactId.Value);
            }

        }

        protected void SaveTouchAsNewTemplate_Click(object sender, EventArgs e)
        {
            var id = Guid.NewGuid();

            var template = new new_touchtemplate()
            {
                new_touchtemplateId = id,
                new_name = ScheduledTouchName.Text,
                new_Subject = Subject.Text,
                new_EmailBody = RadEditor2.Content,
                //new_To = new OptionSetValue(Convert.ToInt32(ToList.SelectedValue)),
                //new_When = TimeOfDay.SelectedValue == "PM",
            };

            crmAccess.AddNewTemplate(template);

            var templates = crmAccess.GetTouchTemplates();

            ScheduledTouchList.Items.Clear();
            ScheduledTemplateList.Items.Add(new ListItem("None", Guid.Empty.ToString()));

            foreach (new_touchtemplate newTouchtemplate in templates)
            {
                ScheduledTemplateList.Items.Add(new ListItem(newTouchtemplate.new_name, newTouchtemplate.Id.ToString()));
            }

            Session["TouchTempleteID"] = id;
            Session["SelectedScheduledTouchID"] = null;

            ModeLabel.Text = "Editing Template.";
        }

        //protected void UpdateTemplate_Click(object sender, EventArgs e)
        //{
        //    var template = new new_touchtemplate()
        //    {
        //        new_touchtemplateId = new Guid(TemplateList.SelectedValue),
        //        new_name = TemplateName.Text,
        //        new_Subject = TemplateSubject.Text,
        //        new_EmailBody = RadEditor1.Content
        //    };

        //    crmAccess.UpdateTemplate(template);

        //}

        private void SetTemplateTable(List<new_touchtemplate> templates)
        {
            var attachmentTable = new DataTable();

            attachmentTable.Columns.Add("Name");
            attachmentTable.Columns.Add("Subject");
            attachmentTable.Columns.Add("ModifiedOn");
            attachmentTable.Columns.Add("ID");

            foreach (new_touchtemplate template in templates)
            {
                var newRow = attachmentTable.NewRow();
                newRow[0] = template.new_name;
                newRow[1] = template.new_Subject;
                newRow[2] = template.ModifiedOn;
                newRow[3] = template.Id.ToString();
                attachmentTable.Rows.Add(newRow);
            }

            Session["Templates"] = attachmentTable;

            TemplateGrid.DataSource = attachmentTable;
            Attachments.DataBind();
        }

        protected void Template_Delete(object sender, CommandEventArgs e)
        {
            var touchesData = (DataTable)Session["Templates"];

            foreach (GridDataItem selectedItem in TemplateGrid.SelectedItems)
            {
                var id = (selectedItem)["ID"].Text;
                var templateId = new Guid(id);

                crmAccess = (CRMAccess)Session["crmAccess"];

                crmAccess.DeleteAnnotation(templateId);

                if (Session["Templates"] != null)
                {
                    DataRow deleteRow = null;
                    foreach (DataRow dataRow in touchesData.Rows)
                    {
                        if (dataRow[3].ToString() == id)
                        {
                            deleteRow = dataRow;
                        }
                    }
                    if (deleteRow != null)
                    {
                        touchesData.Rows.Remove(deleteRow);
                        Session["Templates"] = touchesData;
                    }

                }
            }
            Attachments.DataSource = touchesData;

            Attachments.DataBind();
        }
    }
}