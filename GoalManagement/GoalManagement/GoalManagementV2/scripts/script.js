$(document).ready(function () {
    $('.modify-goals > button').click(function (e) {
        $('.modify-goals .selection').fadeIn();
    });

    $('.goals-wrapper input[value="Save Goals"]').click(function (e) {
        e.preventDefault;
        $('.modify-goals .selection').fadeOut();
    });

    $('.expand-button img ').click(function () {
        $(this).toggleClass('expanded');
        var studentData = $(this).closest('tr').attr('data');
        var $studentInfoModal = $('table.class-list tr.indiv-info[data=' + studentData + '] .indiv-info-wrapper');
        $studentInfoModal.slideToggle();
    });

    $('.indiv-info-wrapper a').click(function (e) {
        e.preventDefault;
        $(this).next('div.activities-payoff-modal').slideToggle();
    });

    $('section.people .people-tabs a').click(function (e) {
        e.preventDefault;

        $(this).parent().addClass('active').siblings().removeClass('active');

        var peopleSectionData = $(this).attr('data');

        $('div.people-body[data=' + peopleSectionData + ']').slideDown().siblings('div.people-body').slideUp();

    });

    $('.hidden-menu').click(function () {
        $('.sidebar').toggleClass('hide');
    });


    $("nav").find("a").each(function (i, e) {
        if (e.href.indexOf(window.location.pathname) > 0) {
            $(e).parent().addClass("active");
        } else {
            $(e).parent().removeClass("active");
        }
    });
});