﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoalManagement.CrmConnectivity
{
    public class SerializableClasses
    {
    }

    [Serializable]
    public class DashboardBlob
    {
        public Guid classid;
        public DateTime lastupdated;
        public List<StudentUpdatePeriod> studentProgress;
        public List<ContactS> students = new List<ContactS>( );
        public List<ContactS> coaches = new List<ContactS>();
        public List<new_studentgoalsS> selectedGoals = new List<new_studentgoalsS>();
        public List<new_classupdateperiodS> progressPeriods = new List<new_classupdateperiodS>();
        public List<new_coachingS> coaching = new List<new_coachingS>();

        public void SetStudents(Guid classId, DateTime updated, List<Contact> studentsI, List<Contact> coachesI, List<new_studentgoals> studentgoalsI,
            List<new_classupdateperiod> classupdateperiodI, List<StudentUpdatePeriod> studentProgressI, List<new_coaching> coachingI, List<new_classstatus> classstatusesI)
        {
            classid = classId;
            lastupdated = updated;
            studentProgress = studentProgressI;

            foreach (var contact in studentsI)
            {
                bool goalsEntered = false;
                foreach (new_classstatus newClassstatuse in classstatusesI)
                {
                    if (newClassstatuse.new_StudentId != null)
                    if( newClassstatuse.new_StudentId.Id == contact.ContactId.Value)
                    {
                        if( newClassstatuse.new_GoalsSelected == true)
                            goalsEntered = true;
                    }
                }
                var student = Set_Contacts(contact, goalsEntered);
                if( student != null)
                    students.Add(student);
            }

            foreach (var contact in coachesI)
            {
                var coach = Set_Contacts(contact, false);
                if (coach != null)
                    coaches.Add(coach);
            }

            foreach (var newStudentgoal in studentgoalsI)
            {
                selectedGoals.Add(SetStudentGoals(newStudentgoal));
            }

            foreach (var newClassupdateperiodS in classupdateperiodI)
            {
                progressPeriods.Add(SetClassPeriods(newClassupdateperiodS));
            }

            foreach (var newCoaching in coachingI)
            {
                coaching.Add(Set_coachingS(newCoaching));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="classupdateperiod"></param>
        /// <returns></returns>
         public new_classupdateperiodS SetClassPeriods(new_classupdateperiod classupdateperiod)
         {
             var c = new new_classupdateperiodS
                         {
                             new_classupdateperiodId = classupdateperiod.new_classupdateperiodId.Value,
                             new_name = classupdateperiod.new_name
                         };
            if (classupdateperiod.new_Start.HasValue) c.new_Start = classupdateperiod.new_Start.Value;
            if (classupdateperiod.new_End.HasValue) c.new_End = classupdateperiod.new_End.Value;
             return c;
         }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="contact"></param>
        /// <param name="GoalsEntered"></param>
        /// <returns></returns>
        public ContactS Set_Contacts(Contact contact, bool GoalsEntered)
        {
            var c = new ContactS
                        {
                            ContactId = contact.ContactId.Value,
                            IsGoalsEntered = GoalsEntered
                        };
            
            c.FirstName = contact.FirstName?? "";
            c.LastName = contact.LastName?? "";

            if (contact.EMailAddress1 == null) return null;

            c.EMailAddress1 = contact.EMailAddress1;
            

            if (contact.new_ContactRxRole != null) c.new_ContactRxRole = contact.new_ContactRxRole.Value;
            else return null;

             if (contact.new_MyCoachId != null) c.new_MyCoachId = contact.new_MyCoachId.Id;
             return c;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="studentgoals"></param>
        /// <returns></returns>
           public new_studentgoalsS  SetStudentGoals (new_studentgoals studentgoals)
           {
               var c = new new_studentgoalsS
                           {
                               new_StudentId = studentgoals.new_StudentId.Id,
                               new_name = studentgoals.new_name,
                               new_Benefit = studentgoals.new_Benefit
                           };
               if(studentgoals.new_IsComplete.HasValue) c.new_IsComplete = studentgoals.new_IsComplete.Value;
               return c;
           }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="coaching"></param>
        /// <returns></returns>
        public new_coachingS Set_coachingS(new_coaching coaching)
        {
            var  c = new new_coachingS
                         {
                             new_ClassUpdatePeriodId = coaching.new_ClassUpdatePeriodId.Id,
                             new_StudentId = coaching.new_StudentId.Id,
                             new_UpdateGrade = coaching.new_UpdateGrade.Value,
                             ModifiedOn = coaching.ModifiedOn.Value,
                             CreatedOn = coaching.CreatedOn.Value,
                             new_Advice = coaching.new_Advice
                         };
            return c;
        }

    }

    [Serializable]
    public class ContactS
    {
        public Guid ContactId;
        public Guid? new_MyCoachId;
        public string FirstName;
        public string LastName;
        public string EMailAddress1;
        public int? new_ContactRxRole;
        public bool IsGoalsEntered;

    }

    [Serializable]
    public class new_studentgoalsS
    {
     
        public Guid     new_StudentId;
        public string   new_name;
        public bool?     new_IsComplete;
        public string   new_Benefit;
    }

    [Serializable]
    public class new_classupdateperiodS
    {
       
        public string new_name;
        public DateTime? new_Start;
        public DateTime? new_End;
        public Guid new_classupdateperiodId;
    }

  
    [Serializable]
    public class new_coachingS
    {
        public Guid new_ClassUpdatePeriodId;
        public Guid new_StudentId;
        public int new_UpdateGrade;
        public DateTime ModifiedOn;
        public DateTime CreatedOn;
        public string new_Advice;
    }
   
        
}