﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="GoalManagement.Account.ResetPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="settings">
        <div class="title">
            <h1>Reset Password</h1>
        </div>
        <div class="settings-body">
            <table class="fixedRow">
                <tr>
                    <th>Email</th>
                    <td>
                        <asp:TextBox ID="EmailAddress" runat="server" Height="21px" Width="336px"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;<asp:Label ID="Warning" runat="server" CssClass="failureNotification" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
            <div class="edit-buttons clearfix">
                <asp:Button ID="ResetPasswordButton" runat="server" Text="Reset Password"
                    OnClick="ResetPasswordButton_Click" />
            </div>
        </div>
    </section>
</asp:Content>
