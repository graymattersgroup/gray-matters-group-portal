﻿<%@ Page Title="Register" Language="C#" MasterPageFile="/Site.master" AutoEventWireup="true"
    CodeBehind="CustomRegister.aspx.cs" Inherits="GoalManagementV2.Account.CustomRegister" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
      <section>
        <div class="title">
            <h1>Account Info</h1>
        </div>
        <div class="content">
       
                <asp:Panel ID="Panel1" runat="server">
                    <asp:Literal ID="ThankyouContent" runat="server"></asp:Literal>
                </asp:Panel>
                <p>
                    Passwords are required to be a minimum of <%= Membership.MinRequiredPasswordLength %> characters in length.
                </p>
                <span class="failureNotification">
                    <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification"
                    ValidationGroup="RegisterUserValidationGroup" />
                <div class="accountInfo">
                </div>
                <table class="fixedRow">
                    <tr>
                        <td class="label">
                            <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="Email" runat="server" CssClass="textEntry" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                                CssClass="failureNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required."
                                ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td class="label">
                            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="Password" runat="server" CssClass="passwordEntry"
                                TextMode="Password" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                                ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="ConfirmPassword" runat="server" CssClass="passwordEntry"
                                TextMode="Password" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="ConfirmPassword" CssClass="failureNotification" Display="Dynamic"
                                ErrorMessage="Confirm Password is required." ID="ConfirmPasswordRequired" runat="server"
                                ToolTip="Confirm Password is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                                CssClass="failureNotification" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                                ValidationGroup="RegisterUserValidationGroup">*</asp:CompareValidator>
                        </td>
                    </tr>

                </table>

                <div class="edit-buttons clearfix">
                    <asp:Button ID="CreateUserButton" runat="server" CommandName="MoveNext" Text="Register User"
                        ValidationGroup="RegisterUserValidationGroup"
                        OnClick="CreateUserButton_Click" />
                </div>
            </div>
    </section>

</asp:Content>
