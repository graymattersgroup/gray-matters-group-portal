﻿using System;
using System.Configuration;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using System.Web.Services.Protocols;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
//using GoalManagement.digitalchalk.graymattersgroup;
using GoalManagementV2.digitalchalk.graymattersgroup;
using GoalManagement.Support;
using Telerik.Web.UI;

namespace GoalManagementV2.Account
{
    public partial class CustomRegister : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //var treeview = Master.FindControl("TreeView1") as TreeView;
                //treeview.Visible = false;




                ThankyouContent.Text = @"Thank you for registering!</br></br>
Please Log in  now.";
//An email will be sent to you for confirmation, before your account will be activated."; 

                Panel1.Visible = false;
                //Panel2.Visible = true;
            }
        }
        

        protected void CreateUserButton_Click(object sender, EventArgs e)
        {
            CRMAccess crmAccess;
            if ( Session["CRM_Access"] != null ) 
                crmAccess = (CRMAccess)Session["CRM_Access"];
            else
                crmAccess = new CRMAccess();
          
            var thisStudent = crmAccess.GetStudent(Email.Text);

            if (thisStudent == null)
            {
                ErrorMessage.Text = string.Format(@"This email address is not currently on the system.<br/>
Please contact <a href='mailto:RxAssist@graymattersgroup.com'>RxAssist@graymattersgroup.com</a> for assistance.<br/><br/>");
                return;
            }

            try
            {

            MembershipUser user = Membership.CreateUser(Email.Text.ToLower(), Password.Text, Email.Text.ToLower());

             user.IsApproved = true;
           
            Membership.UpdateUser(user);

            var lmsService = new DigitalChalkAPIService
            {
                Credentials = new System.Net.NetworkCredential("mkovalcson@onecrmpro.com ", "grisgris")
            };

            //   bool userExists = DoesUserExist("mkovalcson@onecrmpro.com", lmsService);
               
            if( !DoesUserExist(Email.Text.ToLower(), lmsService) )
                {
                    string result = CreateUser(thisStudent.FirstName, thisStudent.LastName, Email.Text.ToLower(),
                                               Email.Text.ToLower(), Password.Text, lmsService);
                }

            var db = new SqlAzureLayer(GetConnectionString());
            //var getMedia = string.Format("select content,type from [dbo].[ManagedContent] where name ='Registration Confirmation Email'");

            //var emailContent = db.GetDataTable(getMedia);

            //if (emailContent.Rows.Count == 1)
            //{
            //    HTMLBody = emailContent.Rows[0][0].ToString();
            //}

            string body = HTMLBody;

            //string currentPageUrl = "";
            //if (Request.ServerVariables["HTTPS"].ToString() == "")
            //{
            //    currentPageUrl = Request.ServerVariables["SERVER_PROTOCOL"].ToString().ToLower().Substring(0, 4).ToString() + @"://" + Request.ServerVariables["SERVER_NAME"].ToString() + ":" + Request.ServerVariables["SERVER_PORT"].ToString() + Request.ServerVariables["SCRIPT_NAME"].ToString();
            //}
            //else
            //{
            //    currentPageUrl = Request.ServerVariables["SERVER_PROTOCOL"].ToString().ToLower().Substring(0, 5).ToString() + @"://" + Request.ServerVariables["SERVER_NAME"].ToString() + ":" + Request.ServerVariables["SERVER_PORT"].ToString() + Request.ServerVariables["SCRIPT_NAME"].ToString();
            //}

           // currentPageUrl = currentPageUrl.Replace("CustomRegister", "ConfirmRegistration");
            //currentPageUrl = currentPageUrl.Replace("/:/", "//");

            //string currentPageUrl = "https://www.graymattersgroup.info/Account/ConfirmRegistration.aspx";

            //var result = db.GetDataTable(string.Format("SELECT Userid FROM dbo.aspnet_Users where UserName = {0}", db.FormatString(Email.Text)));
           
            //var userId = result.Rows[0][0].ToString();

            //var confirmationLink = string.Format("<a href=\"{0}?confirmid={1}\">Click Here</a>", currentPageUrl, userId);

            //body = body.Replace("#CONFIRM", confirmationLink);
            //body = body.Replace("#EMAILADDRESS", Email.Text.ToLower());
            //body = body.Replace("#PASSWORD", Password.Text);
            //var subject = string.Format("Registration Confirmation for Gray Matters Group Aftercare");
     //       EmailNotifications.sendEmailAll(Email.Text.ToLower(), "RxAssist@graymattersgroup.com", subject, body, null, true, false);
                
            CreateUserButton.Enabled = false;
           
            Panel1.Visible = true;
            //Panel2.Visible = false;

            }
            catch (Exception ex)
            {
                if (ex.Message == "The username is already in use.")
                {
                    ErrorMessage.Text = string.Format(@"This email address is already registered..<br/>
Please contact <a href='mailto:RxAssist@graymattersgroup.com'>RxAssist@graymattersgroup.com</a> for assistance.<br/><br/>");
                    EmailNotifications.SendExceptionMessage("GrayMatters ", "CustomRegister Username is already in use " + Email.Text, ex);
                }
                else
                {
                    ErrorMessage.Text = string.Format(@"{0}<br/>", ex.Message);
                    EmailNotifications.SendExceptionMessage("GrayMatters ", "CustomRegister CreateUser for " + Email.Text, ex);
                }
                
            }
        }

        private string GetConnectionString()
        {
            var connectionString2 = ConfigurationManager.ConnectionStrings["Production"].ConnectionString;

            return connectionString2;
        }



        private static void DigitalChalkTestHarness()
        {
            var lmsService = new DigitalChalkAPIService
            {
                Credentials = new System.Net.NetworkCredential("mkovalcson@onecrmpro.com ", "grisgris")
            };

            //   bool userExists = DoesUserExist("mkovalcson@onecrmpro.com", lmsService);

            string result = CreateUser("Joe", "Dirt", "jdirt@test.org", "jdirt@test.org", "abc", lmsService);

        }

        private static string CreateUser(string firstname, string lastname, string emailaddress, string username, string password, DigitalChalkAPIService lmsService)
        {
            auth myAuth = GetAuthorization("v4createUser");

            var newUser = new user
            {
                emailAddress = emailaddress,
                username = username,
                firstName = firstname,
                lastName = lastname,
                password = password,
                passwordReset = "false",
                licenseAgreed = "true"

            };


            var createUserRequest = new createUserRequest()
            {
                auth = new auth[] { myAuth },
                user = new user[] { newUser }
            };


            var result = "";

            //try
            //{
                createUserResponse response = lmsService.createUser(createUserRequest);

                if (response.success == "true")
                {
                    result = "Success";
                }
            ////}
            ////catch (SoapException ex)
            ////{
            ////    result = ex.Message;
            ////}

            return result;
        }

        private static bool DoesUserExist(string username, DigitalChalkAPIService lmsService)
        {
            auth myAuth = GetAuthorization("v4doesUserExist");

            var existRequest = new doesUserExistRequest
            {
                auth = new auth[] { myAuth },
                username = username
            };


            var userExists = false;

            try
            {
                doesUserExistResponse existResponse = lmsService.doesUserExist(existRequest);

                if (existResponse.exists == "true")
                {
                    userExists = true;
                }
            }
            catch (Exception)
            {

            }

            return userExists;
        }


        private static auth GetAuthorization(string apiCall)
        {
            //Computes RFC 2104-compliant HMAC signature using SHA1
            KeyedHashAlgorithm algorithm = new HMACSHA1();
            Encoding encoding = new UTF8Encoding();

            var secretkey = "3c09936e82f1452d9d4a64a4f0ededb2";
            algorithm.Key = encoding.GetBytes(secretkey);

            //var apiCall = "v4doesUserExist";
            var utcDate = DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");
            var signaturePreCoded = string.Format("{0}{1}", apiCall, utcDate);
            var signature = Convert.ToBase64String(algorithm.ComputeHash(encoding.GetBytes(signaturePreCoded.ToCharArray())));

            var digitalChalkAcessKey = "ff808081427f1f690142910d19f3473b";
            var authorization = new auth { accessKey = digitalChalkAcessKey, timestamp = utcDate, signature = signature };

            return authorization;
        }




        private string HTMLBody = @"<html>
	<head>
		<title></title>
		
	<style type=""text/css"">
	
	body   
{
   
    font-size: .80em;
    font-family: ""Helvetica Neue"", ""Lucida Grande"", ""Segoe UI"", Arial, Helvetica, Verdana, sans-serif;
    margin: 0px;
    padding: 0px;
    color: #696969;
}

h1, h2, h3, h4, h5, h6
{
    font-size: 1.5em;
    color: #666666;
   
    text-transform: none;
    font-weight: 200;
    margin-bottom: 0px;
}

h1
{
	color:black;
    font-size: 1.6em;
    padding-bottom: 0px;
    margin-bottom: 0px;
}

h2
{
 	font-size: 1.2em;
    
}

h3
{
    font-size: 1.2em;
}

h4
{
    font-size: 1.1em;
}	table	{	margin-top:15px;		border-collapse:collapse; 	border:0px;	}
	td		{	padding:2px 10px 2px 10px; font-size: 10pt; }
	
	.email	{	background-color:#ffffff; text-align:center;	}
	.label	{	text-align:right;	}
	.value	{	text-align:right;			font-weight:bold;	}
	.count	{	text-align:right; color:black;				}
	.error	{	text-align:right;			color:red;}
	.title	{	color:black; border-bottom:solid 1px black;	}
	.header	{	background-color:DarkBlue; border-top:solid 1px black; border-bottom:solid 1px black;  text-align:center;	}
	td.header { padding:3px; }
	.altrow	{	background-color:#bfcbd6;	}
	.errorRow	{	background-color:#FFdfdf;	}
	.faxRow	{	background-color:#CBCBCB;	}
	.badRow {	background-color:#DDDDDD; border-top:solid 1px black;	}
	
	</style>	
	</head>
	<body style=""vertical-align:top;"">
	<div class=""email"" style="" margin:20px;  padding:5px; text-align:center;"" >
	<div>
	<h2>Thank you for registering for the Gray Matters Group Aftercare Program.</h2>
	<p>Please click the following link to enable your account.</p>
		#CONFIRM
    <p>Your Username: #EMAILADDRESS </p>
    <p>Your Password: #PASSWORD </p>
	</div>
	
	</div>

	</body>
</html>";
    }
}
