﻿using System;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using GoalManagement.Support;

namespace GoalManagement.Account
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             Warning.Text = "";
        } 

        protected void ResetPasswordButton_Click(object sender, EventArgs e)
        {
            // Find user account from email address
            try
            {
                var user = Membership.GetUser(EmailAddress.Text);
            
            user.UnlockUser();
            
            string resetPassword = user.ResetPassword();

            // Generate random password characters
            var randomPassword = CreatePassword();

            user.ChangePassword(resetPassword, randomPassword);

            // Send email to user with new password.
             string messageBody = @"
<html >
	<head>
		<title>Reset Notificiation</title>
				
	<style type=""text/css"">			
		body { font-family:Verdana; }
		.email{ font-size: 10pt; font-family: Verdana;  }				
	</style>
            	
	</head>
		<body >
			<div class=""email"" >		
			<br />			
			<span style=""font-size: medium;""> <strong>
				Gray Matters Group Notification</strong></span><br />
                <br />
                Your password has been reset.<br />
                <br />
                Your new password is <strong>#message#</strong><br /><br />
                Please login using your email address and new password, and then change your password.<br /><br />
                <a href=""https://www.graymattersgroup.info"">https://www.graymattersgroup.info</a><br /><br />
                Thank you.<br /><br />
			</div>				
    </body>
</html>";

                messageBody = messageBody.Replace("#message#", randomPassword);

             EmailNotifications.sendEmailAll(EmailAddress.Text, "RxAssist@graymattersgroup.com", "Gray Matters Group Aftercare Program - Account Activity", messageBody, null, true, false);

             Response.Redirect("~/Account/ResetComplete.aspx", true);

            }
            catch (Exception ex)
            {
                Warning.Text = "Email Address not found";
            }

            
        }

        private readonly char[] RandomCharacters = new[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7', '8', '9' };

        private string CreatePassword()
        {
            string password = "";
            const int passwordLength = 7;
                
            var random = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                password += RandomCharacters[random.Next(0, 31)].ToString();
            }
            
            return password;
        }

    }
}