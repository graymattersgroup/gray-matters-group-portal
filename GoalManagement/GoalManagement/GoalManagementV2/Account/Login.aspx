﻿<%@ Page Title="Gray Matters Group Aftercare - Log In" Language="C#" MasterPageFile="/Site.master"
    AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="GoalManagement.Account.Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        function SetFocus() {
            document.getElementById("ctl00_MainContent_LoginUser_UserName").focus();
        }

        //        function AllowLogin()
        //        {
        //            var checkbox = document.getElementById("AgreeToAgreement");

        //            document.getElementById("ctl00_MainContent_LoginUser_LoginButton").disabled = !checkbox.checked;

        //           
        //            
        //        }


    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <section>
        <div class="title">
            <h1>Account Info</h1>
        </div>
        <div class="settings-body">
            <asp:Login ID="LoginUser" runat="server" EnableViewState="false"
                RenderOuterTable="false" OnAuthenticate="LoginUser_Authenticate">
                <LayoutTemplate>
                    <table class="fixedRow">
                        <tr>
                            <th>Email</th>
                            <td>
                                <asp:TextBox ID="UserName" runat="server" CssClass="textEntry"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                    CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required."
                                    ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <th>Password</th>
                            <td>
                                <asp:TextBox ID="Password" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                    CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                                    ValidationGroup="LoginUserValidationGroup">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td class="checkbox">
                                <asp:CheckBox ID="RememberMe" runat="server" />
                                Keep me logged in <span class="clearfix failureNotification">
                                    <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                                </span>
                                <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification"
                                    ValidationGroup="LoginUserValidationGroup" />
                            </td>
                        </tr>
                    </table>
                    <div class="edit-buttons clearfix">
                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Width="100px" Text="Log In"
                            ValidationGroup="LoginUserValidationGroup" OnClientClick="javascript:Waiting('Loading AfterCare Information....');" />
                    </div>
                </LayoutTemplate>
            </asp:Login>

            <div>
                <table>
                    <tr>
                        <td style="text-align: center;">
                            <asp:HyperLink ID="RegisterHyperLink" runat="server" NavigateUrl="CustomRegister.aspx"
                                EnableViewState="false" CssClass="link-button">Register For An Account</asp:HyperLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:HyperLink ID="HyperLink1" runat="server" EnableViewState="false"
                                    NavigateUrl="ResetPassword.aspx" CssClass="link-button">Reset Password</asp:HyperLink></td>
                    </tr>
                </table>
            </div>
        </div>

    </section>
</asp:Content>
