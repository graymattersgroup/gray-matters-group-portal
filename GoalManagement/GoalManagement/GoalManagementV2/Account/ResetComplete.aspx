﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResetComplete.aspx.cs" Inherits="GoalManagement.Account.ResetComplete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1 {
            font-size: small;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="settings">
        <div class="title">
            <h1>Reset Password</h1>
        </div>
        <div class="content">
            <div>
                <p>Your password has been reset.<br />
                        An email has been sent to you with an automatically
            generated temporary password.<br />
                        <br />
                        Please login using your email address and temporary password,
            and then change your password.</p>
            </div>
        </div>
    </section>
</asp:Content>
