﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoalManagementV2
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MembershipUser currentUser = Membership.GetUser();
            if (Session["ImpersonateUser"] != null)
            {
                var otherUser = (string)Session["ImpersonateUser"];
                currentUser = Membership.GetUser(otherUser);
            }
            if (currentUser != null) Response.Redirect("~/Members/ProfileHome.aspx");

        }
    }
}