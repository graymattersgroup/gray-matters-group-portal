﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using GoalManagement.CrmConnectivity;
//using GoalManagement.Members;
using GoalManagement.Support;
using Telerik.Web.UI;
using System.Configuration;
using System.Web.Security;

namespace GoalManagementV2
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                phNow.Controls.Add(new Literal() { Text = "<time datetime=\"" + DateTime.Now.ToString("yyyy-MM-dd") + "\">" + DateTime.Now.ToString("MMMM dd, yyyy") + "</time>" });
            }

            MembershipUser currentUser = Membership.GetUser();
            if (currentUser == null)
            {
                navBar.Visible = false;
                if (!HttpContext.Current.Request.RawUrl.ToLower().Contains("/account/login.aspx")) Response.Redirect("~/Account/Login.aspx");
            }
        }


        public void SetMenu(string selectedPage)
        {
            var isStudent = ((Session["isStudent"] != null) ? (bool)Session["isStudent"] : false);
            var isCoach = ((Session["isCoach"] != null) ? (bool)Session["isCoach"] : false);
            var myContact = (Contact)Session["StudentInfo"];
            var goalsheetCreated = ((Session["isGoalSheetCreated"] != null) ? (bool)Session["isGoalSheetCreated"] : false);

        }

        protected void LinkLogout(object sender, CommandEventArgs e)
        {
            Session.Abandon();
            Request.Cookies.Clear();
            FormsAuthentication.SignOut();
            var p = HttpContext.Current.Profile;
            Response.Redirect("/Account/Login.aspx");
        }

        //public void SetMenu(string selectedPage)
        //{
        //    var isStudent = ((Session["isStudent"] != null) ? (bool)Session["isStudent"] : false);
        //    var isCoach = ((Session["isCoach"] != null) ? (bool)Session["isCoach"] : false);
        //    var myContact = (Contact)Session["StudentInfo"];
        //    var goalsheetCreated = ((Session["isGoalSheetCreated"] != null) ? (bool)Session["isGoalSheetCreated"] : false);

        //    TreeView1.Nodes.Clear();

        //    var hidePeerReview = false;
        //    var hideResources = false;
        //    var hideCoachSupport = false;

        //    if (Session["MyClasses"] != null)
        //    {
        //        var classes = (List<new_class>)Session["MyClasses"];

        //        if (Session["ClassId"] != null)
        //        {
        //            var classId = (Guid)Session["ClassId"];

        //            foreach (new_class thisClass in classes)
        //            {
        //                if (thisClass.Id == classId)
        //                {
        //                    if (thisClass.new_HidePeerReview.HasValue)
        //                        hidePeerReview = thisClass.new_HidePeerReview.Value;
        //                    if (thisClass.new_HideResourcesandTools.HasValue)
        //                        hideResources = thisClass.new_HideResourcesandTools.Value;
        //                    if (thisClass.new_HideCoachingSupport.HasValue)
        //                        hideCoachSupport = thisClass.new_HideCoachingSupport.Value;
        //                }
        //            }
        //        }
        //    }


        //    if (isCoach) TreeView1.Nodes.Add(AddMenuItem(selectedPage, CentralizedData.DashboardName, "~/Members/dashboard.aspx"));


        //    TreeView1.Nodes.Add(AddMenuItem(selectedPage, CentralizedData.HomeName, "~/Members/ProfileHome.aspx"));

        //    if (isStudent)
        //    {
        //        TreeView1.Nodes.Add(AddMenuItem(selectedPage, CentralizedData.MyGoalsName[goalsheetCreated ? 1 : 0], "~/Members/Goals.aspx"));

        //        if (goalsheetCreated)
        //        {
        //            TreeView1.Nodes.Add(AddMenuItem(selectedPage, CentralizedData.GoalProgress, "~/Members/GoalUpdates.aspx"));

        //            if (!hidePeerReview)
        //                TreeView1.Nodes.Add(AddMenuItem(selectedPage, CentralizedData.CoachStudentsName[1], "~/Members/coaching.aspx"));

        //            if (!hideCoachSupport)
        //                TreeView1.Nodes.Add(AddMenuItem(selectedPage, CentralizedData.CoachingSupportName, "~/Members/ContactCoach.aspx"));

        //            TreeView1.Nodes.Add(AddMenuItem(selectedPage, CentralizedData.MyReports, "~/Members/MyReports.aspx"));

        //            TreeView1.Nodes.Add(AddMenuItem(selectedPage, CentralizedData.CoachInfo, "~/Members/CoachInfo.aspx"));
        //        }
        //    }

        //    if (isCoach)
        //    {

        //        TreeView1.Nodes.Add(AddMenuItem(selectedPage, CentralizedData.CoachStudentsName[0], "~/Members/coaching.aspx"));
        //        TreeView1.Nodes.Add(AddMenuItem(selectedPage, CentralizedData.MyReports, "~/Members/MyReports.aspx"));
        //    }

        //    if (!hideResources) TreeView1.Nodes.Add(AddMenuItem(selectedPage, CentralizedData.ResourcesName, "~/Members/Resources.aspx"));

        //    if (myContact != null)
        //    {
        //        if (myContact.new_IsPortalAdministrator != null)
        //        {
        //            if (myContact.new_IsPortalAdministrator == true)
        //            {
        //                var myAdmin = new TreeNode("Administration", null, null, null, null);

        //                myAdmin.ChildNodes.Add(AddMenuItem(selectedPage, CentralizedData.ClassAdmin,
        //                                                   "~/Administration/ClassAdmin.aspx"));

        //                myAdmin.ChildNodes.Add(AddMenuItem(selectedPage, CentralizedData.ContentManagement,
        //                                                   "~/Administration/ContentEditor.aspx"));
        //                myAdmin.ChildNodes.Add(AddMenuItem(selectedPage, CentralizedData.UserManagement,
        //                                                   "~/Administration/ManageUsers.aspx"));
        //                myAdmin.ChildNodes.Add(AddMenuItem(selectedPage, CentralizedData.TouchManagement,
        //                                                   "http://www.graymattersgroup.info/Administration/TouchEditor.aspx"));

        //                // Used for debugging locally only.
        //                // myAdmin.ChildNodes.Add(AddMenuItem(selectedPage, CentralizedData.TouchManagement,
        //                //                                 "~/Administration/TouchEditor.aspx"));
        //                TreeView1.Nodes.Add(myAdmin);
        //            }
        //        }
        //    }
        //}

        public TreeNode AddMenuItem(string selectedPage, string name, string url)
        {
            var node = new TreeNode(name, null, null, url, "_self");

            if (node.Text == selectedPage) node.Selected = true;
            return node;
        }




        public string GetConnectionString()
        {
            if (Application["connectionString"] == null)
            {
                var connectionString = ConfigurationManager.ConnectionStrings["Production"].ConnectionString;
                Application["connectionString"] = connectionString;
            }
            return Application["connectionString"].ToString();
        }

        public void SetImpersonation(string username)
        {
            ImpersonateUser.Visible = true;
            UserName.Text = username;
        }

        protected void StopImpersonating_Click(object sender, EventArgs e)
        {
            Session.Clear();
            ImpersonateUser.Visible = false;
            Response.Redirect("../Members/ProfileHome.aspx");
        }




    }
}