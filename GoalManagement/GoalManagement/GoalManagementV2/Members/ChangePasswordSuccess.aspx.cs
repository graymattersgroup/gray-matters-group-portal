﻿using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace GoalManagementV2.Members
{
    public partial class ChangePasswordSuccess : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //var logoImage = Master.FindControl("RadBinaryImage1") as RadBinaryImage;

            //logoImage.Visible = false;

            //var mainMenu = Master.FindControl("NavigationMenu") as RadMenu;

            //foreach (RadMenuItem item in mainMenu.Items)
            //{
            //    item.Selected = item.Text == "Manage Subscription";
            //}
            
            var masterPage = (SiteMaster)Page.Master;
            masterPage.SetMenu("");
        }


        //protected void SetMenu()
        //{
        //    var goalsheetCreated = (bool) Session["goalsheetCreated"];
        //    var thisPage = Master.FindControl("PageTitle") as Label;
        //    var myMenu = Master.FindControl("TreeView1") as TreeView;
        //    myMenu.Nodes.Clear();
        //    var current = new TreeNode("AfterCare Home", null, null, "~/Members/ProfileHome.aspx", "_self") { Selected = true };
        //    myMenu.Nodes.Add(current);
        //    if (!goalsheetCreated) myMenu.Nodes.Add(new TreeNode("Create Goal Commitments", null, null, "~/Members/Goals.aspx", "_self"));
        //    if (goalsheetCreated) myMenu.Nodes.Add(new TreeNode("Modify Goal Commitments", null, null, "~/Members/Goals.aspx", "_self"));
        //    if (goalsheetCreated) myMenu.Nodes.Add(new TreeNode("Goal Updates", null, null, "~/Members/GoalUpdates.aspx", "_self"));
        //    myMenu.Nodes.Add(new TreeNode("Resources & Tools", null, null, "~/Members/Resources.aspx", "_self"));
        //}
    }
}
