﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="MyReports.aspx.cs" Inherits="GoalManagement.Members.MyReports" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">

.studenthistoryContainer
{
    position:absolute;
    left:190px;
    top:115px;
    width:720px;
    height:695px;   
    border: 1px solid black;
    background:#ffffff;

}
.studenthistory
{
    padding:20px;
    overflow:auto;  
    width:710px;
    height:650px;
    overflow:auto; 
     background:#ffffff;
       border: 1px solid black;
}

.myanswers { color:blue; }
</style>

<script type="text/javascript">

function PrintContent(divName)
{
var DocumentContainer = document.getElementById(divName);
var WindowObject = window.open("", "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
WindowObject.document.writeln(DocumentContainer.innerHTML);
WindowObject.document.close();
WindowObject.focus();
WindowObject.print();
WindowObject.close();
}



</script>


   <%-- <telerik:HeadTag runat="server" ID="Headtag2">
    </telerik:HeadTag>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function GetSelectedItems()
            {
                alert($find("<%= RadGrid2.MasterTableView.ClientID %>").get_selectedItems().length);
            }
        </script>
    </telerik:RadCodeBlock>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
  <%--  <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel">--%>
    <div id="HelpDiv" class="HelpScreen">
        <asp:Literal ID="HelpLiteral" runat="server"></asp:Literal>
    </div>

   <asp:Panel ID="studentPanel" runat="server" Visible="false">       
        <div  class="studenthistoryContainer">
        <table style=" border-bottom: 2px solid Green; width:100%">
        <tr>
        <td style="padding: 5px; color: Green; font-size: 12pt; font-weight: bold;"> <asp:Label ID="StudentName" runat="server" Text=""></asp:Label>&nbsp;&nbsp;<img src='/Images/Print.gif' onclick="javascript:PrintContent('studentHistory');" />
        </td>
        <td style=" text-align:right; padding:5px; ">
            <asp:Button ID="ExitStudentHistory" runat="server" Text="X" ForeColor="Red" Font-Bold="true" OnClick="ExitStudentHistory_Click" />
        </td>
        </tr>
        </table>        
           

            <div id="studentHistory" class="studenthistory">
                <asp:Panel ID="GradePanel" runat="server" Visible="false">
                </asp:Panel>
               <hr />
               <br />
                <asp:Literal ID="StudentHistoryLiteral" runat="server"></asp:Literal>       
            </div>
        </div>
   </asp:Panel>

   
     <table>

    <tr>
    <td><img src='/Images/help.gif' onmouseover="javascript:showObj('HelpDiv');" onmouseout="javascript:hideObj('HelpDiv');" />&nbsp;
    </td>
    <td style="text-align:right;">Class
        
    </td>
    <td>
        &nbsp;<asp:DropDownList ID="ClassList" runat="server" Font-Size="9pt" 
            onselectedindexchanged="ClassList_SelectedIndexChanged" AutoPostBack="true">
            </asp:DropDownList><br />      
    </td>  
         
     </tr>
     </table>
    <br />
    <br />
    <br />
    <br />
    <table width="600px">
     <tr>
        <td style=" text-align:center;"> 
         <asp:Panel ID="CoachView" runat="server">
         <asp:Button ID="ViewResources" runat="server" Text="Resources Viewed Report" Enabled="true" onclick="ViewResources_Click"  />    
         <br /><br /> 
         </asp:Panel>
            <asp:Panel ID="StudentView" runat="server">
           
                <asp:Button ID="ViewMyGoals" runat="server" Text="View My Goals" Enabled="true" onclick="ViewMyGoals_Click"  />
                <br /><br />
                <asp:Button ID="ViewSelectedUser" runat="server" Text="View My Activities" Enabled="true" OnClick="ViewSelectedUser_Click" />   
                 </asp:Panel>        
        </td>
           
     </tr> 
    </table>



</asp:Content>
