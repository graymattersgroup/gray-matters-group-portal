﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Telerik.Web.UI;

namespace GoalManagementV2
{
    public partial class ProfileHome : System.Web.UI.Page
    {
        protected CRMAccess crmAccess;

        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                divError.Visible = false;
                CoachPanel.Visible = false;

                MembershipUser currentUser = Membership.GetUser();

                if (Session["ImpersonateUser"] != null)
                {
                    var otherUser = (string)Session["ImpersonateUser"];
                    currentUser = Membership.GetUser(otherUser);

                    var masterPage1 = (SiteMaster)Page.Master;
                    masterPage1.SetImpersonation(otherUser);
                }


                if (currentUser == null)
                    Response.Redirect("../Default.aspx");

                var myEmail = currentUser.Email;


                Session["userEmail"] = myEmail;

                //var treeview = Master.FindControl("TreeView1") as TreeView;
                //treeview.Visible = true;

                if (Session["crmAccess"] == null)
                {
                    crmAccess = new CRMAccess(); // Keerthy this is where it is called.
                    Session["crmAccess"] = crmAccess;
                }
                else
                {
                    crmAccess = (CRMAccess)Session["crmAccess"];
                }

                var myContact = crmAccess.GetStudent(myEmail);

                ClassFilter.SelectedIndex = 0;
                Session["FilterClasses"] = (ClassFilter.SelectedIndex == 0);

                if (myContact == null) Response.Redirect("../Default.aspx");

                var isAllClassesAdmin = false;

                if (myContact.new_IsPortalAdministrator.HasValue)
                {
                    if (myContact.new_IsPortalAdministrator.Value)
                    {
                        ClassFilter.Visible = true;

                    }
                }

                if (myContact.new_AllClassesAdmin.HasValue)
                {
                    if (myContact.new_AllClassesAdmin.Value)
                    {
                        isAllClassesAdmin = true;
                    }
                }

                //StudentName.Text = String.Format("{0} {1}", myContact.FirstName, myContact.LastName);

                //if (myContact.ParentContactId != null)
                //{
                //    CompanyName.Text = myContact.ParentCustomerId.Name;
                //}
                //else
                //{
                //    CompanyName.Text = "";
                //}
                if (myContact.new_ManagerId != null)
                {
                    ManagerName.Text = myContact.new_ManagerId.Name;
                }
                else if (!string.IsNullOrEmpty(myContact.ManagerName))
                {
                    ManagerName.Text = myContact.ManagerName;
                }
                else
                {
                    ManagerName.Text = "None Set";
                }

                Session["StudentInfo"] = myContact;

                var classes = crmAccess.GetStudentClasses(myContact.ContactId.Value).ToList();
                bool isStudent = (classes.Count != 0);

                var coachingClasses = crmAccess.GetCoachClasses(myContact.ContactId.Value).ToList();
                bool isCoach = (coachingClasses.Count != 0);

                if (isAllClassesAdmin)
                {
                    var adminclasses = crmAccess.GetClasses().ToList();
                    coachingClasses = adminclasses;
                    classes = adminclasses;
                    isStudent = true;
                    isCoach = true;
                }

                Session["CoachingClasses"] = coachingClasses;
                Session["MyClasses"] = classes;

                Guid classId = Guid.Empty;

                if (coachingClasses.Count == 1)
                {
                    classId = (Guid)coachingClasses[0].new_classId;
                    Session["ClassId"] = classId;
                }

                Boolean classFound = false;

                int count = 0;
                var myClass = new new_class();

                if (isStudent)
                {
                    foreach (new_class newClass in classes)
                    {
                        if (!classFound) myClass = newClass;
                        classFound = true;
                        count++;

                        var addClass = true;

                        if (ClassFilter.SelectedIndex == 0 && newClass.new_ClassComplete.HasValue)
                        {
                            if (newClass.new_ClassComplete.Value) addClass = false;
                        }

                        if (addClass)
                        {
                            ClassList.Items.Add(new ListItem(newClass.new_name, newClass.new_classId.Value.ToString()));
                            //ClassPanel.Visible = true;
                        }

                    }

                    if (count > 1)
                    {
                        ClassList.Visible = true;

                        ClassList.SelectedIndex = 0;
                        if (isCoach) Session["StudentClassId"] = new Guid(ClassList.SelectedValue);
                    }

                    if (!classFound && !isAllClassesAdmin)
                    {
                        showError("You are currently not associated with any classes");
                        return;
                    }
                }


                if (isCoach)
                {
                    Session["isCoach"] = isCoach;
                    // classFound = false;

                    foreach (new_class newClass in coachingClasses)
                    {
                        if (!classFound) myClass = newClass;
                        classFound = true;

                        var addClass = true;
                        if (ClassFilter.SelectedIndex == 0 && newClass.new_ClassComplete.HasValue)
                        {
                            if (newClass.new_ClassComplete.Value) addClass = false;
                        }

                        if (addClass) CoachingClassList.Items.Add(new ListItem(newClass.new_name, newClass.new_classId.Value.ToString()));
                    }


                    if (Session["ClassId"] != null)
                    {
                        classId = (Guid)Session["ClassId"];
                        CoachingClassList.SelectedValue = classId.ToString();
                    }
                    else
                    {
                        CoachingClassList.SelectedIndex = 0;
                        Session["ClassId"] = new Guid(CoachingClassList.SelectedValue);
                    }

                    if (!classFound && !isAllClassesAdmin)
                    {
                        showError("You are currently not associated with any classes");
                        return;
                    }

                    CoachPanel.Visible = true;
                }

                if (classFound == false)
                {
                    showError("You are currently not associated with any classes");
                    return;
                }

                var classStatus = crmAccess.GetStudentClassStatus(myContact.ContactId.Value, myClass.new_classId.Value);

                GoalSheetCreated.Text = "No";
                var goalsheetCreated = false;
                

                foreach (new_classstatus newClassstatus in classStatus)
                {
                    goalsheetCreated = (newClassstatus.new_GoalsSelected == true);
                    GoalSheetCreated.Text = goalsheetCreated ? "Yes" : "No";
                    LastUpdateDate.Text = (newClassstatus.new_LastUpdate.HasValue)
                                              ? newClassstatus.new_LastUpdate.Value.ToString("MM/dd/yyyy")
                                              : "None";
                }

                DateTime classTime = myClass.new_ClassDate.Value;

                ClassDate.Text = classTime.ToShortDateString();
                classId = myClass.new_classId.Value;

                if (!ClassList.Visible) className.Text = myClass.new_name; 

                SetClassIcon(classId, myContact);

                if (Session["ClassId"] == null) Session["ClassId"] = classId;
                Session["ClassInfo"] = myClass;
                Session["CRM_Access"] = crmAccess;
                Session["isGoalSheetCreated"] = goalsheetCreated;
                Session["isStudent"] = isStudent;
                Session["isCoach"] = isCoach;

                //var masterPage = (SiteMaster)Page.Master;
                //masterPage.SetMenu(CentralizedData.HomeName);
                //var db = new SqlAzureLayer(masterPage.GetConnectionString());
                //var result = db.GetDataTable("select content from [dbo].[ManagedContent] where name ='Aftercare Home Help' ");
                //if (result != null && result.Rows.Count > 0)
                //{
                //    HelpLiteral.Text = result.Rows[0][0].ToString();
                //}
                //var radManager = Master.FindControl("RadAjaxManager1") as RadScriptManager;
                //radManager..AjaxSettings.AddAjaxSetting(Button1, Panel1, null);    

            }
        }




        //protected void SetMenu(bool goalsheetCreated, bool isCoach, bool isStudent)
        //{
        //    var thisPage = Master.FindControl("PageTitle") as Label;
        //    var myMenu = Master.FindControl("TreeView1") as TreeView;
        //    myMenu.Nodes.Clear();
        //    var current = new TreeNode("AfterCare Home", null, null, "~/Members/ProfileHome.aspx", "_self"){Selected = true};
        //    myMenu.Nodes.Add(current);

        //    if (isStudent)
        //    {
        //        if (!goalsheetCreated)myMenu.Nodes.Add(new TreeNode("Create My Goals", null, null, "~/Members/Goals.aspx", "_self"));
        //        if (goalsheetCreated)myMenu.Nodes.Add(new TreeNode("Modify My Goals", null, null, "~/Members/Goals.aspx", "_self"));
        //        if (goalsheetCreated)myMenu.Nodes.Add(new TreeNode("Goal Progress", null, null, "~/Members/GoalUpdates.aspx", "_self"));
        //    }

        //    if (isCoach)
        //        myMenu.Nodes.Add(new TreeNode("Coach Students", null, null, "~/Members/coaching.aspx", "_self"));

        //    myMenu.Nodes.Add(new TreeNode("Resources & Tools", null, null, "~/Members/Resources.aspx", "_self"));


        //    var myContact = (Contact)Session["StudentInfo"];

        //    if( myContact.new_IsPortalAdministrator == true)
        //    myMenu.Nodes.Add(new TreeNode("Admin- Content", null, null, "~/Administration/ContentEditor.aspx", "_self"));

        //    Session["goalsheetCreated"] = goalsheetCreated;
        //}

        protected void ClassList_SelectedIndexChanged(object sender, EventArgs e)
        {
            crmAccess = (CRMAccess)Session["CRM_Access"];
            var isCoach = (bool)Session["isCoach"];

            var classes = (List<new_class>)(isCoach ? Session["CoachingClasses"] : Session["MyClasses"]);
            var myContact = (Contact)Session["StudentInfo"];

            var selectedValue = new Guid((sender == CoachingClassList) ? CoachingClassList.SelectedValue : ClassList.SelectedValue);

            Session["ClassId"] = selectedValue;

            if (sender == ClassList) Session["StudentClassId"] = selectedValue;

            foreach (new_class newClass in classes)
            {
                if (selectedValue == newClass.new_classId.Value)
                {
                    var classStatus = crmAccess.GetStudentClassStatus(myContact.ContactId.Value, newClass.new_classId.Value);

                    GoalSheetCreated.Text = "No";
                    var goalsheetCreated = false;

                    foreach (new_classstatus newClassstatus in classStatus)
                    {
                        goalsheetCreated = (newClassstatus.new_GoalsSelected == true);
                        GoalSheetCreated.Text = goalsheetCreated ? "Yes" : "No";
                        LastUpdateDate.Text = (newClassstatus.new_LastUpdate.HasValue)
                                                  ? newClassstatus.new_LastUpdate.Value.ToString("MM/dd/yyyy")
                                                  : "None";
                    }


                    DateTime classTime = newClass.new_ClassDate.Value;

                    ClassDate.Text = classTime.ToShortDateString();
                    var classId = newClass.new_classId.Value;

                    SetClassIcon(classId, myContact);


                    Session["ClassInfo"] = newClass;
                    Session["isGoalSheetCreated"] = goalsheetCreated;
                    Session.Remove("Logo");


                    var masterPage = (SiteMaster)Page.Master;
                    masterPage.SetMenu("AfterCare Home");
                }
            }
        }

        private void SetClassIcon(Guid classId, Contact myContact)
        {
            crmAccess = (CRMAccess)Session["crmAccess"];
            var logoImage = Master.FindControl("RadBinaryImage1") as RadBinaryImage;

            try
            {
                IQueryable<Annotation> logo;

                if (myContact.ParentCustomerId != null)
                {
                    try
                    {
                        logo = crmAccess.GetClassAnnotation(myContact.ParentCustomerId.Id);
                        if (logo.FirstOrDefault() == null) logo = crmAccess.GetClassAnnotation(classId);
                    }
                    catch
                    {
                        logo = crmAccess.GetClassAnnotation(classId);
                    }
                }
                else
                {
                    logo = crmAccess.GetClassAnnotation(classId);
                }

                if (logo.FirstOrDefault() != null)
                {
                    logoImage.Visible = true;
                    logoImage.DataValue = Convert.FromBase64String(logo.FirstOrDefault().DocumentBody);
                    Session["Logo"] = logoImage.DataValue;
                }
                else
                {
                    Session.Remove("Logo");
                    // logoImage.Visible = false;
                }
            }
            catch
            {
                Session.Remove("Logo");
                //  logoImage.Visible = false;
            }
        }

        protected void ClassFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["FilterClasses"] = (ClassFilter.SelectedIndex == 0);

            var coachingClasses = (List<new_class>)Session["CoachingClasses"];
            var classes = (List<new_class>)Session["MyClasses"];

            ClassList.Items.Clear();

            foreach (new_class newClass in classes)
            {
                var addClass = true;

                if (ClassFilter.SelectedIndex == 0 && newClass.new_ClassComplete.HasValue)
                {
                    if (newClass.new_ClassComplete.Value) addClass = false;
                }

                if (addClass)
                {
                    ClassList.Items.Add(new ListItem(newClass.new_name, newClass.new_classId.Value.ToString()));
                    //ClassPanel.Visible = true;
                }

            }

            CoachingClassList.Items.Clear();
            foreach (new_class newClass in coachingClasses)
            {
                var addClass = true;
                if (ClassFilter.SelectedIndex == 0 && newClass.new_ClassComplete.HasValue)
                {
                    if (newClass.new_ClassComplete.Value) addClass = false;
                }

                if (addClass) CoachingClassList.Items.Add(new ListItem(newClass.new_name, newClass.new_classId.Value.ToString()));
            }
        }



        public void showError(String message)
        {
            divError.Visible = true;
            lblError.InnerText = message;
        }
    }
}