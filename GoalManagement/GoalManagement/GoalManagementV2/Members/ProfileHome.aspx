﻿<%@ Page Title="Gray Matters Group Aftercare- Home" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="ProfileHome.aspx.cs" Inherits="GoalManagementV2.ProfileHome" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div  runat="server" id="divError">
        <div class="title">
            <h1>Notifications</h1>
        </div>
        <div >
            <p>
                <label class="indiv-notif" runat="server" id="lblError" />
            </p>
        </div>
    </div>
    <section>
        <div class="title short">
            <h1>Student Class</h1>
        </div>
        <div class="title line">
                <asp:Label runat="server" ID="className" /><asp:DropDownList ID="ClassList" runat="server"
                    AutoPostBack="true" OnSelectedIndexChanged="ClassList_SelectedIndexChanged" Visible="false">
                </asp:DropDownList>&nbsp;
        </div>
    </section>
    <section>
        <div class="title">
            <h1>Class Status</h1>
        </div>
        <table class="fixedRow">
            <%--<thead>
                    <tr>
                        <th>Specific Goal</th>
                        <th>Goal Progress Status</th>
                        <th>Report Progress</th>
                        <th>Edit</th>
                    </tr>
                </thead>--%>
            <tbody>
                <tr>
                    <td>Class Date</td>
                    <td>
                        <asp:Label ID="ClassDate" runat="server" Text="XX/XX/XX"></asp:Label></td>
                </tr>
                <tr>
                    <td>Goal Sheet Created</td>
                    <td>
                        <asp:Label ID="GoalSheetCreated" runat="server" Text="No"></asp:Label></td>
                </tr>
                <tr>
                    <td>Last Update</td>
                    <td>
                        <asp:Label ID="LastUpdateDate" runat="server" Text="None"></asp:Label></td>
                </tr>
                <tr>
                    <td>Your Manager</td>
                    <td>
                        <asp:Label ID="ManagerName" runat="server" Text="Manager Name"></asp:Label></td>
                </tr>
            </tbody>
        </table>

    </section>
    <section runat="server" id="CoachPanel">
        <div class="title short">
            <h1>Coaching Classes</h1>
        </div>
        <div class="title line">
            <asp:DropDownList ID="CoachingClassList" runat="server" AutoPostBack="true"
                OnSelectedIndexChanged="ClassList_SelectedIndexChanged" CssClass="shortSelect">
            </asp:DropDownList>
            <asp:RadioButtonList ID="ClassFilter" runat="server" RepeatDirection="Horizontal"
                Visible="false" AutoPostBack="True" OnSelectedIndexChanged="ClassFilter_SelectedIndexChanged" RepeatLayout="Flow">
                <asp:ListItem Text="Open Classes" Value="1"></asp:ListItem>
                <asp:ListItem Text="All Classes" Value="2"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </section>


    <%--    <asp:Label ID="CompanyName" runat="server" Text="Company Name"></asp:Label>
    <asp:Literal ID="HelpLiteral" runat="server"></asp:Literal>
    <asp:Panel ID="ClassPanel" runat="server" Visible="false"></asp:Panel>
    <asp:Panel ID="CoachPanel" runat="server" Visible="false" class="student-class"></asp:Panel>--%>
</asp:Content>
