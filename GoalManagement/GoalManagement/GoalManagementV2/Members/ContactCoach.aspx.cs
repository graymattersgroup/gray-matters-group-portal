﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Telerik.Web.UI;

namespace GoalManagementV2
{
    public partial class ContactCoach : System.Web.UI.Page
    {
        protected CRMAccess crmAccess;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MembershipUser currentUser = Membership.GetUser();

                if (Session["ImpersonateUser"] != null)
                {
                    var otherUser = (string)Session["ImpersonateUser"];
                    currentUser = Membership.GetUser(otherUser);

                    var masterPage1 = (SiteMaster)Page.Master;
                    masterPage1.SetImpersonation(otherUser);
                }

                if ( currentUser == null) Response.Redirect("../Default.aspx");
                var myEmail = currentUser.Email;
                
                var treeview = Master.FindControl("TreeView1") as TreeView;
                treeview.Visible = true;

                if ( Session["crmAccess"] == null)
                {
                    crmAccess = new CRMAccess();
                    Session["crmAccess"] = crmAccess;
                }
                else
                {
                    crmAccess = (CRMAccess) Session["crmAccess"];
                }

                Contact studentInfo;
                if ( Session["StudentInfo"] != null)
                {
                    studentInfo = (Contact) Session["StudentInfo"];
                }
                else
                {
                    studentInfo = crmAccess.GetStudent(myEmail);
                    Session["StudentInfo"] = studentInfo;
                }

                var myClass = new new_class();
                if  ( Session["ClassInfo"]  != null )
                {
                    myClass = (new_class)Session["ClassInfo"];
                }
                else
                {
                    var classes = crmAccess.GetStudentClasses(studentInfo.ContactId.Value).ToList();
                    if (classes.Count != 0)
                    {
                        Session["ClassInfo"] = classes[0];
                        myClass = (new_class)Session["ClassInfo"];

                    }
                    else
                    {
                        Response.Redirect("~/Members/ProfileHome.aspx");
                    }
                }
                
               
                SetClassIcon(myClass.new_classId.Value, studentInfo);

           
               
                // Get My Coaches
                List<Contact> coaches;
                if (Session["myCoaches"] == null)
                {
                    coaches = crmAccess.GetCoaches(myClass.new_classId.Value);
                    Session["myCoaches"] = coaches;
                }
                else
                {
                    coaches = (List <Contact>)Session["myCoaches"];
                }
               

                foreach (Contact contact in coaches)
                {
                    var aCoach = new ListItem(contact.FirstName + " " + contact.LastName , contact.EMailAddress1);

                    if (studentInfo.new_MyCoachId != null)
                    {
                        if (studentInfo.new_MyCoachId.Id == contact.ContactId)
                        {
                            aCoach.Text += " (my coach)";
                            aCoach.Selected = true;
                        }
                    }
                    CoachesList.Items.Add(aCoach);
                    
                }

                
                var masterPage = (SiteMaster)Page.Master;
                masterPage.SetMenu(CentralizedData.CoachingSupportName);
                var db = new SqlAzureLayer(masterPage.GetConnectionString());
                var result = db.GetDataTable("select content from [dbo].[ManagedContent] where name ='Contact Coach Help' ");
                if (result != null && result.Rows.Count > 0)
                {
                    HelpLiteral.Text = result.Rows[0][0].ToString();
                }
                }
        }

      

        private void SetClassIcon(Guid classId, Contact studentInfo)
        {
            var logoImage = Master.FindControl("RadBinaryImage1") as RadBinaryImage;
            try
            {
                if (Session["Logo"] != null)
                {
                    logoImage.DataValue = (byte[]) Session["Logo"];
                    logoImage.Visible = true;
                }
                else
                {
                    if (studentInfo.ParentCustomerId != null)
                    {
                        var logo = crmAccess.GetClassAnnotation(studentInfo.ParentCustomerId.Id);
                        if (logo.FirstOrDefault() == null) logo = crmAccess.GetClassAnnotation(classId);
                        if (logo.FirstOrDefault() != null)
                        {
                            logoImage.Visible = true;
                            logoImage.DataValue = Convert.FromBase64String(logo.FirstOrDefault().DocumentBody);
                        }
                        else
                        {
                            logoImage.Visible = false;
                        }
                    }
                    else
                    {
                        logoImage.Visible = false;
                    }
                }

            }
            catch
            {
                logoImage.Visible = false;
              
            }
        }


        protected void SaveUpdateInformation_Click(object sender, EventArgs e)
        {
            SendEmail();
        }

        private void SendEmail()
        {
            var studentInfo = (Contact) Session["StudentInfo"];
            
            var fromme = studentInfo.EMailAddress1;
            var myto = "";

            foreach (ListItem item in CoachesList.Items)
            {
                if(item.Selected)
                {
                    myto += item.Value +";";
                }

            }

            var subject = string.Format("AfterCare Request from {0} {1}", studentInfo.FirstName,
                                        studentInfo.LastName);
            
            string filename = "";
            byte[] attachment = null;

            EmailNotifications.sendEmailAll(myto, fromme, subject, Comments.Text, attachment, filename, false, false);
        }
       
    }

 


}