﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Microsoft.Xrm.Sdk;
using Telerik.Reporting.Processing;
using Telerik.Web.UI;


namespace GoalManagementV2.Members
{
    public partial class MyReports : System.Web.UI.Page
    {
        protected CRMAccess crmAccess;
        protected string ThisContactEmail;

        protected void Page_Load(object sender, EventArgs e)
        {
            ThisContactEmail = "";

            if (!IsPostBack)
            {
                if (Session["ImpersonateUser"] != null)
                {
                    var otherUser = (string)Session["ImpersonateUser"];
                    var masterPage1 = (SiteMaster)Page.Master;
                    masterPage1.SetImpersonation(otherUser);
                }

                var isStudent = ((Session["isStudent"] != null) ? (bool)Session["isStudent"] : false);
                var isCoach = ((Session["isCoach"] != null) ? (bool)Session["isCoach"] : false);

                StudentView.Visible = isStudent;
                CoachView.Visible = isCoach;

                if (Request.QueryString["specificstudent"] != null)
                {
                    ThisContactEmail = Request.QueryString["specificstudent"].ToString();
                }
               
                if (Session["crmAccess"] == null)
                {
                    crmAccess = new CRMAccess();
                    Session["crmAccess"] = crmAccess;
                }
                else
                {
                    crmAccess = (CRMAccess) Session["crmAccess"];
                }
                List<new_class> coachingClasses;
                
                var me = (Contact)Session["StudentInfo"];

                if( isCoach)
                {
                    if (Session["CoachingClasses"] != null)
                    {
                        coachingClasses = (List<new_class>) Session["CoachingClasses"];
                    }
                    else
                    {
                        coachingClasses = crmAccess.GetCoachClasses(me.ContactId.Value).ToList();
                        Session["CoachingClasses"] = coachingClasses;
                    }
                }
                else
                {
                    if (Session["MyClasses"] != null)
                    {
                        coachingClasses = (List<new_class>) Session["MyClasses"];
                    }
                    else
                    {
                        coachingClasses = crmAccess.GetStudentClasses(me.ContactId.Value).ToList();
                        Session["MyClasses"] = coachingClasses;
                    }
                }

                foreach (new_class coachingClass in coachingClasses)
                    {
                        ClassList.Items.Add(new ListItem(coachingClass.new_name, coachingClass.new_classId.ToString()));
                    }

                    ClassList.SelectedIndex = 0;

                ClassList.Visible = (ClassList.Items.Count != 1);
                 
                var masterPage = (SiteMaster)Page.Master;
                masterPage.SetMenu( CentralizedData.MyReports);
                var db = new SqlAzureLayer(masterPage.GetConnectionString());
                var result = db.GetDataTable("select content from [dbo].[ManagedContent] where name ='My Reports' ");
                
                if (result != null && result.Rows.Count > 0)
                {
                    HelpLiteral.Text = result.Rows[0][0].ToString();
                }
            }
        }
        
        private void SetClassIcon(Guid classId, Contact myContact)
        {
            crmAccess = (CRMAccess)Session["crmAccess"];
            var logoImage = Master.FindControl("RadBinaryImage1") as RadBinaryImage;

            try
            {
                IQueryable<Annotation> logo;

                if (myContact.ParentCustomerId != null)
                {
                    try
                    {
                        logo = crmAccess.GetClassAnnotation(myContact.ParentCustomerId.Id);
                        if (logo.FirstOrDefault() == null) logo = crmAccess.GetClassAnnotation(classId);
                    }
                    catch
                    {
                        logo = crmAccess.GetClassAnnotation(classId);
                    }


                }
                else
                {
                    logo = crmAccess.GetClassAnnotation(classId);
                }

                if (logo.FirstOrDefault() != null)
                {
                    logoImage.Visible = true;
                    logoImage.DataValue = Convert.FromBase64String(logo.FirstOrDefault().DocumentBody);
                    Session["Logo"] = logoImage.DataValue;
                }
                else
                {
                    Session.Remove("Logo");
                    logoImage.Visible = false;
                }
            }
            catch
            {
                Session.Remove("Logo");
                logoImage.Visible = false;
            }
        }
        

        protected void ClassList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var classId = new Guid(ClassList.SelectedValue);

            Session.Remove("Logo");
            var coach = (Contact)Session["StudentInfo"];

            SetClassIcon(classId, coach);
        }
        
        protected void ViewSelectedUser_Click(object sender, EventArgs e)
        {
            var student = (Contact)Session["StudentInfo"];
            var classId = new Guid(ClassList.SelectedValue);

            crmAccess = (CRMAccess)Session["crmAccess"];

            if (Session["Coaches"] == null)
            {
                Session["Coaches"] = crmAccess.GetClassCoaches(classId);
            }

            var coaches = (List<Contact>)Session["Coaches"];

            studentPanel.Visible = true;
            StudentName.Text = student.FullName;

            var coachFeedback = crmAccess.GetCoachFeedbackbyStudent(classId, student.ContactId.Value);
            StudentHistoryLiteral.Text = ReportHelper.ViewThisSelectedUser(classId, crmAccess, student, coaches, coachFeedback);
        }

   
        protected void ExitStudentHistory_Click(object sender, EventArgs e)
        {
            studentPanel.Visible = false;
        }

        private string GetConnectionString()
        {
            var connectionString2 = ConfigurationManager.ConnectionStrings["Production"].ConnectionString;
            return connectionString2;
        }

      
        protected void ViewMyGoals_Click(object sender, EventArgs e)
        {
            var student = (Contact) Session["StudentInfo"];
            var studentId = student.ContactId.Value;
            var classId = new Guid(ClassList.SelectedValue);

            crmAccess = (CRMAccess)Session["crmAccess"];
            var sql = new SqlAzureLayer(GetConnectionString());

            StudentHistoryLiteral.Text = ReportHelper.ViewThisSelectedUsersGoals(classId, studentId, sql, crmAccess);
            StudentName.Text = "My Goals";
            studentPanel.Visible = true;
        }



        //protected void ViewThisSelectedUser(Guid classId, CRMAccess crmAccess, Contact student, List<Contact> coaches)
        //{
        //    var studentId = student.ContactId.Value;

        //    studentPanel.Visible = true;

        //    string[] progressVAlues = new string[] { "", "None", "Some", "Significant", "Completed" };

        //    StudentName.Text = student.FullName;

        //    var allUpdates = crmAccess.GetAllUpdatePeriods(studentId, classId);
        //    var coachFeedback = crmAccess.GetCoachFeedbackbyStudent(classId, studentId);

        //    // Build complete summary of Updates and feedback.

        //    var sb = new StringBuilder(256);

        //    foreach (StudentUpdatePeriod thisUpdatePeriod in allUpdates)
        //    {
        //        var myUpdate = thisUpdatePeriod;
        //        myUpdate = crmAccess.GetMyUpdatePeriod(studentId, thisUpdatePeriod.ClassUpdatePeriodId,
        //                                               thisUpdatePeriod);

        //        if (thisUpdatePeriod.ClassId != classId) continue;
        //        var start = myUpdate.startDate;
        //        var end = myUpdate.endDate;

        //        sb.AppendFormat("<b>Update Period {0} - {1}</b><br/>", start.ToShortDateString(),
        //                        end.ToShortDateString());

        //        if (myUpdate.PeriodGoalAnswers.Count == 0 || myUpdate.PeriodGoalAnswers[0].ProgressValue == -1)
        //        {
        //            sb.AppendFormat("No Updates.");
        //            sb.AppendFormat("<br/>");
        //            sb.AppendFormat("<br/>");

        //            bool coachingDiv = false;

        //            foreach (new_coaching newCoaching in coachFeedback)
        //            {
        //                if (newCoaching.new_ClassUpdatePeriodId.Id == myUpdate.PeriodId)
        //                {
        //                    if (coachingDiv == false)
        //                    {
        //                        coachingDiv = true;
        //                        sb.AppendFormat("<div style='color:green; border: 1px solid red; padding:5px;'>");
        //                    }

        //                    var coachName = "";
        //                    foreach (Contact coach in coaches)
        //                    {
        //                        if (newCoaching.new_CoachId.Id == coach.ContactId)
        //                        {
        //                            coachName = coach.FirstName + " " + coach.LastName;
        //                            break;
        //                        }

        //                    }
        //                    sb.AppendFormat("<b>Coach Feedback from {0} on {1}</b><br/>", coachName,
        //                                    newCoaching.ModifiedOn.Value.ToShortDateString());

        //                    if (newCoaching.new_UpdateGrade != null)
        //                        sb.AppendFormat("Progress Grade:  {0}<br/><br/>",
        //                                        newCoaching.new_UpdateGrade.Value);



        //                    sb.AppendFormat("{0}", newCoaching.new_Advice);
        //                    sb.AppendFormat("<br/>");

        //                }
        //            }
        //            if (coachingDiv)
        //            {
        //                sb.Append("</div>");
        //                sb.Append("<hr/>");
        //                sb.AppendFormat("<br/>");
        //            }


        //            continue;
        //        }


        //        sb.AppendFormat("<br/>");
        //        sb.AppendFormat(
        //            "<b>Impact Question</b><br/> {0} <br/><span class='myanswers'>{1}</span><br/><br/>",
        //            myUpdate.PeriodImpactQuestion, myUpdate.ImpactAnswer);
        //        sb.AppendFormat("<br/>");
        //        foreach (GoalAnswers answer in myUpdate.PeriodGoalAnswers)
        //        {
        //            var progress = answer.ProgressValue;
        //            if (progress == -1) progress = 0;

        //            sb.AppendFormat(
        //                "<hr/><b>{0}</b> <span class='myanswers'>(progress - {1})</span><br/><br/>",
        //                answer.GoalName, progressVAlues[progress]);
        //            sb.AppendFormat("{0}: <br/><span class='myanswers'>{1}</span><br/><br/>",
        //                            myUpdate.GoalQuestion1, answer.Answer1);
        //            sb.AppendFormat("{0}: <br/><span class='myanswers'>{1}</span><br/>",
        //                            myUpdate.GoalQuestion2, answer.Answer2);
        //            sb.AppendFormat("<br/>");

        //            sb.AppendFormat("I used the following activities in this goal period:<br/>");


        //            bool noActivities = true;
        //            foreach (Activities act in answer.MyActivities)
        //            {
        //                if (act.IsChecked)
        //                {
        //                    noActivities = false;
        //                    sb.AppendFormat("- <span class='myanswers'>{0}</span><br/>", act.Text);
        //                }
        //            }

        //            if (noActivities) sb.AppendFormat("- No Activities Selected");


        //            sb.AppendFormat("<br/><br/>");
        //        }

        //        sb.AppendFormat("Feedback Request: <br/>");
        //        sb.AppendFormat("<span class='myanswers'>{0}</span><br/><br/>",
        //                        thisUpdatePeriod.FeedbackRequest);

        //        sb.AppendFormat("<div style='color:green; border: 1px solid red; padding:5px;'>");
        //        foreach (new_coaching newCoaching in coachFeedback)
        //        {
        //            if (newCoaching.new_ClassUpdatePeriodId.Id == myUpdate.PeriodId)
        //            {
        //                var coachName = "";
        //                foreach (Contact coach in coaches)
        //                {
        //                    if (newCoaching.new_CoachId.Id == coach.ContactId)
        //                    {
        //                        coachName = coach.FirstName + " " + coach.LastName;
        //                        break;
        //                    }

        //                }
        //                sb.AppendFormat("<b>Coach Feedback from {0} on {1}</b><br/>", coachName,
        //                                newCoaching.ModifiedOn.Value.ToShortDateString());

        //                if (newCoaching.new_UpdateGrade != null)
        //                    sb.AppendFormat("Progress Grade:  {0}<br/><br/>",
        //                                    newCoaching.new_UpdateGrade.Value);



        //                sb.AppendFormat("{0}", newCoaching.new_Advice);
        //                sb.AppendFormat("<br/>");

        //            }
        //        }
        //        sb.Append("</div>");

        //        sb.Append("<hr/>");
        //        sb.AppendFormat("<br/>");
        //    }

        //    StudentHistoryLiteral.Text = sb.ToString();
        //}



        //private ClassGoals GetClassGoals(Guid classId)
        //{
        //    crmAccess = (CRMAccess)Session["crmAccess"];
        //    var sql = new SqlAzureLayer(GetConnectionString());
        //    var blob = sql.GetGoalBlob(classId);

        //    if (blob == null)
        //    {
        //        // Get all Students
        //        var goals = crmAccess.GetClassGoals(classId).ToList();

        //        blob = new ClassGoals();

        //        foreach (ClassGoal classGoal in goals)
        //        {
        //            if (classGoal.isdefault ?? false)
        //                classGoal.selected = true;

        //            var activities = crmAccess.GetGoalActivities(classGoal.id).ToList();

        //            foreach (Activity newGoalactivity in activities)
        //            {
        //                var subactivities = crmAccess.GetSubActivities(newGoalactivity.id).ToList();

        //                foreach (SubActivity subActivity in subactivities)
        //                {
        //                    newGoalactivity.subActivities.Add(subActivity);
        //                }
        //                classGoal.activities.Add(newGoalactivity);
        //            }

        //            blob.classGoals.Add(classGoal);
        //        }

        //        blob.classid = classId;
        //        blob.lastupdated = DateTime.Now;
        //        sql.UpdateGoalBlob(blob);

        //    }
        //   return blob;
        //}
        
        
        
        //private ClassGoals GetClassGoals(Guid classId)
        //{
        //    crmAccess = (CRMAccess)Session["crmAccess"];
        //    var sql = new SqlAzureLayer(GetConnectionString());
        //    var blob = sql.GetGoalBlob(classId);

        //    if (blob == null)
        //    {
        //        // Get all Students
        //        var goals = crmAccess.GetClassGoals(classId).ToList();

        //        blob = new ClassGoals();

        //        foreach (ClassGoal classGoal in goals)
        //        {
        //            if (classGoal.isdefault ?? false)
        //                classGoal.selected = true;

        //            var activities = crmAccess.GetGoalActivities(classGoal.id).ToList();

        //            foreach (Activity newGoalactivity in activities)
        //            {
        //                var subactivities = crmAccess.GetSubActivities(newGoalactivity.id).ToList();

        //                foreach (SubActivity subActivity in subactivities)
        //                {
        //                    newGoalactivity.subActivities.Add(subActivity);
        //                }
        //                classGoal.activities.Add(newGoalactivity);
        //            }

        //            blob.classGoals.Add(classGoal);
        //        }

        //        blob.classid = classId;
        //        blob.lastupdated = DateTime.Now;
        //        sql.UpdateGoalBlob(blob);

        //    }
        //   return blob;
        //}


        //public string ViewAStudentsGoals(Guid classId, Guid studentId)
        //{

        //var classGoals = GetClassGoals(classId);

        //    StudentName.Text = "My Goals";
        //    var selectedGoals = crmAccess.GetStudentGoals(classId, studentId);
         
        //    var classGoalPages =  new List<ClassGoal>();

        //    foreach (ClassGoal classGoal in classGoals.classGoals)
        //    {
        //        foreach (new_studentgoals myGoals in selectedGoals)
        //        {
        //            if (myGoals.new_classgoalId.Id == classGoal.id)
        //            {
        //                var newGoal = new ClassGoal {name = classGoal.name,
        //                    payoff = myGoals.new_Benefit,
        //                    selected = true};

        //                var selectedActivities = crmAccess.GetStudentGoalActivities(classId, studentId, classGoal.id);

        //                var newActivities = new List<Activity>();

        //                foreach (Activity anActivity in classGoal.activities)
        //                {
        //                    //if (!newGoalactivity.selected && newGoalactivity.subActivities.Count == 0) continue;
                            
        //                    foreach (new_goalactivity myActivity in selectedActivities)
        //                    {
        //                        if (anActivity.subActivities.Count != 0)
        //                        {
        //                            var selectedsubactivites = crmAccess.GetStudentGoalSubActivities(classId, studentId, anActivity.id);
                                    
        //                            bool haveSubs = false;

        //                            var newActivity = new Activity
        //                            {
        //                                selected = false,
        //                                name = anActivity.name,

        //                            };
        //                            foreach (SubActivity selectedsubactivity in anActivity.subActivities)
        //                            {
                                       
        //                                  foreach (new_subactivity mySubActivity in selectedsubactivites)
        //                                    {
        //                                        if( mySubActivity.Id == selectedsubactivity.id )
        //                                        {
        //                                            haveSubs = true;

        //                                            var newSubActivity = new SubActivity
        //                                                                     {
        //                                                                         name = selectedsubactivity.name,
        //                                                                         selected = true
        //                                                                     };

        //                                            newActivity.subActivities.Add(newSubActivity);
        //                                        }
        //                                    }

                                       
        //                            }

        //                            if (haveSubs)
        //                            {
        //                                newActivities.Add(newActivity);
        //                            }

        //                        }
        //                        else if (myActivity.Id == anActivity.id)
        //                        {
        //                            var newActivity = new Activity {selected = true, name = anActivity.name};
        //                            newActivities.Add(newActivity);
        //                        }
        //                    }
        //                }

        //                newGoal.activities = newActivities;

        //                classGoalPages.Add(newGoal);
        //            }
        //        }
        //    }
          
        //   return ReportHelper.ViewMyCommitments(classGoalPages);
        //}
        

        protected void ViewResources_Click(object sender, EventArgs e)
        {
            crmAccess = (CRMAccess)Session["crmAccess"];

            var className = ClassList.SelectedItem.Text;
            var classId = new Guid(ClassList.SelectedItem.Value);

            var result = ReportHelper.GetTelerikTrackingReport(classId, className, crmAccess);

            var filename = string.Format("ResourcesViewedReport{0}.pdf", className);

            ExportToPDF(result, filename);

        }


        void ExportToPDF(RenderingResult result, string fileName)
        {
            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                             fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
        }
    }
}