﻿<%@ Page Title="Gray Matters Group - Goal Updates" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="ContactCoach.aspx.cs" Inherits="GoalManagement.ContactCoach" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">
.activityHeader
{ text-decoration:underline; color:Black; font-size:12pt; padding-top:10px; padding-bottom:5px;}

.activityItem { padding-left: 10px;}
.activitycheckbox { padding-top:3px; }
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="HelpDiv" class="HelpScreen">
                <asp:Literal ID="HelpLiteral" runat="server"></asp:Literal>
            </div>

            <table  width="100%">  
            <tr>
            <td>             

                     <div style="width: 100%; text-align:left;">
                                                     
                   
                     <br />
               
               
                <div style=" padding:20px;" >
                    <h3>
                        <img src='/Images/help.gif' onmouseover="javascript:showObj('HelpDiv');" onmouseout="javascript:hideObj('HelpDiv');" />&nbsp;&nbsp;Select any coaches or subject matter experts you would like to contact.</h3>
                    <br />
                <asp:CheckBoxList ID="CoachesList" runat="server">
                </asp:CheckBoxList>
                         <br />
                         Message:
                         <asp:TextBox ID="Comments" runat="server" Text=""
                             Height="180px" Width="100%" TextMode="MultiLine"></asp:TextBox>
                    
                     <br />
                     </div>
                     </div>
                <div style="width: 100%; text-align: center;">
                     <asp:Button ID="SaveUpdateInformation" runat="server" BackColor="Navy" ForeColor="White"
                        Text="Send  Email" CssClass="ButtonBlue" OnClick="SaveUpdateInformation_Click" />
                </div>
               
               
               </td>
            </tr>
               
           </table>
         
      </ContentTemplate>
      </asp:UpdatePanel>
</asp:Content>
