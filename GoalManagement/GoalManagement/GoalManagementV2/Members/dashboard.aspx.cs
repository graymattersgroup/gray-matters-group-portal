﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Telerik.Reporting.Processing;
using Telerik.Web.UI;

namespace GoalManagementV2.Members
{
    public partial class dashboard : System.Web.UI.Page
    {
        protected CRMAccess crmAccess;

        protected Contact me;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var masterPage = (SiteMaster)Page.Master;//TODO::widesitemaster???
                masterPage.SetMenu(CentralizedData.DashboardName);

                if (Session["ImpersonateUser"] != null)
                {
                    var otherUser = (string) Session["ImpersonateUser"];
                    var masterPage1 = (SiteMaster)Page.Master;//TODO::widesitemaster???
                    masterPage1.SetImpersonation(otherUser);
                }

                var db = new SqlAzureLayer(masterPage.GetConnectionString());
                var result = db.GetDataTable("select content from [dbo].[ManagedContent] where name ='Dashboard Help' ");
                if (result != null && result.Rows.Count > 0)
                {
                    HelpLiteral.Text = result.Rows[0][0].ToString();
                }
                
                //Guid classId = Guid.Empty;
                //if (Session["ClassId"] != null) classId = (Guid)Session["ClassId"];

                //MembershipUser currentUser = Membership.GetUser();
                //Session.Clear();
                //if (currentUser == null) Response.Redirect("../Default.aspx");

                //var myEmail = currentUser.Email;
                //Session["userEmail"] = myEmail;
                //crmAccess = new CRMAccess();
                //Session["crmAccess"] = crmAccess;
                //var me = crmAccess.GetStudent(myEmail);
                //Session["StudentInfo"] = me;

                //var coachingClasses = crmAccess.GetCoachClasses(me.ContactId.Value).ToList();
                //bool isCoach = (coachingClasses.Count != 0);
                //Session["isCoach"] = isCoach;
                //Session["CoachingClasses"] = coachingClasses;

                if (Session["crmAccess"] == null)
                {
                    crmAccess = new CRMAccess();
                    Session["crmAccess"] = crmAccess;
                }
                else
                {
                    crmAccess = (CRMAccess)Session["crmAccess"];
                }

                List<new_class> coachingClasses;

                me = (Contact)Session["StudentInfo"];

                var isCoach = (bool)Session["isCoach"];

                Guid classId = Guid.Empty;
                if (Session["ClassId"] != null) classId = (Guid)Session["ClassId"];

                if (isCoach)
                {
                    if (Session["CoachingClasses"] != null)
                    {
                        coachingClasses = (List<new_class>)Session["CoachingClasses"];
                    }
                    else
                    {
                        coachingClasses = crmAccess.GetCoachClasses(me.ContactId.Value).ToList();
                        Session["CoachingClasses"] = coachingClasses;
                    }
                    var filterClasses = (bool)Session["FilterClasses"] ;
                    foreach (new_class coachingClass in coachingClasses)
                    {
                        var skipClass = (coachingClass.new_ClassComplete.HasValue &&
                                         coachingClass.new_ClassComplete.Value && filterClasses);
                        
                        if( !skipClass) ClassList.Items.Add(new ListItem(coachingClass.new_name, coachingClass.new_classId.ToString()));
                    }

                    if (classId == Guid.Empty) classId = coachingClasses[0].new_classId.Value;
                    else
                    {
                        foreach (ListItem listItem in ClassList.Items)
                        {
                            var id = classId.ToString();
                            if (listItem.Value == id)
                            {
                                listItem.Selected = true;
                            }
                        }

                    }

                }
                else
                {
                    if (Session["MyClasses"] != null)
                    {
                        coachingClasses = (List<new_class>)Session["MyClasses"];
                    }
                    else
                    {
                        coachingClasses = crmAccess.GetStudentClasses(me.ContactId.Value).ToList();
                        Session["MyClasses"] = coachingClasses;
                    }

                    foreach (new_class coachingClass in coachingClasses)
                    {
                        ClassList.Items.Add(new ListItem(coachingClass.new_name, coachingClass.new_classId.ToString()));
                    }

                    classId = coachingClasses[0].new_classId.Value;

                
                }

                GetData(classId);
            }
            else
            {
                me = (Contact)Session["StudentInfo"];
            }

        }
        
        protected void GetData(Guid classId)
        {
            List<ContactS> students;
            List<new_studentgoalsS> selectedGoals;
            List<ContactS> coaches;
            List<new_classupdateperiodS> progressPeriods;
            List<StudentUpdatePeriod> studentProgress;
            List<new_coachingS> coaching;

            crmAccess = (CRMAccess)Session["crmAccess"];


            var sql = new SqlAzureLayer(GetConnectionString());
            var blob = sql.GetDashboardBlob(classId);

            if (blob == null)
            {
                
                // Get all Students
                var studentsI = crmAccess.GetClassStudents(classId);

                var selectedGoalsI = crmAccess.GetClassSelectedGoals(classId);

                // Get all Coaches
                var coachesI = crmAccess.GetClassCoaches(classId);
                // Get all Progress Periods
                var progressPeriodsI = crmAccess.GetClassUpdatePeriods(classId);

                // Get all Student Progress Periods
                var studentProgressI = crmAccess.GetMyDashboardUpdatePeriods(classId);

                // Get all Coaching
                var coachingI = crmAccess.GetCoachFeedbackbyClass(classId);

                var classStatusesI = crmAccess.GetClassStatuses(classId);

                 blob = new DashboardBlob();

                blob.SetStudents(classId,DateTime.Now,studentsI,coachesI,selectedGoalsI,progressPeriodsI,studentProgressI,coachingI, classStatusesI);

                sql.SaveDashboardBlob(blob);

                }

            LastUpdated.Text = blob.lastupdated.ToLocalTime().ToShortTimeString();

                students = blob.students;
                selectedGoals = blob.selectedGoals;
                coaches = blob.coaches;
                progressPeriods = blob.progressPeriods;
                studentProgress = blob.studentProgress;
                coaching = blob.coaching;

         
            // Calculate Statistics

            var dashTable = new DataTable();

            dashTable.Columns.Add("Coach");
            dashTable.Columns.Add("Student");
            dashTable.Columns.Add("Goals");
            var count = 1;
            var totalPeriods = 0.0;
            foreach (new_classupdateperiodS updatePeriod in progressPeriods)
            {
                totalPeriods++;
                dashTable.Columns.Add(string.Format("{0}-{1}",  updatePeriod.new_Start.Value.ToString("MMM dd"), updatePeriod.new_End.Value.ToString("MMM dd")));
                dashTable.Columns.Add(string.Format("C {0}",count++));
            }

            dashTable.Columns.Add("Totals");
            
            RadGrid1.Width = new Unit(400 + 170*totalPeriods);

            var studentCount = 0;
            var coachCount = 0;
            var totalUpdatesPerPeriod = new double[(int)totalPeriods];
            Session["totalPeriods"] = totalPeriods;

            foreach (ContactS coach in coaches)
            {
                if (coach.new_ContactRxRole == null) continue;

                if (!me.Contains("new_classaccess")) continue;

                if (me.new_ClassAccess.Value == 4)
                {
                    if (me.ContactId != coach.ContactId) continue;
                }

                coachCount++;
                int studentCoachCount = 0;
                var totalCoachUpdatesPerPeriod = new double[(int)totalPeriods];

                foreach (ContactS student in students)
                {
                   

                    if (student.new_MyCoachId != null)
                    {
                        if (student.new_MyCoachId == coach.ContactId)
                        {
                            if (student.FirstName == "Amy")
                            {
                                var stophere = true;
                            }

                            studentCoachCount++;
                            studentCount++;
                            var studentRow = dashTable.NewRow();
                            var email = student.EMailAddress1;

                            if (me.new_ClassAccess.Value == 1)
                            {
                                if (me.ContactId != coach.ContactId) email = "";
                            }

                            studentRow[1] = student.FirstName + " " + student.LastName + "|" + email +"|" + student.ContactId;

                            var allGoals = "";
                            foreach (new_studentgoalsS studentgoals in selectedGoals)
                            {
                                if (studentgoals.new_StudentId == student.ContactId)
                                {
                                    allGoals += string.Format("<b>{0}</b> {1}<br/>Payoff - <br/>{2}<br/><br/>", studentgoals.new_name,
                                                              ((studentgoals.new_IsComplete == true) ? "<b>Complete</b>" : ""), StripHtml(studentgoals.new_Benefit, false));
                                }
                            }
                            if (student.IsGoalsEntered == true) studentRow[2] = allGoals;
                            else
                            {
                                studentRow[2] = "NoGoals";
                            }
                          

                            var period = 0;
                            var completed = 0.0;
                            foreach (new_classupdateperiodS updatePeriod in progressPeriods)
                            {
                                foreach (StudentUpdatePeriod studentUpdatePeriod in studentProgress)
                                {
                                    if (updatePeriod.new_classupdateperiodId == studentUpdatePeriod.ClassUpdatePeriodId && student.ContactId == studentUpdatePeriod.StudentId)
                                    {
                                        totalUpdatesPerPeriod[period]++;
                                        totalCoachUpdatesPerPeriod[period]++;
                                        var value = "Progress:";
                                        bool haveAnAnswer = false;
                                        foreach (GoalAnswers theseAnswers in studentUpdatePeriod.PeriodGoalAnswers)
                                        {
                                            if (value != "Progress:") value += "|";

                                            var status = " - None";
                                            switch (theseAnswers.ProgressValue)
                                            {
                                                case 1:
                                                    status = " - None";
                                                    break;
                                                case 2:
                                                    status = " - Some";
                                                    break;
                                                case 3:
                                                    status = " - Significant";
                                                    break;
                                                case 4:
                                                    status = " - Completed";
                                                    break;
                                            }


                                            var feedback = string.Format("{0}|{1}|{2}|{3}|{4}", studentUpdatePeriod.GoalQuestion1, theseAnswers.Answer1,
                                                                         studentUpdatePeriod.GoalQuestion2, theseAnswers.Answer2, theseAnswers.GoalName + status);
                                            switch (theseAnswers.ProgressValue)
                                            {
                                                case 1: value += "0|" + feedback;
                                                    break;
                                                case 2: value += "1|" + feedback;
                                                    break;
                                                case 3: value += "2|" + feedback;
                                                    break;
                                                case 4: value += "3|" + feedback;
                                                    break;
                                                case 9: value += "0|" + feedback;
                                                    break;
                                            }

                                            haveAnAnswer = true;
                                        }

                                        value += "|i|" + studentUpdatePeriod.PeriodImpactQuestion + "|" + studentUpdatePeriod.ImpactAnswer + "|||";

                                        studentRow[2 * period + 3] = value;

                                        if( haveAnAnswer) completed++;
                                    }

                                    studentRow[2 * period + 4] = " ";
                                    foreach (new_coachingS newCoaching in coaching)
                                    {
                                        if (newCoaching.new_ClassUpdatePeriodId == updatePeriod.new_classupdateperiodId &&
                                            newCoaching.new_StudentId == student.ContactId)
                                        {
                                            var grade = (newCoaching.new_UpdateGrade != null) ? newCoaching.new_UpdateGrade.ToString() : "";
                                            if (grade == "")
                                            {
                                                studentRow[2 * period + 4] += string.Format("{0}",
                                                                                          newCoaching.ModifiedOn.
                                                                                              ToString("MMM dd"));
                                            }
                                            else
                                            {
                                                if (studentRow[2 * period + 4].ToString() == " ")
                                                    studentRow[2 * period + 4] = string.Format("Coaching:{0}|{1}|{2}", grade, newCoaching.CreatedOn.ToShortDateString(),
                                                                                              newCoaching.new_Advice);
                                                else
                                                {
                                                    studentRow[2 * period + 4] += string.Format("|{0}|{1}|{2}", grade, newCoaching.CreatedOn.ToShortDateString(), newCoaching.new_Advice);
                                                }
                                            }
                                        }
                                    }
                                }
                                period++;
                            }

                            studentRow[totalUpdatesPerPeriod.Length * 2 + 3] = string.Format(" {0}:{1}&nbsp;&nbsp; {2}% ", completed, totalPeriods, ((completed / totalPeriods) * 100).ToString("0.00"));

                            dashTable.Rows.Add(studentRow);
                        }
                    }
                }

                if (studentCoachCount > 0)
                {
                    var coachRow = dashTable.NewRow();
                    coachRow[0] = coach.FirstName + " " + coach.LastName;

                    var periodCount = 0;
                    foreach (new_classupdateperiodS updatePeriod in progressPeriods)
                    {
                        var percentage = ((totalCoachUpdatesPerPeriod[periodCount] / studentCoachCount) * 100).ToString("0.00");
                        coachRow[3 + periodCount * 2] = string.Format(" {0}:{1}&nbsp;&nbsp; {2}% ", totalCoachUpdatesPerPeriod[periodCount], studentCoachCount, percentage);
                        periodCount++;
                    }
                    dashTable.Rows.Add(coachRow);
                }
            }
            var totalRow = dashTable.NewRow();
            totalRow[0] = string.Format(" {0} Coaches", coachCount);
            totalRow[1] = string.Format(" {0} Students", studentCount);

            var totalPeriodCount = 0;
            var totalAllPeriods = 0.0;
            foreach (new_classupdateperiodS updatePeriod in progressPeriods)
            {
                totalAllPeriods += totalUpdatesPerPeriod[totalPeriodCount];

                var percentage = ((totalUpdatesPerPeriod[totalPeriodCount] / studentCount) * 100).ToString("0.00");
                totalRow[3 + totalPeriodCount * 2] = string.Format(" {0}:{1} {2}% ", totalUpdatesPerPeriod[totalPeriodCount], studentCount, percentage);
                totalPeriodCount++;
            }

            var totalpercentage = (totalAllPeriods / (totalPeriods * studentCount)) * 100;
            totalRow[totalUpdatesPerPeriod.Length * 2 + 3] = string.Format(" {0}:{1} {2}% ", totalAllPeriods, totalPeriods * studentCount, totalpercentage.ToString("0.00"));
            var spacer = dashTable.NewRow();
            dashTable.Rows.Add(spacer);
            dashTable.Rows.Add(totalRow);
            RadGrid1.DataSource = dashTable;
            RadGrid1.Rebind();

        }

        protected void ClassList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var classId = new Guid(ClassList.SelectedValue);
            var me = (Contact)Session["StudentInfo"];

            SetClassIcon(classId, me);

            Session["ClassId"] = classId;

            GetData(classId);


        }

        protected void Dashboard_ItemDataBound(object sender, GridItemEventArgs e)
        {

            if (e.Item is GridDataItem)// to access a row  
            {
                var isCoach = (bool)Session["isCoach"];

                var item = (GridDataItem)e.Item;


                string coachText = item["Coach"].Text;
                string studentText = item["Student"].Text;

                if (coachText != "&nbsp;" && studentText != "&nbsp;")
                {
                    item.CssClass = "BottomTotals";
                }
                else
                    if (coachText != "&nbsp;")
                    {
                        item.CssClass = "Coach";
                    }
                    else
                    {
                      
                        var email = item["Student"].Text.Split('|');

                        if (email.Length == 3)
                        {
                            var newEmail = email[0];

                            if (!string.IsNullOrEmpty(email[1])) newEmail = string.Format("<img src='/Images/report-icon.png' onclick='javascript:StudentMenu(\"{0}\",\"{1}\",\"{2}\");' /> {1}", email[1], email[0], email[2]);
                            var emailLit = new Literal { Text = newEmail };
                            item["Student"].Controls.Add(emailLit);
                        }

                        var goalImage = "Goals";
                        if( item["Goals"].Text == "NoGoals")
                        {
                            goalImage = "NoGoals";
                        }

                        var image = (isCoach) ? string.Format("<img onmouseover='javascript:InitToolTip(\"{0}\", \"CoachMessage\",450,10,10);' onmouseout='javascript:HideToolTip();' src='/Images/{1}.gif'/>", item["Goals"].Text, goalImage) :
                           string.Format("<img src='/Images/{0}.gif'/>", goalImage);

                        var thisLiteral = new Literal { Text = image };
                        item["Goals"].Controls.Add(thisLiteral);
                        item["Goals"].Style.Add("text-align", "center");

                        foreach (TableCell c in item.Cells)
                        {

                            if (c.Text.StartsWith("Progress:"))
                            {
                                var goalProgress = c.Text.Replace("Progress:", "").Split('|');

                                string images = "";
                                for (int i = 0; i < goalProgress.Length / 6; i++)
                                {
                                    var studentProgress = "";
                                    if (goalProgress[i * 6].ToString() == "i")
                                    {
                                        studentProgress = string.Format("<b>{0}</b><br/></br>{1}", StripHtml(goalProgress[i * 6 + 1], false), StripHtml(goalProgress[i * 6 + 2], false));
                                    }
                                    else
                                    {
                                        studentProgress = string.Format("<b>{4}</b><br/></br><b>{0}</b> {1}<br/></br><b>{2}</b> {3}", StripHtml(goalProgress[i * 6 + 1], false),
                                      StripHtml(goalProgress[i * 6 + 2], false), StripHtml(goalProgress[i * 6 + 3], false), StripHtml(goalProgress[i * 6 + 4], false), goalProgress[i * 6 + 5]);

                                    }

                                    if (isCoach)
                                    {
                                        switch (goalProgress[i * 6])
                                        {
                                            case "0":
                                                images +=
                                                    string.Format(
                                                        "<img onmouseover='javascript:InitToolTip(\"{0}\", \"CoachMessage\", 550,10,10);' onmouseout='javascript:HideToolTip();' src='/Images/red15x15.gif'/>&nbsp;",
                                                        studentProgress);
                                                break;

                                            case "1":

                                                images +=
                                                    string.Format(
                                                        "<img onmouseover='javascript:InitToolTip(\"{0}\", \"CoachMessage\", 550,10,10);' onmouseout='javascript:HideToolTip();' src='/Images/yellow_15x15.gif'/>&nbsp;",
                                                        studentProgress);
                                                break;

                                            case "2":

                                                images +=
                                                    string.Format(
                                                        "<img onmouseover='javascript:InitToolTip(\"{0}\", \"CoachMessage\", 550,10,10);' onmouseout='javascript:HideToolTip();' src='/Images/green15x15.gif'/>&nbsp;",
                                                        studentProgress);
                                                break;

                                            case "3":

                                                images +=
                                                    string.Format(
                                                        "<img onmouseover='javascript:InitToolTip(\"{0}\", \"CoachMessage\", 550,10,10);' onmouseout='javascript:HideToolTip();' src='/Images/blue15x15.gif'/>&nbsp;",
                                                        studentProgress);
                                                break;

                                            case "i":

                                                images +=
                                                    string.Format(
                                                        "<img onmouseover='javascript:InitToolTip(\"{0}\", \"CoachMessage\", 550,10,10);' onmouseout='javascript:HideToolTip();' src='/Images/ImpactIcon.gif'/>&nbsp;",
                                                        studentProgress);
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        switch (goalProgress[i * 6])
                                        {
                                            case "0":
                                                images += string.Format("<img src='/Images/red15x15.gif'/>&nbsp;");
                                                break;

                                            case "1":
                                                images += string.Format("<img  src='/Images/yellow_15x15.gif'/>&nbsp;");
                                                break;

                                            case "2":

                                                images += string.Format("<img  src='/Images/green15x15.gif'/>&nbsp;");
                                                break;

                                            case "3":

                                                images += string.Format("<img  src='/Images/blue15x15.gif'/>&nbsp;");
                                                break;
                                            case "9":
                                                images += string.Format("<img  src='/Images/gray15x15.gif'/>&nbsp;");
                                                break;

                                            default:
                                                break;
                                        }
                                    }

                                }

                                var aLiteral = new Literal { Text = images };
                                c.Controls.Add(aLiteral);
                            }

                            if (c.Text.StartsWith("Coaching:"))
                            {

                                var goalProgress = c.Text.Replace("Coaching:", "").Split('|');
                                string images = "";

                                for (int i = 0; i < goalProgress.Length / 3; i++)
                                {
                                    if (isCoach)
                                    {

                                        var advice = string.Format("<b>{0}</b><br/>{1}", goalProgress[i * 3 + 1], StripHtml(goalProgress[i * 3 + 2], false));
                                        switch (goalProgress[i * 3])
                                        {
                                            case "1":
                                                images +=
                                                    string.Format(
                                                        "<img onmouseover='javascript:InitToolTip(\"{0}\", \"CoachMessage\", 500,10,10);' onmouseout='javascript:HideToolTip();' src='/Images/yellow_15x15.gif'/>&nbsp;",
                                                        advice);
                                                break;

                                            case "2":
                                                images +=
                                                    string.Format(
                                                        "<img onmouseover='javascript:InitToolTip(\"{0}\", \"CoachMessage\", 500,10,10);' onmouseout='javascript:HideToolTip();' src='/Images/green15x15.gif'/>&nbsp;",
                                                        advice);
                                                break;

                                            case "3":
                                                images +=
                                                    string.Format(
                                                        "<img onmouseover='javascript:InitToolTip(\"{0}\", \"CoachMessage\", 500,10,10);' onmouseout='javascript:HideToolTip();' src='/Images/blue15x15.gif'/>&nbsp;",
                                                        advice);
                                                break;

                                            case "9":
                                                images +=
                                                    string.Format(
                                                        "<img onmouseover='javascript:InitToolTip(\"{0}\", \"CoachMessage\", 500,10,10);' onmouseout='javascript:HideToolTip();' src='/Images/coaching.gif'/>&nbsp;",
                                                        advice);
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        switch (goalProgress[i * 3])
                                        {
                                            case "1": images += string.Format("<img  src='/Images/yellow_15x15.gif'/>");
                                                break;

                                            case "2": images += string.Format("<img  src='/Images/green15x15.gif'/>");
                                                break;

                                            case "3": images += string.Format("<img  src='/Images/blue15x15.gif'/>");
                                                break;

                                            case "9": images += string.Format("<img  src='/Images/coaching.gif'/>");
                                                break;

                                            default:
                                                break;
                                        }
                                    }
                                }
                                var aLiteral = new Literal { Text = images };
                                c.Controls.Add(aLiteral);
                                c.Style.Add("text-align", "center");
                            }
                            // colCount++;

                        }


                    }

                item["Totals"].CssClass = "Totals";

                //}
                //catch (Exception ex)
                //{



                //}




            }
           

        }


        public static string StripHtml(string html, bool allowHarmlessTags)
        {
            if (string.IsNullOrEmpty(html)) return string.Empty;

            html = html.Replace("&nbsp;", " ");

            var results = "";
            if (allowHarmlessTags) results = System.Text.RegularExpressions.Regex.Replace(html, "", string.Empty);
            else
                results = System.Text.RegularExpressions.Regex.Replace(html, "<[^>]*>", string.Empty);

            results = results.Replace('"', '`');
            results = results.Replace("'", "`");

            results = results.Replace("\r", "<br/>");
            results = results.Replace('\n', ' ');
            // results = "<pre>" + results + "</pre>";
            return results;

        }


        private void SetClassIcon(Guid classId, Contact myContact)
        {
            crmAccess = (CRMAccess)Session["crmAccess"];
            var logoImage = Master.FindControl("RadBinaryImage1") as RadBinaryImage;

            try
            {
                IQueryable<Annotation> logo;

                if (myContact.ParentCustomerId != null)
                {
                    try
                    {
                        logo = crmAccess.GetClassAnnotation(myContact.ParentCustomerId.Id);
                        if (logo.FirstOrDefault() == null) logo = crmAccess.GetClassAnnotation(classId);
                    }
                    catch
                    {
                        logo = crmAccess.GetClassAnnotation(classId);
                    }


                }
                else
                {
                    logo = crmAccess.GetClassAnnotation(classId);
                }

                if (logo.FirstOrDefault() != null)
                {
                    logoImage.Visible = true;
                    logoImage.DataValue = Convert.FromBase64String(logo.FirstOrDefault().DocumentBody);
                    Session["Logo"] = logoImage.DataValue;
                }
                else
                {
                    Session.Remove("Logo");
                    logoImage.Visible = false;
                }
            }
            catch
            {
                Session.Remove("Logo");
                logoImage.Visible = false;
            }
        }


    

        public void UpdateDashboardBlob(DashboardBlob blob)
        {
            var sql = new SqlAzureLayer(GetConnectionString());


            var createBlob =
                string.Format(
                    @"Update dbo.MainCache
                      set updated = {0}
                      ,set dashboardblob = '{1}'
where classid = {2}
                     ",
                    
                    sql.FormatDateTime(blob.lastupdated),
                    blob,
                    sql.FormatGuid(blob.classid)
                    );
            sql.SqlExecuteNonQuery(createBlob);

        }

      


        private string GetConnectionString()
        {
            var connectionString2 = ConfigurationManager.ConnectionStrings["Production"].ConnectionString;

            return connectionString2;
        }



        protected void Dashboard_columnCreated(object sender, GridColumnCreatedEventArgs e)
        {
            e.Column.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            
        }
        

        protected void ResourcesViewed_Click(object sender, EventArgs e)
        {
            crmAccess = (CRMAccess)Session["crmAccess"];

            var className = ClassList.SelectedItem.Text;
            var classId = new Guid(ClassList.SelectedItem.Value);

            var result =ReportHelper.GetTelerikTrackingReport(classId, className, crmAccess);

            var filename = string.Format("ResourcesViewedReport{0}.pdf",className);

            ExportToPDF(result, filename);

        }
        

        void ExportToPDF(RenderingResult result, string fileName)
        {
            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            Response.AddHeader("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                             fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
        }
    }
    
   

    
}