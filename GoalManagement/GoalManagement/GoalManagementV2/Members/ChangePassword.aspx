﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="ChangePassword.aspx.cs" Inherits="GoalManagementV2.Members.ChangePassword" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <%--<section>
        <div class="title">
            <h1>Account Info</h1>
        </div>
        <div class="content">
            <table class="fixedRow">
                <tr>
                    <td>Name</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Phone Number</td>
                    <td></td>
                </tr>
            </table>
        </div>
    </section>--%>
    <section>
        <div class="title">
            <h1>Change Password</h1>
        </div>
        <div class="content">
            <p>
                New passwords are required to be a minimum of <%= Membership.MinRequiredPasswordLength %> characters in length.
            </p>
            <asp:ChangePassword ID="ChangeUserPassword" runat="server" CancelDestinationPageUrl="~/" EnableViewState="false"
                SuccessPageUrl="ChangePasswordSuccess.aspx" RenderOuterTable="False">
                <ChangePasswordTemplate>
                    <span class="failureNotification">
                        <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                    </span>
                    <asp:ValidationSummary ID="ChangeUserPasswordValidationSummary" runat="server" CssClass="failureNotification"
                        ValidationGroup="ChangeUserPasswordValidationGroup" />
                    <table class="fixedRow">
                        <tr>
                            <th>Old Password</th>
                            <td>
                                <asp:TextBox ID="CurrentPassword" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                    CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Old Password is required."
                                    ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <th>New Password</th>
                            <td>
                                <asp:TextBox ID="NewPassword" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                    CssClass="failureNotification" ErrorMessage="New Password is required." ToolTip="New Password is required."
                                    ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <th>Confirm New</th>
                            <td>
                                <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                    CssClass="failureNotification" Display="Dynamic" ErrorMessage="Confirm New Password is required."
                                    ToolTip="Confirm New Password is required." ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword"
                                    CssClass="failureNotification" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry."
                                    ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:CompareValidator></td>
                        </tr>
                    </table>

                </ChangePasswordTemplate>
            </asp:ChangePassword>
        </div>
        <div class="edit-buttons">
            <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword" Text="Change Password"
                        ValidationGroup="ChangeUserPasswordValidationGroup" />
        </div>
    </section>
</asp:Content>
