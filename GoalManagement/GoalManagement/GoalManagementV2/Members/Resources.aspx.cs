﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Telerik.Web.UI;

namespace GoalManagementV2.Members
{
    public partial class Resources : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var classId = (Guid)Session["ClassId"];

                var masterPage = (SiteMaster)Page.Master;
                masterPage.SetMenu(CentralizedData.ResourcesName);

                if (Session["ImpersonateUser"] != null)
                {
                    var otherUser = (string)Session["ImpersonateUser"];
                    var masterPage1 = (SiteMaster)Page.Master;
                    masterPage1.SetImpersonation(otherUser);
                }

                var db = new SqlAzureLayer(masterPage.GetConnectionString());

                // Find custom Resources Page if one exists
                var result = db.GetHelp("Resources and Tools", classId);

                if( result == " ")
                {
                    // Get default Resources Page is no custom Page exists.
                    var defaultValue = db.GetDataTable("select content from [dbo].[ManagedContent] where classid is null and  name ='Resources and Tools' ");
                    if (defaultValue != null && defaultValue.Rows.Count != 0)
                    {
                        result = defaultValue.Rows[0][0].ToString();
                    }
                }

                AllContent.Text = result;
            }
            
        }
    }
}