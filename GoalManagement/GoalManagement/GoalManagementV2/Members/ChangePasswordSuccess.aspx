﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="ChangePasswordSuccess.aspx.cs" Inherits="GoalManagementV2.Members.ChangePasswordSuccess" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
       <section class="settings">
        <div class="title">
            <h1>Change Password</h1>
        </div>
        <div class="settings-body">
            <div>
                <p>Your password has been changed successfully.</p>
            </div>
        </div>
    </section>
</asp:Content>
