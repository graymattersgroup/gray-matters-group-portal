﻿using System;
using System.Data;
using System.Web;
using Telerik.Reporting.Processing;
using TelerikReporting;

namespace MapSearch.Members
{
    public partial class PrintReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var AdditionalInfo = "Student Name|";
                var printTable = new DataTable();
                printTable.Columns.Add("GoalName");
                printTable.Columns.Add("Activity");
                printTable.Columns.Add("SubActivity");
                var newRow = printTable.NewRow();
                newRow.ItemArray = new[] { "My First Goal", "Activity 1", "" };
                printTable.Rows.Add(newRow);

                var newRow2 = printTable.NewRow();
                newRow2.ItemArray = new[] { "", "Activity 2", "" };
                printTable.Rows.Add(newRow2);

                var newRow3 = printTable.NewRow();
                newRow3.ItemArray = new[] { "", "Activity 3", "" };
                printTable.Rows.Add(newRow3);

                var newRowf = printTable.NewRow();
                newRowf.ItemArray = new[] { "", "" };
                printTable.Rows.Add(newRowf);

                var newRow4 = printTable.NewRow();
                newRow4.ItemArray = new[] { "My Second Goal", "Activity 1", "" };
                printTable.Rows.Add(newRow4);

                var newRow5 = printTable.NewRow();
                newRow5.ItemArray = new[] { "", "Activity 2", "" };
                printTable.Rows.Add(newRow5);
                

                var report = "1";
                var filename = "MyGoals";

                try
                {
                  //  printTable = (DataTable)Session["ReportTable"];
                  //  AdditionalInfo = Session["ReportInfo"].ToString();
                  //  AdditionalInfo.Split('|');
                 //   filename = Session["ReportFileName"].ToString();
                  //  report = Session["ReportType"].ToString();

                    switch (report)
                    {
                        case "1":
                            var objectDataSource = new Telerik.Reporting.ObjectDataSource { DataSource = printTable };
                            var goals = new MyGoals(AdditionalInfo.Split('|')[0]) { DataSource = objectDataSource };
                            ExportToPDF(goals, filename);
                            break;
                    }
                }
                catch
                {
                    Response.End();
                }

            }
        }

        void ExportToPDF(Telerik.Reporting.Report reportToExport, string filename)
        {
            var reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("PDF", reportToExport, null);

            string fileName = result.DocumentName + ".pdf";

            if (filename != "none") fileName = filename;

            Response.Clear();
            Response.ContentType = result.MimeType;
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.Expires = -1;
            Response.Buffer = true;

            //Response.AddHeader("Content-Disposition",
            //                   string.Format("{0};FileName=\"{1}\"",
            //                                 "attachment",
            //                                 fileName));

            Response.BinaryWrite(result.DocumentBytes);
            Response.End();
        }    

    }
}