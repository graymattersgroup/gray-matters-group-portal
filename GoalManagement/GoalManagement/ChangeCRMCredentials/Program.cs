﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using GoalManagement.Support;


namespace ChangeCRMCredentials
{
    class Program
    {
        static void Main(string[] args)
        {
            var username = "";
            var password = "";

            if( args.Length == 2)
            {
                username = args[0];
                password = args[1];
            }
            else
            {
                password = args[0];
            }

            var connectionString = ConfigurationManager.ConnectionStrings["Production"].ConnectionString;
            var db = new SqlAzureLayer(connectionString);

            db.SetCRMAccount(username, password);

            Console.WriteLine("Username set to {0}, Password set to {1}", username, password);
        }
    }
}
