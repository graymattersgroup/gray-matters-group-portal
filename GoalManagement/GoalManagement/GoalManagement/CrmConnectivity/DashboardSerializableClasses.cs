﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace demostuff
{
    /// <summary>
    /// Top Level Serializable Container 
    /// All classes contained must also be serializable 
    /// </summary>
    [Serializable]
    public class DashboardBlob
    {
        public Guid AccountId;
        public DateTime LastUpdated;
        public List<ContactS> students = new List<ContactS>();
    }

    /// <summary>
    /// Nested Serializable class
    /// </summary>
    [Serializable]
    public class ContactS
    {
        public Guid ContactId;
        public string FirstName;
        public string LastName;
        public string EMailAddress1;
    }


    static public class DashboardCaching
    {
/// <summary>
/// Create Top level container and call methods to fill all nested containers
/// </summary>
/// <param name="accountId"></param>
public static DashboardBlob FillDashboardBlob(Guid accountId)
{
    var blob = new DashboardBlob
        {
            AccountId = accountId,
            LastUpdated = DateTime.Now,
            students = GetSerializedContactsByAccount(accountId)
            //pull all other related data and nested data.
        };

    return blob;
}

/// <summary>
/// Query that pulls data from a CRM context and stores it in a serializable class
/// </summary>
/// <param name="accountId"></param>
/// <returns></returns>
public static List<ContactS> GetSerializedContactsByAccount(Guid accountId)
{
    var serializedContacts = 
        (from c in DemoContext.ContactSet
        join students in DemoContext.new_account_studentSet on c.ContactId equals students.contactid
        where students.new_accountid == accountId
        orderby c.LastName, c.FirstName
        select new ContactS
        {
            ContactId = c.ContactId.Value,
            FirstName = c.FirstName,
            LastName = c.LastName,
            EMailAddress1 = c.EMailAddress1
        }).ToList();

    return serializedContacts;
}

/// <summary>
/// Create initial blob entry
/// </summary>
/// <param name="blob"></param>
/// <param name="connectionString"></param>
public static void SaveDashboardBlob(DashboardBlob blob, string connectionString)
{
    var bytes = SerializeAnObject(blob);
    var connection = new SqlConnection(connectionString);
    connection.Open();
    var sqlCmd = new SqlCommand("INSERT INTO dbo.MainCache(accountid,updated,dashboardxml) VALUES (@id,@updated,@xml)", connection);
    sqlCmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier);
    sqlCmd.Parameters["@id"].Value = blob.AccountId;
    sqlCmd.Parameters.Add("@updated", SqlDbType.DateTime);
    sqlCmd.Parameters["@updated"].Value = blob.LastUpdated;
    sqlCmd.Parameters.Add("@xml", SqlDbType.Xml, Int32.MaxValue);
    sqlCmd.Parameters["@xml"].Value = bytes;
    sqlCmd.ExecuteNonQuery();
    connection.Close();
}

/// <summary>
/// Update that writes the blob back to SQL
/// </summary>
/// <param name="blob"></param>
/// <param name="connectionString"></param>
/// <param name="isCreate"></param>
public static void SaveUpdateDashboardBlob(DashboardBlob blob, string connectionString, bool isCreate)
{
    var bytes = SerializeAnObject(blob);
    var connection = new SqlConnection(connectionString);
    SqlCommand sqlCmd;
    connection.Open();
    if( isCreate) 
        sqlCmd = new SqlCommand("INSERT INTO dbo.MainCache(accountid,updated,dashboardxml) VALUES (@id,@updated,@xml)", connection);
    else
        sqlCmd = new SqlCommand("UPDATE dbo.MainCache Set updated=@updated, dashboardxml=@xml where accountid=@id", connection);
    sqlCmd.Parameters.Add("@id", SqlDbType.UniqueIdentifier);
    sqlCmd.Parameters["@id"].Value = blob.AccountId;
    sqlCmd.Parameters.Add("@updated", SqlDbType.DateTime);
    sqlCmd.Parameters["@updated"].Value = blob.LastUpdated;
    sqlCmd.Parameters.Add("@xml", SqlDbType.Xml, Int32.MaxValue);
    sqlCmd.Parameters["@xml"].Value = bytes;
    sqlCmd.ExecuteNonQuery();
    connection.Close();
}

public static string SerializeAnObject(object AnObject)
{
    var Xml_Serializer = new XmlSerializer(AnObject.GetType());
    var Writer = new StringWriter();
    Xml_Serializer.Serialize(Writer, AnObject);
    return Writer.ToString();
}



/// <summary>
/// Read the blob from local SQL database, deserialize it and cast it back to the container class.
/// </summary>
/// <param name="accountId"></param>
/// <param name="connectionString"></param>
/// <returns></returns>
public static DashboardBlob GetDashboardBlob(Guid accountId, string connectionString)
{
    var connection = new SqlConnection(connectionString);
    connection.Open();
    var adapter = new SqlDataAdapter(string.Format("select dashboardxml from dbo.MainCache where accountid='{0}' ", accountId), connection);
    var ds = new DataSet();
    adapter.Fill(ds);
    DataTable table = ds.Tables[0];
    connection.Close();

    if (table.Rows.Count != 0)
    {
        var data = (DashboardBlob)DeSerializeAnObject(table.Rows[0][0].ToString(), typeof(DashboardBlob));
        return data;
    }
    return null;
}

public static Object DeSerializeAnObject(string xmlOfAnObject, Type objectType)
{
    var strReader = new StringReader(xmlOfAnObject);
    var xmlSerializer = new XmlSerializer(objectType);
    var xmlReader = new XmlTextReader(strReader);
    try
    {
        Object anObject = xmlSerializer.Deserialize(xmlReader);
        return anObject;
    }
    finally
    {
        xmlReader.Close();
        strReader.Close();
    }
}


    }


}


