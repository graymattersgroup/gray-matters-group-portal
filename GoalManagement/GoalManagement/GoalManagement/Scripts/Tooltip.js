﻿//***************************************************************************
// Tool Tip Functions  ( IE and FireFox compatible )
//***************************************************************************
// Globals

var myToolTip;	  // Single ToolTip Info Object to reduce number of globals

// this calls PositionToolTip(e) whenever the mouse moves.
document.onmousemove=PositionToolTip; 

//***************************************************************************


/// InitToolTip(myMessage, messageClass, messageWidth, xOffset, yOffset) 
/// Initializes Tool Tip
///
/// arg: myMessage - text to display
/// arg: messageClass - CSS class to control tool tip appearance.
/// optional arg: width - width of text message
/// optional arg: xOffset - 
/// optional arg: yOffset - 
///
/// Note: Called during mouseover event
///
function InitToolTip(myMessage, messageClass, messageWidth, xOffset, yOffset)
{
	if ( !myToolTip ) 
	{
		myToolTip = new Object();
	}
	
	myToolTip.tipDiv = document.getElementById("dhtmltooltip");	// Name of DIV used to display ToolTip	
	
	myToolTip.tipDiv.className	= messageClass;
	myToolTip.tipDiv.innerHTML	= myMessage;
	myToolTip.enableTip = true;
	
	// Set the following values if the arguments exist
	if ( messageWidth )	
	{
		myToolTip.tipDiv.style.width= messageWidth+"px"; // style width over rides class
	}
	
	myToolTip.xOffset = ( xOffset ?  xOffset : -180 ); // default x offset
	myToolTip.yOffset = ( yOffset ?  yOffset : 20 ); // default y offset
		
	return false;
}

/// HideToolTip() 
///
/// Note: Called during mouseout event
///
function HideToolTip()
{
	myToolTip.tipDiv.style.visibility="hidden";
	myToolTip.enableTip=false;
}


/// PositionToolTip(e) - Sets the position of the Tool Tip
///
/// Note: Called during mousemove event
///
function PositionToolTip(e)
{
	if (myToolTip)
	{	
		if (myToolTip.enableTip)
		{			
			var isMozzilla = document.getElementById && !document.all; // For FireFox Compatibility.
			
			// Find cursor position.
			var curX = (isMozzilla)? e.pageX : event.clientX + document.documentElement.scrollLeft; 
			var curY = (isMozzilla)? e.pageY : event.clientY + document.documentElement.scrollTop;
			
			var edgeLeft = ( myToolTip.xOffset < 0 )? myToolTip.xOffset*(-1) : -1000;
			var edgeRight  = ((isMozzilla)? window.innerWidth - e.clientX - 20 : document.documentElement.clientWidth  - event.clientX) - myToolTip.xOffset;
			var edgeBottom = ((isMozzilla)? window.innerWidth - e.clientY - 20 : document.documentElement.clientHeight - event.clientY) - myToolTip.yOffset;
			
			if ( edgeRight < myToolTip.tipDiv.offsetWidth ) // Horisontal Bounds Checking
			{
				//move the horizontal position of the menu to the left by it's width
				myToolTip.tipDiv.style.left = ((isMozzilla)? window.pageXOffset + e.clientX : document.documentElement.scrollLeft + event.clientX) - myToolTip.tipDiv.offsetWidth + "px";
			}
			else if ( curX < edgeLeft )
			{
				myToolTip.tipDiv.style.left = "5px"; // minimum left margin
			}
			else //position div relative to mouse X
			{				
				myToolTip.tipDiv.style.left = curX + myToolTip.xOffset + "px";
			}

			
			if (edgeBottom < myToolTip.tipDiv.offsetHeight) // Vertical Bounds Checking
			{
				myToolTip.tipDiv.style.top = ((isMozzilla)? window.pageYOffset + e.clientY :document.documentElement.scrollTop + event.clientY) - myToolTip.tipDiv.offsetHeight - myToolTip.yOffset + "px";
			}
			else //position div relative to mouse Y
			{
				myToolTip.tipDiv.style.top = curY + myToolTip.yOffset+"px";
			}
			
			myToolTip.tipDiv.style.visibility="visible";			
		}
	}
}