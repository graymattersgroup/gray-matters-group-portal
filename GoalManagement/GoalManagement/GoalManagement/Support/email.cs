using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Net.Mail;
using System.Web.Security;
using System.Web.Services.Protocols;

namespace GoalManagement.Support
{
    public static class EmailNotifications
    {
        // The only reason these settings are hard coded is to make sure that an error email message can be sent in the middle of an exception.
        // This is also used for sending email outside of the MS CRM system.
        //
        // Emails sent through the MS CRM system are created as Email Activities and sent on behalf of David Ryan
        //
        //public static string smtpHost = "smtp.powweb.com";
        //public static int smtpPort = 587;
        //public static string fromAddress = "errors@onecrmpro.com";
        //public static string toAddress = "graymatterserrors@onecrmpro.com";
        //public static string userName ="errors@onecrmpro.com" ;
        //public static string passWord ="grisgris";
        public static string domainName = null;


        public static string smtpHost = "dedrelay.secureserver.net";
        public static int smtpPort = 25;
        public static string fromAddress = "errors@onecrmpro.com";
        public static string toAddress = "graymatterserrors@onecrmpro.com";
        public static string userName = "rxsupport@graymattersgroup.com";
        public static string passWord = "rhino33";

        #region NormalNotifications
        /// <summary>
        /// Sends Email with and without attachments
        /// </summary>
        /// <param name="toemailaddress"></param>
        /// <param name="fromemailaddress"></param>
        /// <param name="subject"></param>
        /// <param name="text"></param>
        /// <param name="pathnames"></param>
        /// <param name="isHTML"></param>
        /// <param name="isHighPriority"></param>
        public static void sendEmailAll(string toemailaddress, string fromemailaddress, string subject, string text, string[] pathnames, bool isHTML, bool isHighPriority)
        {
            try
            {

                if (!String.IsNullOrEmpty(smtpHost))
                {
                    if (string.IsNullOrEmpty(fromemailaddress))
                    {
                        fromemailaddress = fromAddress;
                    }

                    string singleToAddress = toemailaddress;

                    if (toemailaddress.IndexOf(';') != -1)
                    {
                        var toaddresses = toemailaddress.Split(';');
                        singleToAddress = toaddresses[0];
                    }

                    var mail = new MailMessage(fromemailaddress, singleToAddress, subject, text) {IsBodyHtml = isHTML};

                    if (toemailaddress.IndexOf(';') != -1)
                    {
                        var toList = mail.To;

                        var toaddresses = toemailaddress.Split(';');

                        foreach (string address in toaddresses)
                        {
                            if(!string.IsNullOrEmpty(address))
                            if (address != toaddresses[0])
                            {
                                toList.Add(new MailAddress(address));
                            }
                        }
                    }

                  
                    if (isHighPriority)
                    {
                        mail.Priority = MailPriority.High;
                    }

                    if (pathnames != null)
                    {
                        foreach (string pathName in pathnames)
                        {
                            if (File.Exists(pathName))
                            {
                                var attachment = new Attachment(pathName);
                                mail.Attachments.Add(attachment);
                            }
                        }
                    }

                    var smtp = new SmtpClient(smtpHost, smtpPort)
                                   {
                                       EnableSsl = false,
                                       DeliveryMethod = SmtpDeliveryMethod.Network,
                                       Credentials = new NetworkCredential(userName, passWord)
                                   };

                    smtp.Send(mail);
                }
                else
                {
                    Console.WriteLine("Email Send Failed! no smtpHost specified");                   
                }
            }
            catch (Exception )
            {
                
            }
        }

        public static void sendEmailAllCC(List<string> ccemailaddress, string toemailaddress, string fromemailaddress, string subject, string text, string[] pathnames, bool isHTML, bool isHighPriority, bool onerepresentativeEmailSent)
        {
            try
            {

                if (!String.IsNullOrEmpty(smtpHost))
                {
                    if (string.IsNullOrEmpty(fromemailaddress))
                    {
                        fromemailaddress = fromAddress;
                    }

                    string singleToAddress = toemailaddress;

                    if (toemailaddress.IndexOf(';') != -1)
                    {
                        var toaddresses = toemailaddress.Split(';');
                        singleToAddress = toaddresses[0];
                    }

                    var mail = new MailMessage(fromemailaddress, singleToAddress, subject, text) { IsBodyHtml = isHTML };

                    if (toemailaddress.IndexOf(';') != -1)
                    {
                        var toList = mail.To;

                        var toaddresses = toemailaddress.Split(';');

                        foreach (string address in toaddresses)
                        {
                            if (!string.IsNullOrEmpty(address))
                                if (address != toaddresses[0])
                                {
                                    toList.Add(new MailAddress(address));
                                }
                        }
                    }

                    if (ccemailaddress.Count > 0 && !onerepresentativeEmailSent)
                    {
                        var ccList = mail.CC;

                        foreach (string address in ccemailaddress)
                        {
                            ccList.Add(new MailAddress(address));
                        }
                    }

                  
                    if (isHighPriority)
                    {
                        mail.Priority = MailPriority.High;
                    }

                    if (pathnames != null)
                    {
                        foreach (string pathName in pathnames)
                        {
                            if (File.Exists(pathName))
                            {
                                var attachment = new Attachment(pathName);
                                mail.Attachments.Add(attachment);
                            }
                        }
                    }

                    var smtp = new SmtpClient(smtpHost, smtpPort)
                    {
                        EnableSsl = false,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Credentials = new NetworkCredential(userName, passWord)
                    };

                    smtp.Send(mail);
                }
                else
                {
                    Console.WriteLine("Email Send Failed! no smtpHost specified");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     


        public static void sendEmailAll(string toemailaddress, string fromemailaddress, string subject, string text, byte[] attachment, string filename, bool isHTML, bool isHighPriority)
        {
            try
            {

                if (!String.IsNullOrEmpty(smtpHost))
                {
                    if (string.IsNullOrEmpty(fromemailaddress))
                    {
                        fromemailaddress = fromAddress;
                    }

                    string singleToAddress = toemailaddress;
                    if (toemailaddress.IndexOf(';') == -1)
                    {
                        var toaddresses = toemailaddress.Split(';');
                        singleToAddress = toaddresses[0];
                    }

                    var mail = new MailMessage(fromemailaddress, singleToAddress, subject, text) { IsBodyHtml = isHTML };

                    if (toemailaddress.IndexOf(';') != -1)
                    {
                        var toList = mail.To;

                        var toaddresses = toemailaddress.Split(';');

                        foreach (string address in toaddresses)
                        {
                            if( !string.IsNullOrEmpty(address))
                            if (address != toaddresses[0])
                            {
                                toList.Add(new MailAddress(address));
                            }
                        }
                    }

                  
                    if (isHighPriority)
                    {
                        mail.Priority = MailPriority.High;
                    }

                    if (attachment != null)
                    {

                        // add the attachment from a stream
                        var memStream = new MemoryStream(attachment);

                        var streamWriter = new StreamWriter(memStream);
                        streamWriter.Flush();

                        // this is quite important
                        memStream.Position = 0;
                       
                        var thisAttachment = new Attachment(memStream, "application/pdf");
                        thisAttachment.ContentDisposition.FileName = filename + ".pdf";

                        mail.Attachments.Add(thisAttachment);
                    }

                    var smtp = new SmtpClient(smtpHost, smtpPort)
                                   {
                                       EnableSsl = false,
                                       DeliveryMethod = SmtpDeliveryMethod.Network
                                   }; // (smtpPort != null) ? new SmtpClient(smtpHost, smtpPort) : new SmtpClient(smtpHost);

                    if (userName != null && passWord != null)
                    {
                        smtp.Credentials = new NetworkCredential(userName, passWord);
                    }

                    smtp.Send(mail);
                }
                else
                {
                    Console.WriteLine("Email Send Failed! no smtpHost specified");
                }
            }
            catch (Exception ex)
            {
                SendExceptionMessage("Gray Matters Error ", ex.Message, ex);
            }
        }



        public static void sendEmailAll(string toemailaddress, string ccemailaddress, string fromemailaddress, string subject, string text, string[] pathnames, bool isHTML, bool isHighPriority)
        {
            try
            {
               
                if (!String.IsNullOrEmpty(smtpHost))
                {
                    if (string.IsNullOrEmpty(fromemailaddress))
                    {
                        fromemailaddress = fromAddress;
                    }


                    string singleToAddress = toemailaddress;
                    if (toemailaddress.IndexOf(';') != -1)
                    {
                        var toaddresses = toemailaddress.Split(';');
                        singleToAddress = toaddresses[0];
                    }

                    var mail = new MailMessage(fromemailaddress, singleToAddress, subject, text) {IsBodyHtml = isHTML};

                    if (toemailaddress.IndexOf(';') != -1)
                    {
                        var toList = mail.To;

                        var toaddresses = toemailaddress.Split(';');
                       
                        foreach (string address in toaddresses)
                        {
                            if (address != toaddresses[0])
                            {
                                toList.Add(new MailAddress(address));
                            }
                        }
                    }


                    if (!string.IsNullOrEmpty(ccemailaddress.Trim()))
                    {
                        var ccList = mail.CC;
                        var ccAddresses = ccemailaddress.Split(';');

                        foreach (string address in ccAddresses)
                        {
                            ccList.Add(new MailAddress(address));

                        }
                    }

                  
                    if (isHighPriority)
                    {
                        mail.Priority = MailPriority.High;
                    }

                    if (pathnames != null)
                    {
                        foreach (string pathName in pathnames)
                        {
                            if (File.Exists(pathName))
                            {
                                var attachment = new Attachment(pathName);
                                mail.Attachments.Add(attachment);
                            }
                        }
                    }

                    var smtp = new SmtpClient(smtpHost, smtpPort); // (smtpPort != null) ? new SmtpClient(smtpHost, smtpPort) : new SmtpClient(smtpHost);
                    smtp.EnableSsl = false;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    if (userName != null && passWord != null)
                    {
                        smtp.Credentials = new NetworkCredential(userName, passWord);
                    }

                    smtp.Send(mail);
                }
            }
            catch (Exception)
            {
               

            }
        }

      

        #endregion

        #region errorNotifications

        // Can't afford an exception loading this
        public static string errorMessageBody = @"
		<html>
			<head>
				<title>Windows Azure Error email template</title>
				
			<style type=""text/css"">
			
			body { font-family:Verdana; }
			.email{ font-size: 10pt; background-color:#FFDDDD; font-family: Verdana; }
			table { margin-top: 15px; border-collapse:collapse;  border:0px; }
			td { padding:10px; }
			.label { font-size: 10pt; text-align:right;}
			.value { font-size: 10pt; text-align:left; font-weight:bold; }
			.altrow { background-color:#CCCCFC; }
			.title { font-size: 10pt; font-weight:bold; }
			.error { font-size: 10pt; color:red; text-align:right; }
			.header { font-size: 10pt; border-bottom:solid 1px black; }
			
			</style>	
			</head>
			<body >
			<div class=""email"" style="" margin:20px;  padding:10px; "" >
			<span style=""font-size: 14pt;  "">CRM Error on #server
			</span><br/><br/>
			<span style=""font-size: 10pt; "">Error Message
				<br />
				<hr />
				#errorMsg
				<br />
				<br />
			</div>

			</body>
		</html>";

        // Can't afford an exception loading this
        public static string messageBody = @"
		<html>
			<head>
				<title>CRM email template</title>
				
			<style type=""text/css"">
			
			body { font-family:Verdana; }
			.email{ font-size: 10pt; background-color:#9dfffc; font-family: Verdana; }
			table { margin-top: 15px; border-collapse:collapse;  border:0px; }
			td { padding:10px; }
			.label { font-size: 10pt; text-align:right;}
			.value { font-size: 10pt; text-align:left; font-weight:bold; }			
			.title { font-size: 10pt; font-weight:bold; }
			.error { font-size: 10pt; color:red; text-align:right; }
			.header { font-size: 10pt; border-bottom:solid 1px black; }
			
			</style>	
			</head>
			<body >
			<div class=""email"" style="" margin:20px;  padding:10px; "" >
			
			<span style=""font-size: 10pt; "">
				<br /><pre>#message</pre><br />
			</div>

			</body>
		</html>";


        public static void SendExceptionMessage(string serverName, string subject, string[] emailAddress, Exception ex)
        {
            string emailBody = errorMessageBody;

            emailBody = emailBody.Replace("#server", serverName + " " + GetUserName());

            emailBody = emailBody.Replace("#errorMsg", IsExceptionFound(ex, ""));

            for (int i = 0; i < emailAddress.Length; i++)
            {
                sendEmailAll(emailAddress[i], emailAddress[i], serverName + " " +subject,
                             emailBody, null, true, true);
            }

        }

        public static void SendExceptionMessage(string serverName, string subject, string[] emailAddress, Exception ex, string additionalText)
        {
            string emailBody = errorMessageBody;

            emailBody = emailBody.Replace("#server", serverName + " " + GetUserName());

            emailBody = emailBody.Replace("#errorMsg", IsExceptionFound(ex, ""));

            emailBody += additionalText;

            for (int i = 0; i < emailAddress.Length; i++)
            {
                sendEmailAll(emailAddress[i], emailAddress[i], serverName + " " +subject,
                             emailBody, null, true, true);
            }

        }

        public static void SendExceptionMessage(string serverName, string subject, Exception ex)
        {
            string emailBody = errorMessageBody;

            emailBody = emailBody.Replace("#server", serverName + " " + GetUserName());

            string exceptionMessage = IsExceptionFound(ex, "");

            emailBody = emailBody.Replace("#errorMsg", exceptionMessage);

            sendEmailAll(toAddress, fromAddress, serverName + subject,
                         emailBody, null, true, true);
        }

        public static void SendExceptionMessage(string serverName, string subject, Exception ex, string additionalText)
        {
            string emailBody = errorMessageBody;

            emailBody = emailBody.Replace("#server", serverName + " " + GetUserName());

            string exceptionMessage = IsExceptionFound(ex, "");

            emailBody = emailBody.Replace("#errorMsg", additionalText + " " + exceptionMessage);

            sendEmailAll(toAddress, fromAddress, serverName + subject,
                         emailBody, null, true, true);
        }

        public static void SendExceptionMessage(string serverName, string subject, SoapException ex)
        {
            string emailBody = errorMessageBody;

            emailBody = emailBody.Replace("#server", serverName + " " + GetUserName());

            emailBody = emailBody.Replace("#errorMsg", String.Format("Soap detail: {0} <br/> {1}", ex.Detail.InnerXml, IsExceptionFound(ex, "")));



            sendEmailAll(toAddress, fromAddress, serverName + subject,
                         emailBody, null, true, true);
            

        }

       
        public static void SendExceptionMessage(string serverName, string subject, SoapException ex, string additionalText)
        {
           
            string emailBody = errorMessageBody;

            emailBody = emailBody.Replace("#server", serverName + " " + GetUserName());

            emailBody = emailBody.Replace("#errorMsg", String.Format("Soap detail: {0} <br/> {1}", ex.Detail.InnerXml, IsExceptionFound(ex, "")));

            emailBody += additionalText;


            sendEmailAll(toAddress, fromAddress, serverName + subject,
                         emailBody, null, true, true);
           

        }

        public static void SendErrorMessage(string serverName, string subject, string additionalText)
        {

            string emailBody = errorMessageBody;

            emailBody = emailBody.Replace("#server", serverName + " " + GetUserName());

            emailBody = emailBody.Replace("#errorMsg", additionalText);
            


            sendEmailAll(toAddress, fromAddress, serverName + subject,
                         emailBody, null, true, true);

        }

        public static void SendErrorMessage(string serverName, string subject, string additionalText, string thisToAddress)
        {

            string emailBody = errorMessageBody;

            emailBody = emailBody.Replace("#server", serverName + " " + GetUserName());

            emailBody = emailBody.Replace("#errorMsg", additionalText);


            sendEmailAll(thisToAddress, fromAddress, serverName + " " + subject,
                         emailBody, null, true, true);

        }

        public static void SendMessage(string serverName, string subject, string additionalText, string thisToAddress)
        {

            string emailBody = messageBody;

            emailBody = emailBody.Replace("#server", serverName + " " + GetUserName());

            emailBody = emailBody.Replace("#message", additionalText);

            sendEmailAll(thisToAddress, fromAddress, serverName + " " + subject,
                         emailBody, null, true, true);

        }

        public static string GetUserName()
        {
            try
            {
                var currentUser = Membership.GetUser();

                 if ( currentUser != null) return currentUser.UserName;
            }
            catch
            {
            }
          
            return "";
        }

        /// <summary>
        /// Recursive look for inner Exceptions
        /// </summary>
        /// <param name="startPoint"></param>
        /// <param name="recursiveMessage"></param>
        /// <returns></returns>
        public static string IsExceptionFound(Exception startPoint, string recursiveMessage)
        {

            if (startPoint != null)
            {
                var sb = new StringBuilder();

                sb.Append(recursiveMessage);
                sb.Append("<p />");
                sb.AppendFormat("ExceptionType: {0}<br />", startPoint.GetType());
                sb.AppendFormat("Message: {0}<br />", startPoint.Message);
                sb.AppendFormat("Source: {0}<br />", startPoint.Source);
                sb.AppendFormat("StackTrace: {0}<br /><hr/>", startPoint.StackTrace);

                recursiveMessage = sb.ToString();

                //see if any inner exceptions if so then recurse
                if (startPoint.InnerException != null)
                {
                    return IsExceptionFound(startPoint.InnerException, recursiveMessage);
                }
            }

            return recursiveMessage;
        }

        #endregion
    }

    public class Feature
    {
        public string Name;
        public bool? Enabled;
        public string Svalue;
        public int? Ivalue;
    }
}