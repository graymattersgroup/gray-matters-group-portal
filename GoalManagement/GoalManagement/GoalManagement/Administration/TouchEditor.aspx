﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WideSite.Master" AutoEventWireup="true"
    CodeBehind="TouchEditor.aspx.cs" Inherits="GoalManagement.Administration.TouchEditor" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">

            input[type=text]
{
    text-align:left !important;
}
input[type=password]
{
    text-align:left !important;
}
input
{
    text-align:center !important;
    border-radius: 5px;
     -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
}



        table  
{ 
	border-collapse:collapse;border:0px;
	border-spacing: 0px; 
}
       
.TouchDayofWeek { font-size: 10px; }
.TouchDay { padding-left:10px; padding-right:10px; font-size: 10px; }
.TouchName  {font-size: 10px;  }
          
.HideMe { visibility:hidden; display:none;  }       
.directionstable{border-collapse:collapse;border:0px }
table.directionstable tr td {padding:4px;}
.number { color:DarkBlue;}
.header {background:blue; color:white; text-align:left;}
.destination { font-weight:bold; }

.sidemenu { font-size:11px; height:20px;  text-align:center; color:#FFFFFF; Background: url("/Images/tabfill.gif") repeat; }
.sideitem { font-size:9px;}

div.sideitem input
{font-size:9px;}

.lookup {font-size:9px;}

        #Button2
        {
            width: 109px;
        }
        #Button1
        {
            width: 78px;
        }
        #Button3
        {
            width: 78px;
        }
        #SaveSettings
        {
            width: 160px;
        }
        
        #ctl00_MainContent_Menu1 { margin-left:5px;}
        
       .menutab { 
color: #FFFFFF; 
background: url("/Images/wholetab150.gif"); 
text-decoration: none; 
text-align:center;
width:145px;
}

    a.selected { font-weight:bolder; background: url("/Images/wholetabselected150.gif");}
        
    ul.wholemenu li a { color:#ffffff }
        
        
                
        .style1
        {
            width: 470px;
        }
        
        
                
        .style2
        {
            width: 73px;
        }
        
        
                
        .style4
        {
            width: 123px;
        }
        
        
                
    </style>
    

    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Panel ID="MockupPanel" runat="server" Visible="false">
        <asp:Button ID="HideMockup" runat="server" Text="Hide Mockup" 
            onclick="HideMockup_Click" />
        <asp:Literal ID="EmailBodyLiteral" runat="server" ></asp:Literal>
    </asp:Panel>

    <div style="padding-left: 10px;  background-color:#FFFFFF;">

        <telerik:RadTabStrip ID="RadTabStrip1" runat="server" OnTabClick="RadTabStrip_Click"
            Skin="Sitefinity" SelectedIndex="0">
            <Tabs>
            <%--    <telerik:RadTab runat="server" Value="1" Text="Email Templates" Selected="True">             
                </telerik:RadTab>--%>
                <telerik:RadTab runat="server" Value="2" Text="Scheduled Touches" Selected="true" >
                </telerik:RadTab>
                <telerik:RadTab runat="server" Value="3" Text="Copy Touches">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>

      <asp:MultiView ID="resultMultiView" runat="server" ActiveViewIndex="1" Visible="true">
                   
            <asp:View ID="Touches" runat="server">
            <br />
                <table>
                    <tr>                       
                        <td>
                            Class&nbsp;
                        </td>
                        <td class="style2">
                            <asp:DropDownList ID="ClassList" runat="server"  OnSelectedIndexChanged="ClassList_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                            <br />
                        </td>
                        </tr>                     
                       
                        <tr>
                        <td>
                            Scheduled Touch&nbsp;
                        </td>
                        <td class="style2">
                            <asp:DropDownList ID="ScheduledTouchList" runat="server"  OnSelectedIndexChanged="ScheduledTouchList_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>                                 
                                    
                </table>         
               
                   
                
                    <br />
                    <table>
                    <tr>
                    <td>    
                                      
                    <table>
                    <tr>
                        <td>
                            To:
                        </td>
                        <td>
                            <asp:DropDownList ID="ToList" runat="server" Font-Size="9pt">
                            <asp:ListItem Text="Students" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Coaches" Value="2" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Sponsors" Value="3" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Coaches and Sponsors" Value="4" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="All" Value="5" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="None" Value="100000000" Selected="False"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:DropDownList ID="CCList" runat="server" Font-Size="9pt" Visible="false">
                                <asp:ListItem Text="Students" Value="1" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Coaches" Value="2" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Sponsors" Value="3" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Coaches and Sponsors" Value="4" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="None" Value="100000000" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Representative Copy:
                        </td>
                        <td>
                            <asp:DropDownList ID="RepresentativeCopyList" runat="server" Font-Size="9pt">
                                <asp:ListItem Text="Students" Value="1" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Coaches" Value="2" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Sponsors" Value="3" Selected="False"></asp:ListItem>
                                <asp:ListItem Text="Coaches and Sponsors" Value="4" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="None" Value="100000000" Selected="False"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                  
                   
                </table>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <table>
                                    <tr>
                                    <td>Trigger on</td>
                                    <td><asp:RadioButtonList ID="TimeOfDay" runat="server" 
                                        RepeatDirection="Horizontal" >
                                        <asp:ListItem Text="AM" Selected="True"> </asp:ListItem>
                                        <asp:ListItem Text="PM"> </asp:ListItem>
                                    </asp:RadioButtonList>                          </td>
                                    </tr>
                                    </table>
                                    <br />
                                         <telerik:RadDatePicker ID="TriggerDate" Runat="server">
                                    </telerik:RadDatePicker>
                                         <br />
                               
                                   
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="padding-left:30px;"> 
                        <asp:Button ID="ShowMockup" runat="server" onclick="ShowMockup_Click"  
                            Text="Show Mockup" Width="190px" BackColor="#000066" Font-Bold="True" 
                            ForeColor="White" />
                        <br />
                        <asp:Button ID="SendTestEmail" runat="server" OnClick="SendTestEmail_Click"  
                            Text="Send Test Email" Width="190px" BackColor="#000066" Font-Bold="True" 
                            ForeColor="White" />
                        <br />
                        <asp:Button ID="SendTestEmailAll" runat="server" 
                            OnClick="SendTestEmailAll_Click" Text="Send Test Email All Touches" 
                            Width="190px" BackColor="#000066" Font-Bold="True" ForeColor="White" />
                        <br />
                        <br />
                          <asp:Button ID="MoveTouches" runat="server" onclick="MoveTouches_Click" 
                            Text="Move All Touches" Width="120px" BackColor="#006600" Font-Bold="True" 
                            ForeColor="White" />
                        &nbsp;
                        <asp:DropDownList ID="TouchWeeks" runat="server">
                         <asp:ListItem Text="0 Weeks" Value="0"></asp:ListItem>
                            <asp:ListItem Text="1 Week" Value="7"></asp:ListItem>
                            <asp:ListItem Text="2 Weeks" Value="14"></asp:ListItem>
                            <asp:ListItem Text="3 Weeks" Value="21"></asp:ListItem>
                            <asp:ListItem Text="4 Weeks" Value="28"></asp:ListItem>
                            <asp:ListItem Text="-1 Week" Value="-7"></asp:ListItem>
                            <asp:ListItem Text="-2 Weeks" Value="-14"></asp:ListItem>
                            <asp:ListItem Text="-3 Weeks" Value="-21"></asp:ListItem>
                            <asp:ListItem Text="-4 Weeks" Value="-28"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="TouchDays" runat="server">
                         <asp:ListItem Text="0 Days" Value="0"></asp:ListItem>
                            <asp:ListItem Text="1 Day" Value="1"></asp:ListItem>
                            <asp:ListItem Text="2 Days" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3 Days" Value="3"></asp:ListItem>
                            <asp:ListItem Text="4 Days" Value="4"></asp:ListItem>
                            <asp:ListItem Text="-1 Day" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="-2 Days" Value="-2"></asp:ListItem>
                            <asp:ListItem Text="-3 Days" Value="-3"></asp:ListItem>
                            <asp:ListItem Text="-4 Days" Value="-4"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    </tr>
                    </table>
            
                <br />

                <table>                   
                    <tr>
                        <td colspan="1" style="color:Red;" class="style4" >
                            Select Template &nbsp;
                            </td>
                        <td style="color: Red;" class="style1" colspan="2">
                            <asp:DropDownList ID="ScheduledTemplateList" runat="server" AutoPostBack="true" 
                                OnSelectedIndexChanged="ScheduledTemplateList_SelectedIndexChanged">
                            </asp:DropDownList>
                            &nbsp;&nbsp;&nbsp;<asp:Label ID="ModeLabel" runat="server" Text="None Selected"></asp:Label>
                        </td>
                            <td style="padding-left:20px;"> 
                                <asp:Button ID="ViewSelectedUser" runat="server" BackColor="#000066" 
                                    Font-Bold="True" ForeColor="White" OnClick="NewTouch_Click" 
                                    Text="New  ( Clear )" Width="150px" />
                        </td>
                    </tr>
                    <tr>
                    <td  colspan="3">
                        &nbsp;</td>
                         <td> 
                             &nbsp;</td>
                    </tr>
                    <tr>                       
                        <td class="style4">
                            Touch Name
                        </td>
                        <td class="style1" colspan="2">
                            <asp:TextBox ID="ScheduledTouchName" runat="server" Width="500px"></asp:TextBox>
                        </td>
                        <td style="padding-left:20px;"> 
                            <asp:Button ID="SaveTouchAsNewTemplate" runat="server" BackColor="#000066" 
                                Font-Bold="True" ForeColor="White" OnClick="SaveTouchAsNewTemplate_Click" 
                                Text="Save as New Template" Width="150px" />
                        </td>
                    </tr>
                    <tr>
                       
                        <td class="style4">
                            &nbsp; &nbsp;Subject
                        </td>
                        <td class="style1" colspan="2" >
                            <asp:TextBox ID="Subject" runat="server" Width="500px"></asp:TextBox>
                        </td>  
                            <td style="padding-left:20px;"> 
                                <asp:Button ID="SaveTouchAsNew" runat="server" BackColor="#000066" 
                                    Font-Bold="True" ForeColor="White" OnClick="SaveTouchAsNew_Click" 
                                    Text="Save as New Touch" Width="150px" />
                        </td>                      
                    </tr>
                    <tr>
                    <td class="style4">Select Asset
                    </td>
                    <td colspan="2">  <asp:DropDownList ID="AssetList" AutoPostBack="true" 
                            runat="server" onselectedindexchanged="AssetList_SelectedIndexChanged" >
                            </asp:DropDownList>
                    </td>
                        <td style="padding-left:20px;"> 
                            &nbsp;</td>
                    </tr>
                    <tr>
                    <td colspan="3">
                     <asp:TextBox ID="AssetLink" runat="server" Width="600px"></asp:TextBox>
                    </td>
                        <td style="text-align:right;"> 
                            <asp:Button ID="UpdateTouch" runat="server" BackColor="#000066" 
                                Font-Bold="True" ForeColor="White" OnClick="UpdateTouch_Click" Text="Update" 
                                Width="150px" />
                        </td>
                    </tr>
                    </table>
                    <br />

                <telerik:RadEditor ID="RadEditor2" runat="server" Skin="Sitefinity" Height="494px"
                    Width="955px" NewLineBr="true">
                    <%-- <Languages>
                       <telerik:SpellCheckerLanguage Code="en-US" Title="English" />
                   </Languages>--%>
                    <Tools>
                        <telerik:EditorToolGroup>
                            <telerik:EditorTool Name="AjaxSpellCheck" />
                            <telerik:EditorTool Name="Bold" />
                            <telerik:EditorTool Name="Italic" />
                            <telerik:EditorTool Name="Underline" />
                            <telerik:EditorTool Name="StrikeThrough" />
                            <telerik:EditorSeparator />
                            <telerik:EditorTool Name="JustifyLeft" />
                            <telerik:EditorTool Name="JustifyCenter" />
                            <telerik:EditorTool Name="JustifyRight" />
                            <telerik:EditorTool Name="JustifyFull" />
                            <telerik:EditorTool Name="JustifyNone" />
                            <telerik:EditorSeparator />
                            <telerik:EditorTool Name="Indent" />
                            <telerik:EditorTool Name="Outdent" />
                            <telerik:EditorTool Name="InsertOrderedList" />
                            <telerik:EditorTool Name="InsertUnorderedList" />
                            <telerik:EditorTool Name="LinkManager" />
                            <telerik:EditorTool Name="Unlink" />
                        </telerik:EditorToolGroup>
                        <telerik:EditorToolGroup Tag="DropdownToolbar">
                            <telerik:EditorSplitButton Name="ForeColor">
                            </telerik:EditorSplitButton>
                            <telerik:EditorSeparator />
                            <telerik:EditorDropDown Name="FontName">
                            </telerik:EditorDropDown>
                            <telerik:EditorDropDown Name="RealFontSize">
                            </telerik:EditorDropDown>
                        </telerik:EditorToolGroup>
                    </Tools>
                    <ImageManager ViewPaths="~/Images" />
                    <FlashManager ViewPaths="~/Images" />
                    <MediaManager ViewPaths="~/Images" />
                    <DocumentManager ViewPaths="~/Images" />
                    <TemplateManager ViewPaths="~/Images" />
                    <Content>
                    
                    </Content>
                </telerik:RadEditor>
            
            
              
              <div style="  margin:10px; border: 1px solid black; ">
                New Attachment <br />
                <asp:FileUpload ID="FileUploadControl" runat="server" />                        
                <br /> 
                  <asp:Button runat="server" ID="UploadButton" Text="Upload" OnClick="UploadButton_Click"  OnClientClick="javascript:Waiting();" />               
                <asp:Label runat="server" ID="StatusLabel" Text="Upload status: " />
              </div>  
          
                <telerik:RadGrid AutoGenerateColumns="False" ID="Attachments" runat="server" GridLines="None"
                    EnableLinqExpressions="False" Skin="Web20" OnItemDataBound="Attachments_ItemDataBound" 
                    ViewStateMode="Enabled">
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="False" ShowFooter="False"
                        TableLayout="Auto" CommandItemDisplay="Top">
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px;">                               
                                <asp:LinkButton ID="ViewSelected" runat="server" OnCommand="Attachment_View" Visible='<%# Attachments.EditIndexes.Count == 0 %>'><img style="border:0px;vertical-align:middle;" alt="" src="/Images/icon_search_16px.gif" />&nbsp;View Attachment</asp:LinkButton>&nbsp;&nbsp;&nbsp;
                            
                                <asp:LinkButton ID="DeleteSelected" OnCommand="Attachment_Delete" OnClientClick="javascript:return confirm('Delete all selected attachments?')"
                                    runat="server" CommandName="DeleteSelected"><img style="border:0px;vertical-align:middle;" alt="" src="/Images/Delete.gif" />&nbsp;Delete selected attachments</asp:LinkButton>
                            </div>
                        </CommandItemTemplate>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Name" HeaderText="File Name" SortExpression="Name" UniqueName="Name" ReadOnly="True">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FileSize" HeaderText="File Size" SortExpression="FileSize"
                                UniqueName="FileSize" ReadOnly="True">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Type" HeaderText="Type" SortExpression="Type"
                                UniqueName="Type" ReadOnly="True">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ID" HeaderText="ID" SortExpression="ID" UniqueName="ID"     ReadOnly="False"> 
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="True" EnableDragToSelectRows="True" />
                    </ClientSettings>
                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Web20">
                    </HeaderContextMenu>
                </telerik:RadGrid>
               <br />
               <br />
               <br />

              <%--OnItemDataBound="Attachments_ItemDataBound" --%>

               <telerik:RadGrid AutoGenerateColumns="False" ID="TemplateGrid" runat="server" GridLines="None"
                    EnableLinqExpressions="False" Skin="Web20" 
                    ViewStateMode="Enabled">
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="False" ShowFooter="False"
                        TableLayout="Auto" CommandItemDisplay="Top">
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px;">                           
                                    <asp:LinkButton ID="DeleteSelected" OnCommand="Template_Delete" OnClientClick="javascript:return confirm('Delete Template ?')"
                                    runat="server" CommandName="DeleteSelected">
                                    <img style="border:0px;vertical-align:middle;" alt="" src="/Images/Delete.gif" />&nbsp;Delete selected Templates</asp:LinkButton>
                            </div>
                        </CommandItemTemplate>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Name" HeaderText="Touch Name" SortExpression="Name" UniqueName="Name" ReadOnly="True">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Subject" HeaderText="Subject" SortExpression="Subject" UniqueName="Subject" ReadOnly="True">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ModifiedOn" HeaderText="ModifiedOn" SortExpression="ModifiedOn" UniqueName="ModifiedOn" ReadOnly="True">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ID" HeaderText="ID" SortExpression="ID" UniqueName="ID"     ReadOnly="False"> 
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings>
                        <Selecting AllowRowSelect="True" EnableDragToSelectRows="True" />
                    </ClientSettings>
                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Web20">
                    </HeaderContextMenu>
                </telerik:RadGrid>


          
      </asp:View>         



            <asp:View ID="CopyTouches" runat="server">
            <br />
                
                <table>             
                    
                    <tr>
                        <td>
                            Copy From Class&nbsp;
                        </td>
                        <td>
                            <asp:DropDownList ID="TemplateClassList" runat="server" OnSelectedIndexChanged="TemplateClassList_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <asp:Literal ID="TemplateTouchesLiteral" runat="server"></asp:Literal>
                           <%-- <asp:BulletedList ID="TemplateTouchesList" Font-Size="XX-Small" runat="server">
                            </asp:BulletedList>--%>
                        </td>
                       
                    </tr>                   
                </table>
                <br />
                <hr />
                <br />
                <table>
                    <tr>
                        <td>
                            Copy To Class&nbsp;
                        </td>
                        <td>
                            <asp:DropDownList ID="TargetClassList" runat="server" OnSelectedIndexChanged="TargetClassList_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                            &nbsp;
                            <asp:Button ID="Copy" runat="server" Text="Copy Touches" OnClick="Copy_Click" OnClientClick="javascript:Waiting();" />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <asp:Literal ID="TargetTouchesLiteral" runat="server"></asp:Literal>
                        <%--    <asp:BulletedList ID="TargetTouchesList" Font-Size="XX-Small" runat="server">
                            </asp:BulletedList>--%>
                         
                        </td>                    
                    </tr>
                </table>
            </asp:View>
            

        </asp:MultiView>
        </div>
       
         

</asp:Content>
