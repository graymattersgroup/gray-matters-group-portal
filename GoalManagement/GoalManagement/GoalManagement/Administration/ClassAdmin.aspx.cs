﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Crm.Sdk;
using Microsoft.Xrm.Sdk;
using Telerik.Web.UI;

namespace GoalManagement.Administration
{
    public partial class ClassAdmin : System.Web.UI.Page
    {
        protected CRMAccess crmAccess;
        protected Contact myContact;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["crmAccess"] == null)
            {
                crmAccess = new CRMAccess();
                Session["crmAccess"] = crmAccess;

                MembershipUser currentUser = Membership.GetUser();
                if (currentUser == null) Response.Redirect("../Default.aspx");

                if (Session["ImpersonateUser"] != null)
                {
                    var otherUser = (string)Session["ImpersonateUser"];
                    currentUser = Membership.GetUser(otherUser);
                }

                var myEmail = currentUser.Email;
                myContact = crmAccess.GetStudent(myEmail);

                Session["StudentInfo"] = myContact;
              
                var coachingClasses = crmAccess.GetAllClasses().ToList();

                Session["CoachingClasses"] = coachingClasses;

                var accounts = crmAccess.GetAccounts();

                foreach (var account in accounts)
                {
                 AccountList.Items.Add(new ListItem(account.Name, account.Id.ToString()));
                }

            }
            else
            {
                crmAccess = (CRMAccess)Session["crmAccess"];
                myContact = (Contact)Session["StudentInfo"];
            }

            if (myContact.new_IsPortalAdministrator != true) Response.Redirect("../Default.aspx");

            if (!IsPostBack)
            {
                ClassList.Items.Add(new ListItem("select class", Guid.Empty.ToString()));

                var coachingClasses = crmAccess.GetAllClasses().ToList();
                Session["CoachingClasses"] = coachingClasses;

                foreach (new_class coachingClass in coachingClasses)
                {
                    ClassList.Items.Add(new ListItem(coachingClass.new_name, coachingClass.new_classId.ToString()));
                }


                var accounts = crmAccess.GetAccounts();

                foreach (var account in accounts)
                {
                    AccountList.Items.Add(new ListItem(account.Name, account.Id.ToString()));
                }
            }
          
            var masterPage = (WideSiteMaster)Page.Master;
            masterPage.SetMenu(CentralizedData.ClassAdmin);
            var db = new SqlAzureLayer(masterPage.GetConnectionString());
            HelpLiteral.Text = db.GetHelp("Class Admin");
        }

         protected void ClassList_SelectedIndexChanged(object sender, EventArgs e)
         {
             ClassListChanged();
         }

        protected void ClassListChanged()
        {
            var classId = new Guid(ClassList.SelectedValue);
            var thisClass =crmAccess.GetClass(classId);

            Session["SelectedClass"] = thisClass;

            if (thisClass.new_ClassDate != null) ClassDate.DbSelectedDate = thisClass.new_ClassDate.Value.ToLocalTime();
            if (thisClass.new_StartDate != null) AftercareDate.DbSelectedDate = thisClass.new_StartDate.Value.ToLocalTime();
            if( thisClass.new_HidePeerReview != null) HidePeerReview.Checked = thisClass.new_HidePeerReview.Value;
            if( thisClass.new_HideResourcesandTools != null) HideResources.Checked = thisClass.new_HideResourcesandTools.Value;
            if( thisClass.new_HideCoachingSupport != null) HideCoachingSupport.Checked = thisClass.new_HideCoachingSupport.Value;
            if (thisClass.new_ClassComplete != null) ClassComplete.Checked = thisClass.new_ClassComplete.Value;
            //if (thisClass.new_GoalsRequired != null) GoalsRequired.Text = thisClass.new_GoalsRequired.Value.ToString();
            //if (thisClass.new_UpdateTimeoutDays != null) UpdateTimeoutDays.Text = thisClass.new_UpdateTimeoutDays.Value.ToString();

            var goals = crmAccess.GetClassGoals(classId);
            var goalData = new DataTable();
            goalData.Columns.Add("Name", typeof(string));
            goalData.Columns.Add("ID", typeof(string));

            foreach (ClassGoal goal in goals)
            {
                var newRow = goalData.NewRow();

                newRow[0] = goal.name;
                newRow[1] = goal.id;
              
                goalData.Rows.Add(newRow);
            }

            Session["ClassGoals"] = goalData;

            ClassGoalsGrid.DataSource = goalData;
            ClassGoalsGrid.DataBind();

            UpdateTouchGrid(classId);

            //var touches = crmAccess.GetScheduledTouches(classId);

            //var touchesData = new DataTable();
            //touchesData.Columns.Add("TriggerDate", typeof(DateTime));
            //touchesData.Columns.Add("Day", typeof(string));
            //touchesData.Columns.Add("Name", typeof (string));
            //touchesData.Columns.Add("Sent", typeof (Boolean));
            //touchesData.Columns.Add("ID", typeof(string));
          
            //foreach (var newScheduledtouch in touches)
            //{
            //    var newRow = touchesData.NewRow();

            //    newRow[0] = newScheduledtouch.new_TriggerDate.Value.ToLocalTime();
            //    newRow[1] = newScheduledtouch.new_TriggerDate.Value.ToLocalTime().DayOfWeek;
            //    newRow[2] = newScheduledtouch.new_name;
            //    newRow[3] = (newScheduledtouch.new_Sent != null)? newScheduledtouch.new_Sent.Value: false;
            //    newRow[4] = newScheduledtouch.Id.ToString();

            //    touchesData.Rows.Add(newRow);
            //}

            //Session["ScheduledTouches"] = touchesData;
            //Session["OriginalTouches"] = touchesData;

            //ScheduledTouches.DataSource = touchesData;
            //ScheduledTouches.DataBind();


            var updatePeriods = crmAccess.GetClassUpdatePeriods(classId);

            var updatePeriodDate = new DataTable();
            updatePeriodDate.Columns.Add("Name", typeof(string));
            updatePeriodDate.Columns.Add("StartDate", typeof(DateTime));
            updatePeriodDate.Columns.Add("EndDate", typeof(DateTime));
            updatePeriodDate.Columns.Add("ImpactQuestion", typeof(string));
            updatePeriodDate.Columns.Add("ID", typeof(string));
            
            foreach (var newClassupdateperiod in updatePeriods)
            {
                var newRow = updatePeriodDate.NewRow();

                newRow[0] = newClassupdateperiod.new_name;
                newRow[1] = newClassupdateperiod.new_Start.Value;
                newRow[2] = newClassupdateperiod.new_End.Value;
                
                if (newClassupdateperiod.new_ImpactQuestionId != null)
                {
                    newRow[3] = newClassupdateperiod.new_ImpactQuestionId.Name;
                }
                newRow[4] = newClassupdateperiod.Id.ToString();

                updatePeriodDate.Rows.Add(newRow);
            }

            Session["UpdatePeriods"] = updatePeriodDate;
            Session["OriginalUpdates"] = updatePeriodDate;
            UpdatePeriods.DataSource = updatePeriodDate;
            UpdatePeriods.DataBind();
        }

        private void UpdateTouchGrid(Guid classId)
        {
            var touches = crmAccess.GetScheduledTouches(classId);

            var touchesData = new DataTable();
            touchesData.Columns.Add("TriggerDate", typeof(DateTime));
            touchesData.Columns.Add("Day", typeof(string));
            touchesData.Columns.Add("Name", typeof(string));
            touchesData.Columns.Add("Sent", typeof(Boolean));
            touchesData.Columns.Add("ID", typeof(string));

            foreach (var newScheduledtouch in touches)
            {
                var newRow = touchesData.NewRow();

                newRow[0] = newScheduledtouch.new_TriggerDate.Value.ToLocalTime();
                newRow[1] = newScheduledtouch.new_TriggerDate.Value.ToLocalTime().DayOfWeek;
                newRow[2] = newScheduledtouch.new_name;
                newRow[3] = (newScheduledtouch.new_Sent != null) ? newScheduledtouch.new_Sent.Value : false;
                newRow[4] = newScheduledtouch.Id.ToString();

                touchesData.Rows.Add(newRow);
            }

            Session["ScheduledTouches"] = touchesData;
            Session["OriginalTouches"] = touchesData;

            ScheduledTouches.DataSource = touchesData;
            ScheduledTouches.DataBind();
        }

        protected void ScheduledTouches_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            var gdItem = (e.Item as GridDataItem);
            var triggerDate = (gdItem)["TriggerDate"].Controls[0] as RadDatePicker;
            var sent = (gdItem)["Sent"].Controls[0] as CheckBox;
            var id = (gdItem)["ID"].Controls[0] as TextBox;
            
            if (Session["ScheduledTouches"] != null)
            {
                var touchesData = (DataTable)Session["ScheduledTouches"];

                foreach (DataRow dataRow in touchesData.Rows)
                {
                    if( dataRow["ID"].ToString() == id.Text)
                    {
                        dataRow["TriggerDate"] = triggerDate.DbSelectedDate;
                        dataRow["Day"] = ((DateTime) triggerDate.DbSelectedDate).DayOfWeek;
                        dataRow["Sent"] = sent.Checked;
                        crmAccess = (CRMAccess)Session["crmAccess"];

                        crmAccess.UpdateTouchTrigger(new Guid(id.Text),sent.Checked, (DateTime)triggerDate.DbSelectedDate );
                        break;
                    }
                }

                Session["ScheduledTouches"] = touchesData;

                ScheduledTouches.DataSource = touchesData;
                ScheduledTouches.DataBind();
            }
        }


        protected void UpdatePeriods_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            var gdItem = (e.Item as GridDataItem);
            var startDate = (gdItem)["StartDate"].Controls[0] as RadDatePicker;
            var endDate = (gdItem)["EndDate"].Controls[0] as RadDatePicker;
            var id = (gdItem)["ID"].Controls[0] as TextBox;

            if (Session["UpdatePeriods"] != null)
            {
                var touchesData = (DataTable)Session["UpdatePeriods"];

                foreach (DataRow dataRow in touchesData.Rows)
                {
                    if (dataRow["ID"].ToString() == id.Text)
                    {
                        dataRow["StartDate"] = startDate.DbSelectedDate;
                        dataRow["EndDate"] = endDate.DbSelectedDate; 

                        crmAccess = (CRMAccess)Session["crmAccess"];

                        crmAccess.UpdateUpdatePeriod(new Guid(id.Text), (DateTime)startDate.DbSelectedDate, (DateTime)endDate.DbSelectedDate);
                        break;
                    }
                }

                Session["UpdatePeriods"] = touchesData;

                UpdatePeriods.DataSource = touchesData;
                UpdatePeriods.DataBind();
            }
        }


        protected void ScheduledTouches_EditCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (Session["ScheduledTouches"] != null)
            {
                var touchesData = (DataTable)Session["ScheduledTouches"];

                ScheduledTouches.DataSource = touchesData;
                ScheduledTouches.DataBind();
            }
        }
        protected void UpdatePeriods_EditCommand(object sender, GridCommandEventArgs e)
        {
            if (Session["UpdatePeriods"] != null)
            {
                var touchesData = (DataTable)Session["UpdatePeriods"];

                UpdatePeriods.DataSource = touchesData;
                UpdatePeriods.DataBind();
            }
        }


        protected void ScheduledTouches_CancelCommand(object sender, GridCommandEventArgs e)
        {
            if (Session["ScheduledTouches"] != null)
            {
                var touchesData = (DataTable)Session["ScheduledTouches"];

                ScheduledTouches.DataSource = touchesData;
                ScheduledTouches.DataBind();
            }
        }
        protected void UpdatePeriods_CancelCommand(object sender, GridCommandEventArgs e)
        {
            if (Session["UpdatePeriods"] != null)
            {
                var touchesData = (DataTable)Session["UpdatePeriods"];

                UpdatePeriods.DataSource = touchesData;
                UpdatePeriods.DataBind();
            }
        }
      
        protected void StartDayofWeek_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch( StartDayofWeek.SelectedValue)
            {
                case "M":
                    EndDayofWeek.SelectedValue = "F";
                    break;
                case "T":
                    EndDayofWeek.SelectedValue = "M";
                    break;
                case "W":
                    EndDayofWeek.SelectedValue = "T";
                    break;
                case "R":
                    EndDayofWeek.SelectedValue = "W";
                    break;
                case "F":
                    EndDayofWeek.SelectedValue = "R";
                    break;
            }
        }

        protected void CopyClass_Click(object sender, EventArgs e)
        {
            if (ClassList.SelectedIndex != 0)
            {
                CopyClassPanel.Visible = true;

                RequiredFieldValidator1.Enabled = true;
                RequiredFieldValidator2.Enabled = true;
                RequiredFieldValidator3.Enabled = true;
            }

        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            CopyClassPanel.Visible = false;
            RequiredFieldValidator1.Enabled = false;
            RequiredFieldValidator2.Enabled = false;
            RequiredFieldValidator3.Enabled = false;
        }

        protected void DoCopy_Click(object sender, EventArgs e)
        {
            CopytheClass();



        }

        protected void RepeatInterval_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalculateEndDate();
        }

        private void CalculateEndDate()
        {
            var startDate = (DateTime)newAfterCareStartDate.DbSelectedDate;

            var endDate = startDate.AddDays((7 * Convert.ToInt32(RepeatInterval.SelectedValue))-1);

            var day = DayOfWeek.Wednesday;

            switch (EndDayofWeek.SelectedValue)
            {
                case "M": day = DayOfWeek.Monday;
                    break;
                case "T": day = DayOfWeek.Tuesday;
                    break;
                case "W": day = DayOfWeek.Wednesday;
                    break;
                case "R": day = DayOfWeek.Thursday;
                    break;
                case "F": day = DayOfWeek.Friday;
                    break;
            }

            while (endDate.DayOfWeek != day)
            {
                endDate = endDate.AddDays(1);
            }

            FirstEndDate.DbSelectedDate = endDate;
        }

        protected void newAfterCareStartDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            CalculateEndDate();
        }

        protected void CopytheClass()
        {
            var templateClassId = new Guid(ClassList.SelectedValue);

            //CreateNewClass
            var newclass = new new_class();

            newclass.new_CustomerId = new EntityReference("account", new Guid(AccountList.SelectedValue));
            newclass.new_name = newClassName.Text;
            newclass.new_UpdateFrequency = new OptionSetValue(Convert.ToInt32(RepeatInterval.SelectedValue));
            newclass.new_ClassDate = (DateTime)newClassDate.DbSelectedDate;
            newclass.new_StartDate = (DateTime) newAfterCareStartDate.DbSelectedDate;

            var thisClass = (new_class)Session["SelectedClass"];
            
            newclass.new_UpdateQuestionsId = thisClass.new_UpdateQuestionsId;

            var newClassId = Guid.NewGuid();
            newclass.new_classId = newClassId;

            crmAccess.AddNewClass(newclass);

            var targetClassId = newClassId;

            var templateClass = crmAccess.GetClass(templateClassId);
            var targetClass = crmAccess.GetClass(targetClassId);

            var templateAfterHoursStartDate = templateClass.new_StartDate.Value.ToLocalTime();
            var targetAfterHoursStartDate = targetClass.new_StartDate.Value.ToLocalTime();

            var templateDayOftheWeek =  templateClass.new_StartDate.Value.ToLocalTime().DayOfWeek;
            var targetDayOftheWeek = targetClass.new_StartDate.Value.ToLocalTime().DayOfWeek;
            
            var offsetDays = (targetAfterHoursStartDate - templateAfterHoursStartDate).Days;
            
            if (templateAfterHoursStartDate > targetAfterHoursStartDate)
            {
                offsetDays = -(templateAfterHoursStartDate - targetAfterHoursStartDate).Days;
            }

            var offsetAdjustment = 0;

            if (templateDayOftheWeek != targetDayOftheWeek)
            {
                switch (templateDayOftheWeek)
                {
                    case DayOfWeek.Sunday:
                        break;

                    case DayOfWeek.Monday:
                        if (targetDayOftheWeek == DayOfWeek.Monday) offsetAdjustment = 0;
                        if (targetDayOftheWeek == DayOfWeek.Tuesday) offsetAdjustment = -1;
                        if (targetDayOftheWeek == DayOfWeek.Wednesday) offsetAdjustment = -2;
                        if (targetDayOftheWeek == DayOfWeek.Thursday) offsetAdjustment = -3;
                        if (targetDayOftheWeek == DayOfWeek.Friday) offsetAdjustment = -4;
                        if (targetDayOftheWeek == DayOfWeek.Saturday) offsetAdjustment = -5;
                        break;

                    case DayOfWeek.Tuesday:
                        if (targetDayOftheWeek == DayOfWeek.Monday) offsetAdjustment = 1;
                        if (targetDayOftheWeek == DayOfWeek.Tuesday) offsetAdjustment = 0;
                        if (targetDayOftheWeek == DayOfWeek.Wednesday) offsetAdjustment = -1;
                        if (targetDayOftheWeek == DayOfWeek.Thursday) offsetAdjustment = -2;
                        if (targetDayOftheWeek == DayOfWeek.Friday) offsetAdjustment = -3;
                        if (targetDayOftheWeek == DayOfWeek.Saturday) offsetAdjustment = -4;
                        break;

                    case DayOfWeek.Wednesday:
                        if (targetDayOftheWeek == DayOfWeek.Monday) offsetAdjustment = 2;
                        if (targetDayOftheWeek == DayOfWeek.Tuesday) offsetAdjustment = 1;
                        if (targetDayOftheWeek == DayOfWeek.Wednesday) offsetAdjustment = 0;
                        if (targetDayOftheWeek == DayOfWeek.Thursday) offsetAdjustment = -1;
                        if (targetDayOftheWeek == DayOfWeek.Friday) offsetAdjustment = -2;
                        if (targetDayOftheWeek == DayOfWeek.Saturday) offsetAdjustment = -3;
                        break;

                    case DayOfWeek.Thursday:
                        if (targetDayOftheWeek == DayOfWeek.Monday) offsetAdjustment = 3;
                        if (targetDayOftheWeek == DayOfWeek.Tuesday) offsetAdjustment = 2;
                        if (targetDayOftheWeek == DayOfWeek.Wednesday) offsetAdjustment = 1;
                        if (targetDayOftheWeek == DayOfWeek.Thursday) offsetAdjustment = 0;
                        if (targetDayOftheWeek == DayOfWeek.Friday) offsetAdjustment = -1;
                        if (targetDayOftheWeek == DayOfWeek.Saturday) offsetAdjustment = -2;
                        break;

                    case DayOfWeek.Friday:
                        if (targetDayOftheWeek == DayOfWeek.Monday) offsetAdjustment = 4;
                        if (targetDayOftheWeek == DayOfWeek.Tuesday) offsetAdjustment = 3;
                        if (targetDayOftheWeek == DayOfWeek.Wednesday) offsetAdjustment = 2;
                        if (targetDayOftheWeek == DayOfWeek.Thursday) offsetAdjustment = 1;
                        if (targetDayOftheWeek == DayOfWeek.Friday) offsetAdjustment = 0;
                        if (targetDayOftheWeek == DayOfWeek.Saturday) offsetAdjustment = -1;
                        break;

                    case DayOfWeek.Saturday:
                        break;

                }

                offsetDays += offsetAdjustment;

            }



            var touches = crmAccess.GetScheduledTouches(templateClassId);

            foreach (new_scheduledtouch newScheduledtouch in touches)
            {
                var id = Guid.NewGuid();

                var templateDate = newScheduledtouch.new_TriggerDate.Value;

                var targetDate = templateDate.AddDays(offsetDays);
               
                //var hour = targetDate.Hour;
                
                //if( hour != 0)
                //{
                //    targetDate.AddHours(-hour);
                //}

                //if (templateDate.Value.DayOfWeek > targetDate.DayOfWeek)
                //{
                //    targetDate = targetDate.AddDays(1);
                //}
                //if (templateDate.Value.DayOfWeek < targetDate.DayOfWeek)
                //{
                //    targetDate = targetDate.AddDays(-1);
                //}

                var touch = new new_scheduledtouch
                {
                    new_scheduledtouchId = id,
                    new_name = newScheduledtouch.new_name,
                    new_Subject = newScheduledtouch.new_Subject,
                    new_EmailBody = newScheduledtouch.new_EmailBody,
                    new_To = newScheduledtouch.new_To,
                    new_CCAllemail = newScheduledtouch.new_CCAllemail,
                    new_SendRepresentativeCopy = newScheduledtouch.new_SendRepresentativeCopy,
                    new_TriggerDate = targetDate,
                    new_When = newScheduledtouch.new_When,
                    new_ClassId = new EntityReference("new_class", targetClassId),
                    new_Sent = false
                };

                crmAccess.AddNewTouch(touch);

                //  Get Attachments for touch
                var templateAttachments = crmAccess.GetAnnotations(newScheduledtouch.new_scheduledtouchId.Value);

                foreach (Annotation templateAttachment in templateAttachments)
                {
                    crmAccess.CopyAnnotationToScheduledTouch(templateAttachment.AnnotationId.Value, id);
                }

            }

            var updatePeriods = crmAccess.GetClassUpdatePeriods(templateClassId);

            var startDate = (DateTime)newAfterCareStartDate.DbSelectedDate;
            var endDate = (DateTime)FirstEndDate.DbSelectedDate;
            var frequency = Convert.ToInt32(RepeatInterval.SelectedValue);
            var count = 0;
            foreach (new_classupdateperiod existingClassupdateperiod in updatePeriods)
            {
                var newClassUpdatePeriod = new new_classupdateperiod();

                if( count==0)
                {
                    newClassUpdatePeriod.new_Start = startDate;
                    newClassUpdatePeriod.new_End = endDate;
                }
                else
                {
                    newClassUpdatePeriod.new_Start = endDate.AddDays(((count-1) * frequency * 7)+1);
                    newClassUpdatePeriod.new_End = endDate.AddDays(count* frequency * 7);
                }

                newClassUpdatePeriod.new_ImpactQuestionId = existingClassupdateperiod.new_ImpactQuestionId;
                newClassUpdatePeriod.new_OnlyForDemo = existingClassupdateperiod.new_OnlyForDemo;
                newClassUpdatePeriod.new_name = existingClassupdateperiod.new_name;
                newClassUpdatePeriod.new_ClassId = new EntityReference("new_class", targetClassId);

                crmAccess.AddNewUpdatePeriod(newClassUpdatePeriod);
                count++;
            }

            var goalData = (DataTable)Session["ClassGoals"];

            foreach (DataRow row in goalData.Rows)
            {
                CreateNewClassGoalLink(targetClassId, new Guid(row[1].ToString()));
            }

            if( CopyResources.Checked)
            {
                var masterPage = (WideSiteMaster)Page.Master;
                var db = new SqlAzureLayer(masterPage.GetConnectionString());

                // Find custom Resources Page if one exists
                var result = db.GetHelp("Resources and Tools", templateClassId);

                if(result != " " )
                {
                    var createNewResources = string.Format(@"INSERT INTO [GrayMatters].[dbo].[ManagedContent]           
           [type]
           ,[content]
           ,[name]
           ,[classid])
     VALUES
           ,1
           ,{0}
           ,{1}
           ,{2})", result, "Resources and Tools", db.FormatGuid(targetClassId) );

                    db.SqlExecuteNonQuery(createNewResources);
                }
            }


            CopyClassPanel.Visible = false;
            ClassList.Items.Add(new ListItem(newclass.new_name, newClassId.ToString()));
            ClassList.SelectedValue = newClassId.ToString();
            ClassListChanged();
        }

        private void CreateNewClassGoalLink(Guid classId, Guid goalId)
        {
            // Create an AssociateEntities request.
            var request = new AssociateEntitiesRequest();

            request.Moniker1 = new EntityReference("new_class",  classId);
            request.Moniker2 = new EntityReference("new_classgoal",goalId);
            
            // Set the relationship name to associate on.
            request.RelationshipName = "new_class_classgoal";

            // Execute the request.
            crmAccess.OrgService.Execute(request);
        }

        protected void ScheduledTouches_ItemDataBound(object sender, GridItemEventArgs e)
        {
           
            if (e.Item is GridDataItem)
            {
                var item = (GridDataItem)e.Item;
                item["ID"].CssClass = "nodisplay";


                foreach (GridColumn column1 in e.Item.OwnerTableView.RenderColumns)
                {
                    if (column1.UniqueName == "ID")
                    {
                        column1.Visible = false;
                        break;
                    }
                }  

            }
            else
            if (e.Item is GridHeaderItem)
            {
                var item = (GridHeaderItem)e.Item;
                try
                {
                    item["ID"].CssClass = "nodisplay";
                }
                catch { }

            }
            else
                if (e.Item is GridFooterItem)
                {
                    var item = (GridFooterItem)e.Item;
                    try
                    {
                        item["ID"].CssClass = "nodisplay";
                    }
                    catch { }

                }
        }

      
        protected void ScheduledTouches_Command(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "AddDay")
            {

                if (Session["ScheduledTouches"] != null)
                {
                    var touchesData = (DataTable)Session["ScheduledTouches"];

                    var index = e.Item.ItemIndex;

                    touchesData.Rows[index][0] = ((DateTime)touchesData.Rows[index][0]).AddDays(1);
                    touchesData.Rows[index][1] = ((DateTime) touchesData.Rows[index][0]).DayOfWeek;

                    ScheduledTouches.DataSource = touchesData;
                    ScheduledTouches.DataBind();

                    crmAccess.UpdateTouchTrigger(new Guid(touchesData.Rows[index][4].ToString()), (bool)touchesData.Rows[index][3], (DateTime)touchesData.Rows[index][0]);
                    Session["ScheduledTouches"] = touchesData;
                }
            }
            else if (e.CommandName == "SubtractDay")
            {
               
                if (Session["ScheduledTouches"] != null)
                {
                    var touchesData = (DataTable)Session["ScheduledTouches"];

                    var index = e.Item.ItemIndex;

                    touchesData.Rows[index][0] = ((DateTime)touchesData.Rows[index][0]).AddDays(-1);
                    touchesData.Rows[index][1] = ((DateTime)touchesData.Rows[index][0]).DayOfWeek;

                    ScheduledTouches.DataSource = touchesData;
                    ScheduledTouches.DataBind();

                    crmAccess.UpdateTouchTrigger(new Guid(touchesData.Rows[index][4].ToString()), (bool)touchesData.Rows[index][3], (DateTime)touchesData.Rows[index][0]);
                    Session["ScheduledTouches"] = touchesData;
                }
            }
            else if (e.CommandName == "Delete")
            {

                if (Session["ScheduledTouches"] != null)
                {
                    var touchesData = (DataTable)Session["ScheduledTouches"];

                    var index = e.Item.ItemIndex;

                    var touchindex = (string) touchesData.Rows[index][4];

                    crmAccess.OrgService.Delete("new_scheduledtouch",new Guid(touchindex));

                    var classId = new Guid(ClassList.SelectedValue);

                    UpdateTouchGrid(classId);
                }
            }
        }

      
    }
}
