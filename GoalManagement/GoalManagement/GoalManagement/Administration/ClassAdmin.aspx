﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WideSite.Master" AutoEventWireup="true" CodeBehind="ClassAdmin.aspx.cs" Inherits="GoalManagement.Administration.ClassAdmin" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            text-align: right;
        }
        .style2
        {
            text-align: right;
            height: 23px;
        }
        .style3
        {
            height: 23px;
        }
        .style4
        {
            height: 23px;
            width: 160px;
        }
        .style5
        {
            width: 160px;
        }
        
        .header1
        {
             font-size: 1.5em;
    color: #2a3d70;
 
    text-transform: none;
    font-weight: 200;
    margin-bottom: 0px;
            }
            
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="HelpDiv" class="HelpScreen">
        <asp:Literal ID="HelpLiteral" runat="server"></asp:Literal>
    </div>
    
     
    <table>
    <tr>
    <td> <img src='/Images/help.gif' onmouseover="javascript:showObj('HelpDiv');" onmouseout="javascript:hideObj('HelpDiv');" />&nbsp;&nbsp;</td>
    <td><h1>Class Administration</h1></td>
    </tr>
    </table>
   


    <table>
        <tr>
            <td>
                Class&nbsp;
            </td>
            <td>
                <asp:DropDownList ID="ClassList" runat="server" OnSelectedIndexChanged="ClassList_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
                <br />
            </td>
            <td style=" padding-left:20px;">
                <asp:Button ID="CopyClass" runat="server" Text="Copy Selected Class" 
                    onclick="CopyClass_Click" />
             
            </td>
        </tr>  
        <tr>
        <td colspan="3">
            <asp:Panel ID="CopyClassPanel" runat="server" Visible="false">
            <br />
                <table style="background-color: #EEEEEE;">
                    <tr>
                        <td class="style1">
                            New Class Name&nbsp;
                        </td>
                        <td colspan="3">
                            &nbsp;<asp:TextBox ID="newClassName" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ControlToValidate="newClassName" Display="None" Enabled="False" 
                                ErrorMessage="New Class Name Required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            Class Date&nbsp;
                        </td>
                        <td class="style3">
                           
                            &nbsp;<telerik:RadDatePicker ID="newClassDate" runat="server">
                            </telerik:RadDatePicker>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="newClassDate"
                                Display="None" Enabled="False" ErrorMessage="Class Date Required"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            Update Periods Run
                        </td>
                        <td>
                            &nbsp;
                            <asp:DropDownList ID="StartDayofWeek" runat="server" AutoPostBack="true" OnSelectedIndexChanged="StartDayofWeek_SelectedIndexChanged">
                                <asp:ListItem Value="M">Monday</asp:ListItem>
                                <asp:ListItem Value="T">Tuesday</asp:ListItem>
                                <asp:ListItem Value="W">Wednesday</asp:ListItem>
                                <asp:ListItem Selected="True" Value="R">Thursday</asp:ListItem>
                                <asp:ListItem Value="F">Friday</asp:ListItem>
                            </asp:DropDownList>
                            to
                            <asp:DropDownList ID="EndDayofWeek" runat="server" Enabled="false">
                                <asp:ListItem Value="M">Monday</asp:ListItem>
                                <asp:ListItem Value="T">Tuesday</asp:ListItem>
                                <asp:ListItem Selected="True" Value="W">Wednesday</asp:ListItem>
                                <asp:ListItem Value="R">Thursday</asp:ListItem>
                                <asp:ListItem Value="F">Friday</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            Parent Account
                        </td>
                        <td>
                            &nbsp;
                            <asp:DropDownList ID="AccountList" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="style2">
                            Aftercare Start Date
                        </td>
                        <td class="style3">
                            &nbsp;
                            <telerik:RadDatePicker ID="newAfterCareStartDate" runat="server" 
                                AutoPostBack="True" 
                                onselecteddatechanged="newAfterCareStartDate_SelectedDateChanged">
                                <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" 
                                    ViewSelectorText="x">
                                </Calendar>
                                <DateInput AutoPostBack="True" DateFormat="M/d/yyyy" 
                                    DisplayDateFormat="M/d/yyyy">
                                </DateInput>
                                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="newAfterCareStartDate"
                                Display="None" Enabled="False" ErrorMessage="Aftercare Date Required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:CheckBox ID="CopyResources" runat="server" Text="Copy Resources and Tools" />
                            &nbsp; &nbsp;
                        </td>
                        <td>
                            Repeat Interval
                        </td>
                        <td>
                            &nbsp;
                            <asp:DropDownList ID="RepeatInterval" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="RepeatInterval_SelectedIndexChanged">
                                <asp:ListItem Selected="True" Value="2">2 Weeks</asp:ListItem>
                                <asp:ListItem Value="3">3 Weeks</asp:ListItem>
                                <asp:ListItem Value="4">4 Weeks</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>          <asp:Button ID="DoCopy" runat="server" Text="Copy" BackColor="#CCFFCC" 
                                Font-Bold="True" onclick="DoCopy_Click" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="Cancel" runat="server" Text="Cancel" BackColor="#FFFFCC" 
                                onclick="Cancel_Click" CausesValidation="False" />                   
                        </td>
                        <td>
                            First End Date
                        </td>
                        <td>
                            &nbsp;
                            <telerik:RadDatePicker ID="FirstEndDate" runat="server">
                            </telerik:RadDatePicker>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
            </asp:Panel>
        </td>
        </tr>
       </table>
       <br />
      
       <table>
       <tr>
       <td class="style2">Class Date</td>
       <td class="style4">
           &nbsp;
           <telerik:RadDatePicker ID="ClassDate" runat="server" Enabled="False">
           </telerik:RadDatePicker>
       </td>
      
               <td class="style2">
                   Aftercare Start Date
               </td>
               <td class="style3">
                  
                   &nbsp;
                  
                   <telerik:RadDatePicker ID="AftercareDate" runat="server" Enabled="False">
                   </telerik:RadDatePicker>
               </td>
       </tr>          
         
           <tr>
               <td class="style1">
                   Hide Peer Review
               </td>
               <td class="style5">
               
                   &nbsp;
               
                   <asp:CheckBox ID="HidePeerReview" runat="server" Enabled="False" />
               
               </td>
               <td> Hide Coaching Support </td>
               <td>
                   &nbsp;
                   <asp:CheckBox ID="HideCoachingSupport" runat="server" Enabled="False" />
               </td>
           </tr>
           <tr>
               <td class="style1">
                   Hide Resources and Tools
               </td>
               <td class="style5">
                   &nbsp;
                   <asp:CheckBox ID="HideResources" runat="server" Enabled="False" />
               </td>
               <td>
                   Class Complete
               </td>
               <td>
                   &nbsp;
                   <asp:CheckBox ID="ClassComplete" runat="server" Enabled="False" />
               </td>
           </tr>
       </table>
   
    <br />
    <br />
     
       Scheduled Touches
      <telerik:RadGrid AutoGenerateColumns="False" ID="ScheduledTouches" runat="server"
          GridLines="None" EnableLinqExpressions="False" Skin="Web20"
           ViewStateMode="Enabled" OnEditCommand="ScheduledTouches_EditCommand" OnItemDataBound="ScheduledTouches_ItemDataBound"
        
          OnUpdateCommand="ScheduledTouches_UpdateCommand" 
        OnCancelCommand="ScheduledTouches_CancelCommand" 
         OnItemCommand="ScheduledTouches_Command"
        >      
        <MasterTableView AutoGenerateColumns="false" EditMode="InPlace" AllowFilteringByColumn="False"
            ShowFooter="False" TableLayout="Auto">
    
<CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>
    
            <Columns>         
           
            <telerik:GridEditCommandColumn></telerik:GridEditCommandColumn>
            <telerik:GridButtonColumn Text="Delete" CommandName="Delete"></telerik:GridButtonColumn>
            <telerik:GridButtonColumn Text="+ Day" CommandName="AddDay"></telerik:GridButtonColumn>
             <telerik:GridButtonColumn Text="- Day" CommandName="SubtractDay"></telerik:GridButtonColumn>
                <telerik:GridDateTimeColumn DataField="TriggerDate" DataType="System.DateTime" PickerType="DatePicker"
                    HeaderText="Trigger Date" SortExpression="TriggerDate" UniqueName="TriggerDate"
                    DataFormatString="{0:M/dd/yyyy hh:mm tt}">
                </telerik:GridDateTimeColumn>
                <telerik:GridBoundColumn DataField="Day" HeaderText="Day" SortExpression="Day" UniqueName="Day"
                    ReadOnly="True">
                </telerik:GridBoundColumn>
                <telerik:GridCheckBoxColumn DataField="Sent" DataType="System.Boolean" HeaderText="Sent"
                    SortExpression="Sent" UniqueName="Sent">
                </telerik:GridCheckBoxColumn>

                <telerik:GridBoundColumn DataField="Name" HeaderText="Name" SortExpression="Name"
                    UniqueName="Name" ReadOnly="True">
                </telerik:GridBoundColumn>        
              
          
                <telerik:GridBoundColumn DataField="ID" HeaderText="ID" SortExpression="ID" UniqueName="ID"
                    ReadOnly="False" >
                </telerik:GridBoundColumn>
            </Columns>           

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
        </MasterTableView>      

<FilterMenu EnableImageSprites="False"></FilterMenu>

        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Web20">
        </HeaderContextMenu>
    </telerik:RadGrid>
    <br />
    <br />
    UpdatePeriods <br />
    <telerik:RadGrid AutoGenerateColumns="False" ID="UpdatePeriods" runat="server" GridLines="None"
        EnableLinqExpressions="False" Skin="Web20" AllowAutomaticUpdates="False" OnItemDataBound="ScheduledTouches_ItemDataBound"
        OnEditCommand="UpdatePeriods_EditCommand" OnUpdateCommand="UpdatePeriods_UpdateCommand"
        OnCancelCommand="UpdatePeriods_CancelCommand" ViewStateMode="Enabled">
        <MasterTableView AutoGenerateColumns="false" EditMode="InPlace" AllowFilteringByColumn="False"
            ShowFooter="False" TableLayout="Auto">
            <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridEditCommandColumn> </telerik:GridEditCommandColumn>
                <telerik:GridBoundColumn DataField="Name" HeaderText="Name" SortExpression="Name"
                    UniqueName="Name" ReadOnly="True">
                </telerik:GridBoundColumn>
                <telerik:GridDateTimeColumn DataField="StartDate" DataType="System.DateTime" PickerType="DatePicker"
                    HeaderText="Start Date" SortExpression="StartDate" UniqueName="StartDate"
                     DataFormatString="{0:M/dd/yyyy}">
                </telerik:GridDateTimeColumn>
                <telerik:GridDateTimeColumn DataField="EndDate" DataType="System.DateTime" PickerType="DatePicker"
                    HeaderText="End Date" SortExpression="EndDate" UniqueName="EndDate" DataFormatString="{0:M/dd/yyyy}">
                </telerik:GridDateTimeColumn>             
               
                <telerik:GridBoundColumn DataField="ImpactQuestion" HeaderText="ImpactQuestion" SortExpression="ImpactQuestion"
                    UniqueName="ImpactQuestion" ReadOnly="True">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ID" HeaderText="ID" SortExpression="ID" UniqueName="ID"
                    ReadOnly="False">
                </telerik:GridBoundColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Web20">
        </HeaderContextMenu>
    </telerik:RadGrid>
    <br />
    <br />
    Class Goals
    <telerik:RadGrid ID="ClassGoalsGrid" runat="server" Skin="Web20" OnItemDataBound="ScheduledTouches_ItemDataBound">
    </telerik:RadGrid>
</asp:Content>
