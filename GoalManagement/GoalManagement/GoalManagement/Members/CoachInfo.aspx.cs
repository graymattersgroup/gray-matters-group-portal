﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Telerik.Web.UI;

namespace GoalManagement
{
    public partial class CoachInfo : System.Web.UI.Page
    {
        protected CRMAccess crmAccess;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MembershipUser currentUser = Membership.GetUser();

                if ( currentUser == null) 
                    Response.Redirect("../Default.aspx");

                if (Session["ImpersonateUser"] != null)
                {
                    var otherUser = (string)Session["ImpersonateUser"];
                    currentUser = Membership.GetUser(otherUser);

                    var masterPage1 = (SiteMaster)Page.Master;
                    masterPage1.SetImpersonation(otherUser);
                }

                var myEmail = currentUser.Email;
                Session["userEmail"] = myEmail;

                var treeview = Master.FindControl("TreeView1") as TreeView;
                treeview.Visible = true;

                if (Session["crmAccess"] == null)
                {
                    crmAccess = new CRMAccess();
                    Session["crmAccess"] = crmAccess;
                }
                else
                {
                    crmAccess = (CRMAccess)Session["crmAccess"];
                }

                var myContact = crmAccess.GetStudent(myEmail);

                var myCoachId = myContact.new_MyCoachId;

                if (myCoachId == null)
                {
                    CoachName.Text = "No Coach Assigned";
                    CoachEmail.Text = "none";
                    CoachPhone.Text = "(xxx) xxx-xxxx";
                }
                else
                {
                    var myCoach = crmAccess.GetMyCoach(myCoachId.Id);

                    CoachName.Text = String.Format("{0} {1}", myCoach.FirstName, myCoach.LastName);

                    if (myCoach.EMailAddress1 != null)
                    {
                        CoachEmail.Text = myCoach.EMailAddress1;
                        CoachEmail.NavigateUrl = string.Format("mailto:{0}", myCoach.EMailAddress1);
                    }
                    else
                    {
                        CoachEmail.Text = "---------------";
                    }

                    if (myCoach.Address1_Telephone1 != null)
                    {
                        CoachPhone.Text = myCoach.Address1_Telephone1;
                    }
                    else
                    {
                        CoachPhone.Text = "(xxx) xxx-xxxx";
                    }


                    if (myCoach.Address1_Line1 != null)
                    {
                        AddressLine1.Text = myCoach.Address1_Line1;
                        AddressPanel.Visible = true;
                    }

                    if (myCoach.Address1_City != null)
                    {
                        City.Text = myCoach.Address1_City;
                        AddressPanel.Visible = true;
                    }

                    if (myCoach.Address1_StateOrProvince != null)
                    {
                        State.Text = myCoach.Address1_StateOrProvince;
                        AddressPanel.Visible = true;
                    }
                    if (myCoach.Address1_PostalCode != null)
                    {
                        Zip.Text = myCoach.Address1_PostalCode;
                        AddressPanel.Visible = true;
                    }

                }


                var masterPage = (SiteMaster)Page.Master;
                masterPage.SetMenu(CentralizedData.CoachInfo);
                
            }
        }
    }
}