﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.Services.Protocols;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using Telerik.Reporting.Processing;
using Telerik.Web.UI;
using TelerikReporting;
using TableRow = System.Web.UI.WebControls.TableRow;

namespace GoalManagement
{
    public partial class Goals : System.Web.UI.Page
    {
        protected CRMAccess crmAccess;
        protected Contact studentInfo;
       
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["crmAccess"] == null)
            {
                crmAccess = new CRMAccess();
                Session["crmAccess"] = crmAccess;

                MembershipUser currentUser = Membership.GetUser();
                if (currentUser == null) Response.Redirect("../Default.aspx");

                if (Session["ImpersonateUser"] != null)
                {
                    var otherUser = (string)Session["ImpersonateUser"];
                    currentUser = Membership.GetUser(otherUser);

                    var masterPage1 = (SiteMaster)Page.Master;
                    masterPage1.SetImpersonation(otherUser);
                }

                Session["userEmail"] = currentUser.Email;
                var myEmail = currentUser.Email;
                studentInfo = crmAccess.GetStudent(myEmail);
               
                Session["StudentInfo"] = studentInfo;

                var classes = crmAccess.GetStudentClasses(studentInfo.ContactId.Value).ToList();
                var coachingClasses = crmAccess.GetCoachClasses(studentInfo.ContactId.Value).ToList();

                Session["CoachingClasses"] = coachingClasses;
                Session["MyClasses"] = classes;
            }
            else
            {
                crmAccess = (CRMAccess)Session["crmAccess"];
                studentInfo = (Contact)Session["StudentInfo"];

                MembershipUser currentUser = Membership.GetUser();
                
                if (Session["ImpersonateUser"] != null)
                {
                    var otherUser = (string)Session["ImpersonateUser"];
                    currentUser = Membership.GetUser(otherUser);

                    var masterPage1 = (SiteMaster)Page.Master;
                    masterPage1.SetImpersonation(otherUser);
                }

                if (currentUser == null)
                    Response.Redirect("../Default.aspx");
                else
                    Session["userEmail"] = currentUser.Email;

                //var classes = crmAccess.GetStudentClasses(studentInfo.ContactId.Value).ToList();
                //Session["MyClasses"] = classes;
            }


            if (!IsPostBack)
            {
                var treeview = Master.FindControl("TreeView1") as TreeView;
                treeview.Visible = true;

                var classes = (List<new_class>)Session["MyClasses"];

                if (Session["StudentClassId"] != null)
                {
                    Session["ClassId"] = Session["StudentClassId"];

                    var thisId = (Guid) Session["ClassId"];
                    foreach (new_class newClass in classes)
                    {
                        if (thisId == newClass.new_classId.Value)
                        {
                            ClassTitle.Text =  string.Format("{0}<br/>", newClass.new_name);
                            break;
                        }
                    }
                }
                else if (Session["ClassId"] == null)
                {
                    Session["ClassId"] = classes[0].new_classId.Value;
                }

                var classId = (Guid)Session["ClassId"];
                
                SetClassIcon(classId, studentInfo.ParentCustomerId.Id);
  
                Boolean classFound = false;

                var required = 0;
                foreach (new_class newClass in classes)
                {
                    if (classId == newClass.new_classId.Value)
                    {
                        required = newClass.new_GoalsRequired.Value;
                        classFound = true;
                        break;
                    }
                }

                var masterPage = (SiteMaster)Page.Master;
                var goalsheetCreated = ((Session["isGoalSheetCreated"] != null) ? (bool)Session["isGoalSheetCreated"] : false);
                masterPage.SetMenu(goalsheetCreated ? CentralizedData.MyGoalsName[1] : CentralizedData.MyGoalsName[0]);
                var db = new SqlAzureLayer(masterPage.GetConnectionString());
                var result = db.GetDataTable("select content from [dbo].[ManagedContent] where name ='Goal Commitments Help' ");
                if (result != null && result.Rows.Count > 0)
                {
                    HelpLiteral.Text = result.Rows[0][0].ToString();
                }


                if (!classFound)
                {
                    MainTitle.Text = "You are not currently associated with an active AfterCare Program.";
                    return;
                }
                

                GetClassGoals(classId);
                
                Session["ClassId"] = classId;
                Session["StudentId"] = studentInfo.ContactId.Value;
                Session["crmAccess"] = crmAccess;
                Session["GoalPage"] = 0;
                Session["RequiredGoals"] = required;
               
                GetStudentSelections();
                PresentGoals(0, true);
            }
            else
            {
                var page = (int)Session["GoalPage"];
                PresentGoals(page, false);
            }

        }

      

        //protected void SetMenu(bool goalsheetCreated)
        //{

        //    var thisPage = Master.FindControl("PageTitle") as Label;
        //    var myMenu = Master.FindControl("TreeView1") as TreeView;
        //    myMenu.Nodes.Clear();
        //    myMenu.Nodes.Add(new TreeNode("AfterCare Home", null, null, "~/Members/ProfileHome.aspx", "_self"));
        //    var commitment = new TreeNode("Create My Goals", null, null, "~/Members/Goals.aspx", "_self") { Selected = true };
        //    myMenu.Nodes.Add(commitment);
        //    if (goalsheetCreated) myMenu.Nodes.Add(new TreeNode("Goal Updates", null, null, "~/Members/GoalUpdates.aspx", "_self"));
        //    myMenu.Nodes.Add(new TreeNode("Resources & Tools", null, null, "~/Members/Resources.aspx", "_self"));
        //}

        private void SetClassIcon(Guid classId, Guid accountId)
        {
            var logoImage = Master.FindControl("RadBinaryImage1") as RadBinaryImage;
            try
            {
                if (Session["Logo"] != null)
                {
                    logoImage.DataValue = (byte[])Session["Logo"];
                    logoImage.Visible = true;
                }
                else
                {
                    var logo = crmAccess.GetClassAnnotation(accountId);
                    if (logo.FirstOrDefault() == null) logo = crmAccess.GetClassAnnotation(classId);
                    if (logo.FirstOrDefault() != null)
                    {
                        logoImage.Visible = true;
                        logoImage.DataValue = Convert.FromBase64String(logo.FirstOrDefault().DocumentBody);
                    }
                    else
                    {
                        logoImage.Visible = false;
                    }
                }

            }
            catch
            {
                logoImage.Visible = false;
            }
        }

        protected void GoalsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var required = (int)Session["RequiredGoals"];
            var count = 0;

            foreach (ListItem item in GoalsList.Items)
            {
                if (item.Selected)
                {
                    count++;
                    item.Attributes["style"] = "font-weight:bold;";
                }
                else
                {
                    item.Attributes["style"] = "";
                }
            }
            
            SelectedRequired.Text = string.Format("Select Goals - Selected = {0}, Minimum = {1}", count, required);
            NextButton.Enabled = (count >= required);
        }

        protected void ActivityList_SelectedIndexChanged(Object sender, EventArgs e)
        {
            var min = (int)Session["MinActivities"];
            var max = (int)Session["MaxActivities"];
            
            var checkboxList = (List<string>)Session["checkboxIDS"];
            var selectedCount = 0;

            foreach (string acheckList in checkboxList)
            {
                var chList = (CheckBoxList)FindControlRecursive(ActivityTable, acheckList);
                foreach (ListItem item in chList.Items)
                {
                    if (item.Selected)
                    {
                        selectedCount++;
                        item.Attributes["style"] = "";
                    }
                    else
                    {
                        item.Attributes["style"] = "color:#AAAAAA;";
                    }
                }
            }

            SelectedRequired.Text = string.Format("Select Activities - Selected = {0}, Minimum = {1}, Maximum = {2}", selectedCount, min, max);

            NextButton.Enabled = (selectedCount >= min && selectedCount <= max);
        }


     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Root"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Control FindControlRecursive(Control Root, string Id)
        {
            if (Root.ID == Id) return Root;

            return (from Control Ctl in Root.Controls select FindControlRecursive(Ctl, Id)).FirstOrDefault(FoundCtl => FoundCtl != null);
        }

        protected void NextButton_Click(object sender, EventArgs e)
        {
            var classGoals = (ClassGoals)Session["Goals"];
            var classGoalPages = classGoals.classGoals;
            var currentPage = (int)Session["GoalPage"];
            crmAccess = (CRMAccess)Session["crmAccess"];
            PrevButton.Visible = true;

            // If Goal Selection Page
           
            if (currentPage == 0)
            {
                var goalIds = new List<Guid>();

                foreach (ListItem goal in GoalsList.Items)
                {
                    foreach (ClassGoal classGoalPage in classGoalPages)
                    {
                        if (classGoalPage.id == new Guid(goal.Value))
                        {
                            classGoalPage.selected = goal.Selected;
                            break;
                        }
                    }

                    if(goal.Selected) goalIds.Add(new Guid(goal.Value));
                }

            //    BuildLists2(1);

                Session["myGoals"] = goalIds;
                
                // Create/Delete Student Goals
                UpdateStudentGoals(goalIds);
                Session["GoalPage"] = ++currentPage;
                // Present New Information
                PresentGoals(currentPage, true);
            }
            else
            {
              UpdateStudentActivities(currentPage);

                Session["GoalPage"] = ++currentPage;
                // Present New Information
             //   BuildLists2(currentPage);

                PresentGoals(currentPage, true);
            }
            PrevButton.Enabled = true;
        }

        protected void PrevButton_Click(object sender, EventArgs e)
        {
            var currentPage = (int)Session["GoalPage"];

            UpdateStudentActivities(currentPage);

            Session["GoalPage"] = --currentPage;

            // Present New Information
            PresentGoals(currentPage, true);

            if (currentPage == 0)PrevButton.Visible = false;

            NextButton.Visible = true;
        }

        private void UpdateStudentActivities(int currentPage)
        {
            var classGoals = (ClassGoals)Session["Goals"];
            var classGoalPages = classGoals.classGoals;
           
            var count = 1;

            foreach (ClassGoal goalPage in classGoalPages)
            {
                if (!goalPage.selected) continue;
                
                if (count == currentPage)
                {
                    var checkboxList = (List<string>)Session["checkboxIDS"];
                  
                    //goalPage.payoff = PayoffText.Text;

                    bool isActivity;

                    foreach (string acheckList in checkboxList)
                    {
                        var chList = (CheckBoxList) FindControlRecursive(ActivityTable, acheckList);

                        isActivity = !(chList.ID.StartsWith("Sub"));

                        foreach (ListItem item in chList.Items)
                        {
                            var thisId = new Guid(item.Value);

                            if (isActivity)
                            {
                                foreach (Activity thisActivity in goalPage.activities)
                                {
                                    if(thisActivity.id == thisId)
                                    {
                                        thisActivity.selected = item.Selected;
                                    }
                                }
                            }
                            else
                            {
                                foreach (Activity thisActivity in goalPage.activities)
                                {
                                    foreach (SubActivity subActivity in thisActivity.subActivities)
                                    {
                                        if (subActivity.id == thisId)
                                        {
                                            subActivity.selected = item.Selected;
                                        }
                                    }
                                }
                            }

                        }
                    }
                    
                    break;
                }
                count++;
            }

            Session["classGoalPages"] = classGoalPages;
        }

        private void UpdateStudentGoals(List<Guid> selectedGoals)
        {
            crmAccess = (CRMAccess)Session["crmAccess"];
            var studentId = (Guid)Session["StudentId"];
            var classId = (Guid)Session["ClassId"];
            var classGoals = (ClassGoals)Session["Goals"];
            var classGoalPages = classGoals.classGoals;

            List<string> goalName = new List<string>();
            foreach (Guid selectedGoal in selectedGoals)
            {
                foreach (ClassGoal goalPage in classGoalPages)
                {
                     if( selectedGoal == goalPage.id)
                     {
                         goalName.Add(goalPage.name);
                     }
                }
               
            }

            crmAccess.UpdateStudentGoals(selectedGoals, goalName, classId, studentId);
        }

        private void GetStudentSelections()
        {
            crmAccess = (CRMAccess)Session["crmAccess"];
            var studentId = (Guid)Session["StudentId"];
            var classId = (Guid)Session["ClassId"];
            
            // All Goals
            var classgoals = (ClassGoals)Session["Goals"];
            // Selected Goals
            var selectedGoals = crmAccess.GetStudentGoals(classId, studentId);

           // Loop through all Goals and create a list of all Goals and activities
            foreach (ClassGoal classGoal in classgoals.classGoals)
            {
                classGoal.selected = false;
                classGoal.min = classGoal.min ?? 0;
                classGoal.max = classGoal.max ?? 0;

                if (classGoal.isdefault.HasValue)
                {
                    classGoal.selected = (bool)classGoal.isdefault;
                }

                foreach (new_studentgoals studentgoal in selectedGoals)
                {
                    if (classGoal.id == studentgoal.new_classgoalId.Id)
                    {
                        classGoal.selected = true;
                        classGoal.initialselected = true;
                        classGoal.payoff = studentgoal.new_Benefit;
                        classGoal.initialpayoff = studentgoal.new_Benefit;
                        classGoal.isComplete = studentgoal.new_IsComplete??false;

                        var selectedGoalActivities = crmAccess.GetStudentGoalActivities(classId, studentId, classGoal.id).ToList();

                        // If there are no selected activities than this is all subactivities
                        if (selectedGoalActivities.Count != 0)

                            foreach (new_goalactivity selectedGoalActivity in selectedGoalActivities)
                            {
                                foreach (Activity anActivity in classGoal.activities)
                                {
                                    if (selectedGoalActivity.new_goalactivityId.Value == anActivity.id)
                                    {
                                        anActivity.selected = true;
                                        anActivity.initialselected = true;
                                    }
                                }
                            }
                        else
                        {
                            var selectedGoalSubActivities = crmAccess.GetStudentGoalSubActivities(classId, studentId, classGoal.id).ToList();

                            foreach (new_subactivity selectedGoalSubActivity in selectedGoalSubActivities)
                            {
                                foreach (Activity anActivity in classGoal.activities)
                                {
                                    foreach (SubActivity aSubActivity in anActivity.subActivities)
                                    {
                                        if (aSubActivity.id == selectedGoalSubActivity.new_subactivityId.Value)
                                        {
                                            aSubActivity.selected = true;
                                            aSubActivity.initialselected = true;
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }

            Session["Goals"] = classgoals;
        }

        private void GetClassGoals(Guid classId)
        {
            crmAccess = (CRMAccess)Session["crmAccess"];
            var sql = new SqlAzureLayer(GetConnectionString());
            var blob = sql.GetGoalBlob(classId);

            if (blob == null)
            {
                // Get all Students
                var goals = crmAccess.GetClassGoals(classId).ToList();
                
                blob = new ClassGoals();

                foreach (ClassGoal classGoal in goals)
                {
                    if (classGoal.isdefault??false) 
                        classGoal.selected = true;

                    var activities = crmAccess.GetGoalActivities(classGoal.id).ToList();

                    foreach (Activity newGoalactivity in activities)
                    {
                        var subactivities = crmAccess.GetSubActivities(newGoalactivity.id).ToList();

                        foreach (SubActivity subActivity in subactivities)
                        {
                             newGoalactivity.subActivities.Add(subActivity);
                        }
                        classGoal.activities.Add(newGoalactivity);
                    }

                    blob.classGoals.Add(classGoal);
                }

                blob.classid = classId;
                blob.lastupdated = DateTime.Now;
                sql.UpdateGoalBlob(blob);

            }
            Session["Goals"] = blob;
        }


    
        //private void BuildLists2(int page)
        //{
        //    var classGoals = (ClassGoals)Session["Goals"];
        //    var classGoalPages = classGoals.classGoals;
        //    crmAccess = (CRMAccess)Session["crmAccess"];
        //    var studentId = (Guid)Session["StudentId"];
        //    var classId = (Guid)Session["ClassId"];

        //    var loadedGoals = (List<Guid>)Session["loadedGoals"];

        //    var count = 1;
        //    // Loop through all Goals and create a list of all Goals and activities
        //    foreach (ClassGoal newClassgoal in classGoalPages)
        //    {
        //        if (!newClassgoal.selected) continue;

        //        if (count != page)
        //        {
        //            count++;
        //            continue;
        //        }

        //        if (loadedGoals.Contains(newClassgoal.id)) return;

        //        var selectedGoalActivities = crmAccess.GetStudentGoalActivities(classId, studentId, newClassgoal.id).ToList();
                
        //        // If there are no selected activities than this is all subactivities
        //        if (selectedGoalActivities.Count != 0)

        //            foreach (new_goalactivity selectedGoalActivity in selectedGoalActivities)
        //            {
        //                foreach (Activity anActivity in newClassgoal.activities)
        //                {
        //                    if (selectedGoalActivity.new_goalactivityId.Value == anActivity.id)
        //                    {
        //                        anActivity.selected = true;
        //                        anActivity.initialselected = true;
        //                    }
        //                }
        //            }
        //        else
        //        {
        //            var selectedGoalSubActivities = crmAccess.GetStudentGoalSubActivities(classId, studentId, newClassgoal.id).ToList();

        //            foreach (new_subactivity selectedGoalSubActivity in selectedGoalSubActivities)
        //            {
        //                foreach (Activity anActivity in newClassgoal.activities)
        //                {
        //                    foreach (SubActivity aSubActivity in anActivity.subActivities)
        //                    {
        //                        if (aSubActivity.id == selectedGoalSubActivity.new_subactivityId.Value)
        //                        {
        //                            aSubActivity.selected = true;
        //                            aSubActivity.initialselected = true;
        //                        }
        //                    }
        //                }
        //            }
        //        }
               

        //        loadedGoals.Add(newClassgoal.id);
        //    }

        //    Session["loadedGoals"] = loadedGoals;
        //    Session["classGoalPages"] = classGoalPages;
        //}


        private void PresentGoals(int currentPage, bool isInitialize)
        {
            crmAccess = (CRMAccess)Session["crmAccess"];
            var classGoals = (ClassGoals)Session["Goals"];
            var classGoalPages = classGoals.classGoals;
            
            GoalPanel.Visible = (currentPage == 0);
            ActivityPanel.Visible = (currentPage != 0);

            if (currentPage == 0)
            {
                // Display Goals Page
                if (isInitialize)
                {
                    var required = (int)Session["RequiredGoals"];
                    var count = 0;
                    
                    GoalsList.Items.Clear();
                    foreach (ClassGoal newGoal in classGoalPages)
                    {
                        var goalitem = new ListItem(" " + newGoal.name, newGoal.id.ToString())
                                           {
                                               Selected = newGoal.selected
                                           };

                        if( newGoal.isdefault??false )
                        {
                            goalitem.Text += " (Required)";
                            goalitem.Enabled = false;
                        }

                        if ( newGoal.isComplete)
                        {
                            goalitem.Text +=  " (Complete)";
                            goalitem.Enabled = false;
                            newGoal.selected = false;
                        }

                        if (goalitem.Selected)
                        {
                            goalitem.Attributes["style"] = "font-weight:bold;";
                            //var newButton = new ImageButton {ID = "GO" + count};
                            //newButton.ImageUrl = "/Images/Goals.gif";
                            //newButton.Click +=newButton_Click;
                            ////newButton.Height = 17;
                            //GoalEditPanel.Controls.Add(newButton);
                        }
                        //else
                        //{
                        //    var newButton = new ImageButton();
                        //    newButton.ImageUrl = "/Images/NoGoals.gif";
                        //    //newButton.Height = 17;
                        //    GoalEditPanel.Controls.Add(newButton);
                        //}
                     
                        //    var verticalSpace = new Literal();
                        //    verticalSpace.Text = "&nbsp;<br/>";
                        //    GoalEditPanel.Controls.Add(verticalSpace);
                        


                      
                        GoalsList.Items.Add(goalitem);
                        if( newGoal.selected) count++;
                    }

                    SelectedRequired.Text = string.Format("Select Goals - Selected = {0}, Minimum = {1}", count,required);
                    MainTitle.Text = "Goal Commitments";
                    NextButton.Enabled = (count >= required); 
                }
                SaveWork.Visible = false;
            }
            else
            {
                var count = 1;
                var selectedCount = 0;
                var thisGoal = new ClassGoal() { name = "Done" };
                var goalIds = (List<Guid>)Session["myGoals"];
                var activityCount = 0;
                var checkboxList = new List<CheckBoxList>();

                ActivityTable.Rows.Clear();

                var checkboxIdList = new List<string>();

                foreach (ClassGoal newGoal in classGoalPages)
                {
                    if (newGoal.selected)
                    {
                        if (currentPage == count)
                        {
                            thisGoal = newGoal;
                            //if (isInitialize) PayoffText.Text = newGoal.payoff;
                          
                            var maincheckboxList = new CheckBoxList
                            {
                                ID = "Activities",
                                AutoPostBack = true,
                                CellSpacing = 7,
                                CssClass = "chkListStyle",

                            };

                            maincheckboxList.Items.Clear();

                            var subactivityNameCount = 1;

                            foreach (Activity newGoalactivity in newGoal.activities)
                            {
                                var newRow = new TableRow();
                                var leftCell = new TableCell();

                                if (newGoalactivity.subActivities.Count == 0)
                                {
                                    maincheckboxList.SelectedIndexChanged += ActivityList_SelectedIndexChanged;

                                    var newItem = new ListItem(newGoalactivity.name, newGoalactivity.id.ToString())
                                                      {Selected = newGoalactivity.selected};
                                    
                                    if (newItem.Selected)
                                    {
                                        selectedCount++;
                                        newItem.Attributes["style"] = "";
                                    }
                                    else
                                    {
                                        newItem.Attributes["style"] = "color:#AAAAAA;";
                                    }

                                    if (!maincheckboxList.Items.Contains(newItem))
                                    {
                                        maincheckboxList.Items.Add(newItem);
                                    }
                                    activityCount++;

                                    if (activityCount == 1)
                                    {
                                        checkboxIdList.Add(maincheckboxList.ID);
                                        checkboxList.Add(maincheckboxList);
                                        leftCell.Controls.Add(maincheckboxList);
                                        newRow.Cells.Add(leftCell);
                                        ActivityTable.Rows.Add(newRow);
                                    }
                                }
                                else
                                {
                                   
                                    var newHeader = new Label { Text = newGoalactivity.name };
                                    leftCell.Controls.Add(newHeader);
                                    leftCell.CssClass = "activityHeader";
                                    leftCell.ColumnSpan = 2;
                                    newRow.Cells.Add(leftCell);
                                    ActivityTable.Rows.Add(newRow);
                                    var subRow = new TableRow();
                                    var subCell = new TableCell();
                                    var newcheckboxList = new CheckBoxList
                                                              {
                                                                  ID = "Subactivity" + subactivityNameCount++,
                                                                  AutoPostBack = true,
                                                                  CellSpacing = 7,
                                                                  CssClass = "chkListStyle",
                                                              };
                                    
                                 
                                    newcheckboxList.Items.Clear();
                                    newcheckboxList.SelectedIndexChanged += ActivityList_SelectedIndexChanged;

                                    checkboxIdList.Add(newcheckboxList.ID);
                                    foreach (SubActivity newSubactivity in newGoalactivity.subActivities)
                                    {
                                        var newItem = new ListItem(newSubactivity.name, newSubactivity.id.ToString())
                                                          {
                                                              Selected = newSubactivity.selected
                                                          };
                                        
                                        if (newItem.Selected)
                                        {
                                            selectedCount++;
                                            newItem.Attributes["style"] = "";
                                        }
                                        else
                                        {
                                            newItem.Attributes["style"] = "color:#AAAAAA;";
                                        }

                                        newcheckboxList.Items.Add(newItem);
                                        activityCount++;
                                    }

                                    checkboxList.Add(newcheckboxList);
                                    subCell.Controls.Add(newcheckboxList);
                                    subCell.CssClass = "activitycheckbox";
                                    subRow.Cells.Add(subCell);
                                    ActivityTable.Rows.Add(subRow);
                                }
                            }
                            if (isInitialize)
                            {
                                Session["checkboxIDS"] = checkboxIdList;
                                Session["CheckboxList"] = checkboxList;
                            }
                            break;
                        }
                        count++;
                    }

                }
                if (thisGoal.name == "Done")
                {
                    GoalPanel.Visible = false;
                    ActivityPanel.Visible = false;
                    MainTitle.Text = "Goal Commitments Selected. ";
                    SelectedRequired.Text = "Save your changes and you will be ready for Goal Updates. <br/>You can return and revise your Goals and activities at a later time. ";
                    Finish.Visible = true;
                    Finish.Enabled = true;
                    SaveWork.Visible = false;
                    ViewCommitments.Visible = true;
                    ViewCommitments.Enabled = true;
                    NextButton.Visible = false;
                }
                else
                {
                   
                    SaveWork.Visible = true;
                    Finish.Visible = false;
                    Finish.Enabled = false;
                    ViewCommitments.Visible = false;
                    ViewCommitments.Enabled = false;
                    MainTitle.Text = "Goal: " + thisGoal.name;
                    var min = thisGoal.min;
                    var max = (thisGoal.max == 0) ? activityCount : thisGoal.max;
                    Session["MinActivities"] = min;
                    Session["MaxActivities"] = max;
                    Session["SelectedCount"] = selectedCount;


                    SelectedRequired.Text = string.Format("Select Activities - Selected = {0}, Minimum = {1}, Maximum = {2}", selectedCount, min, max);
                    NextButton.Enabled = (selectedCount >= min && selectedCount <= max);
                }

            }

        }

        void newButton_Click(object sender, ImageClickEventArgs e)
        {
            var thisbutton = (ImageButton) sender;

            var text = thisbutton.ID.Substring(2);

            var index = Convert.ToInt32(text);
        }


        protected void Finish_Click(object sender, EventArgs e)
        {
            var currentPage = (int)Session["GoalPage"];
            UpdateStudentActivities(currentPage);

            //Create Class Status Record
            crmAccess = (CRMAccess)Session["crmAccess"];
            var studentId = (Guid)Session["StudentId"];
            var classId = (Guid)Session["ClassId"];

            var classGoalPages = (List<ClassGoal>)Session["classGoalPages"];
         
            // Update Database
            crmAccess.UpdateStudentGoal(classGoalPages, studentId, classId);
            crmAccess.WriteClassStatus(studentId, classId);
            
            var myContact = (Contact)Session["StudentInfo"];
            
            RenderingResult result = ReportHelper.GetTelerikGoalReport(classGoalPages, myContact);
            
            var reportbytearray = result.DocumentBytes;
            
            var toemailaddress = Session["userEmail"].ToString();
            var fromemailaddress = "RxAssist@graymattersgroup.com";
            var subject = "Your Goal Commitments";
            var filename = "mygoals";
            var text = "Attached are your goal commitments.";

            EmailNotifications.sendEmailAll(toemailaddress, fromemailaddress, subject, text, reportbytearray, filename,  false, false);

          
            Response.Redirect("~/Members/ProfileHome.aspx");
        }

        protected void SaveWork_Click(object sender, EventArgs e)
        {
            //Create Class Status Record
            crmAccess = (CRMAccess)Session["crmAccess"];

            try
            {
                var currentPage = (int) Session["GoalPage"];

                UpdateStudentActivities(currentPage);

                // Present New Information
                PresentGoals(currentPage, true);
            }
            catch (SoapException ex)
            {
                EmailNotifications.SendExceptionMessage("Gray Matters", "SaveWork_Click UpdateStudentActivities ", ex);
            }
            catch (Exception ex)
            {
                EmailNotifications.SendExceptionMessage("Gray Matters", "SaveWork_Click UpdateStudentActivities ", ex);
            }

            try
            {
            var studentId = (Guid)Session["StudentId"];
            var classId = (Guid)Session["ClassId"];

            var classGoalPages = (List<ClassGoal>)Session["classGoalPages"];

            // Update Database
            crmAccess.UpdateStudentGoal(classGoalPages, studentId, classId);
            }
            catch (SoapException ex)
            {
                EmailNotifications.SendExceptionMessage("Gray Matters", "SaveWork_Click UpdateStudentGoal", ex);
            }
            catch (Exception ex)
            {
                EmailNotifications.SendExceptionMessage("Gray Matters", "SaveWork_Click UpdateStudentGoal", ex);
            }

           
        }
        

        private string GetConnectionString()
        {
               
           var connectionString2 = ConfigurationManager.ConnectionStrings["Production"].ConnectionString;

           return connectionString2;
        }

        protected void ViewCommitments_Click(object sender, EventArgs e)
        {
            StudentName.Text = "My Goals";
            var classGoalPages = (List<ClassGoal>)Session["classGoalPages"];
            StudentHistoryLiteral.Text = ReportHelper.ViewMyCommitments(classGoalPages);
            studentPanel.Visible = true;
        }
        
        //public string ViewMyCommitments()
        //{
        //    var classGoalPages = (List<ClassGoal>)Session["classGoalPages"];

        //    var sb = new StringBuilder(256);

        //    // Generate Table to send to Telerik report engine to generate an email with PDF attachment to the student.
        //    foreach (ClassGoal goalPage in classGoalPages)
        //    {
        //        if (!goalPage.selected) continue;

        //        sb.AppendFormat("<H2>{0}</H2><br/>", goalPage.name);
        //        sb.AppendFormat("<b>Payoff:</b>  {0}<br/><br/>", goalPage.payoff);

        //        sb.AppendFormat("<b>Activities:</b><br/>");
        //        sb.AppendFormat("<ul>");
        //        foreach (Activity newGoalactivity in goalPage.activities)
        //        {
        //            if (!newGoalactivity.selected && newGoalactivity.subActivities.Count == 0) continue;

        //            if (newGoalactivity.subActivities.Count != 0)
        //            {
        //                foreach (SubActivity newSubactivity in newGoalactivity.subActivities)
        //                {
        //                    if (newSubactivity.selected)
        //                    {
        //                        sb.AppendFormat("<li>{0}</li>", newSubactivity.name);
        //                    }

        //                }
        //            }
        //            else if (newGoalactivity.selected)
        //            {
        //                sb.AppendFormat("<li>{0}</li>", newGoalactivity.name);
        //            }
        //        }
        //        sb.AppendFormat("</ul>");
        //        sb.AppendFormat("<hr/>");
        //        sb.AppendFormat("<br/>");
        //    }

        //    return sb.ToString();
        //}


        protected void ExitStudentHistory_Click(object sender, EventArgs e)
        {
            studentPanel.Visible = false;
        }

      

    }


  
}




    //private List<Activity> GetGoalActivities(Guid goalId)
    //    {
    //        return crmAccess.GetGoalActivities(goalId).ToList();

            // The caching below was causing issues when data became out of sync.
            // The majority of the time is spent collecting the Student data.

//            var goalActivities = new List<new_goalactivity>();

//            var sql = new SqlAzureLayer(GetConnectionString());
//            var dbActivities = sql.GetDataTable(string.Format("select activityid,name,sortorder from dbo.Activity where goalid={0} order by sortorder", sql.FormatGuid(goalId)));

//            if (dbActivities != null && dbActivities.Rows.Count != 0)
//            {
//                foreach (DataRow anActivity in dbActivities.Rows)
//                {
//                    var newGoal = new new_goalactivity
//                    {
//                        new_goalactivityId = sql.GetDataRowGuid(anActivity[0]),
//                        new_name = sql.GetDataRowString(anActivity[2]),
//                        new_Header = sql.GetDataRowString(anActivity[1])
//                    };
//                    goalActivities.Add(newGoal);
//                }

               
//            }
//            else
//            {
//                goalActivities = crmAccess.GetGoalActivities(goalId).ToList();
               
//                // Create Goals in db
//                foreach (new_goalactivity newActivity in goalActivities)
//                {
//                    var createGoal = string.Format(@"INSERT INTO dbo.Activity
//           ([activityid]
//           ,[goalid]
//           ,[sortorder]
//           ,[name])
//     VALUES
//           ({0}
//           ,{1}
//           ,{2}          
//           ,{3}
//           )",
//        sql.FormatGuid(newActivity.new_goalactivityId),
//         sql.FormatGuid(goalId),
//         sql.FormatString(newActivity.new_name),
//         sql.FormatString(newActivity.new_Header)
//       );
//                    sql.SqlExecuteNonQuery(createGoal);
//                }
//            }

//            return goalActivities;

        //}

        //private List<SubActivity> GetSubActivities(Guid activityId)
        //{
        //    return crmAccess.GetSubActivities(activityId).ToList();

//            var subActivities = new List<new_subactivity>();

//            var sql = new SqlAzureLayer(GetConnectionString());
//            var dbSubActivities = sql.GetDataTable(string.Format("select subactivityid,name,sortorder from dbo.SubActivity where activityid={0} order by sortorder", sql.FormatGuid(activityId)));

//            if (dbSubActivities != null && dbSubActivities.Rows.Count != 0)
//            {
//                foreach (DataRow aSubActivity in dbSubActivities.Rows)
//                {
//                    var newGoal = new new_subactivity
//                    {
//                        new_subactivityId = sql.GetDataRowGuid(aSubActivity[0]),
//                        new_Description = sql.GetDataRowString(aSubActivity[1]),
//                        new_name = sql.GetDataRowString(aSubActivity[2])
//                    };
//                    subActivities.Add(newGoal);
//                }


//            }
//            else
//            {
//                subActivities = crmAccess.GetSubActivities(activityId).ToList();

//                // Create Goals in db
//                foreach (new_subactivity newSubActivity in subActivities)
//                {
//                    var createGoal = string.Format(@"INSERT INTO dbo.SubActivity
//           ([subactivityid]
//           ,[activityid]
//           ,[sortorder]
//           ,[name])
//     VALUES
//           ({0}
//           ,{1}
//           ,{2}          
//           ,{3}
//           )",
//        sql.FormatGuid(newSubActivity.new_subactivityId),
//         sql.FormatGuid(activityId),
//         sql.FormatString(newSubActivity.new_name),
//         sql.FormatString(newSubActivity.new_Description)
//       );
//                    sql.SqlExecuteNonQuery(createGoal);
//                }
//            }

//            return subActivities;

     //   }
