﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WideSite.Master" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="GoalManagement.Members.dashboard" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript" src="/Scripts/Tooltip.js"></script>
   
    <script type="text/javascript">

        function PrintContent(divName)
        {
            var DocumentContainer = document.getElementById(divName);
            var WindowObject = window.open("", "PrintWindow", "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }


        document.onclick = PositionMenu;

        var PositionStudentMenu = false;
        var studentMenuPointer;

        function StudentMenu(item1, item2, item3)
        {
            showObj('studentMenu');
            studentMenuPointer = document.getElementById('studentMenu');
            //var menuContent = document.getElementById('studentcontent');

            var message = "";
            message += "<b> Options for " + item2 + "</b>&nbsp; <img src='/Images/exit.gif' onclick=\"javascript:hideObj('studentMenu');\" /><br/><hr/>";
            message += "<a href='coaching.aspx?specificstudent=" + item1 + "&viewinfo=1' onclick=\"javascript:Waiting('Loading student`s activities on Coaching page....');\" >Review Selected Student's Activities</a><br/>";
            message += "<a href='coaching.aspx?specificstudent=" + item1 + "' onclick='javascript:Waiting();' >Coach this student </a><br/>";

            //   menuContent.innerText = sendEmail;
            //   menuContent.innerHTML = item1;

            PositionStudentMenu = true;

            InitStudentMenu(message, 'CoachMessage', 280, 3, 3);
        }

        function InitStudentMenu(myMessage, messageClass, messageWidth, xOffset, yOffset)
        {
            if (!studentMenuPointer)
            {
                studentMenuPointer = new Object();
            }

            studentMenuPointer.tipDiv = document.getElementById("studentMenu"); // Name of DIV used to display ToolTip	

            studentMenuPointer.tipDiv.className = messageClass;
            var menuContent = document.getElementById('studentcontent');
            menuContent.innerHTML = myMessage;
            //studentMenuPointer.tipDiv.innerHTML = myMessage;
            studentMenuPointer.enableTip = true;

            // Set the following values if the arguments exist
            if (messageWidth)
            {
                studentMenuPointer.tipDiv.style.width = messageWidth + "px"; // style width over rides class
            }

            studentMenuPointer.xOffset = (xOffset ? xOffset : -180); // default x offset
            studentMenuPointer.yOffset = (yOffset ? yOffset : 20); // default y offset

            return false;
        }


        function PositionMenu(e)
        {
            if (PositionStudentMenu)
            {
                var isMozzilla = document.getElementById && !document.all; // For FireFox Compatibility.

                // Find cursor position.
                var curX = (isMozzilla) ? e.pageX : event.clientX + document.documentElement.scrollLeft;
                var curY = (isMozzilla) ? e.pageY : event.clientY + document.documentElement.scrollTop;

                var edgeLeft = (studentMenuPointer.xOffset < 0) ? studentMenuPointer.xOffset * (-1) : -1000;
                var edgeRight = ((isMozzilla) ? window.innerWidth - e.clientX - 20 : document.documentElement.clientWidth - event.clientX) - studentMenuPointer.xOffset;
                var edgeBottom = ((isMozzilla) ? window.innerWidth - e.clientY - 20 : document.documentElement.clientHeight - event.clientY) - studentMenuPointer.yOffset;

                if (edgeRight < studentMenuPointer.tipDiv.offsetWidth) // Horisontal Bounds Checking
                {
                    //move the horizontal position of the menu to the left by it's width
                    studentMenuPointer.tipDiv.style.left = ((isMozzilla) ? window.pageXOffset + e.clientX : document.documentElement.scrollLeft + event.clientX) - studentMenuPointer.tipDiv.offsetWidth + "px";
                }
                else if (curX < edgeLeft)
                {
                    studentMenuPointer.tipDiv.style.left = "5px"; // minimum left margin
                }
                else //position div relative to mouse X
                {
                    studentMenuPointer.tipDiv.style.left = curX + studentMenuPointer.xOffset + "px";
                }


                if (edgeBottom < studentMenuPointer.tipDiv.offsetHeight) // Vertical Bounds Checking
                {
                    studentMenuPointer.tipDiv.style.top = ((isMozzilla) ? window.pageYOffset + e.clientY : document.documentElement.scrollTop + event.clientY) - studentMenuPointer.tipDiv.offsetHeight - studentMenuPointer.yOffset + "px";
                }
                else //position div relative to mouse Y
                {
                    studentMenuPointer.tipDiv.style.top = curY + studentMenuPointer.yOffset + "px";
                }

                PositionStudentMenu = false;
            }

        }
    </script>
    <%-- <telerik:HeadTag runat="server" ID="Headtag2">
    </telerik:HeadTag>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function GetSelectedItems()
            {
                alert($find("<%= RadGrid2.MasterTableView.ClientID %>").get_selectedItems().length);
            }
        </script>
    </telerik:RadCodeBlock>--%>
    <style type="text/css">
.Totals { font-weight:bold; white-space:nowrap; }
.Coach{ font-weight:bold; background:#CCCCCC; text-align:center; }

.CoachMessage{ border:solid 1px #000000; width:500px; padding:10px; min-height:40px;
                    position:absolute; z-index:100;
                    visibility:hidden; color:#333333; top:20px;
                    left:90px; background-color:#FFFFFF;                    
}



.BottomTotals {font-weight:bold; background:#000000; color:#FFFFFF; }
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
 <%--   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>

    <div id="dhtmltooltip" ></div>

    <div id="studentMenu" style="display:none; visibility:hidden;" >
       <%-- <div style=" padding-left:150px;">Close <img src='/Images/exit.gif' onclick="javascript:hideObj('studentMenu');" />
        </div>--%>
        <div id="studentcontent">
        </div>
    </div>

    <div id="HelpDiv" class="HelpScreen">
      <asp:Literal ID="HelpLiteral" runat="server"></asp:Literal>
    </div>

        <div id="studentHistory" style="padding: 5px; background: #FFFFFF">
            <table>
           
                <tr>
                    <td>
                        <img src='/Images/help.gif' onmouseover="javascript:showObj('HelpDiv');" onmouseout="javascript:hideObj('HelpDiv');" />&nbsp;&nbsp;
                    </td>
                    <td>
                        Class&nbsp;
                    </td>
                    <td>
                        <asp:DropDownList ID="ClassList" runat="server" Font-Size="9pt" OnSelectedIndexChanged="ClassList_SelectedIndexChanged"
                            AutoPostBack="true" >
                        </asp:DropDownList>                            
                       
              </td>
           <td style=" padding-left:20px;">
<asp:Button ID="ResourcesViewed" runat="server" Text="Resources Viewed Report" 
                   onclick="ResourcesViewed_Click" />
                </td>
                    <td style=" padding-left:20px;">Last Updated: <asp:Label ID="LastUpdated" runat="server" Text=""> CST</asp:Label>
                    </td>
                    
                </table>
                <table>
                <tr>
                    <td colspan="3">
                        <div style="padding: 10px;">
                            Progress:&nbsp;&nbsp;
                            <img src='/Images/red15x15.gif' />
                            None&nbsp;
                            <img src='/Images/yellow_15x15.gif' />
                            Some &nbsp;
                            <img src='/Images/green15x15.gif' />
                            Significant&nbsp;
                            <img src='/Images/blue15x15.gif' />
                            Complete
                            <img src='/Images/ImpactIcon.gif' />
                            Impact Question&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <img src='/Images/Goals.gif' />
                            Goals Set&nbsp;
                            <img src='/Images/NoGoals.gif' />
                            Goals Not Entered&nbsp;
                            <img src='/Images/coaching.gif' />
                            Coaching                           
                        </div>
                    </td>
                </tr>
            </table>
           
                   <%--       <td>
                        &nbsp;&nbsp;
                        <asp:Button ID="ViewGoals" runat="server" Text="View Student Goals" Enabled="false" />
                    </td>
                    <td>
                        &nbsp;&nbsp;
                        <asp:Button ID="ViewUpdates" runat="server" Text="View Student Updates" Enabled="False" />
                    </td>
                    <td>
                        &nbsp;&nbsp;
                        <asp:Button ID="SendEmail" runat="server" Text="Send Email" Enabled="False" />
                    </td>
                    <td>
                        &nbsp;&nbsp;
                        <asp:Button ID="SendFeedback" runat="server" Text="Send Feedback" Enabled="False" />
                    </td>--%>
          
        
            <div style=" overflow: auto; width: 101%">
          <telerik:RadGrid ID="RadGrid1" runat="server" Skin="Web20" Width="1200px" 
                     onitemdatabound="Dashboard_ItemDataBound"  OnColumnCreated="Dashboard_columnCreated" >
                <ClientSettings EnableRowHoverStyle="false" EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="False" UseStaticHeaders="False" />
                    <Selecting AllowRowSelect="False" />
                </ClientSettings>    
                   
    </telerik:RadGrid>
            </div>
        </div>
   <%--     </ContentTemplate>
</asp:UpdatePanel>--%>
   
</asp:Content>
