﻿<%@ Page Title="Register" Language="C#" MasterPageFile="/Site.master" AutoEventWireup="true"
    CodeBehind="CustomRegister.aspx.cs" Inherits="GoalManagement.Account.CustomRegister" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">

.label { text-align:right;}

</style>
    <script type="text/javascript">
       
//        function AllowLogin()
//        {
//            var checkbox = document.getElementById("AgreeToAgreement");

//            document.getElementById("ctl00_MainContent_CreateUserButton").disabled = !checkbox.checked;
//            
//        }


    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
  
   
    <div style="padding-left: 50px;">

        <asp:Panel ID="Panel1" runat="server">            
            <asp:Literal ID="ThankyouContent" runat="server"></asp:Literal>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server">
        <h2>
            Register Your Account
        </h2>
        <%-- <p>
            Use the form below to create a new account.
        </p>--%>
        <p>
            Passwords are required to be a minimum of <%= Membership.MinRequiredPasswordLength %> characters in length.
        </p>
        <span class="failureNotification">
            <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
        </span>
        <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification" 
                ValidationGroup="RegisterUserValidationGroup"/>
        <div class="accountInfo">                     
                       
            <table>
               <%-- <tr>
                    <td class="label">
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="FullName">First Name:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="FullName" runat="server" CssClass="textEntry" Width="200px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="FullName"
                            CssClass="failureNotification" ErrorMessage="Full Name is required." ToolTip="Full Name is required."
                            ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <asp:Label ID="Label3" runat="server" AssociatedControlID="LastName">Last Name:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="LastName" runat="server" CssClass="textEntry" Width="200px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="LastName"
                            CssClass="failureNotification" ErrorMessage="Last Name is required." ToolTip="Last Name is required."
                            ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                    </td>
                </tr>--%>
                <%--  <tr>
                <td class="label"><asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                </td>
                <td> <asp:TextBox ID="UserName" runat="server" CssClass="textEntry" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                            CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required." 
                            ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                </td>
                </tr>--%>
                <tr>
                <td class="label"> <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                </td>
                <td><asp:TextBox ID="Email" runat="server" CssClass="textEntry" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email" 
                            CssClass="failureNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required." 
                            ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                </td>
                </tr>
               <%-- <tr>
                    <td class="label">
                        <asp:Label ID="Label6" runat="server" AssociatedControlID="Email">Confirm E-mail:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Email2" runat="server" CssClass="textEntry" Width="200px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="Email"
                            CssClass="failureNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required."
                            ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                    </td>
                </tr>--%>
                    <tr>
                        <td class="label"><asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                        </td>
                        <td>  <asp:TextBox ID="Password" runat="server" CssClass="passwordEntry" 
                                TextMode="Password" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                            CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required." 
                            ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"> <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label>
                        </td>
                        <td><asp:TextBox ID="ConfirmPassword" runat="server" CssClass="passwordEntry" 
                                TextMode="Password" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="ConfirmPassword" CssClass="failureNotification" Display="Dynamic" 
                            ErrorMessage="Confirm Password is required." ID="ConfirmPasswordRequired" runat="server" 
                            ToolTip="Confirm Password is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" 
                            CssClass="failureNotification" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                            ValidationGroup="RegisterUserValidationGroup">*</asp:CompareValidator>
                        </td>  
                    </tr>
              <%--  <tr>
                    <td class="label">
                        <asp:Label ID="CompanyLabel" runat="server" AssociatedControlID="CompanyList">Your Company:</asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="CompanyList" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="CompanyList"
                            CssClass="failureNotification" ErrorMessage="Your Company is required." ToolTip="Your Company is required."
                            ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                    </td>
                </tr>--%>
             <%--   <tr>
                    <td class="label">
                        <asp:Label ID="Label4" runat="server" AssociatedControlID="ClassList">Your Class:</asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ClassList" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ClassList"
                            CssClass="failureNotification" ErrorMessage="Your Class is required." ToolTip="Your Company is required."
                            ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                    </td>
                </tr>--%>
                <%--  <tr>
                    <td class="label">
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="Address1">Address1:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Address1" runat="server" CssClass="textEntry" Width="350px"></asp:TextBox>                                    
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <asp:Label ID="Label3" runat="server" >Address2:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Address2" runat="server" CssClass="textEntry" Width="350px"></asp:TextBox>
                    </td>
                </tr>--%>
             <%--   <tr>
                    <td class="label">
                        <asp:Label ID="Label4" runat="server" >City:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="City" runat="server" CssClass="textEntry" Width="150px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="label">
                        <asp:Label ID="Label5" runat="server" >State:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="State" runat="server" CssClass="textEntry"  Width="30px" MaxLength="2"></asp:TextBox>
                    </td>
                </tr>--%>
                <%--  <tr>
                    <td class="label">
                        <asp:Label ID="Label6" runat="server" >ZipCode:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="ZipCode" runat="server" CssClass="textEntry" Width="100px"></asp:TextBox>
                    </td>
                </tr>--%>
              <%--  <tr>
                    <td class="label">
                        <asp:Label ID="Label7" runat="server">Phone:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Phone" runat="server" CssClass="textEntry" MaxLength="18" Width="120px"></asp:TextBox>
                    </td>
                </tr>--%>
                   
    <%--                <tr>
                        <td class="label">
                            <asp:Label ID="Label1" runat="server" AssociatedControlID="Notes">Comments:</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="Notes" runat="server" CssClass="textEntry" Height="80px" 
                                TextMode="MultiLine" Width="300px"></asp:TextBox>                                      
                        </td>
                    </tr>--%>
                </table>             
                         
                           
                <%-- <telerik:RadCaptcha ID="RadCaptcha1" ValidationGroup="RegisterUserValidationGroup"
                    ErrorMessage="The code you entered is not valid." Display="Dynamic" runat="server">
                </telerik:RadCaptcha>  --%>
        <%--    <p>
                An email will be sent to you for confirmation, before your account will be activated.               
            </p>--%>

            <br />
            <%--  <input type="checkbox" onclick="javascript:AllowLogin();" id="AgreeToAgreement"/>I agree to the
          <asp:HyperLink ID="HyperLink2" CssClass="loginLink" runat="server" NavigateUrl="UserAgreement.htm"
                Target="_Blank">User Agreement</asp:HyperLink>--%>


                <asp:Button ID="CreateUserButton" runat="server" CommandName="MoveNext" Text="Register User" 
                        ValidationGroup="RegisterUserValidationGroup"  
                    onclick="CreateUserButton_Click"/>                              
                          
                        
        </div>
        </asp:Panel>
    </div>
             
</asp:Content>
