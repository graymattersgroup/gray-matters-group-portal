﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="GoalManagement.Account.ResetPassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="accountInfo">
        <fieldset class="login">
            <legend>Reset Password</legend>
            Enter your email address:  
            <asp:TextBox ID="EmailAddress" runat="server" Height="21px" Width="336px"></asp:TextBox><br />
            <br />
            <asp:Button ID="ResetPasswordButton" runat="server" Text="Reset Password" 
                onclick="ResetPasswordButton_Click" />
            &nbsp;&nbsp;
            <asp:Label ID="Warning" runat="server" ForeColor="#CC0000" Text=""></asp:Label>
            </fieldset>
    </div>
    
</asp:Content>
