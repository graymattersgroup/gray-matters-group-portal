﻿using System;
using System.Configuration;
using System.Web.Security;
using GoalManagement.CrmConnectivity;

namespace GoalManagement
{
    public partial class Tracker : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Guid assetId;
            Guid contactId;
            Guid scheduledTouchId;

            if (Request.QueryString["aid"] != null)
            {
                 assetId = new Guid(Request.QueryString["aid"]);
                 contactId = new Guid(Request.QueryString["cid"]);
                 scheduledTouchId = new Guid(Request.QueryString["sid"]);

                 var crmAccess = new CRMAccess();

                 var asset = crmAccess.GetAsset(assetId);

                 crmAccess.SaveAssetTracking(assetId, contactId, scheduledTouchId, asset.trk_name);

                 Response.Redirect(asset.trk_LinkToAsset);
            }
            else
            {
                assetId             = new Guid("B0DD51BE-855D-E211-8B2F-78E3B5102EA9");
                contactId           = new Guid("EA0D102F-4259-E011-8E49-1CC1DEF1E4A9");
                scheduledTouchId    = new Guid("286609C7-AA86-488E-A13F-0382F2F8C190");
            }

            

        }


      
    }

}