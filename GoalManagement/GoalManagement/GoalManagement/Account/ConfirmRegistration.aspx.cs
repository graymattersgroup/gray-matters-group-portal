﻿using System;
using System.Configuration;
using System.Web.Security;
using GoalManagement.Support;

namespace GoalManagement.Account
{
    public partial class ConfirmRegistration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["confirmid"] != null)
            {
                var confirmId = Request.QueryString["confirmid"].ToString();

                var db = new SqlAzureLayer(GetConnectionString());

                var result =
                    db.GetDataTable(
                        string.Format("SELECT UserName,LastActivityDate FROM dbo.aspnet_Users where Userid = {0}",
                                      db.FormatGuid(new Guid(confirmId))));
                var username = result.Rows[0][0].ToString();

               
                    var user = Membership.GetUser(username);
                    user.IsApproved = true;
                    Membership.UpdateUser(user);
                    Response.Redirect("Login.aspx");
                
            }


        }


        private string GetConnectionString()
        {
                var connectionString2 = ConfigurationManager.ConnectionStrings["Production"].ConnectionString;
                return connectionString2;
        }
    }

}