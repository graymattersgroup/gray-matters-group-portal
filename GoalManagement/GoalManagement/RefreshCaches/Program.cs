﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using GoalManagement.CrmConnectivity;
using GoalManagement.Support;
using System.Configuration;

namespace RefreshCaches
{
    class Program
    {
        static void Main(string[] args)
        {
            var log = new StringBuilder(128);

            try
            {
                Console.WriteLine("Establishing Connection to CRM");

                var crmAccess = new CRMAccess();

                Console.WriteLine("Getting Class List");

                var classes = crmAccess.GetClasses();

                foreach (new_class newClass in classes)
                {
                    try
                    {
                        var classId = newClass.new_classId.Value;

                        if (newClass.new_ClassComplete != null)
                        {
                            if (newClass.new_ClassComplete.Value)
                            {
                                Console.WriteLine("Skip Class Complete - {0}", newClass.new_name);
                                continue;
                            }
                        }

                        log.AppendFormat("Processing Class - {0}<br/>", newClass.new_name);
                        Console.WriteLine("Processing Class - {0}", newClass.new_name);

                        //var sql = new SqlAzureLayer(@"Data Source=DEVBOX\LocalSQL2008;Initial Catalog=graymatters;Integrated Security=SSPI;Connect Timeout=600");
                        var sql = new SqlAzureLayer(@"Data Source=S104-238-81-113\RXINACTION;Initial Catalog=GrayMatters;Integrated Security=SSPI;Connect Timeout=600");

                        var blob = sql.GetDashboardBlob(classId);


                        // Get all Students
                        log.AppendLine("Get Students<br/>");
                        var studentsI = crmAccess.GetClassStudents(classId);

                        log.AppendLine("Get SelectedGoals<br/>");
                        var selectedGoalsI = crmAccess.GetClassSelectedGoals(classId);

                        // Get all Coaches
                        log.AppendLine("Get Coaches<br/>");
                        var coachesI = crmAccess.GetClassCoaches(classId);

                        // Get all Progress Periods
                        log.AppendLine("Get Update Periods<br/>");
                        var progressPeriodsI = crmAccess.GetClassUpdatePeriods(classId);

                        // Get all Student Progress Periods
                        log.AppendLine("Get Student Progress Periods<br/>");
                        var studentProgressI = crmAccess.GetMyDashboardUpdatePeriods(classId);

                        // Get all Coaching
                        log.AppendLine("Get Coaching<br/>");
                        var coachingI = crmAccess.GetCoachFeedbackbyClass(classId);

                        // Get Class Statuses
                        log.AppendLine("Get Class Statuses<br/>");
                        var classStatusesI = crmAccess.GetClassStatuses(classId);

                        var isCreate = (blob == null);

                        blob = new DashboardBlob();

                        blob.SetStudents(classId, DateTime.Now, studentsI, coachesI, selectedGoalsI, progressPeriodsI,
                                         studentProgressI, coachingI, classStatusesI);

                        log.AppendLine("Updating Dashboard Blob<br/>");
                        Console.WriteLine("Updating Dashboard Blob");
                        if (isCreate)
                        {
                            sql.SaveDashboardBlob(blob);
                        }
                        else
                        {
                            sql.UpdateDashboardBlob(blob);
                        }

                        log.AppendLine("Get Class Goals<br/>");
                        var goals = crmAccess.GetClassGoals(classId).ToList();

                        var classBlob = new ClassGoals();

                        foreach (ClassGoal classGoal in goals)
                        {
                            if (classGoal.isdefault ?? false)
                                classGoal.selected = true;

                            var activities = crmAccess.GetGoalActivities(classGoal.id).ToList();

                            foreach (Activity newGoalactivity in activities)
                            {
                                var subactivities = crmAccess.GetSubActivities(newGoalactivity.id).ToList();

                                foreach (SubActivity subActivity in subactivities)
                                {
                                    newGoalactivity.subActivities.Add(subActivity);
                                }
                                classGoal.activities.Add(newGoalactivity);
                            }

                            classBlob.classGoals.Add(classGoal);
                        }

                        classBlob.classid = classId;
                        classBlob.lastupdated = DateTime.Now;

                        log.AppendLine("Updating Goals Blob<br/>");
                        Console.WriteLine("Updating Goals Blob");
                        sql.UpdateGoalBlob(classBlob);

                        Console.WriteLine("");

                    }
                    catch (SoapException ex)
                    {
                        EmailNotifications.SendExceptionMessage("GMG Scheduled Cache Update  Failed", "Soap", ex,log.ToString());

                    }
                    catch (Exception ex)
                    {
                        EmailNotifications.SendExceptionMessage("GMG Scheduled Cache Update Failed", "Exception", ex,log.ToString());
                    }
                }

            }
            catch (SoapException ex)
            {
                EmailNotifications.SendExceptionMessage("GMG Scheduled Cache Update Fatal", "Soap", ex,
                                                        log.ToString());

            }
            catch (Exception ex)
            {
                EmailNotifications.SendExceptionMessage("GMG Scheduled Cache Update Fatal", "Exception", ex,
                                                        log.ToString());
            }
        }
    }
}
